﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AdminPage : System.Web.UI.MasterPage
{
    protected void Page_Init(object sender, EventArgs e)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        if (AllFunctions.ConnectStr == "")
        {
            AllFunctions.ConnectStr = db.Connection.ConnectionString;
        }
        //if (!((tbl_Client)Session["client"]).sql_role)
        /*if (!((T_User)Session["user"]).IsAdmin())
            Response.Redirect("~/Login.aspx");*/
    }
    protected void btnLogout_Click(object sender, EventArgs e)
    {
        //clear cookies
        Response.Cookies["user"].Expires = DateTime.MinValue;
        Response.Cookies["pass"].Expires = DateTime.MinValue;

        Response.Redirect("~/Login.aspx");
    }
}
