﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminPages/AdminPage.master" AutoEventWireup="true" CodeFile="Clients.aspx.cs" Inherits="AdminPages_Clients" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%-- <asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>--%>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
        <table class="LayoutTable">
        <tr>
            <th class="HeaderStyle">לקוחות</th>
        </tr>
        <tr>
            <td align="left" >
                <a href="../Help/HClients.aspx" target="_blank" style="color:#5d7b9d;"><img src="../images/help.gif" style="border:none" />עזרה</a>
            </td>
        </tr>
        <tr>
            <td>
                <table class="LayoutTable">
                    <tr>
                        <td valign="top">
                            <asp:LinqDataSource ID="LinqMaster" runat="server" 
                                ContextTypeName="DataClassesDataContext" 
                                Select="new (ID, Name)" 
                                TableName="T_Clients">
                            </asp:LinqDataSource>
                            <asp:GridView ID="GridMaster" runat="server" Width="100%"
                                AutoGenerateColumns="False"
                                DataSourceID="LinqMaster"
                                DataKeyNames="ID" 
                                >
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True" SelectText="בחר..." />
                                    <asp:BoundField DataField="ID" HeaderText="ID" 
                                        SortExpression="ID" >
                                        <ItemStyle Width="30px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Name" HeaderText="שם פרטי" 
                                        SortExpression="Name" />
                                </Columns>
                                <HeaderStyle CssClass="DetailsFormViewHeader"></HeaderStyle>
                                <RowStyle ForeColor="#333333"></RowStyle>
                                <AlternatingRowStyle BackColor="#f7f6f3"></AlternatingRowStyle>
                                <SelectedRowStyle BackColor="#E2DED6" ForeColor="#284775"></SelectedRowStyle>
                            </asp:GridView>
                            <!--<asp:LinkButton ID="btnNew" runat="server" Text="חדש..." />-->
                        </td>
                        <td valign="top" width="50%">
                            <asp:LinqDataSource ID="LinqDetails" runat="server" 
                                ContextTypeName="DataClassesDataContext" TableName="T_Clients" 
                                Where="ID == @ID" 
                                EnableDelete="True" 
                                EnableInsert="True" 
                                EnableUpdate="True" 
                                OnInserted="LinqDetails_Inserted">
                                <WhereParameters>
                                    <asp:ControlParameter ControlID="GridMaster" DefaultValue="0" 
                                        Name="ID" PropertyName="SelectedValue" Type="Int32" />
                                </WhereParameters>
                            </asp:LinqDataSource>
                            <asp:LinqDataSource ID="LinqUsers" runat="server"
                                ContextTypeName="DataClassesDataContext"
                                TableName="T_Users"
                                Select="new ( ID, FirstName + ' ' + LastName as name )"
                                >
                            </asp:LinqDataSource>
                            <asp:FormView ID="FormViewDetails" runat="server" Width="100%" 
                                DataKeyNames="ID" 
                                DataSourceID="LinqDetails" 
                                HeaderText="פרטים" 
                                HeaderStyle-CssClass="DetailsFormViewHeader"
                                CssClass="DetailsFormView" 
                                OnItemUpdated="FormViewDetails_ItemUpdated"
                                OnItemDeleted="FormViewDetails_ItemDeleted" 
                                ondatabound="FormViewDetails_DataBound" 
                                onitemupdating="FormViewDetails_ItemUpdating" 
                                OnItemInserting="FormViewDetails_ItemUpdating" 
                                onmodechanged="FormViewDetails_ModeChanged">
                                <EditItemTemplate>
                                    <table class="DetailsFormViewLayout">
                                        <tr>
                                            <td>
                                                מס' לקוח
                                            </td>
                                            <td>
                                                <asp:Label ID="sql_clientIDLabel1" runat="server" 
                                                    Text='<%# Eval("ID") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                שם
                                            </td>
                                            <td>
                                                <asp:TextBox ID="sql_first_nameTextBox" runat="server" 
                                                    Text='<%# Bind("Name") %>' />
                                                <asp:RequiredFieldValidator runat="server" ControlToValidate="sql_first_nameTextBox" 
                                                    ErrorMessage="הכנס שם פרטי" Display="Dynamic" Text="*" 
                                                    ValidationGroup="AllValidators" ID="RequiredFieldValidator1"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                ישוב
                                            </td>
                                            <td>
                                                <asp:TextBox ID="TextBox3" runat="server" 
                                                    Text='<%# Bind("City") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                כתובת
                                            </td>
                                            <td>
                                                <asp:TextBox ID="TextBox4" runat="server" 
                                                    Text='<%# Bind("Address") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                מיקוד
                                            </td>
                                            <td>
                                                <asp:TextBox ID="sql_zipTextBox" runat="server" 
                                                    Text='<%# Bind("Zip") %>' />
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" 
                                                    FilterType="Numbers"
                                                    runat="server" Enabled="True" TargetControlID="sql_zipTextBox">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                מדינה
                                            </td>
                                            <td>
                                                <asp:TextBox ID="TextBox6" runat="server" 
                                                    Text='<%# Bind("Country") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                טלפון
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtPhone" runat="server" 
                                                    Text='<%# Bind("Phone") %>' />
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" 
                                                    FilterType="Numbers,Custom" ValidChars="+-() "
                                                    runat="server" Enabled="True" TargetControlID="txtPhone">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                פקס
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFax" runat="server" 
                                                    Text='<%# Bind("Fax") %>' />
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" 
                                                    FilterType="Numbers"
                                                    runat="server" Enabled="True" TargetControlID="txtFax">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                משתמש ראשי
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cmbUsers" runat="server"
                                                    DataSourceID="LinqUsers" 
                                                    DataTextField="name" 
                                                    DataValueField="ID"
                                                    />
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" 
                                        CommandName="Update" Text="שמור" ValidationGroup="AllValidators" />
                                    &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" 
                                        CausesValidation="False" CommandName="Cancel" Text="בטל" />
                                </EditItemTemplate>
                                <InsertItemTemplate>
                                    <table class="DetailsFormViewLayout">
                                        <tr>
                                            <td>
                                                שם
                                            </td>
                                            <td>
                                                <asp:TextBox ID="sql_first_nameTextBox" runat="server" 
                                                    Text='<%# Bind("Name") %>' />
                                                <asp:RequiredFieldValidator runat="server" ControlToValidate="sql_first_nameTextBox" 
                                                    ErrorMessage="הכנס שם פרטי" Display="Dynamic" Text="*" 
                                                    ValidationGroup="AllValidators" ID="RequiredFieldValidator1"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                ישוב
                                            </td>
                                            <td>
                                                <asp:TextBox ID="sql_cityTextBox" runat="server" 
                                                    Text='<%# Bind("City") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                כתובת
                                            </td>
                                            <td>
                                                <asp:TextBox ID="sql_addressTextBox" runat="server" 
                                                    Text='<%# Bind("Address") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                מיקוד
                                            </td>
                                            <td>
                                                <asp:TextBox ID="sql_zipTextBox" runat="server" 
                                                    Text='<%# Bind("Zip") %>' />
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" 
                                                    FilterType="Numbers"
                                                    runat="server" Enabled="True" TargetControlID="sql_zipTextBox">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                מדינה
                                            </td>
                                            <td>
                                                <asp:TextBox ID="sql_countryTextBox" runat="server" 
                                                    Text='<%# Bind("Country") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                טלפון
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtPhone" runat="server" 
                                                    Text='<%# Bind("Phone") %>' />
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" 
                                                    FilterType="Numbers,Custom" ValidChars="+-() "
                                                    runat="server" Enabled="True" TargetControlID="txtPhone">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                פקס
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFax" runat="server" 
                                                    Text='<%# Bind("Fax") %>' />
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" 
                                                    FilterType="Numbers"
                                                    runat="server" Enabled="True" TargetControlID="txtFax">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                משתמש ראשי
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cmbUsers" runat="server"
                                                    DataSourceID="LinqUsers" 
                                                    DataTextField="name" 
                                                    DataValueField="ID"
                                                    />
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" 
                                        CommandName="Insert" Text="שמור" ValidationGroup="AllValidators" />
                                    &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" 
                                        CausesValidation="False" CommandName="Cancel" Text="בטל" />
                                </InsertItemTemplate>
                                <ItemTemplate>
                                    <table class="DetailsFormViewLayout" >
                                        <tr>
                                            <td>
                                                מס' לקוח
                                            </td>
                                            <td>
                                                <asp:Label ID="sql_clientIDLabel" runat="server" 
                                                    Text='<%# Eval("ID") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                שם
                                            </td>
                                            <td>
                                                <asp:Label ID="sql_first_nameLabel" runat="server" 
                                                    Text='<%# Bind("Name") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                ישוב
                                            </td>
                                            <td>
                                                <asp:Label ID="sql_cityLabel" runat="server" 
                                                    Text='<%# Bind("City") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                כתובת
                                            </td>
                                            <td>
                                                <asp:Label ID="sql_addressLabel" runat="server" 
                                                    Text='<%# Bind("Address") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                מיקוד
                                            </td>
                                            <td>
                                                <asp:Label ID="sql_zipLabel" runat="server" 
                                                    Text='<%# Bind("Zip") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                מדינה
                                            </td>
                                            <td>
                                                <asp:Label ID="sql_countryLabel" runat="server" 
                                                    Text='<%# Bind("Country") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                טלפון
                                            </td>
                                            <td>
                                                <asp:Label ID="sql_phoneLabel" runat="server" 
                                                    Text='<%# Bind("Phone") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                פקס
                                            </td>
                                            <td>
                                                <asp:Label ID="sql_faxLabel" runat="server" 
                                                    Text='<%# Bind("Fax") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                משתמש ראשי
                                            </td>
                                            <td>
                                                <asp:Label ID="sql_clientFirstLabel" runat="server" 
                                                    Text='<%# Eval("T_User.FirstName") %>' />
                                                <asp:Label ID="sql_clientLastLabel" runat="server" 
                                                    Text='<%# Eval("T_User.LastName") %>' />
                                            </td>
                                        </tr>
                                    </table>
                                    <asp:CustomValidator ID="DelValidator" runat="server" Display="Dynamic"
                                        EnableClientScript="false" ValidationGroup="AllValidators" Text="*" >
                                    </asp:CustomValidator>
                                    <br />
                                    <asp:LinkButton ID="btnNew" runat="server" Text="חדש" CommandName="New" />&nbsp;
                                    <asp:LinkButton ID="btnEdit" runat="server" Text="עריכה" CommandName="Edit" />&nbsp;
                                    <asp:LinkButton ID="btnDelete" runat="server" Text="מחק" CommandName="Delete" OnClientClick="return confirm('are you sure?');" />
                                </ItemTemplate>
                            </asp:FormView>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="AllValidators" />
                        
                            <br />
                            <asp:Panel ID="pnlUsers" runat="server">
                                <asp:Label ID="lblUsers" runat="server" CssClass="DetailsFormViewHeader"
                                    Width="100%" Text="משתמשים מורשים"></asp:Label>
                                <asp:LinqDataSource ID="LinqClientUsers" runat="server"
                                    ContextTypeName="DataClassesDataContext"
                                    TableName="T_ClientUsers"
                                    Where="ClientID = @ClientID"
                                    Select="new ( ID, UserID, T_User.FirstName + ' ' + T_User.LastName as name )" 
                                    >
                                    <WhereParameters>
                                        <asp:ControlParameter ControlID="GridMaster" DefaultValue="0" 
                                            Name="ClientID" PropertyName="SelectedValue" Type="Int32" />
                                    </WhereParameters>
                                </asp:LinqDataSource>
                                <asp:GridView ID="GridUsers" runat="server" Width="100%"
                                    AutoGenerateColumns="False"
                                    DataSourceID="LinqClientUsers"
                                    onrowdeleting="GridUsers_RowDeleting" 
                                    DataKeyNames="ID">
                                    <Columns>
                                        <asp:TemplateField><ItemTemplate>
                                            <asp:LinkButton ID="LinkButton1" runat="server" 
                                                OnClientClick="return confirm('Are you sure you want to delete this record?');"
                                                CommandName="Delete">
                                                הסר
                                            </asp:LinkButton>
                                        </ItemTemplate></asp:TemplateField>
                                        <asp:BoundField DataField="UserID" HeaderText="ID" 
                                            SortExpression="UserID" >
                                            <ItemStyle Width="30px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="name" HeaderText="שם המשתמש" 
                                            SortExpression="name" />
                                    </Columns>
                                    <%--<HeaderStyle CssClass="DetailsFormViewHeader"></HeaderStyle>--%>
                                    <RowStyle ForeColor="#333333"></RowStyle>
                                    <AlternatingRowStyle BackColor="#f7f6f3"></AlternatingRowStyle>
                                </asp:GridView>
                                <%--<asp:LinqDataSource ID="LinqUsers" runat="server"
                                    ContextTypeName="DataClassesDataContext"
                                    TableName="T_Users"
                                    Select="new ( ID, FirstName + ' ' + LastName as Name )"
                                    >
                                </asp:LinqDataSource>--%>
                                <asp:DropDownList ID="cmbUsers" runat="server"
                                    DataSourceID="LinqUsers" 
                                    DataTextField="name" 
                                    DataValueField="ID"
                                    />
                                <asp:Button ID="btnAdd" runat="server" Text="הסף לרשימה" onclick="btnAdd_Click" />
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

