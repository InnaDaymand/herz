﻿using System;
using System.Web.UI.WebControls;

public partial class AdminPages_Clients : SecuredPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (_db == null)
            _db = new DataClassesDataContext();

        if (GridMaster.SelectedIndex < 0)
            GridMaster.SelectedIndex = 0;
    }
    protected void FormViewDetails_ItemUpdating(object sender, EventArgs e)
    {
        DropDownList list = (DropDownList)this.FormViewDetails.FindControl("cmbUsers");
        if (e is FormViewUpdateEventArgs)
        {
            (e as FormViewUpdateEventArgs).NewValues.Add("PowerUserID", list.SelectedValue);
        }
        else if (e is FormViewInsertEventArgs)
        {
            (e as FormViewInsertEventArgs).Values.Add("PowerUserID", list.SelectedValue);
        }
    }
    protected void FormViewDetails_ItemUpdated(object sender, EventArgs e)
    {
        GridMaster.DataBind();
    }
    protected void FormViewDetails_ItemDeleted(object sender, FormViewDeletedEventArgs e)
    {
        if (e.Exception != null)
        {
            CustomValidator v = (CustomValidator)this.FormViewDetails.FindControl("DelValidator");
            v.ErrorMessage = e.Exception.Message;
            v.IsValid = false;
            //Response.Write(e.Exception.Message);

            e.ExceptionHandled = true;
        }
        GridMaster.DataBind();
    }
    protected void FormViewDetails_DataBound(object sender, EventArgs e)
    {
        T_Client dataItem = (T_Client)FormViewDetails.DataItem;
        try
        {
            DropDownList list = (DropDownList)this.FormViewDetails.FindControl("cmbUsers");
            list.SelectedValue = dataItem.PowerUserID.ToString();
        }
        catch { }
    }
    protected void LinqDetails_Inserted(object sender, LinqDataSourceStatusEventArgs e)
    {
        GridMaster.DataBind();
        FormViewDetails.DataBind();
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        try
        {
            T_ClientUser cu = new T_ClientUser();
            cu.UserID = int.Parse(cmbUsers.SelectedValue);
            cu.ClientID = (int)GridMaster.SelectedValue;

            _db.T_ClientUsers.InsertOnSubmit(cu);
            _db.SubmitChanges();
        }
        catch { }

        GridUsers.DataBind();
    }
    protected void GridUsers_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        _db.ExecuteCommand("DELETE FROM T_ClientUser WHERE ID = {0}", e.Keys[0]);
        GridUsers.DataBind();
        e.Cancel = true;//cancel the event arrival to LinqDetails data source*/
    }
    protected void FormViewDetails_ModeChanged(object sender, EventArgs e)
    {
        if (FormViewDetails.CurrentMode == FormViewMode.ReadOnly)
            pnlUsers.Visible = true;
        else
            pnlUsers.Visible = false;
    }
}
