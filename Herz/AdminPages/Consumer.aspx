﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminPages/AdminPage.master" AutoEventWireup="true" CodeFile="Consumer.aspx.cs" Inherits="AdminPages_Consumer" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register assembly="eWorld.UI, Version=2.0.6.2393, Culture=neutral, PublicKeyToken=24d65337282035f2" namespace="eWorld.UI" tagprefix="ew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .auto-style1 {
            width: 70%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <table width="100%">
        <tr>
            <th colspan="2" class="HeaderStyle">צרכנים</th>
        </tr>
        <tr>
            <td colspan="2">
                <asp:TextBox runat="server" ID="ForFilter" />
                <asp:DropDownList runat="server" ID="GridFields">
                    <asp:ListItem Text="ID" Value="1" />
                    <asp:ListItem Text="שם" Value="2" />
                    <asp:ListItem Text="לקוח" Value="3" />
<%--                    <asp:ListItem Text="" Value="" />
                    <asp:ListItem Text="" Value="" />--%>
                </asp:DropDownList>
                <asp:Button Text="מסנן" runat="server" ID="DoFilter" OnClick="DoFilter_Click" />
                <asp:Button Text="בטל מסנן" runat="server" ID="NoFilter" OnClick="NoFilter_Click" />
            </td>
        </tr>
        <tr>
            <td style="border-style: inset; border-color: inherit; border-width: medium; align-content:flex-end; " dir="rtl" class="auto-style1">
                <asp:GridView runat="server" DataSourceID="tCons" ID="GridMaster"
                    AutoGenerateColumns="False"
                    DataKeyNames="sql_Consumer_ID" 
                    OnSelectedIndexChanged="GridMaster_SelectedIndexChanged"
                    AllowSorting="True" >
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" SelectText="בחר..." />
                        <asp:BoundField DataField="sql_Consumer_ID" HeaderText="ID" 
                            SortExpression="sql_Consumer_ID">
                            <ItemStyle Width="30px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="sql_Consumer_name" HeaderText="שם" 
                            SortExpression="sql_Consumer_name" >
                            <ItemStyle Width="150px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="client" HeaderText="לקוח" >
                            <ItemStyle Width="150px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="OrderNo" HeaderText="מס' רץ" 
                            SortExpression="sql_clientID, OrderNo" />
                        <asp:BoundField DataField="cType" HeaderText="סוג צרכן" 
                            SortExpression="ConsumerType" />
                        <asp:BoundField DataField="BeginDate" HeaderText="תאריך ההתחלה:" 
                            SortExpression="BeginDate" >
                            <ItemStyle Width="120px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="EngName" HeaderText="Eng Name" 
                            SortExpression="EngName" />
                        <asp:BoundField DataField="RusName" HeaderText="Название" 
                            SortExpression="RusName" />
                    </Columns>
                    <HeaderStyle CssClass="DetailsFormViewHeader"></HeaderStyle>
                    <RowStyle ForeColor="#333333"></RowStyle>
                    <AlternatingRowStyle BackColor="#f7f6f3"></AlternatingRowStyle>
                    <SelectedRowStyle BackColor="#E2DED6" ForeColor="#284775"></SelectedRowStyle>
                </asp:GridView>
            </td>
            <td style="width:30%; align-content:flex-end; border:inset; vertical-align:top;" dir="rtl">
                <asp:Label Text="" runat="server" CssClass="HeaderStyleOld" Width="100%" ID="mvTitle" />
                    <asp:Label Text="פרטים" runat="server" CssClass="HeaderStyle" Width="100%" />
                        <table width="100%">
                            <tr>
                                <th style="width:30%; text-align:right; border-bottom-style:dotted">
                                    שם
                                </th>
                                <td style="align-content:flex-end; width:70%;">
                                    <asp:TextBox runat="server" ID="mv0Name" Width="100%" />
                                </td>
                            </tr>
                            <tr>
                                <th style="width:30%; text-align:right; border-bottom-style:dotted">
                                    Name
                                </th>
                                <td style="align-content:flex-end; width:70%;">
                                    <asp:TextBox runat="server" ID="mv0NameE" Width="100%" />
                                </td>
                            </tr>
                            <tr>
                                <th style="width:30%; text-align:right; border-bottom-style:dotted">
                                    Название
                                </th>
                                <td style="align-content:flex-end; width:70%;">
                                    <asp:TextBox runat="server" ID="mv0NameR" Width="100%" />
                                </td>
                            </tr>
                            <tr>
                                <th style="width:30%; text-align:right; border-bottom-style:dotted"">
                                    פעיל
                                </th>
                                <td>
                                    <asp:CheckBox Text="" runat="server" ID="IsActive" />
                                </td>
                            </tr>
                            <tr>
                                <th style="width:30%; text-align:right; border-bottom-style:dotted">
                                    לקוח
                                </th>
                                <td style="align-content:flex-end; width:70%;">
                                    <asp:DropDownList ID="sql_clientIDComboBox" runat="server" Width="100%"
                                        DataSourceID="SqlClients" 
                                        DataTextField="Name" 
                                        DataValueField="ID"
                                        />
                                </td>
                            </tr>
                            <tr>
                                <th style="width:30%; text-align:right; border-bottom-style:dotted"">
                                    מס' רץ
                                </th>
                                <td style="align-content:flex-end; width:70%;">
                                    <asp:TextBox runat="server" ID="OrderNo" Width="100%" />
                                </td>
                            </tr>
                            <tr>
                                <th style="width:30%; text-align:right; border-bottom-style:dotted">
                                    סוג צרכן
                                </th>
                                <td style="align-content:flex-end; width:70%;">
                                    <asp:DropDownList ID="ConsumerType" runat="server" Width="100%"
                                        DataSourceID="SqlConsumerType" 
                                        DataTextField="ConTpName" 
                                        DataValueField="ConTpID"
                                        />
                                </td>
                            </tr>
                            <tr>
                                <th style="width:30%; text-align:right; border-bottom-style:dotted"">
                                    מס' טלפון
                                </th>
                                <td style="align-content:flex-end; width:70%;">
                                    <asp:TextBox runat="server" ID="TelNo" Width="100%" />
                                </td>
                            </tr>
                            <tr>
                                <th style="width:30%; text-align:right; border-bottom-style:dotted">
                                    מודד טמפרטורה
                                </th>
                                <td>
                                    <asp:CheckBox Text="" runat="server" ID="Temperature" />
                                </td>
                            </tr>
                            <tr>
                                <th style="width:30%; text-align:right; border-bottom-style:dotted">
                                    <asp:Literal Text="צפייה וקיבוץ" runat="server" ID="Literal1" /> 
                                    
                                </th>
                                <td style="align-content:flex-end; width:70%;">
                                    <asp:DropDownList ID="SqlShowAndGroup" runat="server" Width="100%" >
                                        <asp:ListItem Text="צפייה וקיבוץ" Value="0" />
                                        <asp:ListItem Text="צפייה והעפלה" Value="1" />
                                        <asp:ListItem Text="רק צפייה" Value="2" />
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <th style="width:30%; text-align:right; border-bottom-style:dotted">
                                    <asp:Literal Text="צריכת חשמל" runat="server" ID="EnergyAir" Visible="false" /> 
                                    
                                </th>
                                <td style="align-content:flex-end; width:70%;">
                                    <asp:DropDownList ID="SqlEnergyForAir" runat="server" Width="100%" Visible="false"/>
                                </td>
                            </tr>
                             <tr>
                                <th style="width:30%; text-align:right; border-bottom-style:dotted">
                                    מקום
                                </th>
                                <td style="align-content:flex-end; width:70%;">
                                    <asp:TextBox runat="server" ID="consumermodel" Width="100%" />
                                </td>
                            </tr>
                       </table>

                        <br />
                        <asp:Button Text="שמור" runat="server" ID="SaveIt" Width="49%" OnClick="SaveIt_Click" />
                        <asp:Button Text="חדש" runat="server" ID="CleanIt" Width="49%" OnClick="CleanIt_Click" />
                        <br />
                        <br />
                        <asp:Button Text="מחק" runat="server" ID="EraseIt" OnClick="EraseIt_Click" Width="100%" />
                        <br />
<%--                        <asp:Button Text="פתך טבלאת פרמטרים" runat="server" ID="GoParams" Width="100%" OnClick="GoParams_Click" Font-Size="X-Large" BackColor="#9498a1"/>
                    </asp:View>
                    <asp:View runat="server">--%>
                    <br />
                    <br />
                        <asp:Label Text="פרמטרים" runat="server" CssClass="HeaderStyle" Width="100%" />
                        <table width="100%">
                            <tr>
                                <th style="width:30%; text-align:right; border-bottom-style:dotted">
                                    סוג מונה
                                </th>
                                <td style="align-content:flex-end; width:70%;">
                                    <asp:DropDownList ID="ChooseTypes" runat="server" Width="100%"
                                        DataSourceID="SqlCounterTypes" 
                                        DataTextField="TypeName" 
                                        DataValueField="TypeName"
                                        />
                                </td>
                            </tr>
                            <tr>
                                <th style="width:30%; text-align:right; border-bottom-style:dotted">
                                    מתח
                                </th>
                                <td style="align-content:flex-end; width:70%;">
                                    <asp:TextBox runat="server" ID="pV" Width="100%" />
                                </td>
                            </tr>
                            <tr>
                                <th style="width:30%; text-align:right; border-bottom-style:dotted">
                                    זרם I
                                </th>
                                <td style="align-content:flex-end; width:70%;">
                                    <asp:TextBox runat="server" ID="pI" Width="100%" />
                                </td>
                            </tr>
                            <tr>
                                <th style="width:30%; text-align:right; border-bottom-style:dotted">
                                    זרם II
                                </th>
                                <td style="align-content:flex-end; width:70%;">
                                    <asp:TextBox runat="server" ID="pI2" Width="100%" />
                                </td>
                            </tr>
                            <tr>
                                <th style="width:30%; text-align:right; border-bottom-style:dotted">
                                    זרם III
                                </th>
                                <td style="align-content:flex-end; width:70%;">
                                    <asp:TextBox runat="server" ID="pI3" Width="100%" />
                                </td>
                            </tr>
                            <tr>
                                <th style="width:30%; text-align:right; border-bottom-style:dotted">
                                    הספק
                                </th>
                                <td style="align-content:flex-end; width:70%;">
                                    <asp:TextBox runat="server" ID="pW" Width="100%" />
                                </td>
                            </tr>
                            <tr>
                                <th style="width:30%; text-align:right; border-bottom-style:dotted">
                                    מקדם הספק
                                </th>
                                <td style="align-content:flex-end; width:70%;">
                                    <asp:TextBox runat="server" ID="pPhi" Width="100%" />
                                </td>
                            </tr>
                            <tr>
                                <th style="width:30%; text-align:right; border-bottom-style:dotted">
                                    שיא הביקוש
                                </th>
                                <td style="align-content:flex-end; width:70%;">
                                    <asp:TextBox runat="server" ID="pCB" Width="100%" />
                                </td>
                            </tr>
                            <tr>
                                <th style="width:30%; text-align:right; border-bottom-style:dotted">
                                    אנרגיה ריאקטיבית
                                </th>
                                <td style="align-content:flex-end; width:70%;">
                                    <asp:TextBox runat="server" ID="pEnR" Width="100%" />
                                </td>
                            </tr>
                            <tr>
                                <th style="width:30%; text-align:right; border-bottom-style:dotted">
                                    הספק ראקטיבי
                                </th>
                                <td style="align-content:flex-end; width:70%;">
                                    <asp:TextBox runat="server" ID="pWR" Width="100%" />
                                </td>
                            </tr>
                            <tr>
                                <th style="width:30%; text-align:right; border-bottom-style:dotted">
                                    הספק שמירה
                                </th>
                                <td style="align-content:flex-end; width:70%;">
                                    <asp:TextBox runat="server" ID="pES1" Width="100%" />
                                </td>
                            </tr>
                            <tr>
                                <th style="width:30%; text-align:right; border-bottom-style:dotted">
                                    סטייה - %%
                                </th>
                                <td style="align-content:flex-end; width:70%;">
                                    <asp:TextBox runat="server" ID="pES2" Width="100%" />
                                </td>
                            </tr>
                            <tr>
                                <th style="width:30%; text-align:right; border-bottom-style:dotted">
                                    זמן שמירה מינימלית - דקות
                                </th>
                                <td style="align-content:flex-end; width:70%;">
                                    <asp:TextBox runat="server" ID="pES3" Width="100%" />
                                </td>
                            </tr>
                            <tr>
                                <th style="width:30%; text-align:right; border-bottom-style:dotted">
                                    פעימה
                                </th>
                                <td style="align-content:flex-end; width:70%;">
                                    <asp:TextBox runat="server" ID="Pulse" Width="100%" />
                                </td>
                            </tr>
                            <tr>
                                <th style="width:30%; text-align:right; border-bottom-style:dotted">
                                    מכפיל תצוגה
                                </th>
                                <td style="align-content:flex-end; width:70%;">
                                    <asp:TextBox runat="server" ID="Display" Width="100%" />
                                </td>
                            </tr>
                            <tr>
                                <th style="width:30%; text-align:right; border-bottom-style:dotted">
                                    תאריך ההתחלה:
                                </th>
                                <td style="align-content:flex-end; width:70%;">
                                    <asp:TextBox ID="txtDateTo" runat="server" Width="100px"></asp:TextBox>
                                    <asp:Image ID="Image2" runat="server" ImageUrl="~/images/SmallCalendar.gif" />
                                    <cc1:CalendarExtender ID="txtDateTo_CalendarExtender" runat="server" 
                                        Enabled="True" TargetControlID="txtDateTo" PopupButtonID="Image2" 
                                            Format="yyyy-MM-dd" TodaysDateFormat="MMMM dd, yyyy" FirstDayOfWeek="Sunday">
                                    </cc1:CalendarExtender>
                                    <ew:TimePicker ID="TimePicker2" runat="server" Width="60px" NumberOfColumns="4" 
                                        PopupWidth="120px" Scrollable="False" ImageUrl="~/images/timepicker.gif" 
                                        MilitaryTime="True" MinuteInterval="ThirtyMinutes" >
                                        <ButtonStyle BorderStyle="None" Width="25px" />
                                    </ew:TimePicker>
                                </td>
                            </tr>
                        </table>

                        <br />
                        <asp:Button Text="שמור" runat="server" ID="SaveParams" Width="49%" OnClick="SaveParams_Click" />
                        <asp:Button Text="הצג" runat="server" ID="ShowAll" Width="49%" OnClick="ShowAll_Click" />
                        <br />
                        <br />
<%--                        <asp:Button Text="פתך טבלאת פרטים" runat="server" ID="GoDetails" Width="100%" OnClick="GoDetails_Click" Font-Size="X-Large" BackColor="#9498a1"/>--%>
<%--                    </asp:View>--%>
<%--                </asp:MultiView>--%>
            </td>
        </tr>
    </table>

    <asp:sqldatasource id="tCons"
        selectcommand="SELECT sql_Consumer_ID, sql_Consumer_name, sql_clientID, (SELECT Name FROM dbo.T_Client WHERE ID = sql_clientID) as client, 
        OrderNo, sql_Consumer_active, BeginDate, ConsumerType, (SELECT ConTpName FROM [T_ConsumerType] WHERE [ConTpID] = ConsumerType) AS cType,
        sql_Consumer_max_over_cons, EngName, RusName FROM tbl_Consumers_0"
        connectionstring="<%$ ConnectionStrings:Money_DisplayConnectionString3 %>" 
        runat="server"/>
    <asp:SqlDataSource id="SqlClients"
        SelectCommand="SELECT [ID], [Name] FROM [dbo].[T_Client]"
        connectionstring="<%$ ConnectionStrings:Money_DisplayConnectionString3 %>" 
        runat="server" />
    <asp:SqlDataSource ID="SqlConsumerType" runat="server" 
        ConnectionString="<%$ ConnectionStrings:Money_DisplayConnectionString3 %>" 
        SelectCommand="SELECT [ConTpID], [ConTpName] FROM [T_ConsumerType]">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlCounterTypes" runat="server"         
        ConnectionString="<%$ ConnectionStrings:Money_DisplayConnectionString3 %>"
        SelectCommand="SELECT NULL as TypeName UNION SELECT [TypeName] FROM [dbo].[T_CounterModes]">
    </asp:SqlDataSource>



</asp:Content>

