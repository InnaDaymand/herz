﻿using System;
using System.Data;

public partial class AdminPages_Consumer : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            CleanDetailes();
        }
    }

    protected void GoParams_Click(object sender, EventArgs e)
    {
        //mView.ActiveViewIndex = 1;
    }

    protected void GoDetails_Click(object sender, EventArgs e)
    {
        //mView.ActiveViewIndex = 0;
    }

    protected void GridMaster_SelectedIndexChanged(object sender, EventArgs e)
    {
        string SQL = "";
        int ConsID = 0;
        int RowInd = GridMaster.SelectedIndex;
        ConsID = (int)GridMaster.SelectedDataKey.Value;
        AllFunctions.ConsID = ConsID.ToString();
        string SS = "";
        SS = GridMaster.Rows[RowInd].Cells[2].Text;
        SS = ClearString(SS);
        mv0Name.Text = mvTitle.Text = SS;
        SS = GridMaster.Rows[RowInd].Cells[7].Text;
        SS = ClearString(SS);
        mv0NameE.Text = SS;
        SS = GridMaster.Rows[RowInd].Cells[8].Text;
        SS = ClearString(SS);
        mv0NameR.Text = SS;
        SQL = "SELECT [sql_Consumer_Model] FROM [dbo].[tbl_Consumers_0] WHERE sql_Consumer_ID = " + ConsID.ToString();
        consumermodel.Text = AllFunctions.GetScalar(SQL);
        SQL = "SELECT [sql_manufacturer] FROM [dbo].[tbl_Consumers_0] WHERE sql_Consumer_ID = " + ConsID.ToString();
        TelNo.Text = AllFunctions.GetScalar(SQL);
        SQL = "SELECT sql_clientID FROM tbl_Consumers_0 WHERE sql_Consumer_ID = " + ConsID.ToString();
        SS = AllFunctions.GetScalar(SQL);
        sql_clientIDComboBox.SelectedValue = SS;
        SQL = "SELECT ConsumerType FROM tbl_Consumers_0 WHERE sql_Consumer_ID = " + ConsID.ToString();
        SS = AllFunctions.GetScalar(SQL);
        ConsumerType.SelectedValue = SS;

        if (SS == "7")
        {
            SqlEnergyForAir.Visible = true;
            EnergyAir.Visible = true;
            SQL = "EXEC dbo.Util_GetConsumersForAir " + ConsID.ToString();
            SqlEnergyForAir.DataSource = AllFunctions.Populate(SQL);
            SqlEnergyForAir.DataTextField = "Name";
            SqlEnergyForAir.DataValueField = "ID";
            SqlEnergyForAir.DataBind();

            SQL = "SELECT ISNULL([ConsumerEnergy], -1)  FROM [dbo].[tbl_Consumers_0] WHERE [sql_Consumer_ID] = " + ConsID.ToString();
            SqlEnergyForAir.SelectedValue = AllFunctions.GetScalar(SQL);
        }
        else
        {
            SqlEnergyForAir.Visible = false;
            EnergyAir.Visible = false;
        }

        SQL = "SELECT sql_Consumer_active FROM tbl_Consumers_0 WHERE sql_Consumer_ID = " + ConsID.ToString();
        SS = AllFunctions.GetScalar(SQL);
        IsActive.Checked = Convert.ToBoolean(SS);

        SQL = "SELECT sql_Consumer_max_over_cons FROM tbl_Consumers_0 WHERE sql_Consumer_ID = " + ConsID.ToString();
        SS = AllFunctions.GetScalar(SQL);
        SqlShowAndGroup.SelectedValue = SS;

        SQL = "SELECT HasTemperature FROM tbl_Consumers_0 WHERE sql_Consumer_ID = " + ConsID.ToString();
        SS = AllFunctions.GetScalar(SQL);
        Temperature.Checked = Convert.ToBoolean(SS);
        OrderNo.Text = GridMaster.Rows[RowInd].Cells[4].Text;

        SQL = "SELECT TOP 1 [ModesID] FROM [dbo].[T_CounterModesParams] WHERE [ConsumerID] = " + ConsID.ToString() + " ORDER BY [FromDate] DESC";
        SS = AllFunctions.GetScalar(SQL);
        FillParams(SS);
        AllFunctions.ParamsID[0] = SS;
        AllFunctions.ParamsIdCur = 0;

        SQL = "SELECT COUNT(*) FROM [dbo].[T_CounterModesParams] WHERE [ConsumerID] = " + ConsID.ToString();
        SS = AllFunctions.GetScalar(SQL);
        ShowAll.Enabled = (bool)(SS != "1");
        if (SS != "0" && SS != "1")
        {
            SQL = "SELECT [ModesID] FROM [dbo].[T_CounterModesParams] WHERE [ConsumerID] = " + ConsID.ToString() + " ORDER BY [FromDate] DESC";
            DataTable ndt = AllFunctions.Populate(SQL);
            for (int i = 0; i < ndt.Rows.Count; i++)
            {
                AllFunctions.ParamsID[i] = ndt.Rows[i][0].ToString();
            }
            ShowAll.Text = "הצג את הקודם";
            ShowAll.Enabled = true;
        }


        //mView.ActiveViewIndex = 0;
    }

    private string ClearString(string ST)
    {
        if (ST.IndexOf("&nbsp;") >= 0)
        {
            ST = ST.Replace("&nbsp;", "");
        }
        if (ST.IndexOf("&quot;") > 0)
        {
            ST = ST.Replace("&quot;", "''");
        }
        return ST;
    }

    protected void SaveIt_Click(object sender, EventArgs e)
    {
        if (AllFunctions.ConsID == "")
        {
            AddNewConsumer();
        }
        else
        {
            UpdateConsumer();
        }
    }

    protected void CleanIt_Click(object sender, EventArgs e)
    {
        CleanDetailes();
    }

    private void CleanDetailes()
    {
        AllFunctions.ConsID = "";
        mv0Name.Text = "";
        mv0NameE.Text = "";
        mv0NameR.Text = "";
        sql_clientIDComboBox.SelectedIndex = -1;
        ConsumerType.SelectedIndex = -1; ;
        IsActive.Checked = false;
        Temperature.Checked = false;
        TelNo.Text = "";
        OrderNo.Text = "1";
        mvTitle.Text = "";
        consumermodel.Text = "";

        FillParams("");
    }

    private void AddNewConsumer()
    {
        if (mv0Name.Text== "")
        {
            return;
        }
        string SQL = "SELECT MAX(sql_Consumer_ID) + 1 FROM tbl_Consumers_0";
        string SS = AllFunctions.GetScalar(SQL);

        SQL = "INSERT INTO tbl_Consumers_0 (sql_Consumer_ID, sql_Consumer_active, sql_Consumer_name, sql_Consumer_consumption, "
            + "sql_Consumer_max_over_cons, sql_clientID, HasTemperature, OrderNo, ConsumerType, EngName, RusName, sql_Consumer_Model, sql_manufacturer ) VALUES ( "
            + SS + ", " + Convert.ToInt32( IsActive.Checked) + ", N'" + mv0Name.Text + "', 50, " + SqlShowAndGroup.SelectedValue.ToString() + ", "
            + sql_clientIDComboBox.SelectedValue.ToString() + ", " + Convert.ToInt32(Temperature.Checked) + ", " + OrderNo.Text + ", "
            + ConsumerType.SelectedValue.ToString() + ", N'" + mv0NameE.Text + "', N'" + mv0NameR.Text + "', N'" + consumermodel.Text
            + "', N'" + TelNo.Text + "' )";
        AllFunctions.DoExec(SQL);
        CleanDetailes();
        GridMaster.DataBind();
    }

    private void UpdateConsumer()
    {
        if (mv0Name.Text== "")
        {
            return;
        }
        //string SS = "";
        string SQL = "UPDATE tbl_Consumers_0 SET "
            + "sql_Consumer_active = " + Convert.ToInt32(IsActive.Checked) + ", "
            + "sql_Consumer_name = N'" + mv0Name.Text + "', "
            + "sql_clientID = " + sql_clientIDComboBox.SelectedValue.ToString() + ", "
            + "HasTemperature = " + Convert.ToInt32(Temperature.Checked) + ", "
            + "OrderNo = " + OrderNo.Text + ", "
            + "ConsumerType = " + ConsumerType.SelectedValue.ToString() + ", "
            + "sql_Consumer_max_over_cons = " + SqlShowAndGroup.SelectedValue.ToString() + ", "
            + "EngName = N'" + mv0NameE.Text + "', "
            + "RusName = N'" + mv0NameR.Text + "', "
            + "sql_Consumer_Model = N'" + consumermodel.Text.Replace("'", "''")+"', "
            + "sql_manufacturer = N'"  + TelNo.Text + "' "
            + "WHERE sql_Consumer_ID = " + AllFunctions.ConsID;
        AllFunctions.DoExec(SQL);

        if (SqlEnergyForAir.Visible)
        {
            if (SqlEnergyForAir.SelectedValue != "")
            {
                SQL = "UPDATE tbl_Consumers_0 SET [ConsumerEnergy] = " + SqlEnergyForAir.SelectedValue.ToString() + " WHERE sql_Consumer_ID = " + AllFunctions.ConsID; 
                AllFunctions.DoExec(SQL);
            }
        }

        CleanDetailes();
        GridMaster.DataBind();
    }

    private void FillParams(string ParamKey)
    {
        txtDateTo.ForeColor = System.Drawing.Color.Black;
        if (ParamKey == "") //clear table
        {
            ChooseTypes.SelectedIndex = -1;
            pV.Text = "1";
            pI.Text = "1";
            pW.Text = "1";
            pPhi.Text = "1";
            pCB.Text = "1";
            pEnR.Text = "1";
            pWR.Text = "1";
            pES1.Text = "1";
            pES2.Text = "1";
            pES3.Text = "1";
            pI2.Text = "1";
            pI3.Text = "1";
            Pulse.Text = "1";
            Display.Text = "1";
            DateTime dt = DateTime.Now;
            txtDateTo.Text = dt.ToString("yyyy-MM-dd");
            TimePicker2.SelectedTime = dt;
            ShowAll.Enabled = false;
            ShowAll.Text = "";
        }
        else
        {
            string SQL = "SELECT [Mode],[ParamV],[ParamI],[ParamW],[ParamPhi],[ParamCB],[ParamEnR],[ParamWR],[ParamES1],[ParamES2],[ParamES3],[ParamI2],[ParamI3],"
                + "[PulseValue],[ParamDisplay],[FromDate] FROM [dbo].[T_CounterModesParams] WHERE [ModesID] = " + ParamKey;
            DataTable ndt = AllFunctions.Populate(SQL);
            ChooseTypes.SelectedValue = ndt.Rows[0][0].ToString();
            pV.Text = ndt.Rows[0][1].ToString();
            pI.Text = ndt.Rows[0][2].ToString();
            pW.Text = ndt.Rows[0][3].ToString();
            pPhi.Text = ndt.Rows[0][4].ToString();
            pCB.Text = ndt.Rows[0][5].ToString();
            pEnR.Text = ndt.Rows[0][6].ToString();
            pWR.Text = ndt.Rows[0][7].ToString();
            pES1.Text = ndt.Rows[0][8].ToString();
            pES2.Text = ndt.Rows[0][9].ToString();
            pES3.Text = ndt.Rows[0][10].ToString();
            pI2.Text = ndt.Rows[0][11].ToString();
            pI3.Text = ndt.Rows[0][12].ToString();
            Pulse.Text = ndt.Rows[0][13].ToString();
            Display.Text = ndt.Rows[0][14].ToString();
            DateTime dt = Convert.ToDateTime(ndt.Rows[0][15].ToString());
            txtDateTo.Text = dt.ToString("yyyy-MM-dd");
            TimePicker2.SelectedTime = dt;
        }
    }

    protected void SaveParams_Click(object sender, EventArgs e)
    {
        string SQL = "", SS = "";
        int II = 0;

        SQL = "SELECT [Mode] FROM [dbo].[T_CounterModesParams] WHERE [ModesID] = " + AllFunctions.ParamsID[0];
        //SQL = "SELECT ABS(DATEDIFF(MINUTE, FromDate, '2017-09-14 10:00')) FROM [dbo].[T_CounterModesParams] WHERE [ModesID] = " + AllFunctions.ParamsID[0];
        SS = AllFunctions.GetScalar(SQL);
        //II = Convert.ToInt32(SS);
        if (SS != ChooseTypes.SelectedValue)
        {
            ParamsSaveNew();
        }
        else
        {
            ParamsUpdate();
        }
    }

    protected void ShowAll_Click(object sender, EventArgs e)
    {
        if (AllFunctions.ParamsIdCur == 0)
        {
            FillParams(AllFunctions.ParamsID[1]);
            AllFunctions.ParamsIdCur = 1;
            ShowAll.Text = "לחזור";
            SaveParams.Enabled = false;
        }
        else
        {
            FillParams(AllFunctions.ParamsID[0]);
            AllFunctions.ParamsIdCur = 0;
            ShowAll.Text = "הצג את הקודם";
            SaveParams.Enabled = true;
        }
    }

    private void ParamsSaveNew()
    {
        string SS = "";
        string SQL = "INSERT INTO T_CounterModesParams (ConsumerID, Mode, ParamV, ParamI, ParamW, ParamPhi, ParamCB, ParamEnR, "
            + "ParamWR, ParamES1, ParamES2, ParamES3, ParamI2, ParamI3, PulseValue, ParamDisplay, FromDate) VALUES ("
            + AllFunctions.ConsID + ", N'" + ChooseTypes.SelectedValue + "', ";

        SS = pV.Text
        + ", " + pI.Text
        + ", " + pW.Text
        + ", " + pPhi.Text
        + ", " + pCB.Text
        + ", " + pEnR.Text
        + ", " + pWR.Text
        + ", " + pES1.Text
        + ", " + pES2.Text
        + ", " + pES3.Text
        + ", " + pI2.Text
        + ", " + pI3.Text
        + ", " + Pulse.Text
        + ", " + Display.Text
        + ", CONVERT(DATETIME, '" + txtDateTo.Text + " " + TimePicker2.SelectedTime.TimeOfDay + "', 102))";

        AllFunctions.DoExec(SQL + SS);

        SQL = "UPDATE tbl_Consumers_0 SET CounterType = N'" 
            + ChooseTypes.SelectedValue + "' WHERE sql_Consumer_ID = " + AllFunctions.ConsID;
        AllFunctions.DoExec(SQL);

        SQL = "UPDATE tbl_Counters SET sql_puls_value = " + Pulse.Text + ", DispCoef = " + Display.Text
            + " WHERE sql_Consumer_ID = " + AllFunctions.ConsID;
         AllFunctions.DoExec(SQL);

        //CleanDetailes();
        GridMaster.DataBind();
    }

    private void ParamsUpdate()
    {
        string SQL = "UPDATE T_CounterModesParams SET "
            + "Mode = '" + ChooseTypes.SelectedValue + "'"
            + ", ParamV = " + pV.Text
            + ", ParamI = " + pI.Text
            + ", ParamW = " + pW.Text
            + ", ParamPhi = " + pPhi.Text
            + ", ParamCB = " + pCB.Text
            + ", ParamEnR = " + pEnR.Text
            + ", ParamWR = " + pWR.Text
            + ", ParamES1 = " + pES1.Text
            + ", ParamES2 = " + pES2.Text
            + ", ParamES3 = " + pES3.Text
            + ", ParamI2 = " + pI2.Text
            + ", ParamI3 = " + pI3.Text
            + ", PulseValue = " + Pulse.Text
            + ", ParamDisplay = " + Display.Text
            + ", FromDate = CONVERT(DATETIME, '" + txtDateTo.Text + " " + TimePicker2.SelectedTime.TimeOfDay + "', 102) WHERE [ModesID] = "
            + AllFunctions.ParamsID[0];

        AllFunctions.DoExec(SQL);

        SQL = "UPDATE tbl_Consumers_0 SET CounterType = N'" 
            + ChooseTypes.SelectedValue + "' WHERE sql_Consumer_ID = " + AllFunctions.ConsID;
        AllFunctions.DoExec(SQL);

        SQL = "UPDATE tbl_Counters SET sql_puls_value = " + Pulse.Text + ", DispCoef = " + Display.Text
            + " WHERE sql_Consumer_ID = " + AllFunctions.ConsID;
         AllFunctions.DoExec(SQL);
    }

    protected void EraseIt_Click(object sender, EventArgs e)
    {

        string SQL = "DELETE FROM tbl_Consumers_0 WHERE sql_Consumer_ID = " + AllFunctions.ConsID;
        AllFunctions.DoExec(SQL);

        SQL = "DELETE FROM T_CounterModesParams WHERE ConsumerID = " + AllFunctions.ConsID;
        AllFunctions.DoExec(SQL);

        SQL = "DELETE FROM  tbl_Counters WHERE sql_Consumer_ID = " + AllFunctions.ConsID;
        AllFunctions.DoExec(SQL);

        CleanDetailes();
        GridMaster.DataBind();
    }

    protected void NoFilter_Click(object sender, EventArgs e)
    {
        for (int i = 0; i < GridMaster.Rows.Count; i++)
        {
            GridMaster.Rows[i].Visible = true;
        }
    }

    protected void DoFilter_Click(object sender, EventArgs e)
    {
        string SS = ForFilter.Text;
        if (SS== "")
        {
            return;
        }
        int ir = Convert.ToInt32(GridFields.SelectedValue);
        for (int i = 0; i < GridMaster.Rows.Count; i++)
        {
            GridMaster.Rows[i].Visible = (bool)(GridMaster.Rows[i].Cells[ir].Text == SS);
        }
    }
}