﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AdminPages_ConsumerType : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void GridType_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        int rr = e.RowIndex;
        string EdVal = e.NewValues[0].ToString();
        string IDC = GridType.Rows[rr].Cells[1].Text;
        string SQL = "UPDATE T_ConsumerType SET ConTpName = N'" + EdVal + "' WHERE ConTpID = " + IDC;
        SqlConsumerTypes.UpdateCommand = SQL;
        GridType.DataBind();
    }

    protected void btnIns_Click(object sender, EventArgs e)
    {
        if (tbIns.Text == "") return;
        string SQL = "INSERT INTO T_ConsumerType (ConTpName) VALUES (N'" + tbIns.Text + "') ";
        AllFunctions.DoExec(SQL);
        GridType.DataBind();
    }

    protected void BB1_Click(object sender, EventArgs e)
    {
        BB2.Font.Bold = false;
        BB1.Font.Bold = true;
        BB3.Font.Bold = false;
        BB4.Font.Bold = false;
        TabsView.ActiveViewIndex = 0;
    }

    protected void BB2_Click(object sender, EventArgs e)
    {
        BB1.Font.Bold = false;
        BB2.Font.Bold = true;
        BB3.Font.Bold = false;
        BB4.Font.Bold = false;
        TabsView.ActiveViewIndex = 1;
    }

    protected void BB3_Click(object sender, EventArgs e)
    {
        BB1.Font.Bold = false;
        BB2.Font.Bold = false;
        BB3.Font.Bold = true;
        BB4.Font.Bold = false;
        TabsView.ActiveViewIndex = 2;
    }

    protected void BB4_Click(object sender, EventArgs e)
    {
        BB1.Font.Bold = false;
        BB2.Font.Bold = false;
        BB3.Font.Bold = false;
        BB4.Font.Bold = true;
        TabsView.ActiveViewIndex = 3;
    }

    protected void btnIns0_Click(object sender, EventArgs e)
    {
        string ncr = ddConsumer.SelectedValue;
        string SQL = "INSERT INTO T_OnOffConsumer(ConsID) VALUES (" + ncr + ") ";
        AllFunctions.DoExec(SQL);
        GridSMS.DataBind();
        ddConsumer.DataBind();
    }

    protected void GridSMS_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string[] FieldName = { "TelNoOn", "TelNoOff", "TelInfo", "TextOn", "TextOff", "TextInfo" };
        int rr = e.RowIndex;
        string IDC = GridSMS.DataKeys[rr].Value.ToString();
        string EdVal = "";
        for (int i = 0; i < 6; i++)
        {
            if (e.NewValues[i] != null)
            {
                EdVal += FieldName[i] + " = N'" + e.NewValues[i] + "',";
            }
        }
        if (EdVal.Length > 1) EdVal = EdVal.Substring(0, EdVal.Length - 1);

        string SQL = "UPDATE T_OnOffConsumer SET " + EdVal + " WHERE ConsID = " + IDC;
        SqlTablesSMS.UpdateCommand = SQL;
        GridType.DataBind();
    }

    protected void GridSMS_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int rr = e.RowIndex;
        string IDC = GridSMS.DataKeys[rr].Value.ToString();
        string SQL = "DELETE FROM T_OnOffConsumer WHERE ConsID = " + IDC;
        SqlTablesSMS.DeleteCommand = SQL;
        GridType.DataBind();
    }

    protected void GridTechData_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

    }

    protected void btnIns1_Click(object sender, EventArgs e)
    {
        if ((tbIns0.Text == "") || (tbIns1.Text == "")) return;
        string SQL = "INSERT INTO T_FactorType ([FactorID], FactorName, [FactorParam]) VALUES (" + tbIns1.Text + ", N'" + tbIns0.Text + "', 1.0)";
        tbIns0.Text = "";
        tbIns1.Text = "";
        AllFunctions.DoExec(SQL);
        GridTechData.DataBind();
    }
}