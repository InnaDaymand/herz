﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminPages/AdminPage.master" AutoEventWireup="true" CodeFile="ConsumersTree.aspx.cs" Inherits="AdminPages_ConsumersTree" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .auto-style1 {
            width: 200px;
        }
        .auto-style2 {
           width:300px;
            height: 380px;
        }
        .auto-style5 {
            width: 120px;
            align-content:center;
            align-items:center;
        }
        .auto-style6 {
            height: 36px;
        }
        .auto-style7 {
            width: 100%;
        }
        .auto-style9 {
            width: 79px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div runat="server" style="align-items:center">
        <asp:DropDownList ID="GetUser" runat="server" AutoPostBack="True" OnSelectedIndexChanged="GetUser_SelectedIndexChanged" Width="300px">
        </asp:DropDownList>
        <br />
        <br />
        <table class="auto-style7">
            <tr class="auto-style7">
                <td rowspan="2" class="auto-style2" align="right" dir="rtl">
                    <asp:TreeView ID="Derevo" runat="server" ImageSet="Simple" OnSelectedNodeChanged="Derevo_SelectedNodeChanged" >
                        <HoverNodeStyle Font-Underline="True" ForeColor="#5555DD" />
                        <NodeStyle Font-Names="Tahoma" Font-Size="10pt" ForeColor="Black" 
                            HorizontalPadding="0px" NodeSpacing="0px" VerticalPadding="0px" />
                        <ParentNodeStyle Font-Bold="False" />
                        <SelectedNodeStyle Font-Underline="True" ForeColor="#5555DD" 
                            HorizontalPadding="0px" VerticalPadding="0px" />
                    </asp:TreeView>
                </td>
                <td class="auto-style6">
                    <table class="auto-style7">
                        <tr>
                            <th colspan="2" align="center">
                                <asp:Label Text="" runat="server" ID="EditNode" Font-Size="X-Large" />
                            </th>
                            <th class="auto-style9">
                                <asp:CheckBox Text="ניתן לצרף" runat="server" ID="IsPlus1" Visible="False" />
                            </th>

                            <th class="auto-style5">
                                <asp:Button Text="עדקן כצרכן ראשי" runat="server" Width="120px" ID="B2" OnClick="B2_Click" Visible="False" />
                            </th>
                            <th class="auto-style5">
                                <asp:Button Text="להסיר צרכן ראשי" runat="server" ID="B3" OnClick="B3_Click" CommandArgument="0" Visible="False" Width="120px" />
                            </th>
                        </tr>
                        <tr class="auto-style6">
                            <th colspan="5"></th>
                        </tr>
                        <tr>
                            <td class="auto-style1">
                                <asp:ListBox runat="server" ID="SecondaryList" Width="100%" AutoPostBack="True" OnSelectedIndexChanged="SecondaryList_SelectedIndexChanged" Visible="False">
                                </asp:ListBox>
                            </td>
                             <td class="auto-style1">
                                <asp:DropDownList runat="server" ID="ConsumersList" Width="100%" Visible="False" AutoPostBack="True" OnSelectedIndexChanged="ConsumersList_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                           <td class="auto-style9">
                               <asp:CheckBox Text="ניתן לצרף" runat="server" ID="IsPlus2" Visible="False" />
                               <br />
                               <asp:TextBox runat="server" Width="40px" ID="OrderID" />
                               <asp:Label Text="מס סדר" runat="server" />
                            </td>
                            <td class="auto-style5">
                                <asp:Button Text="להוסיף צרכן" runat="server" Width="120px" ID="B4" OnClick="B4_Click" Visible="False" />
                            </td>
                            <td class="auto-style5">
                                <asp:Button Text="להסיר צרכן" runat="server" Width="120px" ID="B5" OnClick="B5_Click" CommandArgument="0" Visible="False" />
                            </td>
                        </tr>
                        <%--                        <tr>
                            <td class="auto-style6" colspan="5"></td>
                        </tr>
                        <tr class="auto-style6">
                            <th>צרכן עיקרי</th>
                            <th class="auto-style8">צרכן משני</th>
                            <th class="auto-style9">ניתן לצרף</th>
                            <th></th>
                            <th></th>
                        </tr>
                        <tr>
                            <td class="auto-style1">
                                <asp:DropDownList runat="server" ID="MainsList" Width="100%">
                                </asp:DropDownList>
                            </td>
                            <td class="auto-style8">
                                <asp:DropDownList runat="server" ID="SecnList" Width="100%">
                                </asp:DropDownList>
                            </td>
                            <td class="auto-style9">
                                &nbsp;</td>
                            <td class="auto-style5">
                                &nbsp;</td>
                            <td class="auto-style5">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="auto-style6" colspan="5"></td>
                        </tr>
                        <tr class="auto-style6">
                            <th>צרכן עיקרי</th>
                            <th class="auto-style8">צרכן משני</th>
                            <th class="auto-style9"></th>
                            <th></th>
                            <th></th>
                        </tr>
                        <tr>
                            <td class="auto-style1">
                                <asp:DropDownList runat="server" ID="MList" Width="100%">
                                </asp:DropDownList>
                            </td>
                            <td class="auto-style8">
                                <asp:DropDownList runat="server" ID="SList" Width="100%">
                                </asp:DropDownList>
                            </td>
                            <td class="auto-style9">
                                <%--                                <asp:CheckBox Text="ניתן לצרף" runat="server" />
                            </td>
                            <td class="auto-style5">
                                <%--<asp:Button Text="להוסיף צרכן" runat="server" Width="100%" ID="B4" OnClick="B2_Click" />--%><%--                                <asp:Button Text="להסיר צרכן" runat="server" Width="140%" ID="B0" OnClick="B3_Click" CommandArgument="0" />
                            </td>
                            <td class="auto-style5">
                        </tr>--%>
                 </table>
                </td>
            </tr>
            <%--            <tr>
                <td class="auto-style4">
                    &nbsp;</td>
                <td class="auto-style3">
                    &nbsp;</td>
            </tr>--%>
        </table>
        <br />
        <%--        <br />
        <table>
            <tr>
                <td class="auto-style1">
                    <asp:Literal Text="שם רמה והמפלס העליון" runat="server" />
                </td>
                <td class="auto-style1">
                    <asp:TextBox runat="server" Width="100%" ID="NewNode" />
                </td>
                <td class="auto-style1">
                    <asp:DropDownList runat="server" Width="100%" ID="GetParent">
                    </asp:DropDownList>
                </td>
                <td class="auto-style1">
                    <asp:Button Text="הוספ כמפעל" runat="server" Width="30%" ID="NewAdd" OnClick="NewAdd_Click" Visible="False"/>
                    <asp:Button Text="הוסף" runat="server" Width="40%" ID="ForAdd" OnClick="ForAdd_Click" />
                </td>
            </tr>
        </table>--%>
        <br />
        <div dir="rtl">
            <asp:Label runat="server" ForeColor="Red" Font-Bold="True" Font-Names="Arial" Font-Size="18px" ID="Alarm"/>
        </div>
    </div>
</asp:Content>

