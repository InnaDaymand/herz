﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class AdminPages_ConsumersTree : System.Web.UI.Page
{

    /*
SELECT 
     [ConsumerID]
    ,[ParentConsumer]
    ,[ParentDepartment]
    ,[CanBeAttached]
    ,[ConsumerType]
	,[OrderID]
FROM [md2000Net].[dbo].[T_CompositeConsumer]
    */
    string nop = "";
    int Alarms;
    int TL = 0;

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            GetUsers();
        }
    }

    void GetUsers()
    {
        Alarms = 0;//  
        GetUser.Items.Add("בחר משתמש");
        string SQL = "SELECT -1 AS ID, 'בחר משתמש' AS UName UNION ALL SELECT [ID], [FirstName] + ' ' + [LastName] AS UName FROM[md2000Net].[dbo].[T_User]";
        GetUser.DataSource = AllFunctions.Populate(SQL);
        GetUser.DataTextField = "UName";
        GetUser.DataValueField = "ID";
        GetUser.DataBind();
        GetUser.SelectedIndex = -1;
    }

    void FillGrid(string UID)
    {
        string SQL = "EXEC S_PlantLevelsGet " + UID + ", 0";
        DataTable dt = AllFunctions.Populate(SQL);

        Derevo.Nodes.Clear();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            TreeNode n0 = new TreeNode(dt.Rows[i][1].ToString(), dt.Rows[i][2].ToString());
            string un = dt.Rows[i][0].ToString();
            string tt = dt.Rows[i][5].ToString();
            if (un == "0")
            {
                n0.ToolTip = tt;
                Derevo.Nodes.Add(n0);
            }
            else
            {
                TreeNode nnt = GetNodeByID(un);
                n0.ToolTip = tt;
                nnt.ChildNodes.Add(n0);
                if (dt.Rows[i][3].ToString() == "-1")
                {
                    n0.ShowCheckBox = true; // complementary
                }
            }
        }
    }

    void GetConsumers(string UID)
    {
        string SQL = "";
        SQL = "EXEC [dbo].[S_GetAllConsumersByUserForNewTree]  @AllConsumers = 0, @Lang = 0, @UserID = " + UID;
        ConsumersList.DataSource = AllFunctions.Populate(SQL);
        ConsumersList.DataTextField = "NAME";
        ConsumersList.DataValueField = "ID";
        ConsumersList.DataBind();

        if (ConsumersList.Items.Count > 1)
        {
            ConsumersList.Visible = true;
            B4.Visible = true;
        }
    }

    protected void GetUser_SelectedIndexChanged(object sender, EventArgs e)
    {
        string sd = GetUser.SelectedValue.ToString();
        FillGrid(sd);
        GetConsumers(sd);
    }

    TreeNode GetNodeByID(string sID)
    {
        TreeNode t0 = new TreeNode();
        string vps = "";
        for (int i = 0; i < Derevo.Nodes.Count; i++)
        {
            nop = "";
            vps = CheckNode(Derevo.Nodes[i], sID);
            if (vps != "")
            {
                t0 = Derevo.FindNode(vps);
            }
        }

        return t0;
    }

    string CheckNode(TreeNode cTn, string sFF)
    {
        if (cTn.Value.ToString() == sFF)
        {
            nop = cTn.ValuePath;
        }
        else
        {
            for (int j = 0; j < cTn.ChildNodes.Count; j++)
            {
                if (CheckNode(cTn.ChildNodes[j], sFF) != "")
                {
                    //nop = cTn.ChildNodes[j].ValuePath;
                    break;
                }
            }
        }
        return nop;
    }

    protected void ForAdd_Click(object sender, EventArgs e)
    {
        //string SQL = "";
        //if (NewNode.Text == "")
        //{
        //    return;
        //}

        //if (GetParent.Items.Count == 0)
        //{
        //    SQL = "INSERT INTO [dbo].[T_PlantLevels] (LevelName, UpLevelID, UserID, [OrderL1], [OrderL2], [OrderL3]) VALUES (N'"
        //        + NewNode.Text + "', NULL, " + GetUser.SelectedValue.ToString() + ", 0, 0, 0)";
        //    AllFunctions.DoExec(SQL);
        //}
        //else
        //{
        //    if (GetParent.SelectedIndex != -1)
        //    {
        //        SQL = "SELECT dbo.GetLevels1and2ofPlants(" + GetParent.SelectedValue.ToString() + ")";
        //        string nin = AllFunctions.GetScalar(SQL);
        //        SQL = "INSERT INTO [dbo].[T_PlantLevels] (LevelName, UpLevelID, UserID, [OrderL1], [OrderL2], [OrderL3] ) VALUES (N'"
        //            + NewNode.Text + "', " + GetParent.SelectedValue.ToString() + ", NULL, " 
        //            + nin.Substring(0, nin.Length - 2) + ", " + nin.Substring(nin.Length - 2) + ", 0)";
        //        AllFunctions.DoExec(SQL);
        //    }
        //    else
        //        return;
        //}

        ////AllFunctions.DoExec(SQL);
        //FillGrid(GetUser.SelectedValue.ToString());
        //GetConsumers(GetUser.SelectedValue.ToString());
        //Alarm.Text = "";
        //EditNode.Text = "";
        //NewNode.Text = "";
    }

    protected void Derevo_SelectedNodeChanged(object sender, EventArgs e)
    {
        string SQL = "";
        string SS = "";
        TL = Convert.ToInt32(Derevo.SelectedNode.Value);
        B2.Visible = false;
        B3.Visible = false;
        B4.Visible = false;
        B5.Visible = false;
        IsPlus1.Visible = false;
        if (Derevo.SelectedNode.ShowCheckBox == true)
        {
            SS = "SELECT COUNT(*) FROM [dbo].[T_CompositeConsumer] WHERE [ConsumerID] = " + TL.ToString();
            SS = AllFunctions.GetScalar(SS);
            if (SS == "0")
            {
                IsPlus1.Visible = true;
                B2.Visible = true;
                B3.Visible = false;
            }
            else
            {

                SQL = "SELECT COUNT(*) FROM [dbo].[T_CompositeConsumer] WHERE [ParentConsumer] = " + TL.ToString();
                SQL = AllFunctions.GetScalar(SQL);
                if (SQL == "0")
                {
                    IsPlus1.Visible = false;
                    B2.Visible = false;
                    B3.Visible = true;
                }
            }
            EditNode.Text = "הצרכן נבחר הוא" + " - " + Derevo.SelectedNode.Text;
            SQL = "SELECT [ConsumerID] AS ID, (SELECT[sql_Consumer_name] FROM[dbo].[tbl_Consumers_0] WHERE[sql_Consumer_ID] = ConsumerID) as [Name] "
                + " FROM [dbo].[T_CompositeConsumer] WHERE [ParentConsumer] = " + TL.ToString() + " ORDER BY [OrderID]";
            SecondaryList.DataSource = AllFunctions.Populate(SQL);
            SecondaryList.DataTextField = "Name";
            SecondaryList.DataValueField = "ID";
            SecondaryList.DataBind();
            SecondaryList.Visible = true;
            GetConsumers(GetUser.SelectedValue.ToString());
        }
        else
        {
            EditNode.Text = "המחלקה נבחרה היא" + " - " + Derevo.SelectedNode.Text;
        }

    }

    protected void B1_Click(object sender, EventArgs e) //Cancel
    {
        Alarm.Text = "";
        EditNode.Text = "";
    }

    protected void B2_Click(object sender, EventArgs e) //Save
    {
        TL = Convert.ToInt32(Derevo.SelectedNode.Value);
        string II = "";
        if (IsPlus1.Checked)
        {
            II = "1";
        }
        else II = "0";
        string SQL = "INSERT INTO [dbo].[T_CompositeConsumer] ([ConsumerID],[CanBeAttached],[OrderID]) VALUES ("
                    + TL.ToString() + ", " + II + ", 1)";
        AllFunctions.DoExec(SQL);
        Alarm.Text = "";
        //FillGrid(GetUser.SelectedValue.ToString());
        Alarm.Text = "";
        EditNode.Text = "";
        IsPlus1.Visible = false;
        B2.Visible = false;
        B3.Visible = false;
        GetUsers();
    }

    protected void B3_Click(object sender, EventArgs e)
    {

        TL = Convert.ToInt32(Derevo.SelectedNode.Value);
        string SQL = "DELETE FROM [dbo].[T_CompositeConsumer] WHERE [ConsumerID] = " + TL.ToString();
        AllFunctions.DoExec(SQL);
        Alarm.Text = "";
        //FillGrid(GetUser.SelectedValue.ToString());
        Alarm.Text = "";
        EditNode.Text = "";
        IsPlus1.Visible = false;
        B2.Visible = false;
        B3.Visible = false;
        GetUsers();
    }

    protected void B4_Click(object sender, EventArgs e)
    {
        string CLID = ConsumersList.SelectedValue;
        TL = Convert.ToInt32(Derevo.SelectedNode.Value);
        string II = "";
        if (IsPlus2.Checked)
        {
            II = "1";
        }
        else II = "0";
        string SQL = "INSERT INTO [dbo].[T_CompositeConsumer] ([ConsumerID],[ParentConsumer],[CanBeAttached],[OrderID]) VALUES ("
                    + CLID + ", " + TL.ToString() + ", " + II + ", " + OrderID.Text + ")";
        AllFunctions.DoExec(SQL);
        Alarm.Text = "";
        ConsumersList.DataSource = "";
        ConsumersList.Items.Clear();
        SecondaryList.DataSource = "";
        SecondaryList.Items.Clear();
        //FillGrid(GetUser.SelectedValue.ToString());
        Alarm.Text = "";
        EditNode.Text = "";
        IsPlus1.Visible = false;
        B4.Visible = false;
        B5.Visible = false;
        IsPlus2.Visible = false;
        OrderID.Visible = false;
        
        GetUsers();
    }

    protected void B5_Click(object sender, EventArgs e)
    {
        TL = Convert.ToInt32(Derevo.SelectedNode.Value);
        string CLID = SecondaryList.SelectedValue;
        string SQL = "DELETE FROM [dbo].[T_CompositeConsumer] WHERE [ConsumerID] = " + CLID + " AND [ParentConsumer] = " + TL;
        AllFunctions.DoExec(SQL);
        Alarm.Text = "";
        ConsumersList.DataSource = "";
        ConsumersList.Items.Clear();
        SecondaryList.DataSource = "";
        SecondaryList.Items.Clear();
        //FillGrid(GetUser.SelectedValue.ToString());
        Alarm.Text = "";
        EditNode.Text = "";
        IsPlus2.Visible = false;
        B4.Visible = false;
        B5.Visible = false;
        GetUsers();
    }

    protected void AddConsumers_Click(object sender, EventArgs e)
    {
        //string SQL = "";
        //int cit = 0;
        //for (int i = 0; i < ConsumersList.Items.Count; i++)
        //{
        //    if (ConsumersList.Items[i].Selected)
        //    {
        //        cit++;
        //    }
        //}
        //if ((Derevo.SelectedNode == null) || (cit == 0))
        //{
        //    return;
        //}


        //for (int i = 0; i < ConsumersList.Items.Count; i++)
        //{
        //    if (ConsumersList.Items[i].Selected)
        //    {
        //        SQL = "INSERT INTO T_PlantLevelsToConsumers (PlantLevelID, ConsumerID) VALUES ("
        //            + Derevo.SelectedValue.ToString() + ", "
        //            + ConsumersList.Items[i].Value.ToString() + ")" ;
        //        AllFunctions.DoExec(SQL);

        //    }
        //}

        //string sd = GetUser.SelectedValue.ToString();
        //FillGrid(sd);
        //GetConsumers(sd);
    }

    protected void NewAdd_Click(object sender, EventArgs e)
    {
        //string SQL = "INSERT INTO [dbo].[T_PlantLevels] (LevelName, UpLevelID, UserID) VALUES (N'"
        //    + NewNode.Text + "', NULL, " + GetUser.SelectedValue.ToString() + ")";

        //AllFunctions.DoExec(SQL);
        //FillGrid(GetUser.SelectedValue.ToString());
        //GetConsumers(GetUser.SelectedValue.ToString());
        //Alarm.Text = "";
        //EditNode.Text = "";
        //NewNode.Text = "";
    }

    private int LevelNo(string TTT)
    {
        int ln = 0;
        string SS = "";

        if (TTT == "0")
        {
            return 1;
        }

        SS = TTT.Substring(TTT.Length - 2);
        if (SS != "00")
        {
            ln = 4;
        }
        else
        {
            SS = TTT.Substring(TTT.Length - 4, 2);
            if (SS != "00")
            {
                ln = 3;
            }
            else
            {
            SS = TTT.Substring(0, 2);
                if (SS != "00")
                {
                    ln = 2;
                }
                else ln = 1;
            }
        }
        return ln;
    }

    protected void SecondaryList_SelectedIndexChanged(object sender, EventArgs e)
    {
        B5.Visible = true;
    }

    protected void ConsumersList_SelectedIndexChanged(object sender, EventArgs e)
    {
        IsPlus2.Visible = true;
        OrderID.Visible = true;
    }
}