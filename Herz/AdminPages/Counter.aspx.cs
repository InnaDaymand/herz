﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class AdminPages_Counters : SecuredPage
{
    string CounterID = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (GridMaster.SelectedIndex < 0)
            GridMaster.SelectedIndex = 0;
    }

    ////protected void FormViewDetails_DataBound(object sender, EventArgs e)
    ////{
    ////    GridView gv = new GridView();

    ////    try
    ////    {
    ////        ((DropDownList)this.FormViewDetails.FindControl("TypeComboBox")).SelectedValue = ((tbl_Counter)FormViewDetails.DataItem).sql_Type_ID.ToString();
    ////        ((DropDownList)this.FormViewDetails.FindControl("TariffComboBox")).SelectedValue = ((tbl_Counter)FormViewDetails.DataItem).sql_tariff_ID.ToString();
    ////        //((DropDownList)this.FormViewDetails.FindControl("ConsumerComboBox")).SelectedValue = ((tbl_Counter)FormViewDetails.DataItem).sql_Consumer_ID.ToString();

    ////        gv = (GridView)this.FormViewDetails.FindControl("Tariffes");

    ////        CounterID = ((Label)this.FormViewDetails.FindControl("sql_counter_IDLabel")).Text;
    ////        string SQL = "SELECT TC.FromDate, TC.TariffParam, TT.TariffName FROM T_Tariff AS TT INNER JOIN "
    ////                + "T_TariffCounters AS TC ON TT.TariffID = TC.TariffID WHERE TC.CounterID = " + CounterID;


    ////        DataClassesDataContext dc = new DataClassesDataContext();
    ////        string CS = dc.Connection.ConnectionString;
    ////        SqlConnection CN = new SqlConnection(CS);
    ////        CN.Open();
    ////        SqlCommand command = new SqlCommand(SQL, CN);
    ////        SqlDataAdapter adapter = new SqlDataAdapter();
    ////        adapter.SelectCommand = command;

    ////        DataTable te = new DataTable("Tariffs");
    ////        adapter.Fill(te);

    ////        gv.DataSource = te;
    ////        CN.Close();

    ////        gv.DataBind();

    ////        ((TextBox)this.FormViewDetails.FindControl("TextDateNew")).Text = DateTime.Now.ToShortDateString();

    ////    }
    ////    catch
    ////    {
    ////    }

    ////    try
    ////    {
    ////        if (((tbl_Counter)FormViewDetails.DataItem).SyncTime.HasValue)
    ////        {
    ////            ((TextBox)this.FormViewDetails.FindControl("txtDateFrom")).Text = ((tbl_Counter)FormViewDetails.DataItem).SyncTime.Value.ToString("yyyy-MM-dd");
    ////            ((eWorld.UI.TimePicker)(this.FormViewDetails.FindControl("TimePicker1"))).SelectedTime = ((tbl_Counter)FormViewDetails.DataItem).SyncTime.Value;
    ////        }
    ////    }
    ////    catch
    ////    {
    ////    }
    ////}

    ////protected void FormViewDetails_ItemUpdating(object sender, EventArgs e)
    ////{
    ////    if (e is FormViewUpdateEventArgs)
    ////    {
    ////        (e as FormViewUpdateEventArgs).NewValues.Add("sql_Type_ID", (this.FormViewDetails.FindControl("TypeComboBox") as DropDownList).SelectedValue);
    ////        //(e as FormViewUpdateEventArgs).NewValues.Add("sql_Consumer_ID", (this.FormViewDetails.FindControl("ConsumerComboBox") as DropDownList).SelectedValue);
    ////        (e as FormViewUpdateEventArgs).NewValues.Add("sql_tariff_ID", (this.FormViewDetails.FindControl("TariffComboBox") as DropDownList).SelectedValue);
    ////        if (((TextBox)this.FormViewDetails.FindControl("SerialTextBox")).Text == "")
    ////            (e as FormViewUpdateEventArgs).NewValues["sql_serialNumber"] = null;
    ////        if (((TextBox)this.FormViewDetails.FindControl("TariffCoefficientTextBox")).Text == "")
    ////            (e as FormViewUpdateEventArgs).NewValues["TariffCoefficient"] = null;
    ////        if (((TextBox)this.FormViewDetails.FindControl("txtDispCoef")).Text == "")
    ////            (e as FormViewUpdateEventArgs).NewValues["DispCoef"] = null;
    ////        if (((TextBox)this.FormViewDetails.FindControl("txtFieldVal")).Text == "")
    ////        {
    ////            (e as FormViewUpdateEventArgs).NewValues["SyncTime"] = null;
    ////            (e as FormViewUpdateEventArgs).NewValues["FieldValue"] = null;
    ////            (e as FormViewUpdateEventArgs).NewValues["SynsValue"] = null;
    ////        }
    ////        else
    ////        {
    ////            (e as FormViewUpdateEventArgs).NewValues["SyncTime"] = ((TextBox)this.FormViewDetails.FindControl("txtDateFrom")).Text + " " + ((eWorld.UI.TimePicker)(this.FormViewDetails.FindControl("TimePicker1"))).PostedTime;
    ////            //calc SysValue
    ////            using (DataClassesDataContext db = new DataClassesDataContext())
    ////            {
    ////                try
    ////                {
    ////                    var syncPoint = db.S_CounterValue_GetSyncPoint(int.Parse(((Label)this.FormViewDetails.FindControl("sql_counter_IDLabel")).Text), Convert.ToDateTime((e as FormViewUpdateEventArgs).NewValues["SyncTime"])).First();
    ////                    if (syncPoint.Diff > 1)
    ////                    {
    ////                        //IsValid=false;
    ////                        (e as FormViewUpdateEventArgs).Cancel = true;
    ////                        ((CustomValidator)this.FormViewDetails.FindControl("SyncValidator")).IsValid = false;
    ////                    }
    ////                    else
    ////                        (e as FormViewUpdateEventArgs).NewValues["SysValue"] = (int?)syncPoint.sql_Value;
    ////                }
    ////                catch
    ////                {
    ////                    (e as FormViewUpdateEventArgs).Cancel = true;
    ////                    ((CustomValidator)this.FormViewDetails.FindControl("SyncValidator")).IsValid = false;
    ////                }
    ////            }
    ////        }
    ////    }
    ////    else if (e is FormViewInsertEventArgs)
    ////    {
    ////        (e as FormViewInsertEventArgs).Values.Add("sql_Type_ID", (this.FormViewDetails.FindControl("TypeComboBox") as DropDownList).SelectedValue);
    ////        (e as FormViewInsertEventArgs).Values.Add("sql_Consumer_ID", (this.FormViewDetails.FindControl("ConsumerComboBox") as DropDownList).SelectedValue);
    ////        (e as FormViewInsertEventArgs).Values.Add("sql_tariff_ID", (this.FormViewDetails.FindControl("TariffComboBox") as DropDownList).SelectedValue);
    ////        if (((TextBox)this.FormViewDetails.FindControl("SerialTextBox")).Text == "")
    ////            (e as FormViewInsertEventArgs).Values["sql_serialNumber"] = null;
    ////        if (((TextBox)this.FormViewDetails.FindControl("TariffCoefficientTextBox")).Text == "")
    ////            (e as FormViewInsertEventArgs).Values["TariffCoefficient"] = null;
    ////        if (((TextBox)this.FormViewDetails.FindControl("txtDispCoef")).Text == "")
    ////            (e as FormViewInsertEventArgs).Values["txtDispCoef"] = null;
    ////        /*if (((TextBox)this.FormViewDetails.FindControl("txtFieldVal")).Text == "")
    ////        {
    ////            (e as FormViewInsertEventArgs).Values["SyncTime"] = null;
    ////            (e as FormViewInsertEventArgs).Values["FieldValue"] = null;
    ////            (e as FormViewInsertEventArgs).Values["SynsValue"] = null;
    ////        }*/
    ////    }
    ////}

    ////protected void FormViewDetails_ItemUpdated(object sender, EventArgs e)
    ////{
    ////    GridMaster.DataBind();
    ////}

    ////protected void FormViewDetails_ItemDeleted(object sender, FormViewDeletedEventArgs e)
    ////{
    ////    if (e.Exception != null)
    ////    {
    ////        Response.Write(e.Exception.Message);

    ////        e.ExceptionHandled = true;
    ////    }
    ////    GridMaster.DataBind();
    ////}

    ////protected void LinqDetails_Inserted(object sender, LinqDataSourceStatusEventArgs e)
    ////{
    ////    GridMaster.DataBind();
    ////    //FormViewDetails.DataBind();
    ////}

    protected void TariffAdd(Object sender, CommandEventArgs e)
    {

        string SQL = "INSERT INTO T_TariffCounters VALUES (" + RunNo.Text + ", "
        + TariffComboBox.SelectedValue.ToString() + ", "
        + "CONVERT(SMALLDATETIME, '" + (TextDateNew.Text + " " + TimeNew.PostedTime.ToString())+ "', 103), "
        +TariffCoefficientTextBox.Text + ")";

        AllFunctions.DoExec(SQL);

        /*

       //+ ((Label)this.FormViewDetails.FindControl("sql_counter_IDLabel")).Text + ", "
       //+ (this.FormViewDetails.FindControl("TariffComboBox") as DropDownList).SelectedValue.ToString() + ", "
       //+ ((TextBox)this.FormViewDetails.FindControl("TextDateNew")).Text + " " + ((eWorld.UI.TimePicker)(this.FormViewDetails.FindControl("TimeNew"))).PostedTime.ToString() + "', 103), "
       //+ ((TextBox)this.FormViewDetails.FindControl("TariffCoefficientTextBox")).Text + ")";

       INSERT INTO[dbo].[T_TariffCounters]
           ([CounterID]
           ,[TariffID]
           ,[FromDate]
           ,[TariffParam])

        DataClassesDataContext dc = new DataClassesDataContext();
        string CS = dc.Connection.ConnectionString;
        SqlConnection CN = new SqlConnection(CS);

        SqlCommand command = new SqlCommand(SQL, CN);
        command.Connection.Open();
        command.CommandTimeout = 0;
        command.ExecuteNonQuery();
       */

    }

    protected void DoFilter_Click(object sender, EventArgs e)
    {
        string SQL = "";
        string SS = ForFilter.Text;
        if (SS == "")
        {
            return;
        }
        int ir = Convert.ToInt32(GridFields.SelectedValue);
        if (ir == 0)
        {
            for (int i = 0; i < GridMaster.Rows.Count; i++)
            {
                GridMaster.Rows[i].Visible = (bool)(Convert.ToInt32(GridMaster.Rows[i].Cells[1].Text) <= Convert.ToInt32(SS));
            }
        }
        if (ir == 1)
        {
            for (int i = 0; i < GridMaster.Rows.Count; i++)
            {
                GridMaster.Rows[i].Visible = (bool)(Convert.ToInt32(GridMaster.Rows[i].Cells[1].Text) >= Convert.ToInt32(SS));
            }
        }
        if (ir == 2)
        {
            for (int i = 0; i < GridMaster.Rows.Count; i++)
            {
                GridMaster.Rows[i].Visible = (bool)(GridMaster.Rows[i].Cells[3].Text == SS);
            }
        }
        if (ir == 3)
        {
            SQL = "SELECT [sql_Consumer_name] FROM [dbo].[tbl_Consumers_0] WHERE [sql_Consumer_ID] = " + SS;
            SS = AllFunctions.GetScalar(SQL);
            for (int i = 0; i < GridMaster.Rows.Count; i++)
            {
                GridMaster.Rows[i].Visible = (bool)(GridMaster.Rows[i].Cells[3].Text == SS);
            }
        }
    }

    protected void NoFilter_Click(object sender, EventArgs e)
    {
        for (int i = 0; i < GridMaster.Rows.Count; i++)
        {
            GridMaster.Rows[i].Visible = true;
        }

    }

    protected void CounterTypes_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillConsumers();
    }

    protected void CleanIt_Click(object sender, EventArgs e)
    {
        RunNo.Text = "";
        CounterTypes.SelectedIndex = -1;
        ConsumersIDComboBox.DataSource = null;
        ConsumersIDComboBox.DataBind();
        IsActive.Checked = false;
        IsSplitted.Checked = false;

        TariffComboBox.SelectedIndex = -1;
        TariffCoefficientTextBox.Text = "";

        Tariffes.DataSource = null;
        Tariffes.DataBind();

    }

    protected void EraseIt_Click(object sender, EventArgs e)
    {

    }

    protected void SaveIt_Click(object sender, EventArgs e)
    {
        string SQL = "";

        if (RunNo.Text == "")
        {
            SQL = "SELECT MAX(sql_counter_ID) + 1 AS MM FROM tbl_Counters";
            RunNo.Text = AllFunctions.GetScalar(SQL);


            SQL = "INSERT INTO tbl_Counters (sql_counter_ID, sql_Consumer_ID, sql_serialNumber, sql_Type_ID, sql_active, sql_tariff_ID, sql_puls_value, "
                    + "IsSplitted, TariffCoefficient, SyncTime, FieldValue, SysValue, ShortName, DispCoef ) VALUES ("
                    + RunNo.Text + ", " + ConsumersIDComboBox.SelectedValue + ", 0, " + CounterTypes.SelectedValue.ToString() + ", " + Convert.ToByte(IsActive.Checked) + ", "
                    + TariffComboBox.SelectedValue.ToString() + ", 0, " + Convert.ToByte(IsSplitted.Checked) + ", " + TariffCoefficientTextBox.Text + ", NULL, NULL, NULL, '', 1)";

            AllFunctions.DoExec(SQL);
        }
        else
        {
            SQL = "Update tbl_Counters SET "
                + "sql_Consumer_ID = " + ConsumersIDComboBox.SelectedValue + ", "
                + "sql_Type_ID = " + CounterTypes.SelectedValue.ToString() + ", "
                + "sql_active = " + Convert.ToByte(IsActive.Checked) + ", "
                + "sql_tariff_ID = " + TariffComboBox.SelectedValue.ToString() + ", 0, "
                + "IsSplitted = " + Convert.ToByte(IsSplitted.Checked) + ", "
                + "TariffCoefficient = " + TariffCoefficientTextBox.Text + ""
                + "WHERE sql_counter_ID = " + RunNo.Text;

            AllFunctions.DoExec(SQL);
        }
    }

    protected void GridMaster_SelectedIndexChanged(object sender, EventArgs e)
    {
        CleanIt_Click(sender, e);
        string SQL = "";
        //int ConsID = 0;
        int CountID = 0;
        int RowInd = GridMaster.SelectedIndex;
        CountID = (int)GridMaster.SelectedDataKey.Value;
        AllFunctions.ConsID = CountID.ToString();
        string SS = "";
        SS = GridMaster.Rows[RowInd].Cells[2].Text;

        SS = ClearString(SS);
        mvTitle.Text = "מונה של " + SS;
        SS = GridMaster.Rows[RowInd].Cells[1].Text;
        SS = ClearString(SS);

        RunNo.Text = CountID.ToString();

        SQL = "SELECT FromCounterType FROM tbl_Consumers_0 INNER JOIN T_ConsumerType ON ConsumerType = ConTpID WHERE sql_Consumer_ID = "
            + "(SELECT sql_Consumer_ID FROM tbl_Counters WHERE sql_counter_ID = " + CountID.ToString() + ")" ;
        SS = AllFunctions.GetScalar(SQL);
        CounterTypes.SelectedValue = SS;
        FillConsumers();
        SQL = "SELECT sql_Consumer_ID FROM tbl_Counters WHERE sql_counter_ID = " + CountID.ToString() ;
        SS = AllFunctions.GetScalar(SQL);
        ConsumersIDComboBox.SelectedValue = SS;

        SQL = "SELECT sql_active FROM tbl_Counters WHERE sql_counter_ID = " + CountID.ToString() ;
        SS = AllFunctions.GetScalar(SQL);
        IsActive.Checked = (bool)(SS == "True");

        SQL = "SELECT IsSplitted FROM tbl_Counters WHERE sql_counter_ID = " + CountID.ToString() ;
        SS = AllFunctions.GetScalar(SQL);
        IsSplitted.Checked = (bool)(SS == "True");

        SQL = "SELECT TC.FromDate, TC.TariffParam, TT.TariffName FROM T_Tariff AS TT INNER JOIN T_TariffCounters AS TC ON TT.TariffID = TC.TariffID WHERE TC.CounterID = " + CountID.ToString();
        Tariffes.DataSource = AllFunctions.Populate(SQL);
        Tariffes.DataBind();

        SQL = "SELECT sql_tariff_ID FROM tbl_Counters WHERE (sql_counter_ID = " + CountID.ToString() + ")";
        SS = AllFunctions.GetScalar(SQL);
        TariffComboBox.SelectedValue = SS;

        SQL = "SELECT  TariffCoefficient FROM tbl_Counters WHERE (sql_counter_ID = " + CountID.ToString() + ")";
        TariffCoefficientTextBox.Text = AllFunctions.GetScalar(SQL);


        /*
        RunNo                   Label
        CounterTypes            Drop Down List
        ConsumersIDComboBox     Drop Down List
        IsActive                Check Box
        IsSplitted              Check Box

        Tariffes                Grid View
        */

    }

    private string ClearString(string ST)
    {
        if (ST.IndexOf("&nbsp;") >= 0)
        {
            ST = ST.Replace("&nbsp;", "");
        }
        if (ST.IndexOf("&quot;") > 0)
        {
            ST = ST.Replace("&quot;", "''");
        }
        return ST;
    }

    private void FillConsumers()
    {
        ConsumersIDComboBox.DataSource = null;
        string SS = CounterTypes.SelectedValue;
        string SQL = "SELECT sql_Consumer_ID, sql_Consumer_name FROM tbl_Consumers_0 WHERE ConsumerType IN (SELECT [ConTpID] FROM [dbo].[T_ConsumerType] "
            + " WHERE [FromCounterType] = " + SS + ")";
        ConsumersIDComboBox.DataSource = AllFunctions.Populate(SQL);
        ConsumersIDComboBox.DataTextField = "sql_Consumer_name";
        ConsumersIDComboBox.DataValueField = "sql_Consumer_ID";
        ConsumersIDComboBox.DataBind();
    }
}
