﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminPages/AdminPage.master" AutoEventWireup="true" CodeFile="Counters.aspx.cs" Inherits="AdminPages_Counters" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register assembly="eWorld.UI, Version=2.0.6.2393, Culture=neutral, PublicKeyToken=24d65337282035f2" namespace="eWorld.UI" tagprefix="ew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .auto-style1 {
            margin-right: 0px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</ajaxToolkit:ToolkitScriptManager>
        <table class="LayoutTable">
        <tr>
            <th class="HeaderStyle">מונים</th>
        </tr>
        <tr>
            <td align="left" >
                <a href="../Help/HCounters.aspx" target="_blank" style="color:#5d7b9d;"><img src="../images/help.gif" style="border:none" />עזרה</a>
            </td>
        </tr>
        <tr>
            <td>
                <br />
                <asp:TextBox runat="server" ID="ForFilter" CssClass="auto-style1" />
                <asp:DropDownList runat="server" ID="GridFields">
                    <asp:ListItem Value="0" Text="Less than ID" />
                    <asp:ListItem Text="More than ID" Value="1" />
                    <asp:ListItem Text="שם צרכן" Value="2" />
                    <asp:ListItem Text="מספר צרכן" Value="3" />
                </asp:DropDownList>
                <asp:Button Text="מסנן" runat="server" ID="DoFilter" OnClick="DoFilter_Click" />
                <asp:Button Text="בטל מסנן" runat="server" ID="NoFilter" OnClick="NoFilter_Click" />
                <br />
                <table class="LayoutTable">
                    <tr>
                        <td valign="top">
<%--                            <asp:LinqDataSource ID="LinqMaster" runat="server" 
                                ContextTypeName="DataClassesDataContext" 
                                Select="new (sql_counter_ID, tbl_Consumers_0.sql_Consumer_name as ConsumerName, tbl_CounterType.sql_description as CounterType, T_Tariff.TariffName as TariffName)" 
                                TableName="tbl_Counters">
                            </asp:LinqDataSource>--%>
                            <asp:SqlDataSource runat="server" connectionstring="<%$ ConnectionStrings:Money_DisplayConnectionString3 %>" ID="LinqMaster" 
                                SelectCommand="SELECT [sql_counter_ID], (SELECT [sql_Consumer_name] FROM [dbo].[tbl_Consumers_0] WHERE [sql_Consumer_ID] = [tbl_Counters].sql_Consumer_ID) as ConsumerName,
                                    (SELECT [sql_description] FROM [dbo].[tbl_CounterTypes] WHERE [sql_Type_ID] = [tbl_Counters].sql_Type_ID ) as CounterType, 
                                    (SELECT [TariffName] FROM [dbo].[T_Tariff] WHERE [TariffID] = [tbl_Counters].sql_tariff_ID) as TariffName FROM [dbo].[tbl_Counters]" >
                                </asp:SqlDataSource>
                            <asp:sqldatasource id="tCons"
                                selectcommand="SELECT sql_Consumer_ID, sql_Consumer_name, sql_clientID, (SELECT Name FROM dbo.T_Client WHERE ID = sql_clientID) as client, 
                                OrderNo, sql_Consumer_active, BeginDate, ConsumerType, (SELECT ConTpName FROM [T_ConsumerType] WHERE [ConTpID] = ConsumerType) AS cType,
                                sql_Consumer_max_over_cons, EngName, RusName FROM tbl_Consumers_0"
                                connectionstring="<%$ ConnectionStrings:Money_DisplayConnectionString3 %>" 
                                runat="server"/>
                            <asp:GridView ID="GridMaster" runat="server" Width="100%" AutoGenerateColumns="False" 
                                DataKeyNames="sql_counter_ID" AllowSorting="True" CaptionAlign="Top" DataSourceID="LinqMaster" >
<%--                                PageSize="30" --%>
                                
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True" SelectText="בחר..." />
                                    <asp:BoundField DataField="sql_counter_ID" HeaderText="ID" 
                                        SortExpression="sql_counter_ID">
                                        <ItemStyle Width="30px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="CounterType" HeaderText="סוג" />
                                    <asp:BoundField DataField="ConsumerName" HeaderText="צרכן" 
                                        SortExpression="ConsumerName" />
                                    <asp:BoundField DataField="TariffName" HeaderText="תעריף" />
                                </Columns>
                                <HeaderStyle CssClass="DetailsFormViewHeader"></HeaderStyle>
                                <RowStyle ForeColor="#333333"></RowStyle>
                                <AlternatingRowStyle BackColor="#f7f6f3"></AlternatingRowStyle>
                                <SelectedRowStyle BackColor="#E2DED6" ForeColor="#284775"></SelectedRowStyle>
                            </asp:GridView>
                        </td>
                        <td valign="top" width="50%">
                            <asp:LinqDataSource ID="LinqDetails" runat="server" 
                                ContextTypeName="DataClassesDataContext" TableName="tbl_Counters" 
                                Where="sql_counter_ID == @sql_counter_ID" 
                                EnableDelete="True" 
                                EnableInsert="True" 
                                EnableUpdate="True" 
                                OnInserted="LinqDetails_Inserted">
                                <WhereParameters>
                                    <asp:ControlParameter ControlID="GridMaster" DefaultValue="0" 
                                        Name="sql_counter_ID" PropertyName="SelectedValue" Type="Int32" />
                                </WhereParameters>
                            </asp:LinqDataSource>
                            <asp:LinqDataSource ID="LinqTypes" runat="server" 
                                ContextTypeName="DataClassesDataContext" 
                                Select="new (sql_Type_ID, sql_description)" 
                                TableName="tbl_CounterTypes">
                            </asp:LinqDataSource>
                            <asp:LinqDataSource ID="LinqConsumers" runat="server" 
                                ContextTypeName="DataClassesDataContext" 
                                Select="new (sql_Consumer_ID, sql_Consumer_name as ConsumerName)" 
                                TableName="tbl_Consumers_0s">
                            </asp:LinqDataSource>
                            <asp:LinqDataSource ID="LinqTariffs" runat="server" 
                                ContextTypeName="DataClassesDataContext" 
                                Select="new (TariffID, TariffName)" 
                                TableName="T_Tariffs">
                            </asp:LinqDataSource>
                            <asp:FormView ID="FormViewDetails" runat="server" Width="100%" 
                                DataKeyNames="sql_Consumer_ID" 
                                DataSourceID="LinqDetails" 
                                HeaderText="פרטים" 
                                HeaderStyle-CssClass="DetailsFormViewHeader"
                                CssClass="DetailsFormView" 
                                OnDataBound="FormViewDetails_DataBound"
                                OnItemInserting="FormViewDetails_ItemUpdating"
                                OnItemUpdating="FormViewDetails_ItemUpdating"
                                OnItemUpdated="FormViewDetails_ItemUpdated"
                                OnItemDeleted="FormViewDetails_ItemDeleted" OnPageIndexChanging="FormViewDetails_PageIndexChanging" >
                                <EditItemTemplate>
                                    <table class="DetailsFormViewLayout">
                                        <tr>
                                            <td>
                                                מס' מונה 
                                            </td>
                                            <td>
                                                <asp:Label ID="sql_counter_IDLabel" runat="server" 
                                                    Text='<%# Eval("sql_counter_ID") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                שם קצר
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtShortName" runat="server" MaxLength="10"
                                                    Text='<%# Bind("ShortName") %>' />
                                                (עד 10 תווים)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                סוג
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="TypeComboBox" runat="server" Width="150px"
                                                    DataSourceID="LinqTypes" 
                                                    DataTextField="sql_description" 
                                                    DataValueField="sql_Type_ID" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                צרכן
                                            </td>
                                            <td>
                                                <asp:Label ID="ConsumerNameLabel" runat="server" 
                                                    Text='<%# Eval("tbl_Consumers_0.sql_Consumer_name") %>' />
                                                <%--<asp:DropDownList ID="ConsumerComboBox" runat="server"
                                                    DataSourceID="LinqConsumers" 
                                                    DataTextField="ConsumerName" 
                                                    DataValueField="sql_Consumer_ID" />--%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                מס' סידורי
                                            </td>
                                            <td>
                                                <asp:TextBox ID="SerialTextBox" runat="server" 
                                                    Text='<%# Bind("sql_serialNumber") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                פעימה
                                            </td>
                                            <td>
                                                <asp:TextBox ID="PulseTextBox" runat="server" 
                                                    Text='<%# Bind("sql_puls_value") %>' />
                                                <asp:RequiredFieldValidator runat="server" ControlToValidate="PulseTextBox" 
                                                    ErrorMessage="הכנס פעימה" Display="Dynamic" Text="*" 
                                                    ValidationGroup="AllValidators" ID="RequiredFieldValidator1"></asp:RequiredFieldValidator>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" 
                                                    FilterType="Numbers,Custom" ValidChars=".,E,-"
                                                    runat="server" Enabled="True" TargetControlID="PulseTextBox">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                מכפיל תצוגה
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtDispCoef" runat="server" 
                                                    Text='<%# Bind("DispCoef") %>' />
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" 
                                                    FilterType="Numbers,Custom" ValidChars="."
                                                    runat="server" Enabled="True" TargetControlID="txtDispCoef">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                פעיל
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="ActiveCheckBox" runat="server" 
                                                    Checked='<%# Bind("sql_active") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                מפוצל
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="SplittedCheckBox" runat="server" 
                                                    Checked='<%# Bind("IsSplitted") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                קריאה עדכנית
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFieldVal" runat="server" 
                                                    Text='<%# Bind("FieldValue") %>' />
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" 
                                                    FilterType="Numbers"
                                                    runat="server" Enabled="True" TargetControlID="txtFieldVal">
                                                </cc1:FilteredTextBoxExtender>
                                                <asp:CustomValidator ID="SyncValidator" runat="server" Display="Dynamic"
                                                    Text="*" ErrorMessage="אי אפשר לסנכרן קריאות. להמשך יש לנקות שדה 'קריאה עדכנית'"
                                                    EnableClientScript="false" ValidationGroup="AllValidators" >
                                                </asp:CustomValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                שעת סינכרון
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtDateFrom" runat="server" Width="100px"></asp:TextBox>
                                                <asp:Image ID="Image1" runat="server" ImageUrl="~/images/SmallCalendar.gif" />
                                                <cc1:CalendarExtender ID="txtDateFrom_CalendarExtender" runat="server" 
                                                    Enabled="True" TargetControlID="txtDateFrom" PopupButtonID="Image1" Format="yyyy-MM-dd">
                                                </cc1:CalendarExtender>
                                                <ew:TimePicker ID="TimePicker1" runat="server" Width="60px" 
                                                    MinuteInterval="OneHour" NumberOfColumns="3" PopupLocation="Bottom" 
                                                    PopupWidth="120px" Scrollable="False" ImageUrl="~/images/timepicker.gif" >
                                                    <ButtonStyle BorderStyle="None" Width="25px" />
                                                </ew:TimePicker>
                                            </td>
                                        </tr>
                                        <%--NEW tariffes--%>

                                        <tr>
                                        <td>תעריף
                                        </td>
                                        <td>
                                            <asp:GridView runat="server" ID="Tariffes" AutoGenerateColumns="False" AlternatingRowStyle-Wrap="False">
                                                <Columns>
                                                <asp:BoundField DataField="FromDate" HeaderText="תאריך" ShowHeader="true" ReadOnly="true" />
                                                <asp:BoundField DataField="TariffName" HeaderText="שם תעריף" ShowHeader="true" ReadOnly="true" />
                                                <asp:BoundField DataField="TariffParam" HeaderText="מקדם תעריף" ShowHeader="true" ReadOnly="true" />
                                                </Columns>
                                            </asp:GridView>
                                        </td>
                                        </tr>
                                        <tr bgcolor="Silver">
                                            <td>
                                                תעריף להוספה
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="TariffComboBox" runat="server"
                                                    DataSourceID="LinqTariffs" 
                                                    DataTextField="TariffName" 
                                                    DataValueField="TariffID" />
                                            </td>
                                        </tr>
                                         <tr bgcolor="Silver">
                                            <td>
                                                מקדם תעריף
                                            </td>
                                            <td>
                                                <asp:TextBox ID="TariffCoefficientTextBox" runat="server" 
                                                    Text='<%# Bind("TariffCoefficient") %>' />
<%--                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" 
                                                    FilterType="Numbers,Custom" ValidChars="."
                                                    runat="server" Enabled="True" TargetControlID="TariffCoefficientTextBox">
                                                </cc1:FilteredTextBoxExtender>
--%>
                                            </td>
                                         </tr>
                                        <tr bgcolor="Silver">
                                            <td>
                                                תאריך שינוים
                                            </td>
                                            <td>
                                                <asp:TextBox ID="TextDateNew" runat="server" Width="100px" Text=""></asp:TextBox>
                                                <asp:Image ID="Image2" runat="server" ImageUrl="~/images/SmallCalendar.gif" />
                                                <cc1:CalendarExtender ID="CalendarExtender2" runat="server" 
                                                    Enabled="True" TargetControlID="TextDateNew" PopupButtonID="Image2" Format="yyyy-MM-dd">
                                                </cc1:CalendarExtender>
                                                <ew:TimePicker ID="TimeNew" runat="server" Width="60px" 
                                                    MinuteInterval="OneHour" NumberOfColumns="3" PopupLocation="Bottom" 
                                                    PopupWidth="120px" Scrollable="False" ImageUrl="~/images/timepicker.gif" >
                                                    <ButtonStyle BorderStyle="None" Width="25px" />
                                                </ew:TimePicker>
                                            </td>
                                       </tr>
                                       <tr bgcolor="Silver">
                                        <td colspan="2" align="center">
                                            <asp:Button runat="server" ID="AddToTariff" Text="הוסף לרשימה" OnCommand="TariffAdd" CommandName="Tariffes" />
                                        </td>
                                       </tr>
                                  </table>
                                    <br />
                                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" 
                                        CommandName="Update" Text="שמור" ValidationGroup="AllValidators" />
                                    &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" 
                                        CausesValidation="False" CommandName="Cancel" Text="בטל" />
                                </EditItemTemplate>
                                <InsertItemTemplate>
                                    <table class="DetailsFormViewLayout">
                                        <tr>
                                            <td>
                                                מס' מונה 
                                            </td>
                                            <td>
                                                <asp:TextBox ID="sql_counter_IDText" runat="server" 
                                                    Text='<%# Bind("sql_counter_ID") %>' />
                                                <asp:RequiredFieldValidator runat="server" ControlToValidate="sql_counter_IDText" 
                                                    ErrorMessage="הכנס ID" Display="Dynamic" Text="*" 
                                                    ValidationGroup="AllValidators" ID="RequiredFieldValidator0"></asp:RequiredFieldValidator>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender0" 
                                                    FilterType="Numbers"
                                                    runat="server" Enabled="True" TargetControlID="sql_counter_IDText">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                שם קצר
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtShortName" runat="server" MaxLength="10"
                                                    Text='<%# Bind("ShortName") %>' />
                                                (עד 10 תווים)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                סוג
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="TypeComboBox" runat="server" Width="150px"
                                                    DataSourceID="LinqTypes" 
                                                    DataTextField="sql_description" 
                                                    DataValueField="sql_Type_ID" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                צרכן
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ConsumerComboBox" runat="server"
                                                    DataSourceID="LinqConsumers" 
                                                    DataTextField="ConsumerName" 
                                                    DataValueField="sql_Consumer_ID" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                מס' סידורי
                                            </td>
                                            <td>
                                                <asp:TextBox ID="SerialTextBox" runat="server" 
                                                    Text='<%# Bind("sql_serialNumber") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                תעריף
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="TariffComboBox" runat="server"
                                                    DataSourceID="LinqTariffs" 
                                                    DataTextField="TariffName" 
                                                    DataValueField="TariffID" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                פעימה
                                            </td>
                                            <td>
                                                <asp:TextBox ID="PulseTextBox" runat="server" 
                                                    Text='<%# Bind("sql_puls_value") %>' />
                                                <asp:RequiredFieldValidator runat="server" ControlToValidate="PulseTextBox" 
                                                    ErrorMessage="הכנס פעימה" Display="Dynamic" Text="*" 
                                                    ValidationGroup="AllValidators" ID="RequiredFieldValidator1"></asp:RequiredFieldValidator>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" 
                                                    FilterType="Numbers,Custom" ValidChars="."
                                                    runat="server" Enabled="True" TargetControlID="PulseTextBox">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                מכפיל תצוגה
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtDispCoef" runat="server" 
                                                    Text='<%# Bind("DispCoef") %>' />
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" 
                                                    FilterType="Numbers,Custom" ValidChars="."
                                                    runat="server" Enabled="True" TargetControlID="txtDispCoef">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                פעיל
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="ActiveCheckBox" runat="server" 
                                                    Checked='<%# Bind("sql_active") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                מפוצל
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="SplittedCheckBox" runat="server" 
                                                    Checked='<%# Bind("IsSplitted") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                מקדם תעריף
                                            </td>
                                            <td>
                                                <asp:TextBox ID="TariffCoefficientTextBox" runat="server" 
                                                    Text='<%# Bind("TariffCoefficient") %>' />
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" 
                                                    FilterType="Numbers,Custom" ValidChars="."
                                                    runat="server" Enabled="True" TargetControlID="TariffCoefficientTextBox">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
<%--                                        <tr>
                                            <td>
                                                ערך מעודכן
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFieldVal" runat="server" 
                                                    Text='<%# Bind("TariffCoefficient") %>' />
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" 
                                                    FilterType="Numbers"
                                                    runat="server" Enabled="True" TargetControlID="TariffCoefficientTextBox">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                שעת סינכרון
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtDateFrom" runat="server" Width="100px" ></asp:TextBox>
                                                <asp:Image ID="Image1" runat="server" ImageUrl="~/images/SmallCalendar.gif" />
                                                <cc1:CalendarExtender ID="txtDateFrom_CalendarExtender" runat="server" 
                                                    Enabled="True" TargetControlID="txtDateFrom" PopupButtonID="Image1" Format="yyyy-MM-dd">
                                                </cc1:CalendarExtender>
                                                <ew:TimePicker ID="TimePicker1" runat="server" Width="60px" 
                                                    MinuteInterval="OneHour" NumberOfColumns="3" PopupLocation="Bottom" 
                                                    PopupWidth="120px" Scrollable="False" ImageUrl="~/images/timepicker.gif" >
                                                    <ButtonStyle BorderStyle="None" Width="25px" />
                                                </ew:TimePicker>
                                            </td>--%>
                                    </table>
                                    <br />
                                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" 
                                        CommandName="Insert" Text="שמור" ValidationGroup="AllValidators" />
                                    &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" 
                                        CausesValidation="False" CommandName="Cancel" Text="בטל" />
                                </InsertItemTemplate>
                                <ItemTemplate>
                                    <table class="DetailsFormViewLayout" >
                                        <tr>
                                            <td style="width:150px">
                                                מס' מונה 
                                            </td>
                                            <td>
                                                <asp:Label ID="sql_counter_IDLabel" runat="server" 
                                                    Text='<%# Eval("sql_counter_ID") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                שם קצר 
                                            </td>
                                            <td>
                                                <asp:Label ID="Label1" runat="server" 
                                                    Text='<%# Eval("ShortName") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                סוג
                                            </td>
                                            <td>
                                                <asp:Label ID="CounterTypeLabel" runat="server" 
                                                    Text='<%# Eval("tbl_CounterType.sql_description") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                צרכן
                                            </td>
                                            <td>
                                                <asp:Label ID="ConsumerNameLabel" runat="server" 
                                                    Text='<%# Eval("tbl_Consumers_0.sql_Consumer_name") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                מס' סידורי
                                            </td>
                                            <td>
                                                <asp:Label ID="sql_serialNumberLabel" runat="server" 
                                                    Text='<%# Eval("sql_serialNumber") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                פעימה
                                            </td>
                                            <td>
                                                <asp:Label ID="sql_puls_valueLabel" runat="server" 
                                                    Text='<%# Bind("sql_puls_value") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                מכפיל תצוגה
                                            </td>
                                            <td>
                                                <asp:Label ID="lblDispCoef" runat="server" 
                                                    Text='<%# Bind("DispCoef") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                פעיל
                                            </td>
                                            <td>
                                                <asp:Label ID="sql_activeLabel" runat="server" 
                                                    Text='<%# Eval("sql_active") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                מפוצל
                                            </td>
                                            <td>
                                                <asp:Label ID="IsSplittedLabel" runat="server" 
                                                    Text='<%# Eval("IsSplitted") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                מקדם תעריף
                                            </td>
                                            <td>
                                                <asp:Label ID="TariffCoefficient" runat="server" 
                                                    Text='<%# Eval("TariffCoefficient") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                תעריף
                                            </td>
                                            <td>
                                                <asp:Label ID="TariffNameLabel" runat="server" 
                                                    Text='<%# Eval("T_Tariff.TariffName") %>' />
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                    <asp:LinkButton ID="btnNew" runat="server" Text="חדש" CommandName="New" />&nbsp;
                                    <asp:LinkButton ID="btnEdit" runat="server" Text="עריכה" CommandName="Edit" />&nbsp;
                                    <asp:LinkButton ID="btnDelete" runat="server" Text="מחק" CommandName="Delete" OnClientClick="return confirm('are you sure?');" />
                                </ItemTemplate>
                            </asp:FormView>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="AllValidators" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

