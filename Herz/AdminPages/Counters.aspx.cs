﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class AdminPages_Counters : SecuredPage
{
    string CounterID = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (GridMaster.SelectedIndex < 0)
            GridMaster.SelectedIndex = 0;
    }

    protected void FormViewDetails_DataBound(object sender, EventArgs e)
    {
        GridView gv = new GridView();

        try
        {
            ((DropDownList)this.FormViewDetails.FindControl("TypeComboBox")).SelectedValue = ((tbl_Counter)FormViewDetails.DataItem).sql_Type_ID.ToString();
            ((DropDownList)this.FormViewDetails.FindControl("TariffComboBox")).SelectedValue = ((tbl_Counter)FormViewDetails.DataItem).sql_tariff_ID.ToString();
            //((DropDownList)this.FormViewDetails.FindControl("ConsumerComboBox")).SelectedValue = ((tbl_Counter)FormViewDetails.DataItem).sql_Consumer_ID.ToString();

            gv = (GridView)this.FormViewDetails.FindControl("Tariffes");

            CounterID = ((Label)this.FormViewDetails.FindControl("sql_counter_IDLabel")).Text;
            string SQL = "SELECT TC.FromDate, TC.TariffParam, TT.TariffName FROM T_Tariff AS TT INNER JOIN "
                    + "T_TariffCounters AS TC ON TT.TariffID = TC.TariffID WHERE TC.CounterID = " + CounterID;


            DataClassesDataContext dc = new DataClassesDataContext();
            string CS = dc.Connection.ConnectionString;
            SqlConnection CN = new SqlConnection(CS);
            CN.Open();
            SqlCommand command = new SqlCommand(SQL, CN);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = command;

            DataTable te = new DataTable("Tariffs");
            adapter.Fill(te);

            gv.DataSource = te;
            CN.Close();

            gv.DataBind();

            ((TextBox)this.FormViewDetails.FindControl("TextDateNew")).Text = DateTime.Now.ToShortDateString();

        }
        catch
        {
        }

        try
        {
            if (((tbl_Counter)FormViewDetails.DataItem).SyncTime.HasValue)
            {
                ((TextBox)this.FormViewDetails.FindControl("txtDateFrom")).Text = ((tbl_Counter)FormViewDetails.DataItem).SyncTime.Value.ToString("yyyy-MM-dd");
                ((eWorld.UI.TimePicker)(this.FormViewDetails.FindControl("TimePicker1"))).SelectedTime = ((tbl_Counter)FormViewDetails.DataItem).SyncTime.Value;
            }
        }
        catch
        {
        }
    }

    protected void FormViewDetails_ItemUpdating(object sender, EventArgs e)
    {
        if (e is FormViewUpdateEventArgs)
        {
            (e as FormViewUpdateEventArgs).NewValues.Add("sql_Type_ID", (this.FormViewDetails.FindControl("TypeComboBox") as DropDownList).SelectedValue);
            //(e as FormViewUpdateEventArgs).NewValues.Add("sql_Consumer_ID", (this.FormViewDetails.FindControl("ConsumerComboBox") as DropDownList).SelectedValue);
            (e as FormViewUpdateEventArgs).NewValues.Add("sql_tariff_ID", (this.FormViewDetails.FindControl("TariffComboBox") as DropDownList).SelectedValue);
            if (((TextBox)this.FormViewDetails.FindControl("SerialTextBox")).Text == "")
                (e as FormViewUpdateEventArgs).NewValues["sql_serialNumber"] = null;
            if (((TextBox)this.FormViewDetails.FindControl("TariffCoefficientTextBox")).Text == "")
                (e as FormViewUpdateEventArgs).NewValues["TariffCoefficient"] = null;
            if (((TextBox)this.FormViewDetails.FindControl("txtDispCoef")).Text == "")
                (e as FormViewUpdateEventArgs).NewValues["DispCoef"] = null;
            if (((TextBox)this.FormViewDetails.FindControl("txtFieldVal")).Text == "")
            {
                (e as FormViewUpdateEventArgs).NewValues["SyncTime"] = null;
                (e as FormViewUpdateEventArgs).NewValues["FieldValue"] = null;
                (e as FormViewUpdateEventArgs).NewValues["SynsValue"] = null;
            }
            else
            {
                (e as FormViewUpdateEventArgs).NewValues["SyncTime"] = ((TextBox)this.FormViewDetails.FindControl("txtDateFrom")).Text + " " + ((eWorld.UI.TimePicker)(this.FormViewDetails.FindControl("TimePicker1"))).PostedTime;
                //calc SysValue
                using (DataClassesDataContext db = new DataClassesDataContext())
                {
                    try
                    {
                        var syncPoint = db.S_CounterValue_GetSyncPoint(int.Parse(((Label)this.FormViewDetails.FindControl("sql_counter_IDLabel")).Text), Convert.ToDateTime((e as FormViewUpdateEventArgs).NewValues["SyncTime"])).First();
                        if (syncPoint.Diff > 1)
                        {
                            //IsValid=false;
                            (e as FormViewUpdateEventArgs).Cancel = true;
                            ((CustomValidator)this.FormViewDetails.FindControl("SyncValidator")).IsValid = false;
                        }
                        else
                            (e as FormViewUpdateEventArgs).NewValues["SysValue"] = (int?)syncPoint.sql_Value;
                    }
                    catch 
                    { 
                        (e as FormViewUpdateEventArgs).Cancel = true;
                        ((CustomValidator)this.FormViewDetails.FindControl("SyncValidator")).IsValid = false;
                    }
                }
            }
        }
        else if (e is FormViewInsertEventArgs)
        {
            (e as FormViewInsertEventArgs).Values.Add("sql_Type_ID", (this.FormViewDetails.FindControl("TypeComboBox") as DropDownList).SelectedValue);
            (e as FormViewInsertEventArgs).Values.Add("sql_Consumer_ID", (this.FormViewDetails.FindControl("ConsumerComboBox") as DropDownList).SelectedValue);
            (e as FormViewInsertEventArgs).Values.Add("sql_tariff_ID", (this.FormViewDetails.FindControl("TariffComboBox") as DropDownList).SelectedValue);
            if (((TextBox)this.FormViewDetails.FindControl("SerialTextBox")).Text == "")
                (e as FormViewInsertEventArgs).Values["sql_serialNumber"] = null;
            if (((TextBox)this.FormViewDetails.FindControl("TariffCoefficientTextBox")).Text == "")
                (e as FormViewInsertEventArgs).Values["TariffCoefficient"] = null;
            if (((TextBox)this.FormViewDetails.FindControl("txtDispCoef")).Text == "")
                (e as FormViewInsertEventArgs).Values["txtDispCoef"] = null;
            /*if (((TextBox)this.FormViewDetails.FindControl("txtFieldVal")).Text == "")
            {
                (e as FormViewInsertEventArgs).Values["SyncTime"] = null;
                (e as FormViewInsertEventArgs).Values["FieldValue"] = null;
                (e as FormViewInsertEventArgs).Values["SynsValue"] = null;
            }*/
        }
    }

    protected void FormViewDetails_ItemUpdated(object sender, EventArgs e)
    {
        GridMaster.DataBind();
    }

    protected void FormViewDetails_ItemDeleted(object sender, FormViewDeletedEventArgs e)
    {
        if (e.Exception != null)
        {
            Response.Write(e.Exception.Message);

            e.ExceptionHandled = true;
        }
        GridMaster.DataBind();
    }

    protected void LinqDetails_Inserted(object sender, LinqDataSourceStatusEventArgs e)
    {
        GridMaster.DataBind();
        FormViewDetails.DataBind();
    }

    protected void TariffAdd(Object sender, CommandEventArgs e)
    {

        string SQL = "INSERT INTO T_TariffCounters VALUES (" + ((Label)this.FormViewDetails.FindControl("sql_counter_IDLabel")).Text + ", "
        + (this.FormViewDetails.FindControl("TariffComboBox") as DropDownList).SelectedValue.ToString() + ", "
        + "CONVERT(SMALLDATETIME, '"
        + ((TextBox)this.FormViewDetails.FindControl("TextDateNew")).Text + " " + ((eWorld.UI.TimePicker)(this.FormViewDetails.FindControl("TimeNew"))).PostedTime.ToString()
        + "', 103), " + ((TextBox)this.FormViewDetails.FindControl("TariffCoefficientTextBox")).Text + ")";

        DataClassesDataContext dc = new DataClassesDataContext();
        string CS = dc.Connection.ConnectionString;
        SqlConnection CN = new SqlConnection(CS);

        SqlCommand command = new SqlCommand(SQL, CN);
        command.Connection.Open();
        command.CommandTimeout = 0;
        command.ExecuteNonQuery();



    }

    protected void FormViewDetails_PageIndexChanging(object sender, FormViewPageEventArgs e)
    {

    }

    protected void DoFilter_Click(object sender, EventArgs e)
    {
        string SQL = "";
        string SS = ForFilter.Text;
        if (SS == "")
        {
            return;
        }
        int ir = Convert.ToInt32(GridFields.SelectedValue);
        if (ir == 0)
        {
            for (int i = 0; i < GridMaster.Rows.Count; i++)
            {
                GridMaster.Rows[i].Visible = (bool)(Convert.ToInt32(GridMaster.Rows[i].Cells[1].Text) <= Convert.ToInt32(SS));
            }
        }
        if (ir == 1)
        {
            for (int i = 0; i < GridMaster.Rows.Count; i++)
            {
                GridMaster.Rows[i].Visible = (bool)(Convert.ToInt32(GridMaster.Rows[i].Cells[1].Text) >= Convert.ToInt32(SS));
            }
        }
        if (ir == 2)
        {
            for (int i = 0; i < GridMaster.Rows.Count; i++)
            {
                GridMaster.Rows[i].Visible = (bool)(GridMaster.Rows[i].Cells[3].Text == SS);
            }
        }
        if (ir == 3)
        {
            SQL = "SELECT [sql_Consumer_name] FROM [dbo].[tbl_Consumers_0] WHERE [sql_Consumer_ID] = " + SS;
            SS = AllFunctions.GetScalar(SQL);
            for (int i = 0; i < GridMaster.Rows.Count; i++)
            {
                GridMaster.Rows[i].Visible = (bool)(GridMaster.Rows[i].Cells[3].Text == SS);
            }
        }
    }

    protected void NoFilter_Click(object sender, EventArgs e)
    {
        for (int i = 0; i < GridMaster.Rows.Count; i++)
        {
            GridMaster.Rows[i].Visible = true;
        }

    }
}
