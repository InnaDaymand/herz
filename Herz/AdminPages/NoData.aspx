﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminPages/AdminPage.master" AutoEventWireup="true" CodeFile="NoData.aspx.cs" Inherits="AdminPages_NoData" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .BaseCell {
            width: 100px;
        }
        .DblCell {
            width: 200px;
        }
        .TrlCell {
            width: 300px;
        }
        .QdrCell {
            width: 400px;
        }
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>                
    <cc1:TabContainer ID="TabAlarms" runat="server" ActiveTabIndex="1" Width="100%" Font-Bold="True" Font-Names="Arial" Font-Size="16px">
        <cc1:TabPanel ID="Def" HeaderText="הגדרות" runat="server" BackColor="Silver">
            <ContentTemplate>
                <asp:Panel runat="server" Height="400px" HorizontalAlign="Center" Width="100%" BackColor="Silver" >
                    <br />
                    <br />
                    <table style="width: 820px" align="center" border="2">
                        <tr>
                            <td colspan="1" align="center" dir="rtl" class="BaseCell"></td>
                            <td colspan="1" align="center" dir="rtl" class="BaseCell"></td>
                            <td colspan="1" align="center" dir="rtl" class="BaseCell"></td>
                            <td colspan="1" align="center" dir="rtl" class="BaseCell"></td>
                            <td colspan="1" align="center" dir="rtl" class="BaseCell"></td>
                            <td colspan="1" align="center" dir="rtl" class="BaseCell"></td>
                            <td colspan="1" align="center" dir="rtl" class="BaseCell"></td>
                            <td colspan="1" align="center" dir="rtl" class="BaseCell"></td>
                        </tr>
                        <tr>
                            <th colspan="3" align="center" dir="rtl" class="TrlCell">שם צרחן:</th>
                            <th colspan="1" align="center" dir="rtl" class="BaseCell">פעיל</th>
                            <th colspan="2" align="center" dir="rtl" class="DblCell">תקופתיות</th>
                            <th colspan="2" align="center" dir="rtl" class="DblCell">מספר התרעה</th>
                        </tr>
                        <tr>
                            <td colspan="3" align="center" dir="rtl" class="TrlCell">
                                <asp:DropDownList ID="ConsID" runat="server" Width="100%">
                                </asp:DropDownList>
                            </td>
                            <td colspan="1" align="center" dir="rtl" class="BaseCell">
                                <asp:CheckBox ID="IsActive" runat="server" TextAlign="Left" />
                            </td>
                            <td colspan="2" align="center" dir="rtl" class="DblCell">
                                <asp:TextBox ID="Periodicity" runat="server" Width="100%" >1</asp:TextBox>
                            </td>
                            <td colspan="2" align="center" dir="rtl" class="DblCell">
                                <asp:Label ID="SendID" Text="0" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" align="center" dir="rtl" class="BaseCell"></td>
                            <td colspan="1" align="center" dir="rtl" class="BaseCell"></td>
                            <td colspan="1" align="center" dir="rtl" class="BaseCell"></td>
                            <td colspan="1" align="center" dir="rtl" class="BaseCell"></td>
                            <td colspan="1" align="center" dir="rtl" class="BaseCell"></td>
                            <td colspan="1" align="center" dir="rtl" class="BaseCell"></td>
                            <td colspan="1" align="center" dir="rtl" class="BaseCell"></td>
                            <td colspan="1" align="center" dir="rtl" class="BaseCell"></td>
                        </tr>
                         <tr>
                            <td colspan="1" align="center" dir="rtl" class="BaseCell">
                                <asp:CheckBox ID="DateFrom" Text="מתאריך" runat="server" AutoPostBack="True" OnCheckedChanged="DateFrom_CheckedChanged" />
                            </td>
                            <td colspan="3" align="center" dir="rtl" class="TrlCell">
                                <asp:TextBox ID="FromDate" runat="server" Width="100%"/>
                                <ajaxToolkit:CalendarExtender runat="server" TargetControlID="FromDate" ID="FrDt" Enabled="True" 
                                    FirstDayOfWeek="Sunday" Format="yyyy-MM-dd" ></ajaxToolkit:CalendarExtender>      
                            </td>
                            <td colspan="1" align="center" dir="rtl" class="BaseCell">
                                <asp:CheckBox ID="DateTill" Text="לתאריך" runat="server" AutoPostBack="True" OnCheckedChanged="DateTill_CheckedChanged" />
                            </td>
                            <td colspan="3" align="center" dir="rtl" class="TrlCell">
                                <asp:TextBox ID="TillDate" runat="server" Width="100%" />
                                <ajaxToolkit:CalendarExtender runat="server" TargetControlID="TillDate" ID="ToDt" Enabled="True" 
                                    FirstDayOfWeek="Sunday" Format="yyyy-MM-dd" ></ajaxToolkit:CalendarExtender>      
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" align="center" dir="rtl" class="BaseCell"></td>
                            <td colspan="1" align="center" dir="rtl" class="BaseCell"></td>
                            <td colspan="1" align="center" dir="rtl" class="BaseCell"></td>
                            <td colspan="1" align="center" dir="rtl" class="BaseCell"></td>
                            <td colspan="1" align="center" dir="rtl" class="BaseCell"></td>
                            <td colspan="1" align="center" dir="rtl" class="BaseCell"></td>
                            <td colspan="1" align="center" dir="rtl" class="BaseCell"></td>
                            <td colspan="1" align="center" dir="rtl" class="BaseCell"></td>
                        </tr>
                        <tr>
                            <td colspan="1" align="center" dir="rtl" class="BaseCell">
                                <asp:CheckBox ID="Mail" Text="דו''ל" runat="server" OnCheckedChanged="Mail_CheckedChanged" />
                            </td>
                            <td colspan="3" align="center" dir="rtl" class="TrlCell">
                                <asp:CheckBoxList ID="Mails" runat="server" Height="63px" Width="100%">
                                </asp:CheckBoxList>
                            </td>
                            <td colspan="1" align="center" dir="rtl" class="BaseCell">SMS<br />
                                <asp:TextBox ID="TypeSMS" runat="server" Text="0" Width="30px" Wrap="False" />
                            </td>
                            <td colspan="3" align="center" dir="rtl" class="TrlCell">
                                <asp:CheckBoxList ID="SMSs" runat="server" Height="63px" TextAlign="Left" Width="100%">
                                </asp:CheckBoxList>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="center" dir="rtl" class="QdrCell">
                                <asp:Button ID="SaveData" Text="שמור" runat="server" Width="200px" OnClick="SaveData_Click" Font-Bold="True" Font-Size="16px" />
                            </td>
                            <td colspan="4" align="center" dir="rtl" class="QdrCell">
                                <asp:Button ID="PrepareNew" Text="התרעה חדשה" runat="server" Width="200px" OnClick="PrepareNew_Click" Font-Bold="True" Font-Size="16px" />
                            </td>
                        </tr>
                        </table>
                </asp:Panel>
            </ContentTemplate>
        </cc1:TabPanel>
        <cc1:TabPanel ID="List" HeaderText="רשימת התרעות" runat="server">
            <ContentTemplate>
            <asp:GridView ID="grdAlarms" runat="server"  
                AutoGenerateColumns="False" DataKeyNames="ID" DataSourceID="SourceNoData" AllowSorting="True" 
                Font-Names="Arial" Font-Size="13pt" Width="100%" BackColor="White" OnRowCommand="grdAlarms_RowCommand" style="margin-right: 0px">
                <Columns>
                    <asp:ButtonField ButtonType="Button" CommandName="Haraha" Text="הערכה" />
                    <asp:ButtonField ButtonType="Button" CommandName="Mahak" Text="מחק" />
                    <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" ReadOnly="True" SortExpression="ID" />
                    <asp:BoundField DataField="Consumer" HeaderText="Consumer" SortExpression="Consumer" />
                    <asp:CheckBoxField DataField="Is Active" HeaderText="Is Active" SortExpression="Is Active" />
                    <asp:BoundField DataField="From Date" HeaderText="From Date" ReadOnly="True" SortExpression="From Date" />
                    <asp:BoundField DataField="Till Date" HeaderText="Till Date" ReadOnly="True" SortExpression="Till Date" />
                    <asp:BoundField DataField="Periodicity" HeaderText="Periodicity" SortExpression="Periodicity" />
                    <asp:CheckBoxField DataField="Emails" HeaderText="Emails" SortExpression="Emails" />
                    <asp:BoundField DataField="SMS`s" HeaderText="SMS`s" SortExpression="SMS`s" />
                </Columns>
                <EditRowStyle BackColor="#F7F6F3" />
                <HeaderStyle CssClass="DetailsFormViewHeader"></HeaderStyle>
            </asp:GridView>
            </ContentTemplate>
        </cc1:TabPanel>
        <cc1:TabPanel ID="Logs" HeaderText="התראות לפי תאריך" runat="server">
            <ContentTemplate>
                <asp:Label ID="Label1" runat="server" Text="בחר יום  "></asp:Label>
                <asp:TextBox ID="DateShow" runat="server"></asp:TextBox>
                <ajaxToolkit:CalendarExtender runat="server" TargetControlID="DateShow" ID="CalendarExtender3" Enabled="True" DaysModeTitleFormat="yyyy-MM-dd" FirstDayOfWeek="Sunday" Format="yyyy-MM-dd" ></ajaxToolkit:CalendarExtender>      
                <asp:CheckBox ID="chbSMS" runat="server" Text="אסאמאס" OnCheckedChanged="chbSMS_CheckedChanged"/>
                <asp:CheckBox ID="chbMail" runat="server" Text="מייל" Checked="True" OnCheckedChanged="chbMail_CheckedChanged"/>
                <asp:Button ID="FillDate" runat="server" Text="הצג" Width="40px" OnClick="FillDate_Click" />
                <br />
                <asp:GridView ID="ListDate" runat="server">
                </asp:GridView>
            </ContentTemplate>
        </cc1:TabPanel>
    </cc1:TabContainer>


<%--        <asp:Panel ID="ForEdit" runat="server" Height="240px" HorizontalAlign="Center" Width="931px" BackColor="Silver" >
        <table style="width: 640px" align="center" border="2">
            <tr>
                <th colspan="3" align="center" dir="rtl" class="TrlCell">שם צרחן:</th>
                <th colspan="1" align="center" dir="rtl" class="BaseCell">פעיל</th>
                <th colspan="2" align="center" dir="rtl" class="DblCell">תקופתיות</th>
                <th colspan="2" align="center" dir="rtl" class="DblCell">מספר התרעה</th>
            </tr>
            <tr>
                <td colspan="3" align="center" dir="rtl" class="TrlCell">
                    <asp:DropDownList ID="ConsID" runat="server" Width="100%">
                    </asp:DropDownList>
                </td>
                <td colspan="1" align="center" dir="rtl" class="BaseCell">
                    <asp:CheckBox ID="IsActive" Text="" runat="server" TextAlign="Left" />
                </td>
                <td colspan="2" align="center" dir="rtl" class="DblCell">
                    <asp:TextBox ID="Periodicity" runat="server" Width="100%" />
                </td>
                <td colspan="2" align="center" dir="rtl" class="DblCell">
                    <asp:Label ID="SendID" Text="0" runat="server" />
                </td>
            </tr>
        </table>
        </asp:Panel>--%>
    <div>
        <asp:Panel ID="Table" runat="server" HorizontalAlign="Center">
            <asp:SqlDataSource ID="SourceNoData" runat="server" ConnectionString="<%$ ConnectionStrings:Money_DisplayConnectionString3 %>" 
                SelectCommand="SELECT ID, (SELECT sql_Consumer_name FROM tbl_Consumers_0 WHERE sql_Consumer_ID = ConsID) AS [Consumer], 
                                Active AS [Is Active], CONVERT (VARCHAR(10), CheckFromDate, 103) AS [From Date], 
                                CONVERT (VARCHAR(10), CheckTillDate, 103) AS [Till Date], Periodicity, Email AS Emails, TypeSMS AS [SMS`s] FROM Util_NoDataAlarm">
            </asp:SqlDataSource>
        </asp:Panel>
    </div>
</asp:Content>

