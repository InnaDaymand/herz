﻿using System;
using System.Data;
using System.Web.UI.WebControls;

public partial class AdminPages_NoData : SecuredPage
{
    private string SQL = "";
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            SQL = "SELECT sql_Consumer_ID AS ID, sql_Consumer_name AS Name "
                + "FROM tbl_Consumers_0 WHERE (sql_Consumer_active = 1) AND (NOT (sql_Consumer_name LIKE N'%.'))";
            ConsID.DataSource = AllFunctions.Populate(SQL);
            ConsID.DataValueField = "ID";
            ConsID.DataTextField = "Name";
            ConsID.DataBind();
            ConsID.SelectedIndex = -1;

            SQL = "SELECT  T_Mails.ID, T_Mails.Email + ' - ' + T_User.FirstName + ' ' + T_User.LastName AS Email FROM T_Mails INNER JOIN T_User ON T_Mails.UserID = T_User.ID";
            Mails.DataSource = AllFunctions.Populate(SQL);
            Mails.DataTextField = "Email";
            Mails.DataValueField = "ID";
            Mails.DataBind();

            SQL = "SELECT T_PhoneNumbers.ID, TelNo + ' - ' + T_User.FirstName + ' ' + T_User.LastName AS TelNo FROM T_PhoneNumbers INNER JOIN T_User ON T_PhoneNumbers.UserID = T_User.ID";
            SMSs.DataSource = AllFunctions.Populate(SQL);
            SMSs.DataTextField = "TelNo";
            SMSs.DataValueField = "ID";
            SMSs.DataBind();
        }
    }

    protected void DateFrom_CheckedChanged(object sender, EventArgs e)
    {
        //if (DateFrom.Checked)
        //{
        //    FromDate.Visible = true;
        //}
        //else
        //{
        //    FromDate.Text = "";
        //    FromDate.Visible = false;
        //}
    }

    protected void DateTill_CheckedChanged(object sender, EventArgs e)
    {
        //if (DateTill.Checked)
        //{
        //    TillDate.Visible = true;
        //}
        //else
        //{
        //    TillDate.Text = "";
        //    TillDate.Visible = false;
        //}
    }

    protected void SaveData_Click(object sender, EventArgs e)
    {
        if ((Mails.SelectedIndex < 0) & (SMSs.SelectedIndex < 0))
        {
            DoMessage("Choose the mail or/and SMS!");
            return;
        }
        string sID = "";

        #region Insert
        if ((SendID.Text == "0") | (SendID.Text == "")) //Insert
        {
            sID = ConsID.SelectedValue.ToString();
            sID += ", ";
            sID += IsActive.Checked ? "1": "0";

            //sID += ", " + IsActive.Checked.ToString();
            if (DateFrom.Checked)
            {
                sID += ", CONVERT(DATETIME, '" + FromDate.Text + "', 102)";
            }
            else
            {
                sID += ", NULL";
            }
            if (DateTill.Checked)
            {
                sID += ", CONVERT(DATETIME, '" + TillDate.Text + "', 102)";
            }
            else
            {
                sID += ", NULL";
            }
            sID += ", " + Periodicity.Text;
            if (Mail.Checked) sID += ", 1"; else sID += ", 0";
            //sID += ", " + Mail.Checked.ToString();
            sID += ", " + TypeSMS.Text + ")";

            string SQL = "INSERT INTO Util_NoDataAlarm (ConsID, Active, CheckFromDate, CheckTillDate, Periodicity, Email, TypeSMS) VALUES (";
            SQL += sID + "; SELECT IDENT_CURRENT('Util_NoDataAlarm')";

            sID = AllFunctions.GetScalar(SQL);

        }
        #endregion
        #region Update
        else // Update
        {
            SQL = "UPDATE Util_NoDataAlarm SET ";
            SQL += "ConsID = " + ConsID.SelectedValue.ToString() + ", ";
            SQL += "Active = ";
            SQL +=  IsActive.Checked ? "1, ": "0, ";
            if (DateFrom.Checked)
            {
                SQL += "CheckFromDate = CONVERT(DATETIME, '" + FromDate.Text + "', 102), ";
            }
            else
            {
                SQL += "CheckFromDate = NULL, ";
            }
            if (DateTill.Checked)
            {
                SQL += "CheckTillDate = CONVERT(DATETIME, '" + TillDate.Text + "', 102), ";
            }
            else
            {
                SQL += "CheckTillDate = NULL, ";
            }
            SQL += "Periodicity = " + Periodicity.Text + ", ";
            SQL += "Email = ";
            SQL += Mail.Checked ? "1, " : "0, ";
            SQL += "TypeSMS = " + TypeSMS.Text + " WHERE ID = " + SendID.Text;
            AllFunctions.DoExec(SQL);

            sID = SendID.Text;
            SQL = "DELETE FROM Util_NoDataAlarmMailSMS WHERE NoDataID = " + sID;
            AllFunctions.DoExec(SQL);
             SQL = "DELETE FROM Util_NoDataAlarmAction WHERE NoDataID = " + sID;
            AllFunctions.DoExec(SQL);
       }
        #endregion

        #region Action Data
        for (int i = 0; i < Mails.Items.Count; i++)
        {
            if (Mails.Items[i].Selected)
            {
                SQL = "INSERT INTO Util_NoDataAlarmMailSMS (NoDataID, Email) VALUES (" + sID + ", " + Mails.Items[i].Value.ToString() + ")";
                AllFunctions.DoExec(SQL);
            }
        }
        for (int i = 0; i < SMSs.Items.Count; i++)
        {
            if (SMSs.Items[i].Selected)
            {
                SQL = "INSERT INTO Util_NoDataAlarmMailSMS (NoDataID, SMS) VALUES (" + sID + ", " + SMSs.Items[i].Value.ToString() + ")";
                AllFunctions.DoExec(SQL);
            }
        }

        SQL = "INSERT INTO Util_NoDataAlarmAction (NoDataID, LastDateMail, TimePeriod, QuantitySMS, LastDateSMS, NumberSMS) VALUES (";
        SQL += sID + ", NULL, " + Periodicity.Text + ", " + TypeSMS.Text + ", NULL, " + TypeSMS.Text + ")";
        AllFunctions.DoExec(SQL);
        #endregion

        PrepareNew_Click(sender, e);
        grdAlarms.DataBind();
    }

    protected void PrepareNew_Click(object sender, EventArgs e)
    {
        ConsID.SelectedIndex = -1;
        Mails.ClearSelection();
        SMSs.ClearSelection();
        DateFrom.Checked = false;
        DateTill.Checked = false;
        SendID.Text = "";
        Periodicity.Text = "1";
        IsActive.Checked = false;
        Mail.Checked = false;
        TypeSMS.Text = "0";
    }

    protected void grdAlarms_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Haraha")
        {
            int index = Convert.ToInt32(e.CommandArgument);
            string sa = grdAlarms.Rows[index].Cells[2].Text;
            TabAlarms.ActiveTabIndex = 0;
            FillByID(sa);
        }

        if (e.CommandName == "Mahak")
        {
            int index = Convert.ToInt32(e.CommandArgument);
            string sa = grdAlarms.Rows[index].Cells[2].Text;
            string SQL = "DELETE FROM Util_NoDataAlarm WHERE ID = " + sa;
            AllFunctions.DoExec(SQL);
            grdAlarms.DataBind();
        }
    }

    protected void Mail_CheckedChanged(object sender, EventArgs e)
    {
        //if (!Mail.Checked)
        //{
        //    Mails.ClearSelection();
        //}
    }

    protected void chbSMS_CheckedChanged(object sender, EventArgs e)
    {

    }

    private void FillByID(string aID) 
    {
        string SS = "";
        string SQL = "SELECT ID, [ConsID], [Active], CONVERT(VARCHAR(10), [CheckFromDate], 120), CONVERT(VARCHAR(10), [CheckTillDate], 120), [Periodicity], [Email], [TypeSMS] "
            + "FROM [md2000Net].[dbo].[Util_NoDataAlarm] WHERE ID = " + aID;
        DataTable dTb = AllFunctions.Populate(SQL);

        ConsID.SelectedIndex = -1;
        SS = dTb.Rows[0][1].ToString();
        ListItem lm = ConsID.Items.FindByValue(SS);
        lm.Selected = true;

        SS = dTb.Rows[0][2].ToString();
        if (SS == "True")
        {
            IsActive.Checked = true;
        }
        else
        {
            IsActive.Checked = false;
        }

        SS = dTb.Rows[0][5].ToString();
        Periodicity.Text = SS;

        SS = dTb.Rows[0][6].ToString();
        if (SS == "True")
        {
            Mail.Checked = true;
        }
        else
        {
            Mail.Checked = false;
        }

        SS = dTb.Rows[0][7].ToString();
        TypeSMS.Text = SS;

        SS = dTb.Rows[0][0].ToString();
        SendID.Text = SS;

        SS = dTb.Rows[0][3].ToString();
        if (SS != "")
        {
            DateFrom.Checked = true;
            FromDate.Text = SS;
        }
        SS = dTb.Rows[0][4].ToString();
        if (SS != "")
        {
            DateTill.Checked = true;
            TillDate.Text = SS;
        }

        Mails.ClearSelection();
        SMSs.ClearSelection();
        SQL = "SELECT [Email], [SMS] FROM [dbo].[Util_NoDataAlarmMailSMS] WHERE [NoDataID] = " + aID;
        DataTable dOb = AllFunctions.Populate(SQL);

        for (int i = 0; i < dOb.Rows.Count; i++)
        {
            SS = dOb.Rows[i][0].ToString() ?? "";
            if (SS != "")
            {
                ListItem lml = Mails.Items.FindByValue(SS);
                lml.Selected = true;
            }

            SS = dOb.Rows[i][1].ToString() ?? "";
            if (SS != "")
            {
                ListItem lsl = SMSs.Items.FindByValue(SS);
                lsl.Selected = true;
            }
        }





        /*
        ID	ConsID	Active	CheckFromDate	CheckTillDate	Periodicity	Email	TypeSMS
        1	555	0	2015-02-15 00:00:00	2015-03-11 00:00:00	3	1	0
        */
    }

    protected void FillDate_Click(object sender, EventArgs e)
    {
        if (!((DateShow.Text != "") && ((chbMail.Checked) || (chbSMS.Checked))))
        {
            return;
        }

        string WhereAnd = "AND ([MessageType] = ";
        if ((chbMail.Checked) & (!chbSMS.Checked))
        {
            WhereAnd += "3)";
        }
        if ((!chbMail.Checked) & (chbSMS.Checked))
        {
            WhereAnd += "1)";
        }
        if ((chbMail.Checked) & (chbSMS.Checked))
        {
            WhereAnd += "3 OR [MessageType] = 1)";
        }

        string SQL = "SELECT [Recipient], [Body], [DeliveryTime] FROM [md2000Net].[dbo].[T_AlarmMessage] WHERE Body LIKE '%Checked at " + DateShow.Text + "%' " + WhereAnd;
        ListDate.DataSource = AllFunctions.Populate(SQL);
        ListDate.DataBind();


    }

    protected void chbMail_CheckedChanged(object sender, EventArgs e)
    {

    }

    private void DoMessage(string MsgText)
    {
        string message = MsgText;
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        sb.Append("<script type = 'text/javascript'>");
        sb.Append("window.onload=function(){");
        sb.Append("alert('");
        sb.Append(message);
        sb.Append("')};");
        sb.Append("</script>");
        ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", sb.ToString());
    }
}