﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminPages/AdminPage.master" AutoEventWireup="true" CodeFile="OrganizeFactors.aspx.cs" Inherits="AdminPages_OrganizeFactors" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:SqlDataSource ID="SqlSets" runat="server" 
        DeleteCommand="DELETE FROM T_SummaryFactorSets WHERE (SetID = @SetID)" 
        InsertCommand="INSERT INTO T_SummaryFactorSets(UserID, SetName, SetOrder) VALUES (@UserID, @Name, @Order )" 
        ProviderName="System.Data.SqlClient" 
        SelectCommand="SELECT TFS.SetID, TFS.SetName, TFS.SetOrder, TUZ.FirstName as UserName FROM T_SummaryFactorSets AS TFS INNER JOIN T_User AS TUZ ON TFS.UserID = TUZ.ID">
        <DeleteParameters>
            <asp:ControlParameter ControlID="Lists" DbType="Int32" Direction="Output" Name="@SetID" PropertyName="SelectedDataKey" />
        </DeleteParameters>
        <InsertParameters>
            <asp:ControlParameter ControlID="AllUsers" Name="@UserID" PropertyName="SelectedValue" />
            <asp:ControlParameter Name="@Name"  ControlID="SetName" PropertyName="Text" />
            <asp:ControlParameter Name="@Order"  ControlID="Orders" PropertyName="Text" DefaultValue="1" />
        </InsertParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlUsers" runat="server" ProviderName="System.Data.SqlClient" SelectCommand="SELECT -1 AS ID, 'בחר משתמש' AS Name UNION ALL SELECT [ID], [FirstName] + ' ' +  [LastName] AS Name FROM [T_User]"></asp:SqlDataSource>
    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
        <asp:View runat="server" ID="Listing">
            <div align="center">
                <table>
                   <tr>
                        <th style="direction:rtl; width:250px;">בחר משתמש</th>
                        <th style="direction:rtl; width:250px;">שם מבנה</th>
                        <th  style="direction:rtl; width:50px;">מס' סדר</th>
                        <th style="direction:rtl; width:200px;"></th>
                        <td></td>
                    </tr>
                    <tr>
                        <td style="direction:rtl; width:250px;"">
                            <asp:DropDownList runat="server" Width="100%" DataTextField="Name" DataValueField="ID" ID="AllUsers" OnSelectedIndexChanged="AllUsers_SelectedIndexChanged" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="SetName" BorderStyle="Inset" Text="" Width="100%" Height="24" Font-Names="Arial" Font-Size="12"/>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="Orders" BorderStyle="Inset" Text="1" Width="100%" Height="24" Font-Names="Arial" Font-Size="12"/>
                        </td>
                        <td>
                            <asp:Button Text="הוסף חדש" runat="server" ID="AddData" OnClick="AddData_Click" Height="24px" Width="100%" Font-Names="Arial" Font-Size="12" />
                        </td>
                    </tr>
                </table>
            <br />
            </div>
            <div dir="rtl" align="center">
                <asp:GridView runat="server" ID="Lists" AutoGenerateColumns="False" DataSourceID="SqlSets" DataKeyNames="SetID" OnRowCommand="Lists_RowCommand" >
                    <Columns>
                        <asp:CommandField SelectText="בחר למלוי" ShowSelectButton="True" ButtonType="Button"><ItemStyle HorizontalAlign="Right" Width="60px" /></asp:CommandField>
                        <asp:CommandField DeleteText="מחק..." ShowDeleteButton="True"><ItemStyle HorizontalAlign="Right" Width="40px" /></asp:CommandField>
                        <asp:BoundField DataField="UserName" HeaderText="שם משתמש" 
                            SortExpression="UserName"><ItemStyle HorizontalAlign="Right" Width="250px" /></asp:BoundField>
                        <asp:BoundField DataField="SetID" HeaderText="זיהוי" 
                            SortExpression="SetID"><ItemStyle HorizontalAlign="Left" Width="25px" /></asp:BoundField>
                        <asp:BoundField DataField="SetName" HeaderText="שם מבנה" 
                            SortExpression="SetName"><ItemStyle HorizontalAlign="Right" Width="250px" /></asp:BoundField>
                        <asp:BoundField DataField="SetOrder" HeaderText="מס' סדר" 
                            SortExpression="SetOrder"><ItemStyle HorizontalAlign="Center" Width="50px" /></asp:BoundField>
                        </Columns>
                </asp:GridView>
            </div>
        </asp:View>
        <asp:View runat="server" ID="Defining">
            <asp:Label Text="text" runat="server" ID="NameOfSet" Font-Names="Times New Roman" Font-Size="18px" ForeColor="#0033CC" Font-Bold="True" /><br />
            <table dir="rtl" align="center" >
                <tr>
                    <td>
                        <asp:ListBox runat="server" ID="SetAlls" Width="300" Height="300">
                        </asp:ListBox>
                    </td>
                    <td>
                        <asp:Button Text="---&gt;&gt;&gt;" runat="server" ID="AddTo" Font-Names="Courier New" Font-Size="12pt" OnClick="AddTo_Click"/><br />
                        <asp:Button Text="&lt;&lt;&lt;---" runat="server" ID="Remove" Font-Names="Courier New" Font-Size="12pt" OnClick="Remove_Click"/></td>
                    <td>
                        <asp:ListBox runat="server" ID="SetDefine" Width="300" Height="300">
                        </asp:ListBox>
                    </td>
                </tr>
            </table>


            <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></ajaxToolkit:ToolkitScriptManager>
        </asp:View>
    </asp:MultiView>
</asp:Content>

