﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AdminPages_OrganizeFactors : System.Web.UI.Page
{
    //private string UID;
    protected void Page_Load(object sender, EventArgs e)
    {
        //                ConnectionString="<%connectionStrings:Money_DisplayConnectionString3%>"
        SqlSets.ConnectionString = SqlUsers.ConnectionString = AllFunctions.ConnectStr;
        if (!IsPostBack)
        {
            AllUsers.DataSource = SqlUsers;
            AllUsers.DataBind();
        }
    }

    protected void AddData_Click(object sender, EventArgs e)
    {
        //MultiView1.ActiveViewIndex = 1;
        if ((SetName.Text == "") || (Orders.Text == "") || (AllUsers.SelectedIndex == -1))
        {
            return;
        }

        string SQL = "INSERT INTO T_SummaryFactorSets(UserID, SetName, SetOrder) VALUES (" + AllFunctions.ConsID + ", N'" 
            + SetName.Text.Trim() + "', " + Orders.Text + ")";
        AllFunctions.DoExec(SQL);
        AllUsers.SelectedIndex = -1;
        AllFunctions.ConsID = "";
        SetName.Text = "";
        Orders.Text = "1";
        Lists.DataBind();
    }

    protected void Lists_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int r = Convert.ToInt32(e.CommandArgument.ToString());
            AllFunctions.ForPageN1 = Lists.Rows[r].Cells[3].Text;
            string Uid = AllFunctions.GetScalar("SELECT UserID FROM T_SummaryFactorSets WHERE SetID = " + AllFunctions.ForPageN1);
            //AddTo.Text = sid.ToString();
            NameOfSet.Text = Lists.Rows[r].Cells[4].Text;
            string SQL = "EXEC [dbo].[S_GetAllConsumersByUser] @UserID = " + Uid;
            SetAlls.DataSource = AllFunctions.Populate(SQL);
            SetAlls.DataTextField = "Name";
            SetAlls.DataValueField = "ID";
            SetAlls.DataBind();

            SQL = "SELECT StructID, sql_Consumer_name AS Name FROM T_SummeryFactorStructure INNER JOIN "
                + "tbl_Consumers_0 ON ConsumerID = sql_Consumer_ID WHERE SetID = " + AllFunctions.ForPageN1;
            SetDefine.DataSource = AllFunctions.Populate(SQL);
            SetDefine.DataTextField = "Name";
            SetDefine.DataValueField = "StructID";
            SetDefine.DataBind();
            MultiView1.ActiveViewIndex = 1;
        }
    }

    protected void AddTo_Click(object sender, EventArgs e)
    {
        if (SetAlls.SelectedIndex == -1)
        {
            return;
        }
        string SQL = "INSERT INTO T_SummeryFactorStructure (SetID, ConsumerID, ConsumerParamI) VALUES ("
            + AllFunctions.ForPageN1 + ", " + SetAlls.SelectedValue.ToString() + ", 1)";
        AllFunctions.DoExec(SQL);
        SQL = "SELECT StructID, sql_Consumer_name AS Name FROM T_SummeryFactorStructure INNER JOIN "
            + "tbl_Consumers_0 ON ConsumerID = sql_Consumer_ID WHERE SetID = " + AllFunctions.ForPageN1;
        SetDefine.DataSource = AllFunctions.Populate(SQL);
        SetDefine.DataTextField = "Name";
        SetDefine.DataValueField = "StructID";
        SetDefine.DataBind();
    }

    protected void Remove_Click(object sender, EventArgs e)
    {
        if (SetDefine.SelectedIndex == -1)
        {
            return;
        }
        string SQL = "DELETE FROM T_SummeryFactorStructure WHERE StructID = " + SetDefine.SelectedValue.ToString();
        AllFunctions.DoExec(SQL);
        SQL = "SELECT StructID, sql_Consumer_name AS Name FROM T_SummeryFactorStructure INNER JOIN "
            + "tbl_Consumers_0 ON ConsumerID = sql_Consumer_ID WHERE SetID = " + AllFunctions.ForPageN1;
        SetDefine.DataSource = AllFunctions.Populate(SQL);
        SetDefine.DataTextField = "Name";
        SetDefine.DataValueField = "StructID";
        SetDefine.DataBind();
    }

    protected void AllUsers_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (AllFunctions.ConsID == "")
        {
            AllFunctions.ConsID = AllUsers.SelectedValue.ToString();
        }
    }
}