﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminPages/AdminPage.master" AutoEventWireup="true" CodeFile="Permissions.aspx.cs" Inherits="AdminPages_PermissionsT" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table class="LayoutTable">
        <tr>
            <th class="HeaderStyle">הרשאות</th>
        </tr>
        <tr>
            <td align="left" >
                <a href="../Help/HPermissions.aspx" target="_blank" style="color:#5d7b9d;"><img src="../images/help.gif" style="border:none" />עזרה</a>
            </td>
        </tr>
        <tr>
            <td>
<ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</ajaxToolkit:ToolkitScriptManager>                
                <asp:LinqDataSource ID="LinqRestrictions" runat="server"
                    ContextTypeName="DataClassesDataContext" 
                    TableName="T_RestrictedPaths" 
                    >
                </asp:LinqDataSource>
                <asp:LinqDataSource ID="LinqRoles" runat="server"
                    ContextTypeName="DataClassesDataContext" 
                    TableName="T_Roles" 
                    >
                </asp:LinqDataSource>
                <asp:LinqDataSource ID="LinqUsers" runat="server"
                    ContextTypeName="DataClassesDataContext" 
                    TableName="T_Users" 
                    Select="new ( ID, FirstName + ' ' + LastName as Name )"
                    >
                </asp:LinqDataSource>
                <asp:LinqDataSource ID="LinqRolePermissions" runat="server"
                    ContextTypeName="DataClassesDataContext" 
                    TableName="T_RolePermissions" 
                    Select="new ( ID, RoleID, PathID, Permission, 
                                  T_Role.Name as RoleName, 
                                  T_RestrictedPath.Name as PathName, T_RestrictedPath.Type as PathType )"
                    >
                </asp:LinqDataSource>
                <asp:LinqDataSource ID="LinqUserPermissions" runat="server"
                    ContextTypeName="DataClassesDataContext" 
                    TableName="T_UserPermissions" 
                    Select="new ( ID, UserID, PathID, Permission, 
                                  T_User.FirstName + ' ' + T_User.LastName as UserName, 
                                  T_RestrictedPath.Name as PathName, T_RestrictedPath.Type as PathType )"
                    >
                </asp:LinqDataSource>
                <asp:LinqDataSource ID="LinqRoleUsers" runat="server"
                    ContextTypeName="DataClassesDataContext" 
                    TableName="T_RoleUsers" 
                    Select="new ( ID, RoleID, UserID,  
                                  T_Role.Name as RoleName,
                                  T_User.FirstName + ' ' + T_User.LastName as UserName )"
                    >
                </asp:LinqDataSource>
                                            
                <cc1:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" 
                    AutoPostBack="True">
                    <cc1:TabPanel ID="TabPanel1" runat="server" HeaderText="הגבלות">
                        <HeaderTemplate>
                            הגבלות
                        </HeaderTemplate>
                        <ContentTemplate>
                            <table class="LayoutTable">
                                <tr>
                                    <td valign="top" rowspan="2">
                                        <b>תיקיות ודפים</b>
                                        <asp:GridView ID="GridRestrictions" runat="server" Width="100%"
                                            AutoGenerateColumns="False"
                                            DataSourceID="LinqRestrictions"
                                            DataKeyNames="ID" onrowdatabound="GridRestrictions_RowDataBound" 
                                            >
                                            <Columns>
                                                <asp:CommandField ShowSelectButton="True" SelectText="בחר..." />
                                                <asp:BoundField DataField="ID" HeaderText="ID" 
                                                    SortExpression="ID" >
                                                    <ItemStyle Width="30px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Type" HeaderText="סוג" />
                                                <asp:BoundField DataField="Name" HeaderText="שם" />
                                            </Columns>
                                            <HeaderStyle CssClass="DetailsFormViewHeader"></HeaderStyle>
                                            <RowStyle ForeColor="#333333"></RowStyle>
                                            <AlternatingRowStyle BackColor="#F7F6F3"></AlternatingRowStyle>
                                            <SelectedRowStyle BackColor="#E2DED6" ForeColor="#284775"></SelectedRowStyle>
                                        </asp:GridView>
                                    </td>
                                    <td valign="top" width="50%">
                                        <b>הרשאות לפי סוגי משתמשים</b>
                                        <asp:GridView ID="GridRestrictionRoles" runat="server" Width="100%"
                                            AutoGenerateColumns="False"
                                            DataSourceID="LinqRolePermissions"
                                            DataKeyNames="ID" 
                                            EmptyDataText="אין נתונים"
                                            onrowdeleting="GridRoleRestrictions_RowDeleting" 
                                            onrowdatabound="GridPermissions_RowDataBound" 
                                            >
                                            <Columns>
                                                <asp:TemplateField><ItemTemplate>
                                                    <asp:LinkButton ID="LinkButton1" runat="server" 
                                                        OnClientClick="return confirm('Are you sure you want to delete this record?');"
                                                        CommandName="Delete">
                                                        הסר
                                                    </asp:LinkButton>
                                                </ItemTemplate></asp:TemplateField>
                                                <asp:BoundField DataField="RoleName" HeaderText="סוג משתמש" />
                                                <asp:BoundField DataField="Permission" HeaderText="הרשאה" />
                                            </Columns>
                                            <HeaderStyle CssClass="DetailsFormViewHeader"></HeaderStyle>
                                            <RowStyle ForeColor="#333333"></RowStyle>
                                            <AlternatingRowStyle BackColor="#F7F6F3"></AlternatingRowStyle>
                                            <SelectedRowStyle BackColor="#E2DED6" ForeColor="#284775"></SelectedRowStyle>
                                        </asp:GridView>
                                        סוג משתמש
                                        <asp:DropDownList ID="cmbRoles_Path" runat="server"
                                            DataSourceID="LinqRoles" 
                                            DataTextField="Name" 
                                            DataValueField="ID"
                                            />
                                        הרשאה
                                        <asp:DropDownList ID="cmbRolePermissions_Path" runat="server"/>
                                        <asp:Button ID="btnAddRolePermission_Path" runat="server" Text="הסף לרשימה" 
                                            onclick="btnAddRolePermission_Path_Click" />
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" style="padding-top: 40px;">
                                        <b>הרשאות אישיות למשתמשים</b>
                                        <asp:GridView ID="GridRestrictionUsers" runat="server" Width="100%"
                                            AutoGenerateColumns="False"
                                            DataSourceID="LinqUserPermissions"
                                            DataKeyNames="ID" 
                                            EmptyDataText="אין נתונים"
                                            onrowdeleting="GridUserRestrictions_RowDeleting" 
                                            onrowdatabound="GridPermissions_RowDataBound" 
                                            >
                                            <Columns>
                                                <asp:TemplateField><ItemTemplate>
                                                    <asp:LinkButton ID="LinkButton1" runat="server" 
                                                        OnClientClick="return confirm('Are you sure you want to delete this record?');"
                                                        CommandName="Delete">
                                                        הסר
                                                    </asp:LinkButton>
                                                </ItemTemplate></asp:TemplateField>
                                                <asp:BoundField DataField="UserName" HeaderText="משתמש" />
                                                <asp:BoundField DataField="Permission" HeaderText="הרשאה" />
                                            </Columns>
                                            <HeaderStyle CssClass="DetailsFormViewHeader"></HeaderStyle>
                                            <RowStyle ForeColor="#333333"></RowStyle>
                                            <AlternatingRowStyle BackColor="#F7F6F3"></AlternatingRowStyle>
                                            <SelectedRowStyle BackColor="#E2DED6" ForeColor="#284775"></SelectedRowStyle>
                                        </asp:GridView>
                                        משתמש
                                        <asp:DropDownList ID="cmbUsers_Path" runat="server"
                                            DataSourceID="LinqUsers" 
                                            DataTextField="Name" 
                                            DataValueField="ID"
                                            />
                                        הרשאה
                                        <asp:DropDownList ID="cmbUserPermissions_Path" runat="server"/>
                                        <asp:Button ID="btnAddUserPermission_Path" runat="server" Text="הסף לרשימה" 
                                            onclick="btnAddUserPermission_Path_Click" />
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </cc1:TabPanel>
                    <cc1:TabPanel ID="TabPanel2" runat="server" HeaderText="סוגי משתמשים">
                        <ContentTemplate>
                            <table class="LayoutTable">
                                <tr>
                                    <td valign="top" rowspan="2">
                                        <b>סוגי משתמשים</b>
                                        <asp:GridView ID="GridRoles" runat="server" Width="100%"
                                            AutoGenerateColumns="False"
                                            DataSourceID="LinqRoles"
                                            DataKeyNames="ID" 
                                            >
                                            <Columns>
                                                <asp:CommandField ShowSelectButton="True" SelectText="בחר..." />
                                                <asp:BoundField DataField="ID" HeaderText="ID" 
                                                    SortExpression="ID" >
                                                    <ItemStyle Width="30px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Name" HeaderText="שם" />
                                            </Columns>
                                            <HeaderStyle CssClass="DetailsFormViewHeader"></HeaderStyle>
                                            <RowStyle ForeColor="#333333"></RowStyle>
                                            <AlternatingRowStyle BackColor="#f7f6f3"></AlternatingRowStyle>
                                            <SelectedRowStyle BackColor="#E2DED6" ForeColor="#284775"></SelectedRowStyle>
                                        </asp:GridView>
                                    </td>
                                    <td valign="top" width="50%">
                                        <b>משתמשים</b>
                                        <asp:GridView ID="GridRoleUsers" runat="server" Width="100%"
                                            AutoGenerateColumns="False"
                                            DataSourceID="LinqRoleUsers"
                                            DataKeyNames="ID" 
                                            EmptyDataText="אין נתונים"
                                            onrowdeleting="GridRoleUsers_RowDeleting" 
                                            >
                                            <Columns>
                                                <asp:TemplateField><ItemTemplate>
                                                    <asp:LinkButton ID="LinkButton1" runat="server" 
                                                        OnClientClick="return confirm('Are you sure you want to delete this record?');"
                                                        CommandName="Delete">
                                                        הסר
                                                    </asp:LinkButton>
                                                </ItemTemplate></asp:TemplateField>
                                                <asp:BoundField DataField="UserName" HeaderText="משתמש" />
                                            </Columns>
                                            <HeaderStyle CssClass="DetailsFormViewHeader"></HeaderStyle>
                                            <RowStyle ForeColor="#333333"></RowStyle>
                                            <AlternatingRowStyle BackColor="#f7f6f3"></AlternatingRowStyle>
                                            <SelectedRowStyle BackColor="#E2DED6" ForeColor="#284775"></SelectedRowStyle>
                                        </asp:GridView>
                                        משתמש
                                        <asp:DropDownList ID="cmbUsers_Role" runat="server"
                                            DataSourceID="LinqUsers" 
                                            DataTextField="Name" 
                                            DataValueField="ID"
                                            />
                                        <asp:Button ID="btnAddRoleUser_Role" runat="server" Text="הסף לרשימה" 
                                            onclick="btnAddRoleUser_Role_Click" />
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" style="padding-top: 40px;">
                                        <b>הרשאות</b>
                                        <asp:GridView ID="GridRoleRestrictions" runat="server" Width="100%"
                                            AutoGenerateColumns="False"
                                            DataSourceID="LinqRolePermissions"
                                            DataKeyNames="ID" 
                                            EmptyDataText="אין נתונים"
                                            onrowdeleting="GridRoleRestrictions_RowDeleting" 
                                            onrowdatabound="GridPermissions_RowDataBound" 
                                            >
                                            <Columns>
                                                <asp:TemplateField><ItemTemplate>
                                                    <asp:LinkButton ID="LinkButton1" runat="server" 
                                                        OnClientClick="return confirm('Are you sure you want to delete this record?');"
                                                        CommandName="Delete">
                                                        הסר
                                                    </asp:LinkButton>
                                                </ItemTemplate></asp:TemplateField>
                                                <asp:BoundField DataField="PathName" HeaderText="תיקיה\דף" />
                                                <asp:BoundField DataField="Permission" HeaderText="הרשאה" />
                                            </Columns>
                                            <HeaderStyle CssClass="DetailsFormViewHeader"></HeaderStyle>
                                            <RowStyle ForeColor="#333333"></RowStyle>
                                            <AlternatingRowStyle BackColor="#f7f6f3"></AlternatingRowStyle>
                                            <SelectedRowStyle BackColor="#E2DED6" ForeColor="#284775"></SelectedRowStyle>
                                        </asp:GridView>
                                        תיקיה\דף
                                        <asp:DropDownList ID="cmbPaths_Role" runat="server"
                                            DataSourceID="LinqRestrictions" 
                                            DataTextField="Name" 
                                            DataValueField="ID"
                                            />
                                        הרשאה
                                        <asp:DropDownList ID="cmbRolePermissions_Role" runat="server"/>
                                        <asp:Button ID="btnAddRolePermission_Role" runat="server" Text="הסף לרשימה" 
                                            onclick="btnAddRolePermission_Role_Click" />
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </cc1:TabPanel>
                    <cc1:TabPanel ID="TabPanel3" runat="server" HeaderText="משתמשים">
                        <HeaderTemplate>
                            משתמשים
                        </HeaderTemplate>
                        <ContentTemplate>
                            <table class="LayoutTable">
                                <tr>
                                    <td valign="top" rowspan="2">
                                        <b>משתמשים</b>
                                        <asp:GridView ID="GridUsers" runat="server" Width="100%"
                                            AutoGenerateColumns="False"
                                            DataSourceID="LinqUsers"
                                            DataKeyNames="ID" 
                                            >
                                            <Columns>
                                                <asp:CommandField ShowSelectButton="True" SelectText="בחר..." />
                                                <asp:BoundField DataField="ID" HeaderText="ID" 
                                                    SortExpression="ID" >
                                                    <ItemStyle Width="30px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Name" HeaderText="שם" />
                                            </Columns>
                                            <HeaderStyle CssClass="DetailsFormViewHeader"></HeaderStyle>
                                            <RowStyle ForeColor="#333333"></RowStyle>
                                            <AlternatingRowStyle BackColor="#f7f6f3"></AlternatingRowStyle>
                                            <SelectedRowStyle BackColor="#E2DED6" ForeColor="#284775"></SelectedRowStyle>
                                        </asp:GridView>
                                    </td>
                                    <td valign="top" width="50%">
                                        <b>סוגי משתמשים</b>
                                        <asp:GridView ID="GridUserRoles" runat="server" Width="100%"
                                            AutoGenerateColumns="False"
                                            DataSourceID="LinqRoleUsers"
                                            DataKeyNames="ID" 
                                            EmptyDataText="אין נתונים"
                                            onrowdeleting="GridRoleUsers_RowDeleting" 
                                            >
                                            <Columns>
                                                <asp:TemplateField><ItemTemplate>
                                                    <asp:LinkButton ID="LinkButton1" runat="server" 
                                                        OnClientClick="return confirm('Are you sure you want to delete this record?');"
                                                        CommandName="Delete">
                                                        הסר
                                                    </asp:LinkButton>
                                                </ItemTemplate></asp:TemplateField>
                                                <asp:BoundField DataField="RoleName" HeaderText="סוג משתמש" />
                                            </Columns>
                                            <HeaderStyle CssClass="DetailsFormViewHeader"></HeaderStyle>
                                            <RowStyle ForeColor="#333333"></RowStyle>
                                            <AlternatingRowStyle BackColor="#f7f6f3"></AlternatingRowStyle>
                                            <SelectedRowStyle BackColor="#E2DED6" ForeColor="#284775"></SelectedRowStyle>
                                        </asp:GridView>
                                        סוג משתמש
                                        <asp:DropDownList ID="cmbRoles_User" runat="server"
                                            DataSourceID="LinqRoles" 
                                            DataTextField="Name" 
                                            DataValueField="ID"
                                            />
                                        <asp:Button ID="btnAddRoleUser_User" runat="server" Text="הסף לרשימה" 
                                            onclick="btnAddRoleUser_User_Click" />
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" style="padding-top: 40px;">
                                        <b>הרשאות</b>
                                        <asp:GridView ID="GridUserRestrictions" runat="server" Width="100%"
                                            AutoGenerateColumns="False"
                                            DataSourceID="LinqUserPermissions"
                                            DataKeyNames="ID" 
                                            EmptyDataText="אין נתונים"
                                            onrowdeleting="GridUserRestrictions_RowDeleting" 
                                            onrowdatabound="GridPermissions_RowDataBound" 
                                            >
                                            <Columns>
                                                <asp:TemplateField><ItemTemplate>
                                                    <asp:LinkButton ID="LinkButton1" runat="server" 
                                                        OnClientClick="return confirm('Are you sure you want to delete this record?');"
                                                        CommandName="Delete">
                                                        הסר
                                                    </asp:LinkButton>
                                                </ItemTemplate></asp:TemplateField>
                                                <asp:BoundField DataField="PathName" HeaderText="תיקיה\דף" />
                                                <asp:BoundField DataField="Permission" HeaderText="הרשאה" />
                                            </Columns>
                                            <HeaderStyle CssClass="DetailsFormViewHeader"></HeaderStyle>
                                            <RowStyle ForeColor="#333333"></RowStyle>
                                            <AlternatingRowStyle BackColor="#f7f6f3"></AlternatingRowStyle>
                                            <SelectedRowStyle BackColor="#E2DED6" ForeColor="#284775"></SelectedRowStyle>
                                        </asp:GridView>
                                        תיקיה\דף
                                        <asp:DropDownList ID="cmbPaths_User" runat="server"
                                            DataSourceID="LinqRestrictions" 
                                            DataTextField="Name" 
                                            DataValueField="ID"
                                            />
                                        הרשאה
                                        <asp:DropDownList ID="cmbUserPermissions_User" runat="server"/>
                                        <asp:Button ID="btnAddUserPermission_User" runat="server" Text="הסף לרשימה"
                                            OnClick="btnAddUserPermission_User_Click" />
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </cc1:TabPanel>
                </cc1:TabContainer>
            </td>
        </tr>
    </table>
</asp:Content>

