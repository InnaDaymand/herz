﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AdminPages_PermissionsT : SecuredPage
{
    public enum Permission
    {
        Deny,
        AllowView,
        AllowEdit
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (GridRestrictions.SelectedIndex < 0)
            GridRestrictions.SelectedIndex = 0;
        if (GridRoles.SelectedIndex < 0)
            GridRoles.SelectedIndex = 0;
        if (GridUsers.SelectedIndex < 0)
            GridUsers.SelectedIndex = 0;
        /*switch (TabContainer1.ActiveTabIndex)
        {
            case 0:
                //LinqRolePermissions.Where = String.Format("PathID = {0}", GridRestrictions.DataKeys[0]);
                //LinqRolePermissions.Where = "PathID = @PathID";
                //LinqRolePermissions.WhereParameters.Add(new ControlParameter("PathID", GridRestrictions.ID, "SelectedValue"));
                break;
            case 1:
                break;
        }*/
        if (_db == null)
            _db = new DataClassesDataContext();
    }
    protected void Page_PreRenderComplete(object sender, EventArgs e)
    {
        switch (TabContainer1.ActiveTabIndex)
        {
            case 0:
                LinqRolePermissions.Where = String.Format("PathID = {0}", GridRestrictions.SelectedValue);
                LinqUserPermissions.Where = String.Format("PathID = {0}", GridRestrictions.SelectedValue);
                cmbRolePermissions_Path.DataSource = Enum.GetNames(typeof(Permission));
                cmbRolePermissions_Path.DataBind();
                cmbUserPermissions_Path.DataSource = Enum.GetNames(typeof(Permission));
                cmbUserPermissions_Path.DataBind();
                break;
            case 1:
                LinqRoleUsers.Where = String.Format("RoleID = {0}", GridRoles.SelectedValue);
                LinqRolePermissions.Where = String.Format("RoleID = {0}", GridRoles.SelectedValue);
                cmbRolePermissions_Role.DataSource = Enum.GetNames(typeof(Permission));
                cmbRolePermissions_Role.DataBind();
                break;
            case 2:
                LinqRoleUsers.Where = String.Format("UserID = {0}", GridUsers.SelectedValue);
                LinqUserPermissions.Where = String.Format("UserID = {0}", GridUsers.SelectedValue);
                cmbUserPermissions_User.DataSource = Enum.GetNames(typeof(Permission));
                cmbUserPermissions_User.DataBind();
                break;
        }
    }
    protected void GridRoleRestrictions_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        _db.ExecuteCommand("DELETE FROM T_RolePermission WHERE ID = {0}", e.Keys[0]);
        GridUsers.DataBind();
        e.Cancel = true;//cancel the event arrival to LinqDetails data source*/
    }
    protected void GridUserRestrictions_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        _db.ExecuteCommand("DELETE FROM T_UserPermission WHERE ID = {0}", e.Keys[0]);
        GridUsers.DataBind();
        e.Cancel = true;//cancel the event arrival to LinqDetails data source*/
    }
    protected void GridRoleUsers_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        _db.ExecuteCommand("DELETE FROM T_RoleUser WHERE ID = {0}", e.Keys[0]);
        GridUsers.DataBind();
        e.Cancel = true;//cancel the event arrival to LinqDetails data source*/
    }
    protected void btnAddRolePermission_Path_Click(object sender, EventArgs e)
    {
        AddRolePermission(
            int.Parse(cmbRoles_Path.SelectedValue),
            (int)GridRestrictions.SelectedValue,
            (byte)Enum.Parse(typeof(Permission), cmbRolePermissions_Path.SelectedValue).GetHashCode()
            );

        GridRestrictionRoles.DataBind();
    }
    protected void btnAddUserPermission_Path_Click(object sender, EventArgs e)
    {
        AddUserPermission(
            int.Parse(cmbUsers_Path.SelectedValue),
            (int)GridRestrictions.SelectedValue,
            (byte)Enum.Parse(typeof(Permission), cmbUserPermissions_Path.SelectedValue).GetHashCode()
            );

        GridRestrictionUsers.DataBind();
    }
    protected void btnAddRoleUser_Role_Click(object sender, EventArgs e)
    {
        AddRoleUser(
            (int)GridRoles.SelectedValue,
            int.Parse(cmbUsers_Role.SelectedValue)
            );

        GridRoleUsers.DataBind();
    }
    protected void btnAddRolePermission_Role_Click(object sender, EventArgs e)
    {
        AddRolePermission(
            (int)GridRoles.SelectedValue,
            int.Parse(cmbPaths_Role.SelectedValue),
            (byte)Enum.Parse(typeof(Permission), cmbRolePermissions_Role.SelectedValue).GetHashCode()
            );

        GridRoleRestrictions.DataBind();
    }
    protected void btnAddRoleUser_User_Click(object sender, EventArgs e)
    {
        AddRoleUser(
            int.Parse(cmbRoles_User.SelectedValue),
            (int)GridUsers.SelectedValue
            );

        GridUserRoles.DataBind();
    }
    protected void btnAddUserPermission_User_Click(object sender, EventArgs e)
    {
        AddUserPermission(
            (int)GridUsers.SelectedValue,
            int.Parse(cmbPaths_User.SelectedValue),
            (byte)Enum.Parse(typeof(Permission), cmbUserPermissions_User.SelectedValue).GetHashCode()
            );

        GridUserRestrictions.DataBind();
    }
    void AddRolePermission(int roleId, int pathId, byte permission)
    {
        try
        {
            T_RolePermission rp = new T_RolePermission();
            rp.RoleID = roleId;
            rp.PathID = pathId;
            rp.Permission = permission;

            _db.T_RolePermissions.InsertOnSubmit(rp);
            _db.SubmitChanges();
        }
        catch { }
    }
    void AddUserPermission(int userId, int pathId, byte permission)
    {
        try
        {
            T_UserPermission up = new T_UserPermission();
            up.UserID = userId;
            up.PathID = pathId;
            up.Permission = permission;

            _db.T_UserPermissions.InsertOnSubmit(up);
            _db.SubmitChanges();
        }
        catch { }
    }
    void AddRoleUser(int roleId, int userId)
    {
        try
        {
            T_RoleUser ru = new T_RoleUser();
            ru.RoleID = roleId;
            ru.UserID = userId;

            _db.T_RoleUsers.InsertOnSubmit(ru);
            _db.SubmitChanges();
        }
        catch { }
    }
    protected void GridRestrictions_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if ((char)DataBinder.Eval(e.Row.DataItem, "Type") == 'F')
                e.Row.Cells[2].Text = "Folder";
            else
                e.Row.Cells[2].Text = "Page";
        }
    }
    protected void GridPermissions_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
            e.Row.Cells[2].Text = Enum.GetName(typeof(Permission), DataBinder.Eval(e.Row.DataItem, "Permission"));
    }
}

