﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminPages/AdminPage.master" AutoEventWireup="true" CodeFile="PlantLevels.aspx.cs" Inherits="AdminPages_PlantLevels" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .auto-style1 {
            width: 200px;
        }
        .auto-style2 {
           width:300px;
            height: 380px;
        }
        .auto-style3 {
            height: 380px;
            width: 600px;
        }
        .auto-style4 {
            width: 140px;
            height: 380px;
        }
        .auto-style5 {
            width: 80px;
            height: 30px;
        }
        .auto-style6 {
            width: 200px;
            height: 30px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div runat="server" style="align-items:center">
        <asp:DropDownList ID="GetUser" runat="server" AutoPostBack="True" OnSelectedIndexChanged="GetUser_SelectedIndexChanged" Width="300px">
        </asp:DropDownList>
        <br />
        <br />
        <table>
            <tr>
                <td colspan="3">
                    <table>
                        <tr>
                            <th>שם</th>
                            <th>Name</th>
                            <th>Название</th>
                            <th>ID</th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                        <tr>
                            <td class="auto-style6">
                                <asp:TextBox runat="server" Width="100%" ID="EditNode" ToolTip="Hebrew Name" />
                            </td>
                            <td class="auto-style6">
                                <asp:TextBox runat="server" Width="100%" ID="EngNode" ToolTip="English Name" />
                            </td>
                            <td class="auto-style6">
                                <asp:TextBox runat="server" Width="100%" ID="RusNode" ToolTip="Russion Name" />
                            </td>
                            <td class="auto-style5">
                                <asp:TextBox runat="server"  ID="Numb" ToolTip="Order #" Width="100%" />
                            </td>
                            <td class="auto-style5">
                                <asp:Button Text="בטל" runat="server" Width="100%" ID="B1" OnClick="B1_Click" />
                            </td>
                            <td class="auto-style5">
                                <asp:Button Text="שמור" runat="server" Width="100%" ID="B2" OnClick="B2_Click" />
                            </td>
                            <td class="auto-style5">
                                <asp:Button Text="הסר" runat="server" Width="100%" ID="B3" OnClick="B3_Click" CommandArgument="0" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td rowspan="2" class="auto-style2" align="right" dir="rtl">
                    <asp:TreeView ID="Derevo" runat="server" ImageSet="Simple" OnSelectedNodeChanged="Derevo_SelectedNodeChanged" >
                        <HoverNodeStyle Font-Underline="True" ForeColor="#5555DD" />
                        <NodeStyle Font-Names="Tahoma" Font-Size="10pt" ForeColor="Black" 
                            HorizontalPadding="0px" NodeSpacing="0px" VerticalPadding="0px" />
                        <ParentNodeStyle Font-Bold="False" />
                        <SelectedNodeStyle Font-Underline="True" ForeColor="#5555DD" 
                            HorizontalPadding="0px" VerticalPadding="0px" />
                    </asp:TreeView>
                </td>
                <td class="auto-style4">
                    <asp:Button Text="<<" runat="server" ID="AddConsumers" Width="80%" OnClick="AddConsumers_Click" />
                </td>
                <td class="auto-style3">
                    <asp:CheckBoxList runat="server" Width="100%" ID="ConsumersList" RepeatColumns="3">
                    </asp:CheckBoxList>
                </td>
            </tr>
        </table>
        <br />
        <br />
        <table>
            <tr>
                <td class="auto-style1">
                    <asp:Literal Text="שם רמה והמפלס העליון" runat="server" />
                </td>
                <td class="auto-style1">
                    <asp:TextBox runat="server" Width="100%" ID="NewNode" />
                </td>
                <td class="auto-style1">
                    <asp:DropDownList runat="server" Width="100%" ID="GetParent">
                    </asp:DropDownList>
                </td>
                <td class="auto-style1">
                    <asp:Button Text="הוספ כמפעל" runat="server" Width="30%" ID="NewAdd" OnClick="NewAdd_Click" Visible="False"/>
                    <asp:Button Text="הוסף" runat="server" Width="40%" ID="ForAdd" OnClick="ForAdd_Click" />
                </td>
            </tr>
        </table>
        <br />
        <div dir="rtl">
            <asp:Label runat="server" ForeColor="Red" Font-Bold="True" Font-Names="Arial" Font-Size="18px" ID="Alarm"/>
        </div>
    </div>
</asp:Content>

