﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class AdminPages_PlantLevels : System.Web.UI.Page
{
    string nop = "";
    int Alarms;
    int TL = 0;

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            Alarms = 0;//  
            GetUser.Items.Add("בחר משתמש");
            //string SQL = "SELECT [ID], [FirstName] + ' ' + [LastName] AS UName FROM [md2000Net].[dbo].[T_User]";
            string SQL = "SELECT -1 AS ID, 'בחר משתמש' AS UName UNION ALL SELECT [ID], [FirstName] + ' ' + [LastName] AS UName FROM[md2000Net].[dbo].[T_User]";
            GetUser.DataSource = AllFunctions.Populate(SQL);
            GetUser.DataTextField = "UName";
            GetUser.DataValueField = "ID";
            GetUser.DataBind();
            GetUser.SelectedIndex = -1;

        }
    }

    void FillGrid(string UID)
    {
        string SQL = "EXEC S_PlantLevelsGet " + UID + ", 0";
        DataTable dt = AllFunctions.Populate(SQL);

        Derevo.Nodes.Clear();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            TreeNode n0 = new TreeNode(dt.Rows[i][1].ToString(), dt.Rows[i][2].ToString());
            string un = dt.Rows[i][0].ToString();
            string tt = dt.Rows[i][5].ToString();
            if (un == "0")
            {
                n0.ToolTip = tt;
                Derevo.Nodes.Add(n0);
            }
            else
            {
                TreeNode nnt = GetNodeByID(un);
                if (dt.Rows[i][3].ToString() == "-1")
                {
                    n0.ShowCheckBox = true;
                }
                n0.ToolTip = tt;
                nnt.ChildNodes.Add(n0);
            }
        }

        SQL = " SELECT [LevelID] AS LID, [LevelName] AS Name FROM [dbo].[T_PlantLevels] WHERE [UserID] = " + UID
            + " UNION ALL SELECT[LevelID], [LevelName] FROM[dbo].[T_PlantLevels] WHERE[UpLevelID] IN "
            + "(SELECT[LevelID] FROM [dbo].[T_PlantLevels] WHERE[UserID] = " + UID + ")";

        GetParent.DataSource = AllFunctions.Populate(SQL);
        GetParent.DataTextField = "Name";
        GetParent.DataValueField = "LID";
        GetParent.DataBind();
        GetParent.SelectedIndex = -1;
        Derevo.ExpandDepth = 2;
        //Derevo.ExpandAll();
    }

    void GetConsumers(string UID)
    {
        string SQL = "";
        if ((UID == "13") || (UID == "27"))
        {
            SQL = "EXEC [dbo].[S_GetAllConsumersByUser] @AllConsumers = 4, @Lung = 0, @UserID = " + UID;
        }
        else
            SQL = "EXEC [dbo].[S_GetAllConsumersByUser] @AllConsumers = 3, @Lung = 0, @UserID = " + UID;
        ConsumersList.DataSource = AllFunctions.Populate(SQL);
        ConsumersList.DataTextField = "NAME";
        ConsumersList.DataValueField = "ID";
        ConsumersList.DataBind();
    }

    protected void GetUser_SelectedIndexChanged(object sender, EventArgs e)
    {
        string sd = GetUser.SelectedValue.ToString();
        FillGrid(sd);
        GetConsumers(sd);
    }

    TreeNode GetNodeByID(string sID)
    {
        TreeNode t0 = new TreeNode();
        string vps = "";
        for (int i = 0; i < Derevo.Nodes.Count; i++)
        {
            nop = "";
            vps = CheckNode(Derevo.Nodes[i], sID);
            if (vps != "")
            {
                t0 = Derevo.FindNode(vps);
            }
        }

        return t0;
    }

    string CheckNode(TreeNode cTn, string sFF)
    {
        if (cTn.Value.ToString() == sFF)
        {
            nop = cTn.ValuePath;
        }
        else
        {
            for (int j = 0; j < cTn.ChildNodes.Count; j++)
            {
                if (CheckNode(cTn.ChildNodes[j], sFF) != "")
                {
                    //nop = cTn.ChildNodes[j].ValuePath;
                    break;
                }
            }
        }
        return nop;
    }

    protected void ForAdd_Click(object sender, EventArgs e)
    {
        string SQL = "";
        if (NewNode.Text == "")
        {
            return;
        }

        if (GetParent.Items.Count == 0)
        {
            SQL = "INSERT INTO [dbo].[T_PlantLevels] (LevelName, UpLevelID, UserID, [OrderL1], [OrderL2], [OrderL3]) VALUES (N'"
                + NewNode.Text + "', NULL, " + GetUser.SelectedValue.ToString() + ", 0, 0, 0)";
            AllFunctions.DoExec(SQL);
        }
        else
        {
            if (GetParent.SelectedIndex != -1)
            {
                SQL = "SELECT dbo.GetLevels1and2ofPlants(" + GetParent.SelectedValue.ToString() + ")";
                string nin = AllFunctions.GetScalar(SQL);
                SQL = "INSERT INTO [dbo].[T_PlantLevels] (LevelName, UpLevelID, UserID, [OrderL1], [OrderL2], [OrderL3] ) VALUES (N'"
                    + NewNode.Text + "', " + GetParent.SelectedValue.ToString() + ", NULL, " 
                    + nin.Substring(0, nin.Length - 2) + ", " + nin.Substring(nin.Length - 2) + ", 0)";
                AllFunctions.DoExec(SQL);
            }
            else
                return;
        }

        //AllFunctions.DoExec(SQL);
        FillGrid(GetUser.SelectedValue.ToString());
        GetConsumers(GetUser.SelectedValue.ToString());
        Alarm.Text = "";
        EditNode.Text = "";
        NewNode.Text = "";
    }

    protected void Derevo_SelectedNodeChanged(object sender, EventArgs e)
    {
        //string SS = "";
        TL = LevelNo(Derevo.SelectedNode.ToolTip);
        EditNode.Text = Derevo.SelectedNode.Text;
        if (TL == 4)
        {
            EngNode.Text = AllFunctions.GetScalar("SELECT ISNULL([EngName], '') AS EN FROM [dbo].[tbl_Consumers_0] WHERE [sql_Consumer_ID] = " + Derevo.SelectedValue);
            RusNode.Text = AllFunctions.GetScalar("SELECT ISNULL([RusName], '') AS RU FROM [dbo].[tbl_Consumers_0] WHERE [sql_Consumer_ID] = " + Derevo.SelectedValue);
            Numb.Text =  AllFunctions.GetScalar("SELECT [OrderC] FROM [dbo].[T_PlantLevelsToConsumers] WHERE [ConsumerID] = " + Derevo.SelectedValue);
            EditNode.Enabled = false;
            EngNode.Enabled = false;
        }
        else
        {
            EngNode.Text = AllFunctions.GetScalar("SELECT [EngName] FROM [dbo].[T_PlantLevels] WHERE [LevelID] = " + Derevo.SelectedValue);
            RusNode.Text = AllFunctions.GetScalar("SELECT [RusName] FROM [dbo].[T_PlantLevels] WHERE [LevelID] = " + Derevo.SelectedValue);
            if (TL == 3) Numb.Text = AllFunctions.GetScalar("SELECT [OrderL2] FROM [dbo].[T_PlantLevels] WHERE [LevelID] = " + Derevo.SelectedValue);
            if (TL < 3) Numb.Text = AllFunctions.GetScalar("SELECT [OrderL1] FROM [dbo].[T_PlantLevels] WHERE [LevelID] = " + Derevo.SelectedValue);
            
            EditNode.Enabled = true;
            EngNode.Enabled = true;
        }
    }

    protected void B1_Click(object sender, EventArgs e) //Cancel
    {
        Alarm.Text = "";
        EditNode.Text = "";
    }

    protected void B2_Click(object sender, EventArgs e) //Save
    {
        //if (Derevo.SelectedNode.ShowCheckBox == true)
        //{
        //    return;
        //}
        Alarm.Text = "";
        TreeNode td = Derevo.SelectedNode;
        if ((EditNode.Text == ""))
        {
            return;
        }

        TL = LevelNo(Derevo.SelectedNode.ToolTip);
        if (TL == 4)
        {
            AllFunctions.DoExec("UPDATE [dbo].[T_PlantLevelsToConsumers] SET [OrderC] = " + Numb.Text + " WHERE [ConsumerID] = " 
                + td.Value.ToString()) ;
        }

        if (TL == 3)
        {
            AllFunctions.DoExec("UPDATE  [dbo].[T_PlantLevels] SET LevelName = N'" + EditNode.Text + "', EngName = N'" 
                + EngNode.Text + "', [RusName] = N'" + RusNode.Text + "', [OrderL2] = " + Numb.Text + " WHERE LevelID = " + td.Value.ToString());
        }

        if (TL == 2)
        {
            AllFunctions.DoExec("UPDATE  [dbo].[T_PlantLevels] SET LevelName = N'" + EditNode.Text + "', EngName = N'" 
                + EngNode.Text + "', [RusName] = N'" + RusNode.Text + "', [OrderL1] = " + Numb.Text + " WHERE LevelID = " + td.Value.ToString());
            AllFunctions.DoExec("UPDATE  [dbo].[T_PlantLevels] SET [OrderL1] = " + Numb.Text + " WHERE UpLevelID = " 
                + td.Value.ToString());
        }
        if (TL == 1)
        {
            AllFunctions.DoExec("UPDATE  [dbo].[T_PlantLevels] SET LevelName = N'" + EditNode.Text + "', EngName = N'" 
                + EngNode.Text + "', [RusName] = N'" + RusNode.Text + "' WHERE LevelID = " + td.Value.ToString());
        }
        //string SQL = "UPDATE  [dbo].[T_PlantLevels] SET LevelName = N'" + EditNode.Text + "' WHERE LevelID = "
        //    + td.Value.ToString();
        //AllFunctions.DoExec(SQL);
        FillGrid(GetUser.SelectedValue.ToString());
        Alarm.Text = "";
        EditNode.Text = "";
        NewNode.Text = "";

    }

    protected void B3_Click(object sender, EventArgs e)
    {
        string SS = "";
        if (Derevo.SelectedNode.ShowCheckBox == true)
        {
            SS = "DELETE FROM  [dbo].[T_PlantLevelsToConsumers] WHERE [ConsumerID] = " 
                + Derevo.SelectedNode.Value.ToString();
            AllFunctions.DoExec(SS);
            string sd = GetUser.SelectedValue.ToString();
            FillGrid(sd);
            GetConsumers(sd);

            Alarm.Text = "";
            EditNode.Text = "";
            NewNode.Text = "";
            return;
        }

        Alarms++;
        if (B3.CommandArgument == "0")
        {
            B3.CommandArgument = "1";
            int cnds = Derevo.SelectedNode.ChildNodes.Count;
            if (cnds > 0) SS = "This node (" + Derevo.SelectedNode.Text + ") contains child nodes."
                  + "To remove it and all the chain lower - click the button 'הסר'";
            else SS = "To remove node - click the button 'הסר'";
            Alarm.Text = SS;
            return;
        }
        else
        {
            if (B3.CommandArgument == "1")
            {
                B3.CommandArgument = "0";
                Alarms = 0;
                Alarm.Text = "";
                TreeNode td = Derevo.SelectedNode;

                string SQL = "EXEC [dbo].[S_PlantLevelsRemove] " + td.Value.ToString();
                AllFunctions.DoExec(SQL);

                FillGrid(GetUser.SelectedValue.ToString());
            }
        }
        Alarm.Text = "";
        EditNode.Text = "";
        NewNode.Text = "";
    }

    protected void AddConsumers_Click(object sender, EventArgs e)
    {
        string SQL = "";
        int cit = 0;
        for (int i = 0; i < ConsumersList.Items.Count; i++)
        {
            if (ConsumersList.Items[i].Selected)
            {
                cit++;
            }
        }
        if ((Derevo.SelectedNode == null) || (cit == 0))
        {
            return;
        }


        for (int i = 0; i < ConsumersList.Items.Count; i++)
        {
            if (ConsumersList.Items[i].Selected)
            {
                SQL = "INSERT INTO T_PlantLevelsToConsumers (PlantLevelID, ConsumerID) VALUES ("
                    + Derevo.SelectedValue.ToString() + ", "
                    + ConsumersList.Items[i].Value.ToString() + ")" ;
                AllFunctions.DoExec(SQL);

            }
        }

        string sd = GetUser.SelectedValue.ToString();
        FillGrid(sd);
        GetConsumers(sd);
    }

    protected void NewAdd_Click(object sender, EventArgs e)
    {
        //string SQL = "INSERT INTO [dbo].[T_PlantLevels] (LevelName, UpLevelID, UserID) VALUES (N'"
        //    + NewNode.Text + "', NULL, " + GetUser.SelectedValue.ToString() + ")";

        //AllFunctions.DoExec(SQL);
        //FillGrid(GetUser.SelectedValue.ToString());
        //GetConsumers(GetUser.SelectedValue.ToString());
        //Alarm.Text = "";
        //EditNode.Text = "";
        //NewNode.Text = "";
    }

    private int LevelNo(string TTT)
    {
        int ln = 0;
        string SS = "";

        if (TTT == "0")
        {
            return 1;
        }

        SS = TTT.Substring(TTT.Length - 2);
        if (SS != "00")
        {
            ln = 4;
        }
        else
        {
            SS = TTT.Substring(TTT.Length - 4, 2);
            if (SS != "00")
            {
                ln = 3;
            }
            else
            {
            SS = TTT.Substring(0, 2);
                if (SS != "00")
                {
                    ln = 2;
                }
                else ln = 1;
            }
        }
        return ln;
    }


}