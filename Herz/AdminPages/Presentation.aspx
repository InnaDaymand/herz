﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminPages/AdminPage.master" AutoEventWireup="true" CodeFile="Presentation.aspx.cs" Inherits="AdminPages_Presentation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="PresentationSQL">
        <Columns>
            <asp:BoundField DataField="שם הלקוח" HeaderText="שם הלקוח" ReadOnly="True" SortExpression="שם הלקוח" />
            <asp:BoundField DataField="שם הצרחן" HeaderText="שם הצרחן" ReadOnly="True" SortExpression="שם הצרחן" />
            <asp:BoundField DataField="ID" HeaderText="ID" SortExpression="ID" />
            <asp:BoundField DataField="שם הלקוח בסיס" HeaderText="שם הלקוח בסיס" ReadOnly="True" SortExpression="שם הלקוח בסיס" />
            <asp:BoundField DataField="שם הצרחן בסיס" HeaderText="שם הצרחן בסיס" ReadOnly="True" SortExpression="שם הצרחן בסיס" />
            <asp:BoundField DataField="Basic ID" HeaderText="Basic ID" SortExpression="Basic ID" />
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="PresentationSQL" runat="server" ConnectionString="<%$ ConnectionStrings:Money_DisplayConnectionString3 %>" SelectCommand="

SELECT 
(SELECT T_Client.Name
FROM T_Client INNER JOIN
tbl_Consumers_0 ON T_Client.ID = tbl_Consumers_0.sql_clientID
WHERE (tbl_Consumers_0.sql_Consumer_ID = CompCons)) AS 'שם הלקוח',

(SELECT sql_Consumer_name
FROM tbl_Consumers_0 
WHERE (sql_Consumer_ID = CompCons)) AS 'שם הצרחן',

CompCons AS 'ID', 

(SELECT T_Client.Name
FROM T_Client INNER JOIN
tbl_Consumers_0 ON T_Client.ID = tbl_Consumers_0.sql_clientID
WHERE (tbl_Consumers_0.sql_Consumer_ID = ElemCons)) AS 'שם הלקוח בסיס',

(SELECT sql_Consumer_name
FROM tbl_Consumers_0 
WHERE (sql_Consumer_ID = ElemCons)) AS 'שם הצרחן בסיס',

ElemCons AS 'Basic ID'
FROM V_CompoundCounters
WHERE CompCons IN (
SELECT sql_Consumer_ID
FROM tbl_Consumers_0
WHERE (sql_clientID IN (SELECT  ID FROM T_Client
WHERE PowerUserID = (SELECT ID FROM T_User WHERE (UserName = 'מצגת')))))

"></asp:SqlDataSource>
</asp:Content>

