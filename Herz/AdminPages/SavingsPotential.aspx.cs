﻿using System;
using System.Web.UI.WebControls;
using System.Data;

public partial class AdminPages_SavingsPotential : System.Web.UI.Page
{

    int RowNo;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["SearchByDeviceID"] = "0";
            RowNo = -1;
            string SQL = "SELECT [TypeID], [Name] FROM [ForKrakovich].[dbo].[ProfileType]";
            nTypeID.DataSource = AllFunctions.Populate(SQL);
            nTypeID.DataTextField = "Name";
            nTypeID.DataValueField = "TypeID";
            //nTypeID.SelectedIndex = -1;
            nTypeID.DataBind();

            SQL = "SELECT [ID], [LastName] FROM [md2000Net].[dbo].[T_User]";
            nUsID.DataSource = AllFunctions.Populate(SQL);
            nUsID.DataTextField = "LastName";
            nUsID.DataValueField = "ID";
            nUsID.DataBind();

/*
            SQL = "";
            cmbCons.DataSource = AllFunctions.Populate("EXEC [dbo].[S_GetAllConsumersByUser] 6, 1");
            cmbCons.DisplayMember = "NAME";
            cmbCons.ValueMember = "ID";

            chListConsumers.DataSource = AllFunctions.Populate("EXEC [dbo].[S_GetAllConsumersByUser] 6, 1");
            chListConsumers.DisplayMember = "NAME";
            chListConsumers.ValueMember = "ID";
*/

            //nUsID.SelectedIndex = -1;
            FillGrid();
        }
    }

    protected void DGV_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Haraha")
        {
            RowNo = Convert.ToInt32(e.CommandArgument);
            PrID.Text = DGV.DataKeys[RowNo].Value.ToString();
            FillByID(RowNo);
            string sa = DGV.Rows[RowNo].Cells[4].Text;
            switch (sa)
            {
                case "By Devices":
                    FillDevices();
                    MultiView1.ActiveViewIndex = 0;
                    break;
                case "By Taoz":
                    MultiView1.ActiveViewIndex = 1;
                    break;
                case "Less than Average":
                    MultiView1.ActiveViewIndex = 3;
                    FillUnilever();
                    break;
                default:
                    FillCalendar();
                    MultiView1.ActiveViewIndex = 2;
                    break;
            }
        }

        if (e.CommandName == "Mahak")
        {
            RowNo = Convert.ToInt32(e.CommandArgument);
            string kr = DGV.DataKeys[RowNo].Value.ToString();
            string SQL = "EXEC [ForKrakovich].[dbo].[RemoveProfile] @ID = " + kr;
            AllFunctions.DoExec(SQL);
            RowNo = -1;
            PrID.Text = "";
            FillGrid();
        }
    }

    private void FillByID(int rID)
    {
        UsID.Text = DGV.Rows[rID].Cells[2].Text;
        Name.Text = DGV.Rows[rID].Cells[3].Text;
        TypeID.Text = DGV.Rows[rID].Cells[4].Text;
        Note.Text = DGV.Rows[rID].Cells[5].Text;
        PrID.Text = DGV.DataKeys[rID].Value.ToString();
    }

    protected void UpdateIt_Click(object sender, EventArgs e)
    {
        string kr = PrID.Text;

        string SQL = "UPDATE [ForKrakovich].dbo.ProfileReport SET Name = N'" 
            + Name.Text + "', Note = N'" + Note.Text + "' WHERE PrID = " + kr ;
        AllFunctions.DoExec(SQL);

        RowNo = -1;
        FillGrid();

        UsID.Text = "";
        Name.Text = "";
        TypeID.Text = "";
        Note.Text = "";
        MultiView1.ActiveViewIndex = -1;
    }

    protected void Cancel_Click(object sender, EventArgs e)
    {
        RowNo = -1;
        UsID.Text = "";
        Name.Text = "";
        TypeID.Text = "";
        Note.Text = "";
        MultiView1.ActiveViewIndex = -1;
    }

    protected void AddIt_Click(object sender, EventArgs e)
    {
        if (nName.Text == "")
        {
            return;
        }

        string SQL = "INSERT INTO [ForKrakovich].dbo.ProfileReport (UsID, Name, TypeID, Note) VALUES ("
            + nUsID.SelectedValue.ToString() + ", N'" + nName.Text + "', " +nTypeID.SelectedValue.ToString() 
            + ", N'" + nNote.Text + "')";
        AllFunctions.DoExec(SQL);
        FillGrid();
    }

    protected void EmptyIt_Click(object sender, EventArgs e)
    {
        nName.Text = "";
        nNote.Text = "";
    }

    private void FillGrid()
    {
        PrID.Text = "";
        string SQL = "SELECT PR.UsID, PR.Name, PT.Name AS 'Profile Type', ISNULL(PR.Note, '') AS Note, PR.TypeID, PR.PrID, (SELECT [LastName] FROM T_User WHERE ID = PR.UsID) AS 'User Name' "
                     + " FROM [ForKrakovich].dbo.ProfileReport AS PR INNER JOIN[ForKrakovich].dbo.ProfileType AS PT ON PR.TypeID = PT.TypeID";
        DGV.DataSource = AllFunctions.Populate(SQL);
        DGV.DataBind();
    }

    private void FillDevices()
    {
        string SS = "";
        string kr = PrID.Text;
        string SQL = "DECLARE @I INT; SELECT @I = [UsID] FROM[ForKrakovich].[dbo].[ProfileReport] WHERE [PrID] = "
                + kr + "; EXEC [dbo].[S_GetAllConsumersByUser] @I, 1";
        DDL1.DataSource = AllFunctions.Populate(SQL);
        DDL1.DataTextField = "NAME";
        DDL1.DataValueField = "ID";
        DDL1.DataBind();

        DDL2.DataSource = AllFunctions.Populate(SQL);
        DDL2.DataTextField = "NAME";
        DDL2.DataValueField = "ID";
        DDL2.DataBind();

        chbConsumers.DataSource  = AllFunctions.Populate(SQL);
        chbConsumers.DataTextField = "NAME";
        chbConsumers.DataValueField = "ID";
        chbConsumers.DataBind();

        SQL = "SELECT [PSID],[FirstOper],[SecondOper],[SafterF],[SnotWorks],[IntervalAter],[IntervalBefore] "
            + " FROM [ForKrakovich].[dbo].[PrepareForSearch] WHERE [PrID] = " + kr;
        DataTable dt = AllFunctions.Populate(SQL);
        if (dt.Rows.Count > 0)
        {
            Session["SearchByDeviceID"] = dt.Rows[0][0].ToString();
            LT1.Text = dt.Rows[0][1].ToString();
            LT2.Text = dt.Rows[0][2].ToString();

            tbAfterOne.Text = dt.Rows[0][3].ToString();
            tbSecondNotWork.Text = dt.Rows[0][4].ToString();
            tbIntervalAfterTwo.Text = dt.Rows[0][5].ToString();
            tbIntervalBeforeOne.Text = dt.Rows[0][6].ToString();

            DDL1.SelectedValue = LT1.Text;
            DDL2.SelectedValue = LT2.Text;

            SQL = "SELECT Consumers FROM  [ForKrakovich].[dbo].[SearchConsumers] WHERE [PrID] = " + kr;
            DataTable dct = AllFunctions.Populate(SQL);

            for (int r = 0; r < dct.Rows.Count; r++)
            {
                SS = dct.Rows[r][0].ToString();
                for (int i = 0; i < chbConsumers.Items.Count; i++)
                {
                    if (chbConsumers.Items[i].Value.ToString() == SS)
                    {
                        chbConsumers.Items[i].Selected = true;
                    }
                }
            }
        }
    }

    /*
        tbAfterOne
        tbSecondNotWork
        tbIntervalAfterTwo
        tbIntervalBeforeOne
    */

    protected void SaveDefinitions_Click(object sender, EventArgs e)
    {
        string SbD = Session["SearchByDeviceID"].ToString();
        string SQL = "";
        if (SbD != "0")
        {
            SQL = "UPDATE [ForKrakovich].[dbo].[PrepareForSearch] SET "
                + "[FirstOper] = " + DDL1.SelectedValue.ToString() + ", "
                + "[SecondOper] = " + DDL2.SelectedValue.ToString() + ", "
                + "[SafterF] = " + tbAfterOne.Text + ", "
                + "[SnotWorks] = " + tbSecondNotWork.Text + ", "
                + "[IntervalAter] = " + tbIntervalAfterTwo.Text + ", "
                + "[IntervalBefore] = " + tbIntervalBeforeOne.Text
                + "WHERE PSID = " + SbD;
        }
        else
        {
            SQL = "INSERT INTO [ForKrakovich].[dbo].[PrepareForSearch] ([FirstOper], [SecondOper], [SafterF], "
                + "[SnotWorks], [IntervalAter], [IntervalBefore], [PrID]) VALUES(" + DDL1.SelectedValue.ToString() + ", " 
                + DDL2.SelectedValue.ToString() + ", " + tbAfterOne.Text + ", "
                + tbSecondNotWork.Text + ", " + tbIntervalAfterTwo.Text + ", " + tbIntervalBeforeOne.Text + ", " + PrID.Text + ")";
        }
        AllFunctions.DoExec(SQL);

        SQL = "DELETE FROM  [ForKrakovich].[dbo].[SearchConsumers] WHERE [PrID] = " + PrID.Text ;
        AllFunctions.DoExec(SQL);

        for (int i = 0; i < chbConsumers.Items.Count; i++)
        {
            if (chbConsumers.Items[i].Selected)
            {
                chbConsumers.Items[i].Selected = true;
                SQL = "INSERT INTO [ForKrakovich].[dbo].[SearchConsumers] (PrID, Consumers) VALUES (" + PrID.Text + ", "
                    + chbConsumers.Items[i].Value.ToString() + ")";
                AllFunctions.DoExec(SQL);
            }
        }

        FillGrid();
        MultiView1.ActiveViewIndex = -1;
    }

    private void FillCalendar()
    {
        string SS = "";
        string kr = PrID.Text;
        string SQL = "DECLARE @I INT; SELECT @I = [UsID] FROM[ForKrakovich].[dbo].[ProfileReport] WHERE [PrID] = "
                + kr + "; EXEC [dbo].[S_GetAllConsumersByUser] @I, 1";

        chbConsCalendar.DataSource = AllFunctions.Populate(SQL);
        chbConsCalendar.DataTextField = "NAME";
        chbConsCalendar.DataValueField = "ID";
        chbConsCalendar.DataBind();

        SQL = "SELECT [FromTime], [TillTime], [PcID], [DayOfWeek] FROM [ForKrakovich].[dbo].[PrepareCalendar] WHERE [PrID] = " + kr 
            + " ORDER BY [DayOfWeek]";
        DataTable dct = AllFunctions.Populate(SQL);

        ForDay1.Text = ForDay2.Text = ForDay3.Text = ForDay4.Text = ForDay5.Text = ForDay6.Text = ForDay7.Text = "";
        Dur1.Text = Dur2.Text = Dur3.Text = Dur4.Text = Dur5.Text = Dur6.Text = Dur7.Text = "";
        ID1.Text = ID2.Text = ID3.Text = ID4.Text = ID5.Text = ID6.Text = ID7.Text = "";

        if (dct.Rows.Count > 0)
        {
            ForDay1.Text = dct.Rows[0][0].ToString();
            Dur1.Text = dct.Rows[0][1].ToString();
            ID1.Text = dct.Rows[0][2].ToString();

            ForDay2.Text = dct.Rows[1][0].ToString();
            Dur2.Text = dct.Rows[1][1].ToString();
            ID2.Text = dct.Rows[1][2].ToString();

            ForDay3.Text = dct.Rows[2][0].ToString();
            Dur3.Text = dct.Rows[2][1].ToString();
            ID3.Text = dct.Rows[2][2].ToString();

            ForDay4.Text = dct.Rows[3][0].ToString();
            Dur4.Text = dct.Rows[3][1].ToString();
            ID4.Text = dct.Rows[3][2].ToString();

            ForDay5.Text = dct.Rows[4][0].ToString();
            Dur5.Text = dct.Rows[4][1].ToString();
            ID5.Text = dct.Rows[4][2].ToString();

            ForDay6.Text = dct.Rows[5][0].ToString();
            Dur6.Text = dct.Rows[5][1].ToString();
            ID6.Text = dct.Rows[5][2].ToString();

            ForDay7.Text = dct.Rows[6][0].ToString();
            Dur7.Text = dct.Rows[6][1].ToString();
            ID7.Text = dct.Rows[6][2].ToString();

            SQL = "SELECT Consumers FROM [ForKrakovich].[dbo].SearchConsumers WHERE PrID = " + kr;
            dct = AllFunctions.Populate(SQL);

            for (int r = 0; r < dct.Rows.Count; r++)
            {
                SS = dct.Rows[r][0].ToString();
                for (int i = 0; i < chbConsCalendar.Items.Count; i++)
                {
                    if (chbConsCalendar.Items[i].Value.ToString() == SS)
                    {
                        chbConsCalendar.Items[i].Selected = true;
                    }
                }
            }
        }
    }

    protected void SaveCalendar_Click(object sender, EventArgs e)
    {
        string SQL = "";
        if ((ID1.Text == "0") || (ID1.Text == ""))
        {
            SQL = "INSERT INTO[ForKrakovich].[dbo].[PrepareCalendar] ([PrID], [DayOfWeek], [FromTime], [TillTime]) VALUES("
                + PrID.Text + ", 1," + ForDay1.Text + "," + Dur1.Text + ")";
            AllFunctions.DoExec(SQL);
            SQL = "INSERT INTO[ForKrakovich].[dbo].[PrepareCalendar] ([PrID], [DayOfWeek], [FromTime], [TillTime]) VALUES("
                + PrID.Text + ", 2, " + ForDay2.Text + ", " + Dur2.Text + ")";
            AllFunctions.DoExec(SQL);
            SQL = "INSERT INTO[ForKrakovich].[dbo].[PrepareCalendar] ([PrID], [DayOfWeek], [FromTime], [TillTime]) VALUES("
                + PrID.Text + ", 3, " + ForDay3.Text + ", " + Dur3.Text + ")";
            AllFunctions.DoExec(SQL);
            SQL = "INSERT INTO[ForKrakovich].[dbo].[PrepareCalendar] ([PrID], [DayOfWeek], [FromTime], [TillTime]) VALUES("
                + PrID.Text + ", 4, " + ForDay4.Text + ", " + Dur4.Text + ")";
            AllFunctions.DoExec(SQL);
            SQL = "INSERT INTO[ForKrakovich].[dbo].[PrepareCalendar] ([PrID], [DayOfWeek], [FromTime], [TillTime]) VALUES("
                + PrID.Text + ", 5, " + ForDay5.Text + "," + Dur5.Text + ")";
            AllFunctions.DoExec(SQL);
            SQL = "INSERT INTO[ForKrakovich].[dbo].[PrepareCalendar] ([PrID], [DayOfWeek], [FromTime], [TillTime]) VALUES("
                + PrID.Text + ", 6, " + ForDay6.Text + "," + Dur6.Text + ")";
            AllFunctions.DoExec(SQL);
            SQL = "INSERT INTO[ForKrakovich].[dbo].[PrepareCalendar] ([PrID], [DayOfWeek], [FromTime], [TillTime]) VALUES("
                + PrID.Text + ", 7, " + ForDay7.Text + "," + Dur7.Text + ")";
            AllFunctions.DoExec(SQL);
        }
        else
        {
            SQL = "UPDATE [ForKrakovich].[dbo].[PrepareCalendar] SET [DayOfWeek] = 1, [FromTime] = " + ForDay1.Text + ", [TillTime] = "
                + Dur1.Text + " WHERE [PcID] = " + ID1.Text;
            AllFunctions.DoExec(SQL);
            SQL = "UPDATE [ForKrakovich].[dbo].[PrepareCalendar] SET [DayOfWeek] = 2, [FromTime] = " + ForDay2.Text + ", [TillTime] = "
                + Dur2.Text + " WHERE [PcID] = " + ID2.Text;
            AllFunctions.DoExec(SQL);
            SQL = "UPDATE [ForKrakovich].[dbo].[PrepareCalendar] SET [DayOfWeek] = 3, [FromTime] = " + ForDay3.Text + ", [TillTime] = "
                + Dur3.Text + " WHERE [PcID] = " + ID3.Text;
            AllFunctions.DoExec(SQL);
            SQL = "UPDATE [ForKrakovich].[dbo].[PrepareCalendar] SET [DayOfWeek] = 4, [FromTime] = " + ForDay4.Text + ", [TillTime] = "
                + Dur4.Text + " WHERE [PcID] = " + ID4.Text;
            AllFunctions.DoExec(SQL);
            SQL = "UPDATE [ForKrakovich].[dbo].[PrepareCalendar] SET [DayOfWeek] = 5, [FromTime] = " + ForDay5.Text + ", [TillTime] = "
                + Dur5.Text + " WHERE [PcID] = " + ID5.Text;
            AllFunctions.DoExec(SQL);
            SQL = "UPDATE [ForKrakovich].[dbo].[PrepareCalendar] SET [DayOfWeek] = 6, [FromTime] = " + ForDay6.Text + ", [TillTime] = "
                + Dur6.Text + " WHERE [PcID] = " + ID6.Text;
            AllFunctions.DoExec(SQL);
            SQL = "UPDATE [ForKrakovich].[dbo].[PrepareCalendar] SET [DayOfWeek] = 7, [FromTime] = " + ForDay7.Text + ", [TillTime] = "
                + Dur7.Text + " WHERE [PcID] = " + ID7.Text;
            AllFunctions.DoExec(SQL);
        }

        SQL = "DELETE FROM [ForKrakovich].[dbo].[SearchConsumers] WHERE PrID = " + PrID.Text;
        AllFunctions.DoExec(SQL);

        for (int i = 0; i < chbConsCalendar.Items.Count; i++)
        {
            if (chbConsCalendar.Items[i].Selected)
            {
                chbConsCalendar.Items[i].Selected = true;
                SQL = "INSERT INTO [ForKrakovich].[dbo].[SearchConsumers] (PrID, Consumers) VALUES (" + PrID.Text + ", "
                    + chbConsCalendar.Items[i].Value.ToString() + ")";
                AllFunctions.DoExec(SQL);
            }
        }

        FillGrid();
        MultiView1.ActiveViewIndex = -1;
    }

    protected void SelectAll_Click(object sender, EventArgs e)
    {
        bool ws = !chbConsCalendar.Items[0].Selected;
        for (int i = 0; i < chbConsCalendar.Items.Count; i++)
        {
            chbConsCalendar.Items[i].Selected = ws;
        }
    }

    protected void AddUnilever_Click(object sender, EventArgs e)
    {
        //PrID.Text = DGV.DataKeys[RowNo].Value.ToString();
        lbAtt.Text = "";
        string SQL = "SELECT COUNT(*) FROM  [ForKrakovich].[dbo].[ForUnilever] WHERE [PrRepID] = " + PrID.Text;
        if ((AllFunctions.GetScalar(SQL) == "1") && (UnileverID.Text.Length == 0))
        {
            lbAtt.Text = "לא להכניס חדש! העריך מטבלא";
            
            return;
        }

        int WhichDay = 0;
        //if (IsWeekday.Checked)
        //{
        //    WhichDay = 1;
        //}
        int IsKosher = 0;
        if (Kosher.Checked)
        {
            IsKosher = 1;
        }
        if (UnileverID.Text.Length == 0)
        {
            //TODO Save data
            SQL = "INSERT INTO [ForKrakovich].[dbo].[ForUnilever] ([PrRepID], [IsWeekdate], [IsKosher], [Percents] ) VALUES ("
                + PrID.Text + ", 0, " + Percent.Text + ", " + ParamH.Text + ")";
        }
        else
        {
            SQL = "UPDATE [ForKrakovich].[dbo].[ForUnilever] "
                + "SET [Param]= " + ParamH.Text + ","
                //+ "[IsKosher] = " + IsKosher.ToString() + ","
                + "[Percents] = " + Percent.Text
                 + " WHERE [fuID] = " + UnileverID.Text;
        }
        AllFunctions.DoExec(SQL);
        FillUnilever();
    }

    private void FillUnilever()
    {
        string SS = "";
        //IsWeekday.Enabled = true;
        //IsWeekday.Checked = false;
        UnileverID.Text = "";
        //FromDay.Text = "";
        //FromTime.Text = "";
        //ToDay.Text = "";
        //ToTime.Text = "";
        Kosher.Checked = false;
        Percent.Text = "100";
        string SQL = "SELECT [IsWeekdate], [IsKosher], [Percents], [Param], [fuID] FROM [ForKrakovich].[dbo].[ForUnilever] WHERE  [PrRepID] = " + PrID.Text;
        //string SQL = "SELECT [IsWeekdate], [FromDate], [FromTime], [TillDate], [TillTime], "
        //+ " [IsKosher], [Percent], [fuID] FROM [ForKrakovich].[dbo].[ForUnilever] WHERE  [PrRepID] = " 
        //+ PrID.Text + " ORDER BY 1 DESC, 8";

        FromTable.DataSource = AllFunctions.Populate(SQL);
        FromTable.DataBind();

        SQL = "DECLARE @I INT; SELECT @I = [UsID] FROM[ForKrakovich].[dbo].[ProfileReport] WHERE [PrID] = "
                + PrID.Text + "; EXEC [dbo].[S_GetAllConsumersByUser] @I, 1";

        chbConsUnilever.DataSource = AllFunctions.Populate(SQL);
        chbConsUnilever.DataTextField = "NAME";
        chbConsUnilever.DataValueField = "ID";
        chbConsUnilever.DataBind();

        SQL = "SELECT Consumers FROM [ForKrakovich].[dbo].SearchConsumers WHERE PrID = " + PrID.Text;
        DataTable dct = AllFunctions.Populate(SQL);

        for (int r = 0; r < dct.Rows.Count; r++)
        {
            SS = dct.Rows[r][0].ToString();
            for (int i = 0; i < chbConsUnilever.Items.Count; i++)
            {
                if (chbConsUnilever.Items[i].Value.ToString() == SS)
                {
                    chbConsUnilever.Items[i].Selected = true;
                }
            }
        }

    }

    protected void FromTable_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        lbAtt.Text = "";
        int RowFromTable = Convert.ToInt32(e.CommandArgument);
        if (e.CommandName == "ToEdit")
        {
            //FromDay.Text = FromTable.Rows[RowFromTable].Cells[3].Text;
            //FromTime.Text = FromTable.Rows[RowFromTable].Cells[4].Text;
            //ToDay.Text = FromTable.Rows[RowFromTable].Cells[5].Text;
            //ToTime.Text = FromTable.Rows[RowFromTable].Cells[6].Text;
            ParamH.Text = FromTable.Rows[RowFromTable].Cells[5].Text;
            Percent.Text = FromTable.Rows[RowFromTable].Cells[4].Text;
            UnileverID.Text = FromTable.Rows[RowFromTable].Cells[6].Text;
            //string SS = "SELECT  [IsWeekdate]  FROM [ForKrakovich].[dbo].[ForUnilever] WHERE [fuID] = " + UnileverID.Text;
            ////IsWeekday.Checked = Convert.ToBoolean( AllFunctions.GetScalar(SS));
            //SS = "SELECT  [IsKosher]  FROM [ForKrakovich].[dbo].[ForUnilever] WHERE [fuID] = " + UnileverID.Text;
            //Kosher.Checked = Convert.ToBoolean( AllFunctions.GetScalar(SS));
            //IsWeekday.Enabled = false;
        }
        if (e.CommandName == "ToDelete")
        {
            string SS = "DELETE FROM [ForKrakovich].[dbo].[ForUnilever] WHERE [fuID] = " + FromTable.Rows[RowFromTable].Cells[6].Text;
            AllFunctions.DoExec(SS);
            FillUnilever();
        }
    }

    protected void CancelIt_Click(object sender, EventArgs e)
    {
        FillUnilever();
    }

    protected void SaveConsumersUnilever_Click(object sender, EventArgs e)
    {

        string SQL = "DELETE FROM  [ForKrakovich].[dbo].[SearchConsumers] WHERE [PrID] = " + PrID.Text;
        AllFunctions.DoExec(SQL);

        for (int i = 0; i < chbConsUnilever.Items.Count; i++)
        {
            if (chbConsUnilever.Items[i].Selected)
            {
                chbConsUnilever.Items[i].Selected = true;
                SQL = "INSERT INTO [ForKrakovich].[dbo].[SearchConsumers] (PrID, Consumers) VALUES (" + PrID.Text + ", "
                    + chbConsUnilever.Items[i].Value.ToString() + ")";
                AllFunctions.DoExec(SQL);
            }
        }
    }

    protected void SelectAllUn_Click(object sender, EventArgs e)
    {
        bool ws = !chbConsUnilever.Items[0].Selected;
        for (int i = 0; i < chbConsUnilever.Items.Count; i++)
        {
            chbConsUnilever.Items[i].Selected = ws;
        }
    }
}