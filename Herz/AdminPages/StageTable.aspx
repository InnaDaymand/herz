﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminPages/AdminPage.master" AutoEventWireup="true" CodeFile="StageTable.aspx.cs" Inherits="AdminPages_StageTable" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:SqlDataSource ID="DataTemp" runat="server"
        ConnectionString="<%$ ConnectionStrings:Money_DisplayConnectionString3 %>" 
        SelectCommand="SELECT TOP 100 [CounterID]
                          ,[DateValue]
                          ,[DataValue]
                          ,[DateInsert]
                      FROM [dbo].[ZT_StagingTable] ORDER BY [DateValue] DESC">
    </asp:SqlDataSource>                  
<asp:GridView ID="TempView" runat="server" DataSourceID="DataTemp">
</asp:GridView>
</asp:Content>

