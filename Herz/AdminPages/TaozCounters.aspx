﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminPages/AdminPage.master" AutoEventWireup="true" CodeFile="TaozCounters.aspx.cs" Inherits="AdminPages_TaozCounters" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</ajaxToolkit:ToolkitScriptManager>
        <table class="LayoutTable">
        <tr>
            <th class="HeaderStyle">מוני תעו"ז</th>
        </tr>
        <tr>
            <td align="left" >
                <a href="../Help/HTaozCounters.aspx" target="_blank" style="color:#5d7b9d;"><img src="../images/help.gif" style="border:none" />עזרה</a>
            </td>
        </tr>
        <tr>
            <td>
                <table class="LayoutTable">
                    <tr>
                        <td valign="top">מונים ראשיים
                            <asp:LinqDataSource ID="LinqMaster" runat="server" 
                                ContextTypeName="DataClassesDataContext" 
                                Select="new (sql_counter_ID, sql_Consumer_ID, tbl_Consumers_0.sql_Consumer_name as ConsumerName, tbl_CounterType.sql_description as CounterType, T_Tariff.TariffName as TariffName)" 
                                TableName="tbl_Counters" Where="IsSplitted == true">
                            </asp:LinqDataSource>
                            <asp:GridView ID="GridMaster" runat="server" Width="100%"
                                AutoGenerateColumns="False"
                                DataSourceID="LinqMaster" 
                                DataKeyNames="sql_counter_ID" AllowSorting="True" 
                                >
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True" SelectText="בחר..." />
                                    <asp:BoundField DataField="sql_counter_ID" HeaderText="ID" 
                                        SortExpression="sql_counter_ID">
                                        <ItemStyle Width="30px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="CounterType" HeaderText="סוג" 
                                        SortExpression="CounterType" />
                                    <asp:BoundField DataField="ConsumerName" HeaderText="צרכן" 
                                        SortExpression="ConsumerName" />
                                    <asp:BoundField DataField="sql_Consumer_ID" HeaderText="" 
                                        Visible="false" />
                                    <asp:BoundField DataField="TariffName" HeaderText="תעריף" 
                                        SortExpression="TariffName" />
                                </Columns>
                                <HeaderStyle CssClass="DetailsFormViewHeader"></HeaderStyle>
                                <RowStyle ForeColor="#333333"></RowStyle>
                                <AlternatingRowStyle BackColor="#f7f6f3"></AlternatingRowStyle>
                                <SelectedRowStyle BackColor="#E2DED6" ForeColor="#284775"></SelectedRowStyle>
                            </asp:GridView>
                        </td>
                        <td valign="top">
                            מוני עומס
                            <asp:LinqDataSource ID="LinqDetails" runat="server"
                                ContextTypeName="DataClassesDataContext" 
                                onselecting="LinqDetails_Selecting" >
                                <WhereParameters>
                                    <asp:ControlParameter Name="counterId" ControlID="GridMaster" PropertyName="SelectedValue" Type="Int32" />
                                </WhereParameters>
                            </asp:LinqDataSource>
                            <asp:LinqDataSource ID="LinqLoad" runat="server" 
                                ContextTypeName="DataClassesDataContext" TableName="T_TaozLoads">
                            </asp:LinqDataSource>
                            <asp:LinqDataSource ID="LinqCounter" runat="server"
                                ContextTypeName="DataClassesDataContext" 
                                Select='new (sql_counter_ID, sql_counter_ID + ": " + T_Tariff.TariffName as TariffName)' 
                                TableName="tbl_Counters" 
                                Where="IsSplitted == false && sql_Consumer_ID == @consumerId" 
                                onselecting="LinqCounter_Selecting">
                                <WhereParameters>
                                    <asp:Parameter Name="consumerId" Type="Int32" />
                                </WhereParameters>
                            </asp:LinqDataSource>
                            <asp:GridView ID="GridDetails" runat="server" Width="100%"
                                AutoGenerateColumns="False"
                                DataSourceID="LinqDetails"
                                DataKeyNames="sql_counter_ID" 
                                onrowdeleting="GridDetails_RowDeleting" 
                                >
                                <Columns>
                                    <asp:TemplateField><ItemTemplate>
                                        <asp:LinkButton ID="LinkButton1" runat="server" 
                                            OnClientClick="return confirm('Are you sure you want to delete this record?');"
                                            CommandName="Delete">
                                            מחק
                                        </asp:LinkButton>
                                    </ItemTemplate></asp:TemplateField>
                                    <asp:BoundField DataField="LoadName" HeaderText="עומס" />
                                    <asp:BoundField DataField="sql_counter_ID" HeaderText="ID" 
                                        SortExpression="sql_counter_ID">
                                        <ItemStyle Width="30px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="TariffName" HeaderText="תעריף" 
                                        SortExpression="TariffName" />
                                </Columns>
                                <HeaderStyle CssClass="DetailsFormViewHeader"></HeaderStyle>
                                <RowStyle ForeColor="#333333"></RowStyle>
                                <AlternatingRowStyle BackColor="#f7f6f3"></AlternatingRowStyle>
                                <SelectedRowStyle BackColor="#E2DED6" ForeColor="#284775"></SelectedRowStyle>
                            </asp:GridView>
                            עומס
                            <asp:DropDownList ID="cmbLoads" runat="server" DataSourceID="LinqLoad" 
                                DataTextField="LoadName" DataValueField="LoadID">
                            </asp:DropDownList>&nbsp;
                            מונה
                            <asp:DropDownList ID="cmbCounters" runat="server" 
                            DataSourceID="LinqCounter" DataTextField="TariffName" DataValueField="sql_counter_ID">
                            </asp:DropDownList>&nbsp;
                            <asp:Button ID="btnAdd" runat="server" Text="Add" onclick="btnAdd_Click" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>    
</asp:Content>

