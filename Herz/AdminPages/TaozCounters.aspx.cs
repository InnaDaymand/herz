﻿using System;
using System.Linq;
using System.Web.UI.WebControls;

public partial class AdminPages_TaozCounters : SecuredPage
{
    private DataClassesDataContext db = new DataClassesDataContext();

    protected void Page_Load(object sender, EventArgs e)
    {
        /*int cID;
        var x = from c in db.tbl_Counters
                where
                    (from t in db.T_TaozCounters
                     where t.MainCounterID == cID
                     select t.CounterID).Contains(c.sql_counter_ID)
                select new
                {
                    c.sql_counter_ID,
                    TariffName = c.T_Tariff.TariffName
                };*/
        if (GridMaster.SelectedIndex < 0)
            GridMaster.SelectedIndex = 0;
    }
    protected void LinqDetails_Selecting(object sender, LinqDataSourceSelectEventArgs e)
    {/*
        var res = from c in db.tbl_Counters
                  where
                      (from t in db.T_TaozCounters
                       where t.MainCounterID == (int)e.WhereParameters["counterId"] //(int)GridMaster.SelectedValue
                       select t.CounterID).Contains(c.sql_counter_ID)
                  select new
                  {
                      c.sql_counter_ID,
                      TariffName = c.T_Tariff.TariffName
                  };
        e.Result = res;
        */
        var res = from t in db.T_TaozCounters
                  join c in db.tbl_Counters on t.CounterID equals c.sql_counter_ID
                  where t.MainCounterID == (int)e.WhereParameters["counterId"]
                  select new
                  {
                      t.T_TaozLoad.LoadName,
                      c.sql_counter_ID,
                      TariffName = c.T_Tariff.TariffName
                  };
        e.Result = res;

        try
        {
            cmbCounters.DataBind();
        }
        catch { }
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        try
        {
            T_TaozCounter tc = new T_TaozCounter();
            tc.CounterID = int.Parse(cmbCounters.SelectedValue);
            tc.MainCounterID = (int)GridMaster.SelectedValue;
            tc.LoadID = int.Parse(cmbLoads.SelectedValue);

            db.T_TaozCounters.InsertOnSubmit(tc);
            db.SubmitChanges();
        }
        catch { }

        GridDetails.DataBind();
    }
    protected void GridDetails_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        db.S_TaozCounter_Delete((int)e.Keys[0]);
        GridDetails.DataBind();
        e.Cancel = true;//cancel the event arrival to LinqDetails data source
    }
    protected void LinqCounter_Selecting(object sender, LinqDataSourceSelectEventArgs e)
    {
        GridMaster.Columns[4].Visible = true;
        e.WhereParameters["consumerId"] = int.Parse(GridMaster.SelectedRow.Cells[4].Text);
        GridMaster.Columns[4].Visible = false;
    }
}
