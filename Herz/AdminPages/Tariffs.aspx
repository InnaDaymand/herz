﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminPages/AdminPage.master" AutoEventWireup="true" CodeFile="Tariffs.aspx.cs" Inherits="AdminPages_Tariffs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%-- <asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>--%>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
רזולוציה<asp:TextBox ID="tbResol" runat="server" Width="30px" Text="0" 
        ontextchanged="tbResol_TextChanged" />דקות<asp:Button ID="Button1" 
        runat="server" Height="24px" Text="שמור" />
&nbsp;
<ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</ajaxToolkit:ToolkitScriptManager>    
    <table class="LayoutTable">
        <tr>
            <th class="HeaderStyle">תעריפים</th>
        </tr>
        <tr>
            <td align="left" >
                &nbsp;<a href="../Help/HTariffs.aspx" target="_blank" style="color:#5d7b9d;"><img src="../images/help.gif" style="border:none" />עזרה</a>
            
<%--            <td align="right" >
            </td>
--%>            
            </td>
        </tr>
        <tr>
            <td>
                <table class="LayoutTable">
                    <tr>
                        <td valign="top" >תעריפים
                            <asp:LinqDataSource ID="LinqTariffs" runat="server" 
                                ContextTypeName="DataClassesDataContext" 
                                Select="new (TariffID, TariffName, Period, T_TariffType.TariffTypeName as TariffType)" 
                                TableName="T_Tariffs">
                            </asp:LinqDataSource>
                            <asp:GridView ID="GridTariffs" runat="server" Width="100%"
                                AutoGenerateColumns="False"
                                DataSourceID="LinqTariffs" 
                                DataKeyNames="TariffID" 
                                >
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True" />
                                    <asp:BoundField DataField="TariffID" HeaderText="ID" 
                                        SortExpression="TariffID">
                                        <ItemStyle Width="30px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="TariffName" HeaderText="שם" 
                                        SortExpression="TariffName" />
                                    <asp:BoundField DataField="TariffType" HeaderText="סוג" 
                                        SortExpression="TariffType" />
                                    <asp:BoundField DataField="Period" HeaderText="מחזור" 
                                        SortExpression="Period" />
                                </Columns>
                                <HeaderStyle CssClass="DetailsFormViewHeader"></HeaderStyle>
                                <RowStyle ForeColor="#333333"></RowStyle>
                                <AlternatingRowStyle BackColor="#f7f6f3"></AlternatingRowStyle>
                                <SelectedRowStyle BackColor="#E2DED6" ForeColor="#284775"></SelectedRowStyle>
                            </asp:GridView>
                        </td>
                        <td valign="top" <%--width="20%"--%>>מדרגות\שלבים<%--Select="new (TariffStepID, Range)"--%>
                            <asp:LinqDataSource ID="LinqSteps" runat="server" 
                                ContextTypeName="DataClassesDataContext" TableName="T_TariffSteps" 
                                Where="TariffID == @TariffID" 
                                EnableUpdate="true">
                                <WhereParameters>
                                    <asp:ControlParameter ControlID="GridTariffs" 
                                        Name="TariffID" PropertyName="SelectedValue" Type="Int32" />
                                </WhereParameters>
                            </asp:LinqDataSource>
                            <asp:GridView ID="GridSteps" runat="server" Width="100%"
                                AutoGenerateColumns="False" 
                                DataSourceID="LinqSteps" 
                                DataKeyNames="TariffStepID">
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True" ShowEditButton="true"  />
                                    <asp:BoundField DataField="TariffStepID" HeaderText="ID" ReadOnly="true"
                                        SortExpression="TariffStepID">
                                        <ItemStyle Width="30px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Range" HeaderText="סף" 
                                        SortExpression="Range" />
                                </Columns>
                                <HeaderStyle CssClass="DetailsFormViewHeader"></HeaderStyle>
                                <RowStyle ForeColor="#333333"></RowStyle>
                                <AlternatingRowStyle BackColor="#f7f6f3"></AlternatingRowStyle>
                                <SelectedRowStyle BackColor="#E2DED6" ForeColor="#284775"></SelectedRowStyle>
                            </asp:GridView> 
                        </td>
                        <td valign="top">ערכים<%--Select="new (id, ValidFromDate, Value)"--%>
                            <asp:LinqDataSource ID="LinqValues" runat="server" 
                                ContextTypeName="DataClassesDataContext" 
                                TableName="T_TariffStepValues" 
                                Where="TariffStepID == @TariffStepID" 
                                OnInserted="LinqValues_Inserted" 
                                onselecting="LinqValues_Selecting" 
                                EnableUpdate="true" EnableInsert="true">
                                <WhereParameters>
                                    <asp:ControlParameter ControlID="GridSteps" Name="TariffStepID" 
                                        PropertyName="SelectedValue" Type="Int32" />
                                </WhereParameters> 
                            </asp:LinqDataSource>
                            <asp:GridView ID="GridValues" runat="server" Width="100%" 
                                DataSourceID="LinqValues" 
                                AutoGenerateColumns="False" 
                                AutoGenerateEditButton="true"
                                DataKeyNames="id" >
                                <Columns>
<%--                                    <asp:CommandField ShowSelectButton="True" SelectText="בחר..." />--%>
                                    <asp:BoundField DataField="id" HeaderText="ID" ReadOnly="true"
                                        SortExpression="id">
                                        <ItemStyle Width="30px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ValidFromDate" HeaderText="בתוקף מ" ReadOnly="true"
                                        DataFormatString="{0:d}" />
                                    <asp:BoundField DataField="Value" HeaderText="ערך" 
                                        />
                                </Columns>
                                <HeaderStyle CssClass="DetailsFormViewHeader"></HeaderStyle>
                                <RowStyle ForeColor="#333333"></RowStyle>
                                <AlternatingRowStyle BackColor="#f7f6f3"></AlternatingRowStyle>
                                <SelectedRowStyle BackColor="#E2DED6" ForeColor="#284775"></SelectedRowStyle>
                            </asp:GridView>
                            <div style="width:270px">
                                תאריך 
                                <asp:TextBox ID="txtNewDate" runat="server" Width="70px"/>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtNewDate" 
                                    ErrorMessage="הכנס תאריך" Display="Dynamic" Text="*" 
                                    ValidationGroup="NewValValidator" ID="RequiredFieldValidator1"></asp:RequiredFieldValidator>
                                <asp:Image ID="Image1" runat="server" ImageUrl="~/images/SmallCalendar.gif" />
                                <cc1:CalendarExtender ID="txtNewDate_CalendarExtender" runat="server" 
                                    Enabled="True" TargetControlID="txtNewDate" PopupButtonID="Image1" Format="yyyy-MM-dd">
                                </cc1:CalendarExtender>
                                ערך
                                <asp:TextBox ID="txtNewValue" runat="server" Width="50px"/>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtNewValue" 
                                    ErrorMessage="הכנס ערך" Display="Dynamic" Text="*" 
                                    ValidationGroup="NewValValidator" ID="RequiredFieldValidator2"></asp:RequiredFieldValidator>
                                <cc1:FilteredTextBoxExtender ID="txtNewValue_FilteredTextBoxExtender" 
                                    FilterType="Numbers,Custom" ValidChars="."
                                    runat="server" Enabled="True" TargetControlID="txtNewValue">
                                </cc1:FilteredTextBoxExtender>
                                <asp:Button ID="btnAddValue" runat="server" Text="Add" 
                                    onclick="btnAddValue_Click"  ValidationGroup="NewValValidator"/>
                            </div>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="NewValValidator" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

