﻿using System;
using System.Web.UI.WebControls;

public partial class AdminPages_Tariffs : SecuredPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (GridTariffs.SelectedIndex < 0)
            GridTariffs.SelectedIndex = 0;
        if (GridSteps.SelectedIndex < 0)
            GridSteps.SelectedIndex = 0;
    }
    protected void btnAddValue_Click(object sender, EventArgs e)
    {
        //string sel = LinqValues.Select;
        //LinqValues.Select = "";
        System.Collections.Specialized.ListDictionary dict = new System.Collections.Specialized.ListDictionary();
        dict.Add("TariffStepID",this.GridSteps.SelectedValue);
        dict.Add("ValidFromDate",this.txtNewDate.Text);
        dict.Add("Value",this.txtNewValue.Text);
        LinqValues.Insert(dict);
        //LinqValues.Select = sel;

        txtNewDate.Text = "";
        txtNewValue.Text = "";
    }
    protected void LinqValues_Inserted(object sender, LinqDataSourceStatusEventArgs e)
    {
        GridValues.DataBind();
        //FormViewDetails.DataBind();
    }
    protected void LinqValues_Selecting(object sender, LinqDataSourceSelectEventArgs e)
    {
        if (GridSteps.Rows.Count == 0)
        {
            e.Cancel = true;
            btnAddValue.Enabled = false;
        }
        else
            btnAddValue.Enabled = true;
        /*if (e.WhereParameters["TariffStepID"] == null)
            e.WhereParameters["TariffStepID"] = -1;*/
    }
    protected void tbResol_TextChanged(object sender, EventArgs e)
    {
        Session["Resolution"] = tbResol.Text;
    }
}
