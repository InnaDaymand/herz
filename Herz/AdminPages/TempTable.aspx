﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminPages/AdminPage.master" AutoEventWireup="true" CodeFile="TempTable.aspx.cs" Inherits="AdminPages_TempTable" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>

<%@ Register assembly="eWorld.UI, Version=2.0.6.2393, Culture=neutral, PublicKeyToken=24d65337282035f2" namespace="eWorld.UI" tagprefix="ew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div align="center">

<ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</ajaxToolkit:ToolkitScriptManager>
                <table id="ManageTemp" runat="server" >
            <tr>
                <td>
                    <asp:CheckBox ID="chRun" runat="server" AutoPostBack="True" 
                        BorderStyle="Outset" Checked="True" oncheckedchanged="chRun_CheckedChanged" 
                        Text="לפעול" TextAlign="Left" Visible="False" />
                </td>
                <td>
                    <asp:Button ID="btnDel" runat="server" text="למחוק" onclick="btnDel_Click" />
                </td>
                <td>
                    עד-
                    <asp:TextBox ID="txtDateTo" runat="server" Width="80px" style="margin-right: 4px"></asp:TextBox>
                    <asp:Image ID="Image2" runat="server" ImageUrl="~/images/SmallCalendar.gif" />
                    <cc1:CalendarExtender ID="txtDateTo_CalendarExtender" runat="server" 
                        Enabled="True" TargetControlID="txtDateTo" PopupButtonID="Image2" 
                        Format="yyyy-MM-dd" FirstDayOfWeek="Sunday">
                    </cc1:CalendarExtender>
                 </td>
                 <td>
                     <asp:Button ID="btnFilter" runat="server" text="סינן" 
                         onclick="btnFilter_Click" />
                 </td>
                 <td>
                    <asp:TextBox ID="FilterValue" runat="server" Width="120px" 
                         ToolTip="example: TypeValue = 1 AND ConsID = 77" ></asp:TextBox>
                 </td>
                 <td>
                     <asp:Button ID="btnTop" runat="server" text="מוגבל" onclick="btnTop_Click" />
                 </td>
                 <td>
                    <asp:TextBox ID="TopRows" runat="server" Text="50" Width="50px" ></asp:TextBox>
                 </td>
                 <td>
                     <asp:DropDownList ID="StEn" runat="server" >
                         <asp:ListItem Value="1" Text="שורות מקודימה" />
                         <asp:ListItem Value="2" Text="שורות מאחורה" />
                     </asp:DropDownList>
                 </td>
            </tr>
        </table>
    </div>
    <div align="center">
    <asp:SqlDataSource ID="DataTemp" runat="server"
        ConnectionString="<%$ ConnectionStrings:Money_DisplayConnectionString3 %>" >
     </asp:SqlDataSource>
    <asp:GridView ID="GridView1" runat="server" DataSourceID="DataTemp">
    </asp:GridView>
    </div>
</asp:Content>

