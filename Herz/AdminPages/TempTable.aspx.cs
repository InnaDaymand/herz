﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class AdminPages_TempTable : System.Web.UI.Page
{
    //string tops = "";
    //string tail = "";
    //string where = "";
    string ForGrid = "CONVERT(decimal(18,0), [SentValue]) as ValueGet "
            + ", CONVERT(decimal(18,0), [CalcValue]) as ValueOut, [ConsID], [DateValue], [TypeValue], [FuncName]"
            + " FROM [dbo].[ZT_CalculationData]";

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            AllFunctions.tops = "";
            AllFunctions.tail = "";
            AllFunctions.where = "";
        }
        DataTemp.SelectCommand = "SELECT " + AllFunctions.tops + ForGrid + AllFunctions.where + AllFunctions.tail;
        GridView1.DataBind();

    }
    protected void chRun_CheckedChanged(object sender, EventArgs e)
    {
        //
    }
    protected void btnTop_Click(object sender, EventArgs e)
    {
        int rs = Convert.ToInt32(TopRows.Text);
        int se = Convert.ToInt32(StEn.SelectedValue);
        if (rs > 0)
        {
            if (se == 1)
            {
                AllFunctions.tops = " TOP " + rs.ToString();
                AllFunctions.tail = " ORDER BY [DateValue] ";
            }
            else
            {
                AllFunctions.tops = " TOP " + rs.ToString();
                AllFunctions.tail = " ORDER BY [DateValue] DESC ";
            }
        }
        DataTemp.SelectCommand = "SELECT " + AllFunctions.tops + ForGrid + AllFunctions.where + AllFunctions.tail;
        GridView1.DataBind();
    }
    protected void btnFilter_Click(object sender, EventArgs e)
    {
        if (FilterValue.Text == "")
        {
            AllFunctions.where = "";
        }
        else
        {
            AllFunctions.where = " WHERE " + FilterValue.Text + " ";
        }
        DataTemp.SelectCommand = "SELECT " + AllFunctions.tops + ForGrid + AllFunctions.where + AllFunctions.tail;
        GridView1.DataBind();
    }
    protected void btnDel_Click(object sender, EventArgs e)
    {
        string SQL = "";
        DateTime dm;
        if (DateTime.TryParse(txtDateTo.Text, out dm))
        {
            SQL = "DELETE FROM [dbo].[ZT_CalculationData] WHERE DateValue < '" + dm.ToString("yyyy-MM-dd") + "'";

            string CS = DataTemp.ConnectionString;

            using (SqlConnection CN = new SqlConnection(CS))
            {
                SqlCommand command = new SqlCommand(SQL, CN);
                CN.Open();
                command.ExecuteNonQuery();
            }
        }
    }
}