﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminPages/AdminPage.master" AutoEventWireup="true" CodeFile="Users.aspx.cs" Inherits="AdminPages_Users" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%-- <asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>--%>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</ajaxToolkit:ToolkitScriptManager>
    <table class="LayoutTable">
        <tr>
            <th class="HeaderStyle">משתמשים</th>
        </tr>
        <tr>
            <td align="left" >
                <a href="../Help/HUsers.aspx" target="_blank" style="color:#5d7b9d;"><img alt="Help" src="../images/help.gif" style="border:none" />עזרה</a>
            </td>
        </tr>
        <tr>
            <td>
                <table class="LayoutTable">
                    <tr>
                        <td valign="top" align="center">
                            <asp:LinqDataSource ID="LinqMaster" runat="server" 
                                ContextTypeName="DataClassesDataContext" 
                                Select="new (ID, FirstName, LastName)" 
                                TableName="T_Users">
                            </asp:LinqDataSource>
                            <asp:GridView ID="GridMaster" runat="server" Width="100%"
                                AutoGenerateColumns="False"
                                DataSourceID="LinqMaster"
                                DataKeyNames="ID" >
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True" SelectText="בחר..." />
                                    <asp:BoundField DataField="ID" HeaderText="ID" 
                                        SortExpression="ID" >
                                        <ItemStyle Width="30px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="FirstName" HeaderText="שם פרטי" 
                                        SortExpression="FirstName" />
                                    <asp:BoundField DataField="LastName" HeaderText="שם משפחה" 
                                        SortExpression="LastName" />
                                </Columns>
                                <HeaderStyle CssClass="DetailsFormViewHeader"></HeaderStyle>
                                <RowStyle ForeColor="#333333"></RowStyle>
                                <AlternatingRowStyle BackColor="#f7f6f3"></AlternatingRowStyle>
                                <SelectedRowStyle BackColor="#E2DED6" ForeColor="#284775"></SelectedRowStyle>
                            </asp:GridView>
                            <br />דוא"ל
                            <br />
                                <asp:GridView ID="GridMails" runat="server" DataSourceID="DataSource1" > 
                                </asp:GridView>
                                <asp:SqlDataSource 
                                    ID="DataSource1" 
                                    runat="server"
                                    ConnectionString="<%$ ConnectionStrings:Money_DisplayConnectionString3 %>"
                                    SelectCommand="SELECT FirstName + N' ' + LastName AS Name, T_Mails.Email 
                                        FROM T_User INNER JOIN T_Mails ON T_User.ID = T_Mails.UserID">
                                </asp:SqlDataSource>
                                <asp:TextBox ID="AddMail" runat="server" />
                                <asp:Button Text="Add" runat="server" ID="MailAdd" 
                                onclick="MailAdd_Click" />
                            <br />
                            <asp:SqlDataSource ID="DataSourceSMS" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:Money_DisplayConnectionString3 %>"
                                SelectCommand="SELECT FirstName + ' ' + LastName as Name, ID  
                                    FROM T_PhoneNumbers AS TFN INNER JOIN T_User ON TFN.UserID = T_User.ID">
                            </asp:SqlDataSource>
                            <asp:SqlDataSource ID="DataSourceTel" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:Money_DisplayConnectionString3 %>"
                                SelectCommand="SELECT FirstName + ' ' + LastName as Name, TFN.TelNo  
                                    FROM T_PhoneNumbers AS TFN INNER JOIN T_User ON TFN.UserID = T_User.ID">
                            </asp:SqlDataSource>
                            <br />סמס
                            <br />
                            <br />
                                <asp:GridView ID="GridSMS" runat="server" DataSourceID="DataSourceTel" >
                                </asp:GridView>
                            <br />
                            <asp:TextBox runat="server" ID="TelNo" />
                            <asp:Button Text="Add" runat="server" ID="SMSAdd" 
                                onclick="SMSAdd_Click" />
                            <!--<asp:LinkButton ID="btnNew" runat="server" Text="חדש..." />-->
                        </td>
                        <td valign="top" width="50%">
                            <asp:LinqDataSource ID="LinqDetails" runat="server" 
                                ContextTypeName="DataClassesDataContext" 
                                TableName="T_Users" 
                                Where="ID == @ID" 
                                EnableDelete="True" 
                                EnableInsert="True" 
                                EnableUpdate="True" 
                                OnInserted="LinqDetails_Inserted">
                                <WhereParameters>
                                    <asp:ControlParameter ControlID="GridMaster" DefaultValue="0" 
                                        Name="ID" PropertyName="SelectedValue" Type="Int32" />
                                </WhereParameters>
                            </asp:LinqDataSource>
                            <asp:LinqDataSource ID="LinqRoles" runat="server"
                                ContextTypeName="DataClassesDataContext"
                                TableName="T_Roles"
                                Select="new ( ID, Name )"
                                >
                            </asp:LinqDataSource>
                            <asp:FormView ID="FormViewDetails" runat="server" Width="100%" 
                                DataKeyNames="ID" 
                                DataSourceID="LinqDetails" 
                                HeaderText="פרטים" 
                                HeaderStyle-CssClass="DetailsFormViewHeader"
                                CssClass="DetailsFormView" 
                                OnItemUpdated="FormViewDetails_ItemUpdated"
                                OnItemDeleted="FormViewDetails_ItemDeleted" 
                                onmodechanged="FormViewDetails_ModeChanged" >
                                <EditItemTemplate>
                                    <table class="DetailsFormViewLayout">
                                        <tr>
                                            <td>
                                                מס' משתמש
                                            </td>
                                            <td>
                                                <asp:Label ID="sql_clientIDLabel1" runat="server" 
                                                    Text='<%# Eval("ID") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                שם פרטי
                                            </td>
                                            <td>
                                                <asp:TextBox ID="sql_first_nameTextBox" runat="server" 
                                                    Text='<%# Bind("FirstName") %>' />
                                                <asp:RequiredFieldValidator runat="server" ControlToValidate="sql_first_nameTextBox" 
                                                    ErrorMessage="הכנס שם פרטי" Display="Dynamic" Text="*" 
                                                    ValidationGroup="AllValidators" ID="RequiredFieldValidator1"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                שם משפחה
                                            </td>
                                            <td>
                                                <asp:TextBox ID="sql_last_nameTextBox" runat="server" 
                                                    Text='<%# Bind("LastName") %>' />
                                                <asp:RequiredFieldValidator runat="server" ControlToValidate="sql_last_nameTextBox" 
                                                    ErrorMessage="הכנס שם משפחה" Display="Dynamic" Text="*" 
                                                    ValidationGroup="AllValidators" ID="RequiredFieldValidator2"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                נייד
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtMobile" runat="server" 
                                                    Text='<%# Bind("Mobile") %>' />
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" 
                                                    FilterType="Numbers,Custom" ValidChars="+-() "
                                                    runat="server" Enabled="True" TargetControlID="txtMobile">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                        </tr>

<%--                                        <tr>
                                            <td>
                                                דוא"ל
                                            </td>
                                            <td>
                                                <asp:TextBox ID="TextBox10" runat="server" 
                                                    Text='<%# Bind("Email") %>' />
                                            </td>
                                        </tr>
--%>
                                        <tr>
                                            <td>
                                                שם משתמש
                                            </td>
                                            <td>
                                                <asp:TextBox ID="sql_usernameTextBox" runat="server" 
                                                    Text='<%# Bind("UserName") %>' />
                                                <asp:RequiredFieldValidator runat="server" ControlToValidate="sql_usernameTextBox" 
                                                    ErrorMessage="הכנס שם משתמש" Display="Dynamic" Text="*" 
                                                    ValidationGroup="AllValidators" ID="RequiredFieldValidator3"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                סיסמה
                                            </td>
                                            <td>
                                                <asp:TextBox ID="TextBox12" runat="server" 
                                                    Text='<%# Bind("Password") %>' />
                                            </td>
                                        </tr>
                                        <%--<tr>
                                            <td>
                                                סוג משתמש
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cmbRoles" runat="server"
                                                    DataSourceID="LinqRoles" 
                                                    DataTextField="Name" 
                                                    DataValueField="ID"
                                                    />
                                            </td>
                                        </tr>--%>

                                        <tr>
                                            <td>
                                                שם לתצוגה
                                            </td>
                                            <td>
                                                <asp:TextBox ID="TextBox1" runat="server" 
                                                    Text='<%# Bind("ShowNameHeb") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Display Name
                                            </td>
                                            <td>
                                                <asp:TextBox ID="TextBox3" runat="server" 
                                                    Text='<%# Bind("ShowNameEng") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Отображаемое имя
                                            </td>
                                            <td>
                                                <asp:TextBox ID="TextBox4" runat="server" 
                                                    Text='<%# Bind("ShowNameRus") %>' />
                                            </td>
                                        </tr>


                                        <tr>
                                            <td>
                                                סיסמה
                                            </td>
                                            <td>
                                                <asp:TextBox ID="TextBox2" runat="server" 
                                                    Text='<%# Bind("Password") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                שפת ברירת מחדל
                                            </td>
                                            <td>
                                                <asp:TextBox ID="DefLan" runat="server" 
                                                    Text='<%# Bind("DefaultLanguage") %>' />
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" 
                                        CommandName="Update" Text="שמור" ValidationGroup="AllValidators" />
                                    &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" 
                                        CausesValidation="False" CommandName="Cancel" Text="בטל" />
                                </EditItemTemplate>
                                <InsertItemTemplate>
                                    <table class="DetailsFormViewLayout">
                                        <tr>
                                            <td>
                                                שם פרטי
                                            </td>
                                            <td>
                                                <asp:TextBox ID="sql_first_nameTextBox" runat="server" 
                                                    Text='<%# Bind("FirstName") %>' />
                                                <asp:RequiredFieldValidator runat="server" ControlToValidate="sql_first_nameTextBox" 
                                                    ErrorMessage="הכנס שם פרטי" Display="Dynamic" Text="*" 
                                                    ValidationGroup="AllValidators" ID="RequiredFieldValidator1"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                שם משפחה
                                            </td>
                                            <td>
                                                <asp:TextBox ID="sql_last_nameTextBox" runat="server" 
                                                    Text='<%# Bind("LastName") %>' />
                                                <asp:RequiredFieldValidator runat="server" ControlToValidate="sql_last_nameTextBox" 
                                                    ErrorMessage="הכנס שם משפחה" Display="Dynamic" Text="*" 
                                                    ValidationGroup="AllValidators" ID="RequiredFieldValidator2"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                נייד
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtMobile" runat="server" 
                                                    Text='<%# Bind("Mobile") %>' />
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" 
                                                    FilterType="Numbers,Custom" ValidChars="+-() "
                                                    runat="server" Enabled="True" TargetControlID="txtMobile">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                דוא"ל
                                            </td>
                                            <td>
                                                <asp:TextBox ID="sql_emailTextBox" runat="server" 
                                                    Text='<%# Bind("Email") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                שם משתמש
                                            </td>
                                            <td>
                                                <asp:TextBox ID="sql_usernameTextBox" runat="server" 
                                                    Text='<%# Bind("UserName") %>' />
                                                <asp:RequiredFieldValidator runat="server" ControlToValidate="sql_usernameTextBox" 
                                                    ErrorMessage="הכנס שם משתמש" Display="Dynamic" Text="*" 
                                                    ValidationGroup="AllValidators" ID="RequiredFieldValidator3"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                סיסמה
                                            </td>
                                            <td>
                                                <asp:TextBox ID="sql_passwordTextBox" runat="server" 
                                                    Text='<%# Bind("Password") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                סוג משתמש
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cmbRoles" runat="server"
                                                    DataSourceID="LinqRoles" 
                                                    DataTextField="Name" 
                                                    DataValueField="ID"
                                                    />
                                            </td>
                                        </tr>
                                        <tr> 
                                            <td colspan = "2">
                                                <asp:ListBox ID="ListMailes" runat="server"></asp:ListBox>
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" 
                                        CommandName="Insert" Text="שמור" ValidationGroup="AllValidators" />
                                    &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" 
                                        CausesValidation="False" CommandName="Cancel" Text="בטל" />
                                </InsertItemTemplate>
                                <ItemTemplate>
                                    <table class="DetailsFormViewLayout" >
                                        <tr>
                                            <td>
                                                מס' משתמש
                                            </td>
                                            <td>
                                                <asp:Label ID="sql_clientIDLabel" runat="server" 
                                                    Text='<%# Eval("ID") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                שם פרטי
                                            </td>
                                            <td>
                                                <asp:Label ID="sql_first_nameLabel" runat="server" 
                                                    Text='<%# Bind("FirstName") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                שם משפחה
                                            </td>
                                            <td>
                                                <asp:Label ID="sql_last_nameLabel" runat="server" 
                                                    Text='<%# Bind("LastName") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                נייד
                                            </td>
                                            <td>
                                                <asp:Label ID="sql_mobileLabel" runat="server" 
                                                    Text='<%# Bind("Mobile") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                דוא"ל
                                            </td>
                                            <td>
                                                <asp:Label ID="sql_emailLabel" runat="server" 
                                                    Text='<%# Bind("Email") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                שם משתמש
                                            </td>
                                            <td>
                                                <asp:Label ID="sql_usernameLabel" runat="server" 
                                                    Text='<%# Bind("UserName") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                סיסמה
                                            </td>
                                            <td>
                                                <asp:Label ID="sql_passwordLabel" runat="server" 
                                                    Text='<%# Bind("Password") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                שם לתצוגה
                                            </td>
                                            <td>
                                                <asp:Label ID="Sql_Heb" runat="server" 
                                                    Text='<%# Eval("ShowNameHeb") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Display Name
                                            </td>
                                            <td>
                                                <asp:Label ID="Sql_Eng" runat="server" 
                                                    Text='<%# Eval("ShowNameEng") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Отображаемое имя
                                            </td>
                                            <td>
                                                <asp:Label ID="Sql_Rus" runat="server" 
                                                    Text='<%# Eval("ShowNameRus") %>' />
                                            </td>
                                        </tr>
                                    </table>
                                    <asp:CustomValidator ID="DelValidator" runat="server" Display="Dynamic"
                                        EnableClientScript="false" ValidationGroup="AllValidators" Text="*" >
                                    </asp:CustomValidator>
                                    <br />
                                    <asp:LinkButton ID="btnNew" runat="server" Text="חדש" CommandName="New" />&nbsp;
                                    <asp:LinkButton ID="btnEdit" runat="server" Text="עריכה" CommandName="Edit" />&nbsp;
                                    <asp:LinkButton ID="btnDelete" runat="server" Text="מחק" CommandName="Delete" OnClientClick="return confirm('are you sure?');" />
                                </ItemTemplate>
                            </asp:FormView>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="AllValidators" />
                        
                            <br />
                            <asp:Panel ID="pnlClients" runat="server">
                                <asp:Label ID="lblClients" runat="server" CssClass="DetailsFormViewHeader"
                                    Width="100%" Text="רשאי לראות לקוחות"></asp:Label>
                                <asp:LinqDataSource ID="LinqClientUsers" runat="server"
                                    ContextTypeName="DataClassesDataContext"
                                    TableName="T_ClientUsers"
                                    Where="UserID = @UserID"
                                    Select="new ( ID, ClientID, T_Client.Name as name )" 
                                    >
                                    <WhereParameters>
                                        <asp:ControlParameter ControlID="GridMaster" DefaultValue="0" 
                                            Name="UserID" PropertyName="SelectedValue" Type="Int32" />
                                    </WhereParameters>
                                </asp:LinqDataSource>
                                <asp:GridView ID="GridClients" runat="server" Width="100%"
                                    AutoGenerateColumns="False"
                                    DataSourceID="LinqClientUsers"
                                    onrowdeleting="GridClients_RowDeleting" 
                                    DataKeyNames="ID">
                                    <Columns>
                                        <asp:TemplateField><ItemTemplate>
                                            <asp:LinkButton ID="LinkButton1" runat="server" 
                                                OnClientClick="return confirm('Are you sure you want to delete this record?');"
                                                CommandName="Delete">
                                                הסר
                                            </asp:LinkButton>
                                        </ItemTemplate></asp:TemplateField>
                                        <asp:BoundField DataField="ClientID" HeaderText="ID" 
                                            SortExpression="ClientID" >
                                            <ItemStyle Width="30px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="name" HeaderText="שם הלקוח" 
                                            SortExpression="name" />
                                    </Columns>
                                    <%--<HeaderStyle CssClass="DetailsFormViewHeader"></HeaderStyle>--%>
                                    <RowStyle ForeColor="#333333"></RowStyle>
                                    <AlternatingRowStyle BackColor="#f7f6f3"></AlternatingRowStyle>
                                </asp:GridView>
                                <asp:LinqDataSource ID="LinqClients" runat="server"
                                    ContextTypeName="DataClassesDataContext"
                                    TableName="T_Clients"
                                    Select="new ( ID, Name )"
                                    >
                                </asp:LinqDataSource>
                                <asp:DropDownList ID="cmbClients" runat="server"
                                    DataSourceID="LinqClients" 
                                    DataTextField="Name" 
                                    DataValueField="ID"
                                    />
                                <asp:Button ID="btnAdd" runat="server" Text="הסף לרשימה" onclick="btnAdd_Click" />
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

