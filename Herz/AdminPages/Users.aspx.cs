﻿using System;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class AdminPages_Users : SecuredPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (_db == null)
            _db = new DataClassesDataContext();

        if (GridMaster.SelectedIndex < 0)
            GridMaster.SelectedIndex = 0;

    }
    protected void FormViewDetails_ItemUpdated(object sender, EventArgs e)
    {
        GridMaster.DataBind();
    }
    protected void FormViewDetails_ItemDeleted(object sender, FormViewDeletedEventArgs e)
    {
        if (e.Exception != null)
        {
            CustomValidator v = (CustomValidator)this.FormViewDetails.FindControl("DelValidator");
            v.ErrorMessage = e.Exception.Message;
            v.IsValid = false;
            //Response.Write(e.Exception.Message);

            e.ExceptionHandled = true;
        }
        GridMaster.DataBind();
    }
    protected void LinqDetails_Inserted(object sender, LinqDataSourceStatusEventArgs e)
    {
        DropDownList list = (DropDownList)this.FormViewDetails.FindControl("cmbRoles");
        T_RoleUser ru = new T_RoleUser();
        //ru.ID = 0;
        ru.RoleID = int.Parse(list.SelectedValue);
        ru.UserID = (e.Result as T_User).ID;
        _db.T_RoleUsers.InsertOnSubmit(ru);
        _db.SubmitChanges();

        GridMaster.DataBind();
        FormViewDetails.DataBind();
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        try
        {
            T_ClientUser cu = new T_ClientUser();
            cu.ClientID = int.Parse(cmbClients.SelectedValue);
            cu.UserID = (int)GridMaster.SelectedValue;

            _db.T_ClientUsers.InsertOnSubmit(cu);
            _db.SubmitChanges();
        }
        catch { }

        GridClients.DataBind();
    }
    protected void GridClients_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        _db.ExecuteCommand("DELETE FROM T_ClientUser WHERE ID = {0}", e.Keys[0]);
        GridClients.DataBind();
        e.Cancel = true;//cancel the event arrival to LinqDetails data source*/
    }
    protected void FormViewDetails_ModeChanged(object sender, EventArgs e)
    {
        if (FormViewDetails.CurrentMode == FormViewMode.ReadOnly)
            pnlClients.Visible = true;
        else
            pnlClients.Visible = false;
    }
    protected void MailAdd_Click(object sender, EventArgs e)
    {
        int UserID = 0;
        UserID = (int)FormViewDetails.SelectedValue;
        string SQL = "INSERT INTO T_Mails (UserID, Email) VALUES (" +  UserID.ToString() + ", N'" + AddMail.Text + "')";
        string CS = _db.Connection.ConnectionString;
        using (SqlConnection CN = new SqlConnection(CS))
        {
            SqlCommand command = new SqlCommand(SQL, CN);
            try
            {
                CN.Open();
                command.ExecuteScalar();
            }
            catch (Exception)
            {
                
            }
        }
        Page.DataBind();
    }
    protected void SMSAdd_Click(object sender, EventArgs e)
    {
        int UserID = 0;
        UserID = (int)FormViewDetails.SelectedValue;
        string SQL = "INSERT INTO T_PhoneNumbers (UserID, TelNo) VALUES (" + UserID.ToString() + ", N'" + TelNo.Text + "')";
        string CS = _db.Connection.ConnectionString;
        using (SqlConnection CN = new SqlConnection(CS))
        {
            SqlCommand command = new SqlCommand(SQL, CN);
            try
            {
                CN.Open();
                command.ExecuteScalar();
            }
            catch (Exception)
            {
                
            }
        }
        Page.DataBind();
    }

}
