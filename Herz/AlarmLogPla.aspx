﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PublicPage.master" AutoEventWireup="true" CodeFile="AlarmLogPla.aspx.cs" Inherits="MemberPages_AlarmLogPla" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <meta http-equiv="refresh" content="300" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div>
        <table class="LayoutTable">
            <tr>
                <th class="HeaderStyle">יומן תקלות</th>
            </tr>
            <tr>
                <td>
                    <%--<asp:Label ID="lblFilter" runat="server" Text="סינון לפי לקוח" />
                    <asp:DropDownList ID="cmbClients" runat="server" AutoPostBack="True" 
                        DataTextField="clientName" DataValueField="ID" 
                        onselectedindexchanged="cmbClients_SelectedIndexChanged">
                    </asp:DropDownList>
                    <asp:LinqDataSource ID="LinqClients" runat="server" 
                        ContextTypeName="DataClassesDataContext" 
                        Select="new (ID, Name as clientName)" 
                        TableName="T_Clients">
                    </asp:LinqDataSource>--%>
                </td>
            </tr>
            <tr>
                <td>
<%--                    <asp:Label ID="lblName" runat="server" Text="ClientName" />--%> 
                   <asp:LinqDataSource ID="LinqAlarms" runat="server" 
                        ContextTypeName="DataClassesDataContext" 
                        Select="new (
                                    T_Alarm.Name as name,
                                    T_Alarm.tbl_Counter.tbl_Consumers_0.sql_Consumer_name as consumer,
                                    T_Alarm.tbl_Counter.tbl_CounterType.sql_description + '-' + T_Alarm.tbl_Counter.T_Tariff.TariffName as counter,
                                    CheckTime,
                                    Value,
                                    T_Alarm.Tolerance,
                                    T_Alarm.tbl_Counter.tbl_Consumers_0.T_Client.Name as client
                                    )"
                        TableName="T_AlarmLogs"
                        OrderBy="CheckTime DESC" >
                        <%--Where="T_Alarm.tbl_Counter.tbl_Consumers_0.tbl_Client.sql_clientID == @ClientID" >
                        <WhereParameters>
                            <asp:SessionParameter SessionField="ClientID" Type="Int32" Name="ClientID" />
                        </WhereParameters>--%>
                    </asp:LinqDataSource>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" PageSize="20"
                        AutoGenerateColumns="False" DataSourceID="LinqAlarms" 
                        OnRowDataBound="GridView1_RowDataBound">
                        <Columns>
                            <asp:BoundField DataField="name" HeaderText="שם" InsertVisible="False"  />
                            <asp:BoundField DataField="consumer" HeaderText="צרכן" />
                            <asp:BoundField DataField="counter" HeaderText="מונה ותעריף" Visible="false" />
                            <asp:BoundField DataField="CheckTime" HeaderText="זמן" />
                            <asp:BoundField DataField="Value" HeaderText="צריכה" />
                            <asp:BoundField DataField="client" HeaderText="לקוח" />
                        </Columns>
                        <HeaderStyle CssClass="DetailsFormViewHeader" />
                        <RowStyle ForeColor="#333333" />
                        <AlternatingRowStyle BackColor="#f7f6f3" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                    *רמות התקלות:&nbsp;
                    <asp:Label Text=" 30% " BackColor="Yellow" runat="server"></asp:Label>&nbsp;
                    <asp:Label Text=" 50% " BackColor="Tomato" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

