﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MemberPages_AlarmLogPla : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        /*if (!IsPostBack)
        {
            if (!_user.IsAdmin())//not admin
            {
                //GridView1.Columns[5].Visible = false;

                cmbClients.Visible = false;
                lblFilter.Visible = false;
            }
            else
            {
                cmbClients.DataSource = LinqClients;
                cmbClients.DataBind();
                cmbClients.Items.Insert(0, "כל הלקוחות");
            }
        }*/
        //if (!_user.IsAdmin())//not admin
        {
            //LinqAlarms.Where = "T_Alarm.tbl_Counter.tbl_Consumers_0.tbl_Client.sql_clientID == @ClientID";
            //LinqAlarms.WhereParameters.Add("ClientID", TypeCode.Int32, _client.sql_clientID.ToString());
            //LinqAlarms.Where = "T_Alarm.tbl_Counter.tbl_Consumers_0.T_Client.ID == @ClientID";
            //LinqAlarms.WhereParameters.Add("ClientID", TypeCode.Int32, _client.sql_clientID.ToString());
            DataClassesDataContext db = new DataClassesDataContext();
            GridView1.DataSourceID = "";
            //var clientIDs = DataClassLoader.ClientsAllowedForUser(db, _user.ID).Select(x => x.ID);
            GridView1.DataSource = from alarm
                                       in db.T_AlarmLogs//.Where(c => clientIDs.Contains(c.T_Alarm.tbl_Counter.tbl_Consumers_0.sql_clientID))
                                   where alarm.T_Alarm.tbl_Counter.tbl_Consumers_0.sql_clientID == 6
                                   orderby alarm.CheckTime descending
                                   select new
                                   {
                                       name = alarm.T_Alarm.Name,
                                       consumer = alarm.T_Alarm.tbl_Counter.tbl_Consumers_0.sql_Consumer_name,
                                       counter = alarm.T_Alarm.tbl_Counter.tbl_CounterType.sql_description + '-' + alarm.T_Alarm.tbl_Counter.T_Tariff.TariffName,
                                       alarm.CheckTime,
                                       alarm.Value,
                                       alarm.T_Alarm.Tolerance,
                                       client = alarm.T_Alarm.tbl_Counter.tbl_Consumers_0.T_Client.Name
                                   };
            GridView1.PageIndexChanging += new GridViewPageEventHandler(GridView1_PageIndexChanging);
        }
    }
    void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataBind();
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if((double)DataBinder.Eval(e.Row.DataItem,"Tolerance")<.5)
                e.Row.Cells[4].BackColor = System.Drawing.Color.Yellow;
            else
                e.Row.Cells[4].BackColor = System.Drawing.Color.Tomato;
        }
    }
    /*protected void cmbClients_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (cmbClients.SelectedIndex == 0)//no filter
            LinqAlarms.Where = "";
        else
        {
            //LinqAlarms.Where = "T_Alarm.tbl_Counter.tbl_Consumers_0.tbl_Client.sql_clientID == @ClientID";
            LinqAlarms.Where = "T_Alarm.tbl_Counter.tbl_Consumers_0.sql_clientID == @ClientID";
            LinqAlarms.WhereParameters.Clear();
            LinqAlarms.WhereParameters.Add("ClientID", TypeCode.Int32, cmbClients.SelectedValue);
        }
        GridView1.DataBind();
    }*/
}
