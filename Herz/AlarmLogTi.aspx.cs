﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MemberPages_AlarmLogTi : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int clientId = 1;
        DataClassesDataContext db = new DataClassesDataContext();
        GridView1.DataSourceID = "";
        GridView1.DataSource = from alarm
                                   in db.T_AlarmLogs
                               where alarm.T_Alarm.tbl_Counter.tbl_Consumers_0.sql_clientID == clientId
                               orderby alarm.CheckTime descending
                               select new
                               {
                                   name = alarm.T_Alarm.Name,
                                   consumer = alarm.T_Alarm.tbl_Counter.tbl_Consumers_0.sql_Consumer_name,
                                   counter = alarm.T_Alarm.tbl_Counter.tbl_CounterType.sql_description + '-' + alarm.T_Alarm.tbl_Counter.T_Tariff.TariffName,
                                   alarm.CheckTime,
                                   alarm.Value,
                                   alarm.T_Alarm.Tolerance,
                                   client = alarm.T_Alarm.tbl_Counter.tbl_Consumers_0.T_Client.Name
                               };
        GridView1.PageIndexChanging += new GridViewPageEventHandler(GridView1_PageIndexChanging);
    }
    void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataBind();
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if((double)DataBinder.Eval(e.Row.DataItem,"Tolerance")<.5)
                e.Row.Cells[4].BackColor = System.Drawing.Color.Yellow;
            else
                e.Row.Cells[4].BackColor = System.Drawing.Color.Tomato;
        }
    }
}
