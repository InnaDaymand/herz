﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for AllFunctions
/// </summary>
public enum Season
{
    summer = 1, autumn, winter = 2, spring = 3
};
public enum consumptionType
{
    Shefel = 3, Geva = 2, Pisgah = 1
};
public enum Voltage
{
    Ilion = 1, High = 2, Low = 3, Default = 4
};
public class AllFunctions
{
    static public System.DateTime DateForStore;
    static public string BrotherWidth;
    static public string ConnectStr = "";
    static public string ForCompareSQL = "";
    static public string tops = "";
    static public string tail = "";
    static public string where = "";
    static public int QtyMenu = 0;
    static public string[,] ConsMenu = new string[100, 3];
    static public string[] ForPrint = new string[13];
    static public string[] ParamsID = new string[5];
    static public int ParamsIdCur = 0;
    static public bool EnchParam = false;
    static public string ConsID = "";
    static public string IDforEP = "";
    static public string IDforGraphDoh = "";
    static public string ConsName = "";
    static public string ConsumerType = "";
    static public int ChartWidth = 940;
    static public int LangCur = 0;          // 0 - Hebrew, 1 - English, so on ...
    static public string ForPageN1 = "";
    static public string ForPageN2 = "";
    static public string ForPageN3 = "";
    static public bool HorMenuNo2 = false;
    /*
static public void colorTable(DataGridView T, Season season)
{
    for (int j = 0; j < 24; j++)
    {
        for (int i = 2; i < 9; i++) // i -column number 
        {

            consumptionType t = ConsumptionType(9 - i, season, j);
            //  consumptionType t = ConsumptionType(i-1 , season, j);
            switch (t)
            {
                case consumptionType.Shefel:
                    T.Rows[j].Cells[i].Style.BackColor = Color.Green;

                    break;
                case consumptionType.Geva:
                    T.Rows[j].Cells[i].Style.BackColor = Color.Yellow;
                    break;
                case consumptionType.Pisgah:
                    T.Rows[j].Cells[i].Style.BackColor = Color.Red;
                    break;
            }

        }


    }

}
*/
    static public consumptionType ConsumptionType(int selectedDay, Season season, int Hour)
    {
        if ((selectedDay >= 1) && (selectedDay <= 5))
        {
            if (season == Season.summer)
            {
                if ((Hour >= 0) && (Hour < 7)) return consumptionType.Shefel;
                else if ((Hour >= 7) && (Hour < 11)) return consumptionType.Geva;
                else if ((Hour >= 11) && (Hour < 17)) return consumptionType.Pisgah;
                else if ((Hour >= 17) && (Hour < 23)) return consumptionType.Geva;
                else return consumptionType.Shefel;
            }
            else
                if (season == Season.winter)
                {
                    if ((Hour >= 0) && (Hour < 6)) return consumptionType.Shefel;
                    else
                        if ((Hour >= 6) && (Hour < 17)) return consumptionType.Geva;
                        else
                            if ((Hour >= 17) && (Hour < 22)) return consumptionType.Pisgah;
                            else
                                if ((Hour >= 22) && (Hour < 23)) return consumptionType.Geva;
                                else return consumptionType.Shefel;

                }
                else  // season is spring or autumn
                    if ((Hour >= 0) && (Hour < 6)) return consumptionType.Shefel;
                    else
                        if ((Hour >= 6) && (Hour < 8)) return consumptionType.Geva;
                        else
                            if ((Hour >= 8) && (Hour < 22)) return consumptionType.Pisgah;
                            else
                                if ((Hour >= 22) && (Hour < 23)) return consumptionType.Geva;
                                else
                                    return consumptionType.Shefel;

        }
        else
            if (selectedDay == 6)
            {
                if (season == Season.summer)
                {
                    if ((Hour >= 0) && (Hour < 7)) return consumptionType.Shefel;
                    else
                        if ((Hour >= 7) && (Hour < 22)) return consumptionType.Geva;
                        else return consumptionType.Shefel;
                }
                else
                    if (season == Season.winter)
                    {
                        if ((Hour >= 0) && (Hour < 8)) return consumptionType.Shefel;
                        else
                            if ((Hour >= 7) && (Hour < 12)) return consumptionType.Geva;
                            else
                                if ((Hour >= 12) && (Hour < 15)) return consumptionType.Shefel;
                                else
                                    if ((Hour >= 15) && (Hour < 22)) return consumptionType.Geva;
                                    else return consumptionType.Shefel;
                    }
                    else // season is spring or autumn
                    {
                        if ((Hour >= 0) && (Hour < 7)) return consumptionType.Shefel;
                        else
                            if ((Hour >= 7) && (Hour < 23)) return consumptionType.Geva;
                            else
                                return consumptionType.Shefel;
                    }
            }
            else // seventh day, last week day 
            {
                if (season == Season.summer)
                {
                    if ((Hour >= 0) && (Hour < 19)) return consumptionType.Shefel;
                    else
                        if ((Hour >= 19) && (Hour < 22)) return consumptionType.Geva;
                        else return consumptionType.Shefel;
                }
                else
                    if (season == Season.winter)
                    {
                        if ((Hour >= 0) && (Hour < 17)) return consumptionType.Shefel;
                        if ((Hour >= 17) && (Hour < 22)) return consumptionType.Geva;
                        else return consumptionType.Shefel;
                    }
                    else
                    {
                        if ((Hour >= 0) && (Hour < 17)) return consumptionType.Shefel;
                        else
                            if ((Hour >= 17) && (Hour < 23)) return consumptionType.Geva;
                            else return consumptionType.Shefel;
                    }
            }


    }
    static public float Tarrif(Voltage V, Season season, consumptionType C)
    {
        float tarrif = 0;
        /*  int i = (int)C;
          int j = (int)V;
          int k = (int)season;
            return rates[i-1,j-1,k-1];
        */
        switch (V)
        {
            case Voltage.Ilion:
                {
                    switch (season)
                    {
                        case Season.summer:
                            {
                                switch (C)
                                {
                                    case consumptionType.Pisgah:
                                        tarrif = 0.6556F;
                                        break;
                                    case consumptionType.Geva:
                                        tarrif = 0.4066F;
                                        break;
                                    case consumptionType.Shefel:
                                        tarrif = 0.1125F;
                                        break;
                                }

                            }
                            break;
                        case Season.winter:
                            {
                                switch (C)
                                {
                                    case consumptionType.Pisgah:
                                        tarrif = 0.6337F;
                                        break;
                                    case consumptionType.Geva:
                                        tarrif = 0.3564F;
                                        break;
                                    case consumptionType.Shefel:
                                        tarrif = 0.1029F;
                                        break;
                                }
                            }
                            break;
                        case Season.spring:
                            {
                                switch (C)
                                {
                                    case consumptionType.Pisgah:
                                        tarrif = 0.5188F;
                                        break;
                                    case consumptionType.Geva:
                                        tarrif = 0.3034F;
                                        break;
                                    case consumptionType.Shefel:
                                        tarrif = 0.1067F;
                                        break;
                                }
                            }
                            break;
                    }

                }  //מתח עליון
                break;
            case Voltage.High:
                {
                    switch (season)
                    {
                        case Season.summer:
                            {
                                switch (C)
                                {
                                    case consumptionType.Pisgah:
                                        tarrif = 0.7210F;
                                        break;
                                    case consumptionType.Geva:
                                        tarrif = 0.4443F;
                                        break;
                                    case consumptionType.Shefel:
                                        tarrif = 0.1343F;
                                        break;
                                }
                            }
                            break;
                        case Season.winter:
                            {
                                switch (C)
                                {
                                    case consumptionType.Pisgah:
                                        tarrif = 0.685F;
                                        break;
                                    case consumptionType.Geva:
                                        tarrif = 0.3848F;
                                        break;
                                    case consumptionType.Shefel:
                                        tarrif = 0.124F;
                                        break;

                                }

                            }
                            break;
                        case Season.spring:
                            {
                                switch (C)
                                {
                                    case consumptionType.Pisgah:
                                        tarrif = 0.5484F;
                                        break;
                                    case consumptionType.Geva:
                                        tarrif = 0.3279F;
                                        break;
                                    case consumptionType.Shefel:
                                        tarrif = 0.1276F;
                                        break;
                                }
                            }
                            break;
                    }
                } // מתח גבוע
                break;
            case Voltage.Low:
                {
                    switch (season)
                    {
                        case Season.summer:
                            {
                                switch (C)
                                {
                                    case consumptionType.Pisgah:
                                        tarrif = 0.8328F;
                                        break;
                                    case consumptionType.Geva:
                                        tarrif = 0.5222F;
                                        break;
                                    case consumptionType.Shefel:
                                        tarrif = 0.1897F;
                                        break;
                                }
                            }
                            break;
                        case Season.winter:
                            {
                                switch (C)
                                {
                                    case consumptionType.Pisgah:
                                        tarrif = 0.7844F;
                                        break;
                                    case consumptionType.Geva:
                                        tarrif = 0.4526F;
                                        break;
                                    case consumptionType.Shefel:
                                        tarrif = 0.1784F;
                                        break;
                                }
                            }
                            break;
                        case Season.spring:
                            {
                                switch (C)
                                {
                                    case consumptionType.Pisgah:
                                        tarrif = 0.62F;
                                        break;
                                    case consumptionType.Geva:
                                        tarrif = 0.3894F;
                                        break;
                                    case consumptionType.Shefel:
                                        tarrif = 0.1817F;
                                        break;
                                }
                            }
                            break;
                    }
                }// מתח נמוך
                break;
        }

        return tarrif;

    }
    static public int HowManyRedHours(Season S, int Day, int Begin, int End)
    {
        int temp = 0;
        for (int i = Begin; i < End; i++)
            if (ConsumptionType(Day, S, i) == consumptionType.Pisgah) temp++;
        return temp;
    }
    static public int HowManyYellowHours(Season S, int Day, int Begin, int End)
    {
        int temp = 0;
        for (int i = Begin; i < End; i++)
            if (ConsumptionType(Day, S, i) == consumptionType.Geva) temp++;
        return temp;
    }
    static public int HowManyGreenHours(Season S, int Day, int Begin, int End)
    {
        int temp = 0;
        for (int i = Begin; i < End; i++)
            if (ConsumptionType(Day, S, i) == consumptionType.Shefel) temp++;
        return temp;
    }

    public static string GetScalar(string SQL)
    {
        string SS = null;
        using (SqlConnection CN = new SqlConnection(ConnectStr))
        {
            SqlCommand command = new SqlCommand(SQL, CN);
            CN.Open();
            try
            {
                SS = command.ExecuteScalar().ToString();
            }
            catch (Exception es)
            {

                SS = "";
            }
        }
        return SS;
    }

    public static DataTable Populate(string SQL)
    {
        SqlConnection CN = new SqlConnection(ConnectStr);
        CN.Open();

        SqlCommand command = new SqlCommand(SQL, CN);
        SqlDataAdapter adapter = new SqlDataAdapter();
        adapter.SelectCommand = command;

        DataTable table = new DataTable();
        adapter.Fill(table);

        return table;
    }

    public static void DoExec(string SQL)
    {
        using (SqlConnection CN = new SqlConnection(ConnectStr))
        {
            SqlCommand command = new SqlCommand(SQL, CN);
            command.Connection.Open();
            command.CommandTimeout = 0;
            command.ExecuteNonQuery();
        }
    }


}
