﻿using System;
using System.Data.SqlClient;

/// <summary>
/// Summary description for CounterData
/// </summary>
public class CounterData
{
    public static float GetCounterTotalRatePerTimeWindow(string CounterID, DateTime startDate, DateTime endDate)
    {
        try
        {
            string dateStartString = string.Format("{0}-{1}-{2} {3}",
                startDate.Year, startDate.Month, startDate.Day, startDate.TimeOfDay.ToString());

            string dateEndString = string.Format("{0}-{1}-{2} {3}",
                endDate.Year, endDate.Month, endDate.Day, endDate.TimeOfDay.ToString());

            float rate = 0;
            float puls = 0;
            //float startValue = GetCounterValue(CounterID, CounterID, dateStartString, out puls, out rate);
            //float endValue = GetCounterValue(CounterID, CounterID, dateEndString, out puls, out rate);
            float startValue = GetClosestValue(CounterID, CounterID, dateStartString, out puls, out rate);
            float resValue = GetCounterDifference(CounterID, CounterID, dateStartString, dateEndString);

            return resValue * rate * puls;
        }
        catch { }

        return 0;
    }

    public static float GetCounterTotalPerTimeWindow(string CounterID, DateTime startDate, DateTime endDate)
    {
        try
        {
            string dateStartString = string.Format("{0}-{1}-{2} {3}",
                startDate.Year, startDate.Month, startDate.Day, startDate.TimeOfDay.ToString());

            string dateEndString = string.Format("{0}-{1}-{2} {3}",
                endDate.Year, endDate.Month, endDate.Day, endDate.TimeOfDay.ToString());

            float rate = 0;
            float puls = 0;
            float startValue = GetCounterValue(CounterID, CounterID, dateStartString, out puls, out rate);
            float endValue = GetCounterValue(CounterID, CounterID, dateEndString, out puls, out rate);

            return (endValue - startValue) * puls;
        }
        catch { }

        return 0;
    }

    private static object GetValueFromDB(string query, ref object pulsValue, ref object rateValue)
    {
        pulsValue = null;
        rateValue = null;

        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Money_DisplayConnectionString3"].ConnectionString);
        SqlCommand command = new SqlCommand(query, con);

        try
        {
            con.Open();
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                reader.Read();
                pulsValue = reader[1];
                rateValue = reader[2];
                return reader[0];
            }
            else
                return null;
        }
        finally
        {
            con.Close();
        }
    }

    private static float GetCounterValue(string consumerID, string counterID, string date, out float pulsValue, out float rateValue)
    {
        pulsValue = 0;
        rateValue = 0;
        object oPulsValue = null;
        object oRateValue = null;
        string queryGetCounter = string.Format("SELECT  tbl_countersValue.sql_Value, tbl_Counters.sql_puls_value, tbl_rates.sql_value " +
        "FROM tbl_Counters " +
        "INNER JOIN " +
        "tbl_rates ON tbl_Counters.sql_tariff_ID = tbl_rates.sql_tariff_ID " +
        "INNER JOIN tbl_countersValue ON tbl_Counters.sql_counter_ID = tbl_countersValue.sql_counter_id " +
        "WHERE (tbl_Counters.sql_counter_ID = {0}) AND (tbl_Counters.sql_Consumer_ID = {1}) " +
        "AND (tbl_countersValue.sql_DateTimeMeasuring = CONVERT(DATETIME, '{2}', 102))", counterID, consumerID, date);

        object res = GetValueFromDB(queryGetCounter, ref oPulsValue, ref oRateValue);
        if (res == null)
            return 0;
        else
        {
            try
            {
                pulsValue = (float)Convert.ToDouble(oPulsValue);
                rateValue = (float)Convert.ToDouble(oRateValue);
                return (float)Convert.ToDouble(res);
            }
            catch (Exception ex)
            { }
        }
        return 999;
    }
    private static float GetClosestValue(string consumerID, string counterID, string date, out float pulsValue, out float rateValue)
    {
        pulsValue = 0;
        rateValue = 0;
        object oPulsValue = null;
        object oRateValue = null;
        string queryGetCounter = string.Format("SELECT TOP (1) tbl_countersValue.sql_Value, tbl_Counters.sql_puls_value, tbl_rates.sql_value " +
        "FROM tbl_Counters " +
        "INNER JOIN " +
        "tbl_rates ON tbl_Counters.sql_tariff_ID = tbl_rates.sql_tariff_ID " +
        "INNER JOIN tbl_countersValue ON tbl_Counters.sql_counter_ID = tbl_countersValue.sql_counter_id " +
        "WHERE (tbl_Counters.sql_counter_ID = {0}) AND (tbl_Counters.sql_Consumer_ID = {1}) " +
        "AND (tbl_countersValue.sql_DateTimeMeasuring >= CONVERT(DATETIME, '{2}', 102))", counterID, consumerID, date);

        object res = GetValueFromDB(queryGetCounter, ref oPulsValue, ref oRateValue);
        if (res == null)
            return 0;
        else
        {
            try
            {
                pulsValue = (float)Convert.ToDouble(oPulsValue);
                rateValue = (float)Convert.ToDouble(oRateValue);
                return (float)Convert.ToDouble(res);
            }
            catch (Exception ex)
            { }
        }
        return 999;
    }
    private static float GetCounterDifference(string consumerID, string counterID, string beginDate, string endDate)
    {
        string query = String.Format(
            "SELECT MAX(sql_Value) - MIN(sql_Value) FROM tbl_countersValue " +
            "WHERE sql_Consumer_ID={0} AND sql_Counter_ID={1}" +
            "AND sql_DateTimeMeasuring BETWEEN '{2}' AND '{3}'",
            consumerID, counterID, beginDate, endDate);

        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Money_DisplayConnectionString3"].ConnectionString);
        SqlCommand command = new SqlCommand(query, con);

        float res = 0;
        try
        {
            con.Open();
            res = (float)command.ExecuteScalar();
        }
        finally
        {
            con.Close();
        }
        return res;
    }
}
