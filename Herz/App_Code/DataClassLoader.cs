﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DataClassLoader
/// </summary>
public class DataClassLoader
{
    private DataClassesDataContext db;

    private List<T_TariffStep> _tSteps;
    private List<T_TariffStepValue> _tValues;

    public DataClassLoader()
    {
        db = new DataClassesDataContext();
        //db.ObjectTrackingEnabled = false;

        LoadTariffs();
    }

    public void LoadTariffs()
    {
        _tSteps = (from step in db.T_TariffSteps
                   select step).ToList();

        _tValues = (from val in db.T_TariffStepValues
                    select val).ToList();
    }

    public IEnumerable<tbl_Consumers_0> GetTemperConsumers(string ConsumerIdList)
    {
        string[] IDs = ConsumerIdList.Split(',');
        var consumers = from consumer in db.tbl_Consumers_0s
                        where consumer.HasTemperature != null &&
                              consumer.HasTemperature.Value &&
                              IDs.Contains(consumer.sql_Consumer_ID.ToString())
                        select consumer;
        return consumers;
    }

    public IEnumerable<tbl_Counter> GetCounters(string ConsumerIdList)
    {
        var counters = from counter in db.S_Counters_Get(ConsumerIdList)
                       join tCounter in db.tbl_Counters
                            on counter.sql_counter_ID equals tCounter.sql_counter_ID
                       //join taozCounter in db.T_TaozCounters
                       //     on counter.sql_counter_ID equals taozCounter.CounterID
                       where !counter.IsSplitted
                       select tCounter;

        return counters;
    }
/*    public IEnumerable<V_Counter> GetCounters(string ConsumerIdList)
    {
        var counters = from counter in db.S_Counters_Get(ConsumerIdList)
                       join tCounter in db.V_Counters
                            on counter.sql_counter_ID equals tCounter.sql_counter_ID
                       where !counter.IsSplitted
                       select tCounter;

        return counters;
    }*/

    public T_TaozCounter GetTaozCounter(int CounterID)
    {
        try
        {
            return (from taozCounter in db.T_TaozCounters
                    where taozCounter.CounterID == CounterID
                    select taozCounter).First();
        }
        catch
        {
            //return null;
            T_TaozCounter counter = new T_TaozCounter();
            counter.CounterID = 0;
            counter.MainCounterID = 0;
            counter.LoadID = 0;
            return counter;
        }
    }

    public IEnumerable<DataSample> GetTemperatureData(int ConsumerId, DateTime StartDate, DateTime EndDate)
    {
        var samples = from sample in db.S_TemperatureValue_Get(ConsumerId, StartDate, EndDate)
                      select new DataSample
                      {
                          Timestamp = sample.Timestamp,
                          Value = sample.Value
                      };
        return samples;
    }

    public IEnumerable<DataSample> GetCounterDataTest(tbl_Counter Counter, DateTime StartDate, DateTime EndDate)
    {
        DateTime start = DateTime.Now;

        IEnumerable<DataSample> samples;

        string cmd = string.Format("select sql_DateTimeMeasuring, sql_Value, ValueChange as diff from tbl_countersValue where sql_counter_id={0} and sql_DateTimeMeasuring between '{1}' and '{2}'",
                                    Counter.sql_counter_ID, 
                                    ((Counter.T_Tariff.TariffType == 3)?
                                        new DateTime(StartDate.Year, StartDate.Month, 1) : StartDate).ToString("yyyy-MM-dd HH:mm"), 
                                    EndDate.ToString("yyyy-MM-dd HH:mm"));
        var samDif = db.ExecuteQuery<S_CounterValueDiff_GetResult>(cmd);
        samples = from sample in samDif
        //samples = from sample in db.S_CounterValueDiff_Get(Counter.sql_counter_ID, StartDate, EndDate)
                  where sample.diff != null
                  select new DataSample
                  {
                      Timestamp = sample.sql_DateTimeMeasuring,
                      Value = sample.diff * Counter.sql_puls_value,
                      Cost = sample.diff * Counter.sql_puls_value
                            //* GetTariffValue(Counter, sample.sql_DateTimeMeasuring)
                            * (Counter.TariffCoefficient.HasValue ? Counter.TariffCoefficient.Value : 1)
                  };
        var dur = DateTime.Now - start;

        DataSample[] arr = samples.ToArray();
        //accumulate monthly consumption
        double accum = 0;
        for (int i = 0; i < arr.Length; i++)
        {
            if (Counter.T_Tariff.TariffType == 3)//valueDependent
            {
                //if moving to next month, reset accumulation
                if (i > 0 && arr[i].Timestamp.Month > arr[i - 1].Timestamp.Month)
                    accum = 0;
                accum += arr[i].Value.Value;
            }
            //apply tariff
            arr[i].Cost *= GetTariffValue(Counter, arr[i].Timestamp, accum);
        }
        samples = arr;

        if (Counter.T_Tariff.TariffType == 3)//valueDependent
            samples = samples.Where(s => s.Timestamp >= StartDate);

        return samples;
    }
    public IEnumerable<DataSample> GetCounterData(tbl_Counter Counter, DateTime StartDate, DateTime EndDate)
    {
        //DateTime start = DateTime.Now;

        IEnumerable<DataSample> samples;

        string cmd = string.Format("select sql_DateTimeMeasuring, sql_Value, ValueChange as diff from tbl_countersValue where sql_counter_id={0} and sql_DateTimeMeasuring between '{1}' and '{2}'",
                                    Counter.sql_counter_ID,
                                    ((Counter.T_Tariff.TariffType == 3) ?
                                        new DateTime(StartDate.Year, StartDate.Month, 1) : StartDate).ToString("yyyy-MM-dd HH:mm"),
                                    EndDate.ToString("yyyy-MM-dd HH:mm"));
        var samDif = db.ExecuteQuery<S_CounterValueDiff_GetResult>(cmd);
        samples = from sample in samDif
                  where sample.diff != null
                  select new DataSample
                  {
                      Timestamp = sample.sql_DateTimeMeasuring,
                      Value = sample.diff * Counter.sql_puls_value,
                      Cost = sample.diff * Counter.sql_puls_value
                            //* GetTariffValue(Counter, sample.sql_DateTimeMeasuring)
                            * (Counter.TariffCoefficient.HasValue ? Counter.TariffCoefficient.Value : 1)
                  };
        //var dur = DateTime.Now - start;

        DataSample[] arr = samples.ToArray();
        //accumulate monthly consumption
        double accum = 0;
        for (int i = 0; i < arr.Length; i++)
        {
            if (Counter.T_Tariff.TariffType == 3)//valueDependent
            {
                //if moving to next month, reset accumulation
                if (i > 0 && arr[i].Timestamp.Month > arr[i - 1].Timestamp.Month)
                    accum = 0;
                accum += arr[i].Value.Value;
            }
            //apply tariff
            arr[i].Cost *= GetTariffValue(Counter, arr[i].Timestamp, accum);
        }
        samples = arr;

        if (Counter.T_Tariff.TariffType == 3)//valueDependent
            samples = samples.Where(s => s.Timestamp >= StartDate);

        //kav yarok
        int[] kavYarokCounters =    new int[3] { 51, 52, 53 };
        int[] tanurKeshetCounters = new int[3] { 47, 48, 49 };
        if (kavYarokCounters.Contains(Counter.sql_counter_ID))
        {   //special calculation
            tbl_Counter tanurCounter = db.tbl_Counters.First(c => c.sql_counter_ID == Counter.sql_counter_ID - 4);
            if (tanurCounter == null)
                return samples;

            IEnumerable<DataSample> diffSamples;
            cmd = string.Format("select sql_DateTimeMeasuring, sql_Value, ValueChange as diff from tbl_countersValue where sql_counter_id={0} and sql_DateTimeMeasuring between '{1}' and '{2}'",
                                        tanurCounter.sql_counter_ID, StartDate.ToString("yyyy-MM-dd HH:mm"), EndDate.ToString("yyyy-MM-dd HH:mm"));
            var samDifTanur = db.ExecuteQuery<S_CounterValueDiff_GetResult>(cmd);
            diffSamples = from sample in samDifTanur
            //diffSamples = from sample in db.S_CounterValueDiff_Get(tanurCounter.sql_counter_ID, StartDate, EndDate)
                      where sample.diff != null
                      select new DataSample
                      {
                          Timestamp = sample.sql_DateTimeMeasuring,
                          Value = sample.diff * tanurCounter.sql_puls_value,
                          Cost = sample.diff * tanurCounter.sql_puls_value
                                * (GetTariffValue(tanurCounter, sample.sql_DateTimeMeasuring)
                                    * (tanurCounter.TariffCoefficient.HasValue ? tanurCounter.TariffCoefficient.Value : 1)
                                    -  GetTariffValue(Counter, sample.sql_DateTimeMeasuring)
                                  )
                      };

            samples = from sample in samples
                      join dsample in diffSamples on sample.Timestamp equals dsample.Timestamp
                      select new DataSample
                      {
                          Timestamp = sample.Timestamp,
                          Value = sample.Value,
                          Cost = sample.Cost + dsample.Cost
                      };//
        }

        return samples;
    }
    public void /*IEnumerable<DataSample>*/ GetCounterDataPivot(IEnumerable<tbl_Counter> Counters, DateTime StartDate, DateTime EndDate)
    {
        //string[] list = CountersList.Split(',');

        string request = "SELECT tm,{0} FROM " +
                         "(SELECT sql_Value,sql_Counter_ID,sql_DateTimeMeasuring AS tm FROM tbl_countersValue " +
                         "WHERE sql_DateTimeMeasuring BETWEEN '{1:yyyy-MM-dd HH:mm}' AND '{2:yyyy-MM-dd HH:mm}') AS p " +
                         "PIVOT (min(sql_Value) FOR sql_Counter_ID IN ({0})) AS pvt";

        string counters = "";
        //foreach (string counter in list)
        //    counters += String.Format(",[{0}]", counter);
        foreach (var counter in Counters)
            counters += String.Format(",[{0}]", counter.sql_counter_ID);
        counters = counters.Substring(1);//remove first comma

        request = String.Format(request, counters, StartDate, EndDate);

        //PivotedTable t = new PivotedTable(list.Length);
        //var pvt = db.ExecuteQuery(typeof(PivotedSample), request, "");
        
        /*
        var src = from samples in db.tbl_countersValues
                  where list.Contains(samples.sql_counter_id.ToString())
                  select samples;
        var res = src.Pivot(c => c.sql_DateTimeMeasuring, c => c.sql_counter_id, a => a.Min(c => c.sql_Value));
        */
    }

    /*    public Array GetCounterData(tbl_Counter Counter, DateTime StartDate, DateTime EndDate)
        {
            var samples = 
                          from sample in db.S_CounterValueDiff_Get(Counter.sql_counter_ID, StartDate, EndDate)
                          select new 
                          {
                              Timestamp = sample.sql_DateTimeMeasuring,
                              Value = sample.diff * Counter.sql_puls_value,
                              Cost = sample.diff * Counter.sql_puls_value * GetTariffValue(Counter, sample.sql_DateTimeMeasuring)
                          };

            return samples.ToArray();
        }*/

    private double GetTariffValue(tbl_Counter Counter, DateTime Timestamp)
    {
        var steps = from tariffSteps in _tSteps// db.T_TariffSteps
                    where tariffSteps.TariffID == Counter.sql_tariff_ID// tariffId
                    select tariffSteps;

        T_TariffStep step;
        if (Counter.T_Tariff.TariffType == 2)//timeDependent
        {
            step = (from tariffSteps in steps
                    where tariffSteps.Range.Contains(String.Format("{0:00}", Timestamp.Month))
                    select tariffSteps).First();
        }
        else if (Counter.T_Tariff.TariffType == 3)//valueDependent
        {
            double accum = db.S_CounterAccumulation_Get(Counter.sql_counter_ID, new DateTime(Timestamp.Year, Timestamp.Month, 1), Timestamp) * (double)Counter.sql_puls_value;

            step = (from tariffSteps in steps
                    where Convert.ToInt32(tariffSteps.Range) > accum
                    select tariffSteps).First();
        }
        else //=1
            step = steps.First();

        double tariffVal = (from tariffValues in _tValues// db.T_TariffStepValues
                            where tariffValues.ValidFromDate <= Timestamp &&
                                  tariffValues.TariffStepID == step.TariffStepID
                            orderby tariffValues.ValidFromDate descending
                            select tariffValues.Value).First();

        return tariffVal;
    }
    private double GetTariffValue(tbl_Counter Counter, DateTime Timestamp, double Accum)
    {
        var steps = from tariffSteps in _tSteps// db.T_TariffSteps
                    where tariffSteps.TariffID == Counter.sql_tariff_ID// tariffId
                    select tariffSteps;

        T_TariffStep step;
        if (Counter.T_Tariff.TariffType == 2)//timeDependent
        {
            step = (from tariffSteps in steps
                    where tariffSteps.Range.Contains(String.Format("{0:00}", Timestamp.Month))
                    select tariffSteps).First();
        }
        else if (Counter.T_Tariff.TariffType == 3)//valueDependent
        {
            //double accum = db.S_CounterAccumulation_Get(Counter.sql_counter_ID, new DateTime(Timestamp.Year, Timestamp.Month, 1), Timestamp) * (double)Counter.sql_puls_value;

            step = (from tariffSteps in steps
                    where Convert.ToInt32(tariffSteps.Range) > Accum
                    select tariffSteps).First();
        }
        else //=1
            step = steps.First();

        double tariffVal = (from tariffValues in _tValues// db.T_TariffStepValues
                            where tariffValues.ValidFromDate <= Timestamp &&
                                  tariffValues.TariffStepID == step.TariffStepID
                            orderby tariffValues.ValidFromDate descending
                            select tariffValues.Value).First();

        return tariffVal;
    }

    /*public tbl_Client GetClient(string Username, string Password)
    {
        return (from clients in db.tbl_Clients
                where clients.sql_username == Username
                     && clients.sql_password == Password
                select clients).First();
    }
    public T_User GetClient(string Username, string Password)
    {
        return (from clients in db.T_Users
                where clients.UserName == Username
                     && clients.Password == Password
                select clients).First();
    }*/
    public IEnumerable<tbl_Consumers_0> GetComsumers(int ClientID)
    {
        if (ClientID == 0)//admin
            return from consumer in db.tbl_Consumers_0s
                   select consumer;
        else
            return from consumer in db.tbl_Consumers_0s
                   where consumer.sql_clientID == ClientID
                   select consumer;
    }
/*    public IEnumerable<T_Alarm> GetAlarms(int ClientID)
    {
        return from alarm in db.T_Alarms
               where alarm.tbl_Counter.tbl_Consumers_0.tbl_Client.sql_clientID == ClientID
               select alarm;
    }*/
    public DataSample CalcCounterLastValue(int CounterID, DateTime FromDate, DateTime ToDate)
    {
        DataSample sample;
        try
        {
            var res = db.S_CounterValue_GetLatest(CounterID, FromDate, ToDate).First();
            sample = new DataSample(res.LastTime, res.LastValue.Value);
        }
        catch
        {
            sample = null;
        }
        return sample;
    }

    public static IEnumerable<T_Client> ClientsAllowedForUser(int UserId)
    {
        DataClassesDataContext db = new DataClassesDataContext();

        return ClientsAllowedForUser(db, UserId);
    }
    public static IEnumerable<T_Client> ClientsAllowedForUser(DataClassesDataContext DB,int UserId)
    {
        var ownedClients = from c in DB.T_Clients
                           where c.PowerUserID == UserId
                           select c;

        var allowedClients = from c in DB.T_Clients
                             where
                                (from cu in DB.T_ClientUsers
                                 where cu.UserID == UserId
                                 select cu.ClientID).Contains(c.ID)
                             select c;

        return ownedClients.Union(allowedClients)/*.Distinct()*/;
    }

}

public class DataSample
{
    public DateTime Timestamp;
    public double? Value;
    public double? Cost;
    //public int CounterID;

    public DataSample(DateTime timestamp, double? value, double? cost)
    {
        Timestamp = timestamp;
        Value = value;
        Cost = cost;
    }
    public DataSample(DateTime timestamp, double? value/*, double? cost*/)
    {
        Timestamp = timestamp;
        Value = value;
        //Cost = cost;
    }
    public DataSample() { }
}

public partial class tbl_Counter
{
    //public IEnumerable<DataSample> Data;
    public List<DataSample> Data;

    public T_TaozCounter TaozCounter;
}
public partial class tbl_Consumers_0
{
    public List<DataSample> Data;
}
public partial class T_User
{
    public bool IsAdmin()
    {
        if (T_RoleUsers.Count(ru => ru.RoleID == 1 && ru.UserID == ID) > 0)
            return true;
        else
            return false;
    }
    public IEnumerable<T_Client> AllowedClients()
    {
        var ownedClients = from c in T_Clients
                           where c.PowerUserID == ID
                           select c;

        var ids = from cu in T_ClientUsers
                  where cu.UserID == ID
                  select cu.ClientID;
        var allowedClients = from c in T_Clients
                             where
                                ids.Contains(c.ID)
                             select c;
        /*var allowedClients = from c in T_Clients
                             where
                                (from cu in T_ClientUsers
                                 where cu.UserID == ID
                                 select cu.ClientID).Contains(c.ID)
                             select c;*/

        return ownedClients.Union(allowedClients)/*.Distinct()*/;
    }
}

public partial class T_Clients
{
    /*public static IEnumerable<T_Client> AllowedForUser(int UserId)
    {
        var ownedClients = from c in D
                           where c.PowerUserID == ID
                           select c;

        var allowedClients = from c in T_Clients
                             where
                                (from cu in T_ClientUsers
                                 where cu.UserID == ID
                                 select cu.ClientID).Contains(c.ID)
                             select c;

        return ownedClients.Union(allowedClients)/*.Distinct()*///;
    //}*/
}

/*
public static class LinqExtenions 
{
    public static Dictionary<TKey1, Dictionary<TKey2, TValue>> Pivot<TSource, TKey1, TKey2, TValue>(
        this IEnumerable<TSource> source, 
        Func<TSource, TKey1> key1Selector,
        Func<TSource, TKey2> key2Selector,
        Func<IEnumerable<TSource>, TValue> aggregate)
    {
        return source.GroupBy(key1Selector).Select(
            x => new {
                X = x.Key,
                Y = x.GroupBy(key2Selector).Select(
                z => new {
                Z = z.Key,
                V = aggregate(z)
                }
                ).ToDictionary(e => e.Z, o => o.V)
            }
        ).ToDictionary(e => e.X, o => o.Y);
    } 
}*/