﻿using System.Configuration;
using System.Net;

/// <summary>
/// Summary description for DataLoader
/// </summary>
public class DataLoader
{
    private WebClient _webClient;
    private string _serviceUrl;

    public DataLoader()
    {
        _webClient = new WebClient();

        _serviceUrl = ConfigurationManager.AppSettings["DataServiceUrl"];
        if (!_serviceUrl.EndsWith("/"))
            _serviceUrl += "/";
    }

    public string GetData(string ServiceName)
    {
        return _webClient.DownloadString(_serviceUrl + ServiceName);
    }
}
