﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web.UI.DataVisualization.Charting;

/// <summary>
/// Summary description for GraphBuilder
/// </summary>
public class GraphBuilder
{
    public string sResol = "0";
    public string ListIDs = "";
    public DateTime DateFrom;
    public DateTime DateUntil;
    public DataTable LowerTable;
    public string Resol = "";
    public string sError = "";
    public DataTable TableVal;
    public DataTable TableCos;
    public DataRow drRes;
    private DataTable te;
    string NameV = "";
    string NameC = "";

    DataClassLoader _dLoader;

    List<tbl_Counter> _counters;
    List<DataSample> _countersLastVal;

    List<tbl_Consumers_0> _temperConsumers;

    List<Chart> _charts;

    List<DateTime> _timestamps;
    List<DataSample> _emptyPoints;
    /*
    DataSample[] data1 = {
                      //new DataSample(new DateTime(2010,1,2),null),
                      new DataSample(new DateTime(2010,1,2,0,30,0),1),
                      new DataSample(new DateTime(2010,1,2,1,0,0),1),
                      new DataSample(new DateTime(2010,1,2,1,30,0),1),
                      new DataSample(new DateTime(2010,1,2,2,0,0),1),
                      new DataSample(new DateTime(2010,1,2,2,30,0),1),
                      new DataSample(new DateTime(2010,1,2,3,0,0),1),
                      new DataSample(new DateTime(2010,1,2,3,30,0),1),
                      new DataSample(new DateTime(2010,1,2,4,0,0),1),
                      new DataSample(new DateTime(2010,1,2,4,30,0),1),
                      new DataSample(new DateTime(2010,1,2,5,0,0),1)
                         };
    DataSample[] data2 = {
                      //new DataSample(new DateTime(2010,1,2),null),
                      new DataSample(new DateTime(2010,1,2,0,30,0),0),
                      new DataSample(new DateTime(2010,1,2,1,0,0),0),
                      new DataSample(new DateTime(2010,1,2,1,30,0),0),
                      new DataSample(new DateTime(2010,1,2,2,0,0),0),
                      new DataSample(new DateTime(2010,1,2,2,30,0),0),
                      new DataSample(new DateTime(2010,1,2,3,0,0),0),
                      new DataSample(new DateTime(2010,1,2,3,30,0),0),
                      new DataSample(new DateTime(2010,1,2,4,0,0),0),
                      new DataSample(new DateTime(2010,1,2,4,30,0),0),
                      new DataSample(new DateTime(2010,1,2,5,0,0),0),
                      new DataSample(new DateTime(2010,1,2,5,30,0),1),
                      new DataSample(new DateTime(2010,1,2,6,0,0),1),
                      new DataSample(new DateTime(2010,1,2,6,30,0),1),
                      new DataSample(new DateTime(2010,1,2,7,0,0),1),
                      new DataSample(new DateTime(2010,1,2,7,30,0),1),
                      new DataSample(new DateTime(2010,1,2,8,0,0),1),
                      new DataSample(new DateTime(2010,1,2,8,30,0),1),
                      new DataSample(new DateTime(2010,1,2,9,0,0),1),
                      new DataSample(new DateTime(2010,1,2,9,30,0),1),
                      new DataSample(new DateTime(2010,1,2,10,0,0),1)
                         };*/

    private double overallCost = 0;

    public double OverallCost { get { return overallCost; } }

    public GraphBuilder()
    {
        //_dLoader = new DataLoader();
        _dLoader = new DataClassLoader();

        _counters = new List<tbl_Counter>();
        _countersLastVal = new List<DataSample>();

        _charts = new List<Chart>();

        _timestamps = new List<DateTime>();
        _emptyPoints = new List<DataSample>();
    }

    public int GetCounters(string ConsumerIdList)
    {
        var counters = _dLoader.GetCounters(ConsumerIdList);
        foreach (var c in counters)
        {
            //if (c.sql_Type_ID == 1)//electricity
                c.TaozCounter = _dLoader.GetTaozCounter(c.sql_counter_ID);

            _counters.Add(c);
        }
        return _counters.Count;
    }

    /// <summary>
    /// Transfer data to another form
    /// </summary>
    /// <param name="ConsumerIdList"></param>
    public void GetTemperConsumers(string ConsumerIdList)
    {
        _temperConsumers = _dLoader.GetTemperConsumers(ConsumerIdList).ToList();
    }

    public void GetData(DateTime StartDate, DateTime EndDate)
    {
//        _from = StartDate;
        foreach (var c in _counters)
        {
            c.Data = _dLoader.GetCounterData(c, StartDate, EndDate).ToList();
            FillTimestamps(c.Data);

            _countersLastVal.Add(_dLoader.CalcCounterLastValue(c.sql_counter_ID, StartDate, EndDate));
        }
    }

    public void GetTemperatureData(DateTime StartDate, DateTime EndDate)
    {
        DataClassesDataContext dc = new DataClassesDataContext();
        string CS = dc.Connection.ConnectionString;
        string SQL = "";
        foreach (var t in _temperConsumers)
        {
            SQL = "EXEC dbo.S_CalculateTemperature N'" + StartDate.ToString("yyyy/MM/dd HH:mm:ss") + "', N'" 
                + EndDate.ToString("yyyy/MM/dd HH:mm:ss") + "', N'" +  t.sql_Consumer_ID + "'";
            using (SqlConnection CN = new SqlConnection(CS))
            {
                try
                {
                    CN.Open();
                    SqlCommand command = new SqlCommand(SQL, CN);
                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = command;

                    te = new DataTable();
                    adapter.Fill(te);

                    //for (int i = 0; i < te.Rows.Count; i++)
                    //{
                    //    DataSample ds = new DataSample();
                    //    ds.Timestamp = (DateTime)te.Rows[i][0];
                    //    ds.Value = (double)te.Rows[i][1];
                    //    t.Data.Add(ds);
                    //}
                }
                catch (Exception e)
                {
                    string s = e.Message;
                }
            }

            //t.Data = _dLoader.GetTemperatureData(t.sql_Consumer_ID, StartDate, EndDate).ToList();
        }
    }

    /// <summary>
    /// Preparing data for Chart
    /// </summary>
    public void Build()
    {
        AllFunctions.HorMenuNo2 = false;

        #region Check Type of Device
        int II = 0;
        DataClassesDataContext dc = new DataClassesDataContext();
        string SQL = "SELECT SUM(POWER(2, TID)) FROM ( "
            + "SELECT ([TypeFC]) as TID FROM [dbo].[V_ConsumersType] WHERE  [ConsID] IN (" + ListIDs + ") "
            + "GROUP BY [TypeFC]) AS TB ";
        //string SQL = "EXEC dbo.S_CheckUniformity '" + ListIDs + "'";
        string CS = dc.Connection.ConnectionString;
        using (SqlConnection CN = new SqlConnection(CS))
        {
            SqlCommand command = new SqlCommand(SQL, CN);
            try
            {
                CN.Open();
                II = Convert.ToInt32(command.ExecuteScalar().ToString());
            }
            catch (Exception)
            {
            }
        }

        #endregion

        #region Electricity Consumers NOT Taoz
        if ((2 & II) == 2)
        {
            DataSet dst = new DataSet();
            SqlDataAdapter sda = new SqlDataAdapter("", "");
            DataTable[] dtb = new DataTable[5];

            SQL = " [dbo].[S_CalculateNotTaoz] N'" + DateFrom.ToString("yyyy/MM/dd HH:mm:ss") + "', N'" + DateUntil.ToString("yyyy/MM/dd HH:mm:ss") + "', N'" + ListIDs + "', " + sResol ;
            SqlConnection CN = new SqlConnection(CS);

            SqlCommand selectCMD = new SqlCommand(SQL, CN);
            selectCMD.CommandTimeout = 30;
            sda.SelectCommand = selectCMD;
            CN.Open();
            sda.Fill(dst, "Customers");
            CN.Close();

            dtb[0] = dst.Tables[0];
            TableVal = dtb[1] = dst.Tables[1];
            TableCos = dtb[2] = dst.Tables[2];
            dtb[3] = dst.Tables[3];
            Resol = "רזולוציה " + dtb[3].Rows[0][0].ToString();
            drRes = dtb[3].Rows[0];
            dtb[4] = dst.Tables[4];
            LowerTable = dtb[4];
            NameV = "ValueNotTaoz";
            NameC = "CostNotTaoz";

            DataRow drValue = dtb[1].Rows[0];
            DataRow drCost = dtb[2].Rows[0];

            Chart chart = new Chart();
            chart.Titles.Add((dtb[4].Rows[0][0].ToString()).Trim());
            AdjustAppearence(chart, 0);

            chart.ID = "001";
            chart.ChartAreas[0].AxisY.Title = dtb[4].Rows[0][1].ToString();
            if (AllFunctions.ConsumerType == "3")
            {
                chart.Legends[NameV].Title = "יצור, " + chart.ChartAreas[0].AxisY.Title;;
            }
            if (AllFunctions.ConsumerType == "0")
            {
                chart.Legends[NameV].Title = "צריכה, " + chart.ChartAreas[0].AxisY.Title;
            }

            _charts.Add(chart);

            Series series1 = new Series("צריכה" + drValue[0].ToString());
            series1.ChartType = SeriesChartType.StackedColumn;
            series1.ChartArea = chart.ChartAreas[0].Name;
            series1.XValueType = ChartValueType.DateTime;
            series1.ToolTip = "#VALX{g} - #VALY{0.###}";
            series1.IsVisibleInLegend = true;
            series1.Legend = chart.Legends[0].Name;
            chart.Series.Add(series1);

            Series series2 = new Series("עלות" + drCost[0].ToString());
            series2.ChartType = SeriesChartType.StackedColumn;
            series2.ChartArea = chart.ChartAreas[1].Name;
            series2.XValueType = ChartValueType.DateTime;
            series2.ToolTip = "#VALX{g} - #VALY{0.##}";
            series2.LabelFormat = "0.##";
            series2.Legend = chart.Legends[1].Name;
            chart.Series.Add(series2);

            if (AllFunctions.ConsumerType == "3")
            {
                series1.LegendText = "יצור";
            }
            if (AllFunctions.ConsumerType == "0")
            {
                series1.LegendText = "צריכה";
            }

            series2.LegendText = "עלות";

            for (int i = 0; i < dtb[0].Rows.Count; i++)
            {
                series1.Points.AddXY(dtb[0].Rows[i][0], dtb[0].Rows[i][1]);
                series2.Points.AddXY(dtb[0].Rows[i][0], dtb[0].Rows[i][2]);
            }

            overallCost = (double)drCost[0];
        }

        #endregion

        #region Consumers Water or Gas
        if (((8 & II) == 8) || ((16 & II) == 16))
        {
            DataSet dst = new DataSet();
            SqlDataAdapter sda = new SqlDataAdapter("", "");
            DataTable[] dtb = new DataTable[5];

            DataTable[] dta = new DataTable[5];
            SQL = " [dbo].[S_CalculateWater] N'" + DateFrom.ToString("yyyy/MM/dd HH:mm:ss") + "', N'" + DateUntil.ToString("yyyy/MM/dd HH:mm:ss") + "', N'" + ListIDs + "', " + sResol;
            SqlConnection CN = new SqlConnection(CS);

            SqlCommand selectCMD = new SqlCommand(SQL, CN);
            selectCMD.CommandTimeout = 30;
            sda.SelectCommand = selectCMD;
            CN.Open();
            sda.Fill(dst, "WATERS");
            CN.Close();

            dta[0] = dst.Tables[0];
            TableVal = dta[1] = dst.Tables[1];
            TableCos = dta[2] = dst.Tables[2];
            dta[3] = dst.Tables[3];
            Resol = "רזולוציה " + dta[3].Rows[0][0].ToString();
            drRes = dta[3].Rows[0];
            dta[4] = dst.Tables[4];
            LowerTable = dta[4];
            NameV = "ValueWater";
            NameC = "CostWater";

            DataRow drValue = dta[1].Rows[0];
            DataRow drCost = dta[2].Rows[0];

            Chart chart = new Chart();
            chart.Titles.Add((dta[4].Rows[0][0].ToString()).Trim());
            AdjustAppearence(chart, 0);
            chart.ID = "003";
            chart.ChartAreas[0].AxisY.Title = dta[4].Rows[0][1].ToString();
            chart.Legends[NameV].Title = "צריכה, " + chart.ChartAreas[0].AxisY.Title;
            _charts.Add(chart);

            Series series1 = new Series("צריכה" + drValue[0].ToString());
            series1.ChartType = SeriesChartType.StackedColumn;
            series1.ChartArea = chart.ChartAreas[0].Name;
            series1.XValueType = ChartValueType.DateTime;
            series1.ToolTip = "#VALX{g} - #VALY{0.###}";
            series1.IsVisibleInLegend = true;
            series1.Legend = chart.Legends[0].Name;                       
            chart.Series.Add(series1);

            Series series2 = new Series("עלות" + drCost[0].ToString());
            series2.ChartType = SeriesChartType.StackedColumn;
            series2.ChartArea = chart.ChartAreas[1].Name;
            series2.XValueType = ChartValueType.DateTime;
            series2.ToolTip = "#VALX{g} - #VALY{0.##}";
            series2.LabelFormat = "0.##";
            series2.Legend = chart.Legends[1].Name;
            chart.Series.Add(series2);

            series1.LegendText = "צריכה";
            series2.LegendText = "עלות";

            for (int i = 0; i < dta[0].Rows.Count; i++)
            {
                series1.Points.AddXY(dta[0].Rows[i][0], dta[0].Rows[i][1]);
                series2.Points.AddXY(dta[0].Rows[i][0], dta[0].Rows[i][2]);
            }

            if (drCost[0] == System.DBNull.Value)
            {
                overallCost = 0.0;
                sError = "העלות יכולה להיות מחושבת רק מתחילת החודש!";
            }
            else overallCost = (double)drCost[0];
            
        }

        #endregion

        #region Electricity Consumers Taoz

        if ((4 & II) == 4)
        {
            AllFunctions.HorMenuNo2 = true;
            #region Get data tables for Electricity Consumers

            DataSet dst = new DataSet();
            SqlDataAdapter sda = new SqlDataAdapter("", "");
            DataTable[] dtb = new DataTable[5];

            TableVal = null;
            TableCos = null;
            dst = new DataSet();
            sda = new SqlDataAdapter("", "");
            //DataTable[] dtb = new DataTable[5];

            string SS = "";
            SQL = "EXEC [dbo].[S_CalculateCompoundCounters] N'" + DateFrom.ToString("yyyy/MM/dd HH:mm:ss") + "', N'" + DateUntil.ToString("yyyy/MM/dd HH:mm:ss") + "', N'" + ListIDs + "', " + sResol;

            SqlConnection CN = new SqlConnection(CS);

            SqlCommand selectCMD = new SqlCommand(SQL, CN);
            selectCMD.CommandTimeout = 0;

            sda.SelectCommand = selectCMD;

            CN.Open();

            sda.Fill(dst, "Customers");

            CN.Close();

            dtb[0] = dst.Tables[0];
            TableVal = dtb[1] = dst.Tables[1];
            TableCos = dtb[2] = dst.Tables[2];
            dtb[3] = dst.Tables[3];
            Resol = "רזולוציה " + dtb[3].Rows[0][0].ToString();
            dtb[4] = dst.Tables[4];
            LowerTable = dtb[4];

            DataRow drValue = dtb[1].Rows[0];
            DataRow drCost = dtb[2].Rows[0];
            NameV = "ValueTaoz";
            NameC = "CostTaoz";

            drValue = dtb[1].Rows[0];
            drCost = dtb[2].Rows[0];
            drRes = dtb[3].Rows[0];

            /* - */
            #endregion

            string taozSummaryConsumption = "";
            string taozSummaryCost = "";

            Chart chart = new Chart();
            chart.Titles.Add("חשמל");
            AdjustAppearence(chart, 0);
            chart.ID = "002";
            chart.ChartAreas[0].AxisY.Title = "קוט''ש";
            if (AllFunctions.ConsumerType == "3")
            {
                chart.Legends[NameV].Title = "יצור, קוט''ש";
            }
            if (AllFunctions.ConsumerType == "0")
            {
                chart.Legends[NameV].Title = "צריכה, קוט''ש";
            }
            _charts.Add(chart);

            double totalConsumption = 0, totalCost = 0;
                string[] LoadT = {"Pisga", "Geva", "Shefel"};

                for (int i = 0; i < 3; i++)
                {
                     {
                        taozSummaryConsumption +=
                            String.Format("{0}: {1:0.##} {2}    ", LoadT[i], drValue[i], chart.ChartAreas[0].AxisY.Title);
                        taozSummaryCost +=
                            String.Format("{0}: {1:0.##} {2}    ", LoadT[i], drCost[i], chart.ChartAreas[1].AxisY.Title);
                    }
                    AddSeries(chart, i, dtb[0]);
                }
                totalConsumption = (double)drValue[3];
                totalCost = (double)drCost[3];
                overallCost += totalCost;

                if (taozSummaryConsumption != "")
                {
                    LegendItem footer = new LegendItem();
                    footer.Cells.Add(LegendCellType.Text, String.Format("{0:#,##0.##}", totalConsumption), ContentAlignment.BottomCenter);
                    footer.Cells.Add(LegendCellType.Text, "סה\"כ", ContentAlignment.BottomCenter);
                    footer.Cells.Add(LegendCellType.Text, "", ContentAlignment.BottomCenter);
                    footer.Cells[1].CellSpan = 2;
                    chart.Legends[NameV].CustomItems.Add(footer);
                }
                if (taozSummaryCost != "")
                {
                    LegendItem footer = new LegendItem();
                    footer.Cells.Add(LegendCellType.Text, String.Format("{0:#,##0.##}", totalCost), ContentAlignment.BottomCenter);
                    footer.Cells.Add(LegendCellType.Text, "סה\"כ", ContentAlignment.BottomCenter);
                    footer.Cells.Add(LegendCellType.Text, "", ContentAlignment.BottomCenter);
                    footer.Cells[1].CellSpan = 2;
                    chart.Legends[NameC].CustomItems.Add(footer);
                }
            //}
        }
        #endregion

        #region Flow Air

        if ((32 & II) == 32)
        {
            Chart chart = new Chart();
            chart.Titles.Add("");

            chart.Width = 850;
            chart.Height = 400;
            chart.Titles[0].Font = new Font("Arial", 14);
            chart.BackColor = Color.AliceBlue;

            chart.ChartAreas.Add(new ChartArea("Air"));
            chart.ChartAreas[0].AxisX.LabelStyle.Format = "dd MMM\nHH:mm";
            chart.ChartAreas[0].AxisX.TitleFont = new Font("Arial", 11, FontStyle.Bold);
            chart.ChartAreas[0].AxisX.TitleForeColor = Color.Red;
            chart.ID = "011";
            chart.Legends.Add("Air");
            chart.Legends["Air"].IsDockedInsideChartArea = false;
            chart.Legends["Air"].DockedToChartArea = "Air";
            //
            chart.ChartAreas[0].AxisY.Title = "נמ``ק";

            _charts.Add(chart);
            
            AddAirSeries(chart);
        }

        #endregion

        #region Flow Steam

        if ((64 & II) == 64)
        {
            Chart chart = new Chart();
            chart.Titles.Add("");

            chart.Width = 850;
            chart.Height = 400;
            chart.Titles[0].Font = new Font("Arial", 14);
            chart.BackColor = Color.AliceBlue;

            chart.ChartAreas.Add(new ChartArea("STEAM"));
            chart.ChartAreas[0].AxisX.LabelStyle.Format = "dd MMM\nHH:mm";
            chart.ChartAreas[0].AxisX.TitleFont = new Font("Arial", 11, FontStyle.Bold);
            chart.ChartAreas[0].AxisX.TitleForeColor = Color.Red;
            chart.ID = "012";
            chart.Legends.Add("Steam");
            chart.Legends["Steam"].IsDockedInsideChartArea = false;
            chart.Legends["Steam"].DockedToChartArea = "STEAM";
            //
            chart.ChartAreas[0].AxisY.Title = "קג/ש";

            _charts.Add(chart);
            
            AddSteamSeries(chart);
        }

        #endregion

        #region Temperature Consumers

        //temperature
        if (_temperConsumers.Count > 0)
        {
            //create chart
            Chart chart = new Chart();
            chart.Titles.Add("טמפרטורה");

            //AdjustAppearence(chart);
            chart.Width = 850;
            chart.Height = 400;
            chart.Titles[0].Font = new Font("Arial", 14);
            chart.BackColor = Color.AliceBlue;

            chart.ChartAreas.Add(new ChartArea("Temperature"));
            chart.ChartAreas[0].AxisX.LabelStyle.Format = "dd MMM\nHH:mm";// "g";
            chart.ChartAreas[0].AxisX.TitleFont = new Font("Arial", 11, FontStyle.Bold);
            chart.ChartAreas[0].AxisX.TitleForeColor = Color.Red;
            chart.ID = "004";
            chart.Legends.Add("Temperature");
            chart.Legends["Temperature"].IsDockedInsideChartArea = false;
            chart.Legends["Temperature"].DockedToChartArea = "Temperature";
            //
            chart.ChartAreas[0].AxisY.Title = "°C";

            _charts.Add(chart);

            //add series
            //foreach (var t in _temperConsumers)
            //{
                AddTemperSeries(chart);
            //}

            //set AxisX title
            //if (_temperConsumers.Count == 1 && _temperConsumers[0].Data.Count > 0)
                    //chart.ChartAreas[0].AxisX.Title = chart.Series[0].Points.Last().YValues[0].ToString();
            ////if ((_temperConsumers.Count == 1) && (te.Rows.Count > 0))
            ////    chart.ChartAreas[0].AxisX.Title = string.Format("קריאה אחרונה: {0:0.#} מעלות", te.Rows[te.Rows.Count - 1][1].ToString());
        }
        #endregion
    }

    public void BuildEn()
    {
        AllFunctions.HorMenuNo2 = false;

        double Rate = Convert.ToDouble(System.Configuration.ConfigurationManager.AppSettings["DollarExchangeRate"]);

        #region Check Type of Device
        int II = 0;
        DataClassesDataContext dc = new DataClassesDataContext();
        string SQL = "SELECT SUM(POWER(2, TID)) FROM ( "
            + "SELECT ([TypeID]) as TID FROM [dbo].[V_ConsumersType] WHERE  [ConsID] IN (" + ListIDs + ") "
            + "GROUP BY [TypeID]) AS TB ";
        string CS = dc.Connection.ConnectionString;
        using (SqlConnection CN = new SqlConnection(CS))
        {
            SqlCommand command = new SqlCommand(SQL, CN);
            try
            {
                CN.Open();
                II = Convert.ToInt32(command.ExecuteScalar().ToString());
            }
            catch (Exception)
            {
            }
        }

        #endregion

        #region Electricity Consumers NOT Taoz
        if ((2 & II) == 2)
        {
            DataSet dst = new DataSet();
            SqlDataAdapter sda = new SqlDataAdapter("", "");
            DataTable[] dtb = new DataTable[5];

            SQL = " [dbo].[S_CalculateNotTaoz] N'" + DateFrom.ToString("yyyy/MM/dd HH:mm:ss") + "', N'" + DateUntil.ToString("yyyy/MM/dd HH:mm:ss") + "', N'" + ListIDs + "', " + sResol;
            SqlConnection CN = new SqlConnection(CS);

            SqlCommand selectCMD = new SqlCommand(SQL, CN);
            selectCMD.CommandTimeout = 30;
            sda.SelectCommand = selectCMD;
            CN.Open();
            sda.Fill(dst, "Customers");
            CN.Close();

            dtb[0] = dst.Tables[0];
            TableVal = dtb[1] = dst.Tables[1];
            TableCos = dtb[2] = dst.Tables[2];
            dtb[3] = dst.Tables[3];
            Resol = "רזולוציה " + dtb[3].Rows[0][0].ToString();
            drRes = dtb[3].Rows[0];
            dtb[4] = dst.Tables[4];
            LowerTable = dtb[4];
            NameV = "ValueNotTaoz";
            NameC = "CostNotTaoz";

            DataRow drValue = dtb[1].Rows[0];
            DataRow drCost = dtb[2].Rows[0];

            Chart chart = new Chart();
            chart.Titles.Add((dtb[4].Rows[0][0].ToString()).Trim());
            AdjustAppearence(chart, 1);

            chart.ID = "001";
            chart.ChartAreas[0].AxisY.Title = dtb[4].Rows[0][1].ToString();
            if (AllFunctions.ConsumerType == "3")
            {
                chart.Legends[NameV].Title = "יצור, " + chart.ChartAreas[0].AxisY.Title; ;
            }
            if (AllFunctions.ConsumerType == "0")
            {
                chart.Legends[NameV].Title = "צריכה, " + chart.ChartAreas[0].AxisY.Title;
            }

            _charts.Add(chart);

            Series series1 = new Series("צריכה" + drValue[0].ToString());
            series1.ChartType = SeriesChartType.StackedColumn;
            series1.ChartArea = chart.ChartAreas[0].Name;
            series1.XValueType = ChartValueType.DateTime;
            series1.ToolTip = "#VALX{g} - #VALY{0.###}";
            series1.IsVisibleInLegend = true;
            series1.Legend = chart.Legends[0].Name;
            chart.Series.Add(series1);

            Series series2 = new Series("עלות" + drCost[0].ToString());
            series2.ChartType = SeriesChartType.StackedColumn;
            series2.ChartArea = chart.ChartAreas[1].Name;
            series2.XValueType = ChartValueType.DateTime;
            series2.ToolTip = "#VALX{g} - #VALY{0.##}";
            series2.LabelFormat = "0.##";
            series2.Legend = chart.Legends[1].Name;
            chart.Series.Add(series2);

            if (AllFunctions.ConsumerType == "3")
            {
                series1.LegendText = "יצור";
            }
            if (AllFunctions.ConsumerType == "0")
            {
                series1.LegendText = "צריכה";
            }

            series2.LegendText = "עלות";

            for (int i = 0; i < dtb[0].Rows.Count; i++)
            {
                series1.Points.AddXY(dtb[0].Rows[i][0], dtb[0].Rows[i][1]);
                series2.Points.AddXY(dtb[0].Rows[i][0], dtb[0].Rows[i][2]);
            }

            overallCost = (double)drCost[0];
        }

        #endregion

        #region Consumers Water or Gas
        if (((8 & II) == 8) || ((16 & II) == 16))
        {

            DataSet dst = new DataSet();
            SqlDataAdapter sda = new SqlDataAdapter("", "");
            DataTable[] dtb = new DataTable[5];

            DataTable[] dta = new DataTable[5];
            SQL = " [dbo].[S_CalculateWater] N'" + DateFrom.ToString("yyyy/MM/dd HH:mm:ss") + "', N'" + DateUntil.ToString("yyyy/MM/dd HH:mm:ss") + "', N'" + ListIDs + "', " + sResol;
            SqlConnection CN = new SqlConnection(CS);

            SqlCommand selectCMD = new SqlCommand(SQL, CN);
            selectCMD.CommandTimeout = 30;
            sda.SelectCommand = selectCMD;
            CN.Open();
            sda.Fill(dst, "WATERS");
            CN.Close();

            dta[0] = dst.Tables[0];
            TableVal = dta[1] = dst.Tables[1];
            TableCos = dta[2] = dst.Tables[2];
            dta[3] = dst.Tables[3];
            Resol = "רזולוציה " + dta[3].Rows[0][0].ToString();
            drRes = dta[3].Rows[0];
            dta[4] = dst.Tables[4];
            LowerTable = dta[4];
            NameV = "ValueWater";
            NameC = "CostWater";

            DataRow drValue = dta[1].Rows[0];
            DataRow drCost = dta[2].Rows[0];

            Chart chart = new Chart();
            chart.Titles.Add("Water");
            AdjustAppearence(chart, 1);
            chart.ID = "003";
            chart.ChartAreas[0].AxisY.Title = "M³/h";
            chart.Legends[NameV].Title = "Consuming, " + chart.ChartAreas[0].AxisY.Title;
            _charts.Add(chart);

            Series series1 = new Series("Consuming" + drValue[0].ToString());
            series1.ChartType = SeriesChartType.StackedColumn;
            series1.ChartArea = chart.ChartAreas[0].Name;
            series1.XValueType = ChartValueType.DateTime;
            series1.ToolTip = "#VALX{g} - #VALY{0.###}";
            series1.IsVisibleInLegend = true;
            series1.Legend = chart.Legends[0].Name;
            chart.Series.Add(series1);

            Series series2 = new Series("Cost" + drCost[0].ToString());
            series2.ChartType = SeriesChartType.StackedColumn;
            series2.ChartArea = chart.ChartAreas[1].Name;
            series2.XValueType = ChartValueType.DateTime;
            series2.ToolTip = "#VALX{g} - #VALY{0.##}";
            series2.LabelFormat = "0.##";
            series2.Legend = chart.Legends[1].Name;
            chart.Series.Add(series2);

            series1.LegendText = "Consuming";
            series2.LegendText = "Cost";

            for (int i = 0; i < dta[0].Rows.Count; i++)
            {
                series1.Points.AddXY(dta[0].Rows[i][0], dta[0].Rows[i][1]);
                series2.Points.AddXY(dta[0].Rows[i][0], (float)dta[0].Rows[i][2] / Rate);
            }

            if (drCost[0] == System.DBNull.Value)
            {
                overallCost = 0.0;
                sError = "The cost can be calculated only from the beginning of the month!";
            }
            else overallCost = (double)drCost[0];

        }

        #endregion

        #region Electricity Consumers Taoz

        if ((4 & II) == 4)
        {
            AllFunctions.HorMenuNo2 = true;
            #region Get data tables for Electricity Consumers

            DataSet dst = new DataSet();
            SqlDataAdapter sda = new SqlDataAdapter("", "");
            DataTable[] dtb = new DataTable[5];

            TableVal = null;
            TableCos = null;
            dst = new DataSet();
            sda = new SqlDataAdapter("", "");
            //DataTable[] dtb = new DataTable[5];

            string SS = "";
            SQL = "EXEC [dbo].[S_CalculateCompoundCounters] N'" + DateFrom.ToString("yyyy/MM/dd HH:mm:ss") + "', N'" + DateUntil.ToString("yyyy/MM/dd HH:mm:ss") + "', N'" + ListIDs + "', " + sResol;

            SqlConnection CN = new SqlConnection(CS);

            SqlCommand selectCMD = new SqlCommand(SQL, CN);
            selectCMD.CommandTimeout = 0;

            sda.SelectCommand = selectCMD;

            CN.Open();

            sda.Fill(dst, "Customers");

            CN.Close();

            dtb[0] = dst.Tables[0];
            TableVal = dtb[1] = dst.Tables[1];
            TableCos = dtb[2] = dst.Tables[2];
            dtb[3] = dst.Tables[3];
            Resol = "Resolution: " + dtb[3].Rows[0][0].ToString();
            dtb[4] = dst.Tables[4];
            LowerTable = dtb[4];

            DataRow drValue = dtb[1].Rows[0];
            DataRow drCost = dtb[2].Rows[0];
            NameV = "ValueTaoz";
            NameC = "CostTaoz";

            drValue = dtb[1].Rows[0];
            drCost = dtb[2].Rows[0];
            drRes = dtb[3].Rows[0];

            /* - */
            #endregion

            string taozSummaryConsumption = "";
            string taozSummaryCost = "";

            Chart chart = new Chart();
            chart.Titles.Add("Electricity");
            AdjustAppearence(chart, 1);
            chart.ID = "002";
            chart.ChartAreas[0].AxisY.Title = "kWh";
            if (AllFunctions.ConsumerType == "3")
            {
                chart.Legends[NameV].Title = "Production, kWh";
            }
            if (AllFunctions.ConsumerType == "0")
            {
                chart.Legends[NameV].Title = "Consuming, kWh";
            }
            _charts.Add(chart);

            double totalConsumption = 0, totalCost = 0;
            string[] LoadT = { "Pisga", "Geva", "Shefel" };

            for (int i = 0; i < 3; i++)
            {
                {
                    taozSummaryConsumption +=
                        String.Format("{0}: {1:0.##} {2}    ", LoadT[i], drValue[i], chart.ChartAreas[0].AxisY.Title);
                    taozSummaryCost +=
                        String.Format("{0}: {1:0.##} {2}    ", LoadT[i], drCost[i], chart.ChartAreas[1].AxisY.Title);
                }
                AddSeriesEn(chart, i, dtb[0]);
            }
            totalConsumption = (double)drValue[3];
            totalCost = (double)drCost[3] / Rate;
            overallCost += totalCost;

            if (taozSummaryConsumption != "")
            {
                LegendItem footer = new LegendItem();
                footer.Cells.Add(LegendCellType.Text, String.Format("{0:#,##0.##}", totalConsumption), ContentAlignment.BottomLeft);
                footer.Cells.Add(LegendCellType.Text, "Total", ContentAlignment.BottomLeft);
                footer.Cells.Add(LegendCellType.Text, "", ContentAlignment.BottomLeft);
                footer.Cells[1].CellSpan = 2;
                chart.Legends[NameV].CustomItems.Add(footer);
            }
            if (taozSummaryCost != "")
            {
                LegendItem footer = new LegendItem();
                footer.Cells.Add(LegendCellType.Text, String.Format("{0:#,##0.##}", totalCost), ContentAlignment.BottomLeft);
                footer.Cells.Add(LegendCellType.Text, "Total", ContentAlignment.BottomLeft);
                footer.Cells.Add(LegendCellType.Text, "", ContentAlignment.BottomLeft);
                footer.Cells[1].CellSpan = 2;
                chart.Legends[NameC].CustomItems.Add(footer);
            }
            //}
        }
        #endregion

        #region Temperature Consumers

        //temperature
        if (_temperConsumers.Count > 0)
        {
            //create chart
            Chart chart = new Chart();
            chart.Titles.Add("Temperature");

            //AdjustAppearence(chart);
            chart.Width = 850;
            chart.Height = 400;
            chart.Titles[0].Font = new Font("Arial", 14);
            chart.BackColor = Color.AliceBlue;

            chart.ChartAreas.Add(new ChartArea("Temperature"));
            chart.ChartAreas[0].AxisX.LabelStyle.Format = "dd MMM\nHH:mm";// "g";
            chart.ChartAreas[0].AxisX.TitleFont = new Font("Arial", 11, FontStyle.Bold);
            chart.ChartAreas[0].AxisX.TitleForeColor = Color.Red;
            chart.ID = "004";
            chart.Legends.Add("Temperature");
            chart.Legends["Temperature"].IsDockedInsideChartArea = false;
            chart.Legends["Temperature"].DockedToChartArea = "Temperature";
            //
            chart.ChartAreas[0].AxisY.Title = "°C";

            _charts.Add(chart);

            //add series
            //foreach (var t in _temperConsumers)
            //{
            AddTemperSeries(chart);
            //}

            //set AxisX title
            //if (_temperConsumers.Count == 1 && _temperConsumers[0].Data.Count > 0)
            //chart.ChartAreas[0].AxisX.Title = chart.Series[0].Points.Last().YValues[0].ToString();
            ////if ((_temperConsumers.Count == 1) && (te.Rows.Count > 0))
            ////    chart.ChartAreas[0].AxisX.Title = string.Format("קריאה אחרונה: {0:0.#} מעלות", te.Rows[te.Rows.Count - 1][1].ToString());
        }
        #endregion
    }
    public void BuildRu()
    {
        AllFunctions.HorMenuNo2 = false;

        double Rate = Convert.ToDouble(System.Configuration.ConfigurationManager.AppSettings["DollarExchangeRate"]);

        #region Check Type of Device
        int II = 0;
        DataClassesDataContext dc = new DataClassesDataContext();
        string SQL = "SELECT SUM(POWER(2, TID)) FROM ( "
            + "SELECT ([TypeID]) as TID FROM [dbo].[V_ConsumersType] WHERE  [ConsID] IN (" + ListIDs + ") "
            + "GROUP BY [TypeID]) AS TB ";
        string CS = dc.Connection.ConnectionString;
        using (SqlConnection CN = new SqlConnection(CS))
        {
            SqlCommand command = new SqlCommand(SQL, CN);
            try
            {
                CN.Open();
                II = Convert.ToInt32(command.ExecuteScalar().ToString());
            }
            catch (Exception)
            {
            }
        }

        #endregion

        #region Electricity Consumers NOT Taoz
        if ((2 & II) == 2)
        {
            DataSet dst = new DataSet();
            SqlDataAdapter sda = new SqlDataAdapter("", "");
            DataTable[] dtb = new DataTable[5];

            SQL = " [dbo].[S_CalculateNotTaoz] N'" + DateFrom.ToString("yyyy/MM/dd HH:mm:ss") + "', N'" + DateUntil.ToString("yyyy/MM/dd HH:mm:ss") + "', N'" + ListIDs + "', " + sResol;
            SqlConnection CN = new SqlConnection(CS);

            SqlCommand selectCMD = new SqlCommand(SQL, CN);
            selectCMD.CommandTimeout = 30;
            sda.SelectCommand = selectCMD;
            CN.Open();
            sda.Fill(dst, "Customers");
            CN.Close();

            dtb[0] = dst.Tables[0];
            TableVal = dtb[1] = dst.Tables[1];
            TableCos = dtb[2] = dst.Tables[2];
            dtb[3] = dst.Tables[3];
            Resol = "Разрешение " + dtb[3].Rows[0][0].ToString();
            drRes = dtb[3].Rows[0];
            dtb[4] = dst.Tables[4];
            LowerTable = dtb[4];
            NameV = "ValueNotTaoz";
            NameC = "CostNotTaoz";

            DataRow drValue = dtb[1].Rows[0];
            DataRow drCost = dtb[2].Rows[0];

            Chart chart = new Chart();
            chart.Titles.Add((dtb[4].Rows[0][0].ToString()).Trim());
            AdjustAppearence(chart, 1);

            chart.ID = "001";
            chart.ChartAreas[0].AxisY.Title = dtb[4].Rows[0][1].ToString();
            if (AllFunctions.ConsumerType == "3")
            {
                chart.Legends[NameV].Title = "Производство, " + chart.ChartAreas[0].AxisY.Title; ;
            }
            if (AllFunctions.ConsumerType == "0")
            {
                chart.Legends[NameV].Title = "Потребление, " + chart.ChartAreas[0].AxisY.Title;
            }

            _charts.Add(chart);

            Series series1 = new Series("Потребление" + drValue[0].ToString());
            series1.ChartType = SeriesChartType.StackedColumn;
            series1.ChartArea = chart.ChartAreas[0].Name;
            series1.XValueType = ChartValueType.DateTime;
            series1.ToolTip = "#VALX{g} - #VALY{0.###}";
            series1.IsVisibleInLegend = true;
            series1.Legend = chart.Legends[0].Name;
            chart.Series.Add(series1);

            Series series2 = new Series("Стоимость" + drCost[0].ToString());
            series2.ChartType = SeriesChartType.StackedColumn;
            series2.ChartArea = chart.ChartAreas[1].Name;
            series2.XValueType = ChartValueType.DateTime;
            series2.ToolTip = "#VALX{g} - #VALY{0.##}";
            series2.LabelFormat = "0.##";
            series2.Legend = chart.Legends[1].Name;
            chart.Series.Add(series2);

            if (AllFunctions.ConsumerType == "3")
            {
                series1.LegendText = "Производство";
            }
            if (AllFunctions.ConsumerType == "0")
            {
                series1.LegendText = "Потребление";
            }

            series2.LegendText = "Стоимость";

            for (int i = 0; i < dtb[0].Rows.Count; i++)
            {
                series1.Points.AddXY(dtb[0].Rows[i][0], dtb[0].Rows[i][1]);
                series2.Points.AddXY(dtb[0].Rows[i][0], dtb[0].Rows[i][2]);
            }

            overallCost = (double)drCost[0];
        }

        #endregion

        #region Consumers Water or Gas
        if (((8 & II) == 8) || ((16 & II) == 16))
        {

            DataSet dst = new DataSet();
            SqlDataAdapter sda = new SqlDataAdapter("", "");
            DataTable[] dtb = new DataTable[5];

            DataTable[] dta = new DataTable[5];
            SQL = " [dbo].[S_CalculateWater] N'" + DateFrom.ToString("yyyy/MM/dd HH:mm:ss") + "', N'" + DateUntil.ToString("yyyy/MM/dd HH:mm:ss") + "', N'" + ListIDs + "', " + sResol;
            SqlConnection CN = new SqlConnection(CS);

            SqlCommand selectCMD = new SqlCommand(SQL, CN);
            selectCMD.CommandTimeout = 30;
            sda.SelectCommand = selectCMD;
            CN.Open();
            sda.Fill(dst, "WATERS");
            CN.Close();

            dta[0] = dst.Tables[0];
            TableVal = dta[1] = dst.Tables[1];
            TableCos = dta[2] = dst.Tables[2];
            dta[3] = dst.Tables[3];
            Resol = "Разрешение " + dta[3].Rows[0][0].ToString();
            drRes = dta[3].Rows[0];
            dta[4] = dst.Tables[4];
            LowerTable = dta[4];
            NameV = "ValueWater";
            NameC = "CostWater";

            DataRow drValue = dta[1].Rows[0];
            DataRow drCost = dta[2].Rows[0];

            Chart chart = new Chart();
            chart.Titles.Add("Вода");
            AdjustAppearence(chart, 2);
            chart.ID = "003";
            chart.ChartAreas[0].AxisY.Title = "M³/ч";
            chart.Legends[NameV].Title = "Потребление, " + chart.ChartAreas[0].AxisY.Title;
            _charts.Add(chart);

            Series series1 = new Series("Потребление" + drValue[0].ToString());
            series1.ChartType = SeriesChartType.StackedColumn;
            series1.ChartArea = chart.ChartAreas[0].Name;
            series1.XValueType = ChartValueType.DateTime;
            series1.ToolTip = "#VALX{g} - #VALY{0.###}";
            series1.IsVisibleInLegend = true;
            series1.Legend = chart.Legends[0].Name;
            chart.Series.Add(series1);

            Series series2 = new Series("Стоимость" + drCost[0].ToString());
            series2.ChartType = SeriesChartType.StackedColumn;
            series2.ChartArea = chart.ChartAreas[1].Name;
            series2.XValueType = ChartValueType.DateTime;
            series2.ToolTip = "#VALX{g} - #VALY{0.##}";
            series2.LabelFormat = "0.##";
            series2.Legend = chart.Legends[1].Name;
            chart.Series.Add(series2);

            series1.LegendText = "Потребление";
            series2.LegendText = "Стоимость";

            for (int i = 0; i < dta[0].Rows.Count; i++)
            {
                series1.Points.AddXY(dta[0].Rows[i][0], dta[0].Rows[i][1]);
                series2.Points.AddXY(dta[0].Rows[i][0], (float)dta[0].Rows[i][2] / Rate);
            }

            if (drCost[0] == System.DBNull.Value)
            {
                overallCost = 0.0;
                sError = "Стоимость может быть рассчитана только с начала месяца!";
            }
            else overallCost = (double)drCost[0];

        }

        #endregion

        #region Electricity Consumers Taoz

        if ((4 & II) == 4)
        {
            AllFunctions.HorMenuNo2 = true;
            #region Get data tables for Electricity Consumers

            DataSet dst = new DataSet();
            SqlDataAdapter sda = new SqlDataAdapter("", "");
            DataTable[] dtb = new DataTable[5];

            TableVal = null;
            TableCos = null;
            dst = new DataSet();
            sda = new SqlDataAdapter("", "");
            //DataTable[] dtb = new DataTable[5];

            string SS = "";
            SQL = "EXEC [dbo].[S_CalculateCompoundCounters] N'" + DateFrom.ToString("yyyy/MM/dd HH:mm:ss") + "', N'" + DateUntil.ToString("yyyy/MM/dd HH:mm:ss") + "', N'" + ListIDs + "', " + sResol;

            SqlConnection CN = new SqlConnection(CS);

            SqlCommand selectCMD = new SqlCommand(SQL, CN);
            selectCMD.CommandTimeout = 0;

            sda.SelectCommand = selectCMD;

            CN.Open();

            sda.Fill(dst, "Customers");

            CN.Close();

            dtb[0] = dst.Tables[0];
            TableVal = dtb[1] = dst.Tables[1];
            TableCos = dtb[2] = dst.Tables[2];
            dtb[3] = dst.Tables[3];
            Resol = "Разрешение: " + dtb[3].Rows[0][0].ToString();
            dtb[4] = dst.Tables[4];
            LowerTable = dtb[4];

            DataRow drValue = dtb[1].Rows[0];
            DataRow drCost = dtb[2].Rows[0];
            NameV = "ValueTaoz";
            NameC = "СтоимостьTaoz";

            drValue = dtb[1].Rows[0];
            drCost = dtb[2].Rows[0];
            drRes = dtb[3].Rows[0];

            /* - */
            #endregion

            string taozSummaryConsumption = "";
            string taozSummaryCost = "";

            Chart chart = new Chart();
            chart.Titles.Add("Электричество");
            AdjustAppearence(chart, 2);
            chart.ID = "002";
            chart.ChartAreas[0].AxisY.Title = "кВтч";
            if (AllFunctions.ConsumerType == "3")
            {
                chart.Legends[NameV].Title = "Производство, кВтч";
            }
            if (AllFunctions.ConsumerType == "0")
            {
                chart.Legends[NameV].Title = "Потребление, кВтч";
            }
            _charts.Add(chart);

            double totalConsumption = 0, totalCost = 0;
            string[] LoadT = { "Пиковая зона", "Полупиковая", "Ночная зона" };

            for (int i = 0; i < 3; i++)
            {
                {
                    taozSummaryConsumption +=
                        String.Format("{0}: {1:0.##} {2}    ", LoadT[i], drValue[i], chart.ChartAreas[0].AxisY.Title);
                    taozSummaryCost +=
                        String.Format("{0}: {1:0.##} {2}    ", LoadT[i], drCost[i], chart.ChartAreas[1].AxisY.Title);
                }
                AddSeriesRu(chart, i, dtb[0]);
            }
            totalConsumption = (double)drValue[3];
            totalCost = (double)drCost[3] / Rate;
            overallCost += totalCost;

            if (taozSummaryConsumption != "")
            {
                LegendItem footer = new LegendItem();
                footer.Cells.Add(LegendCellType.Text, String.Format("{0:#,##0.##}", totalConsumption), ContentAlignment.BottomLeft);
                footer.Cells.Add(LegendCellType.Text, "Итого", ContentAlignment.BottomLeft);
                footer.Cells.Add(LegendCellType.Text, "", ContentAlignment.BottomLeft);
                footer.Cells[1].CellSpan = 2;
                chart.Legends[NameV].CustomItems.Add(footer);
            }
            if (taozSummaryCost != "")
            {
                LegendItem footer = new LegendItem();
                footer.Cells.Add(LegendCellType.Text, String.Format("{0:#,##0.##}", totalCost), ContentAlignment.BottomLeft);
                footer.Cells.Add(LegendCellType.Text, "Итого", ContentAlignment.BottomLeft);
                footer.Cells.Add(LegendCellType.Text, "", ContentAlignment.BottomLeft);
                footer.Cells[1].CellSpan = 2;
                chart.Legends[NameC].CustomItems.Add(footer);
            }
            //}
        }
        #endregion

        #region Temperature Consumers

        //temperature
        if (_temperConsumers.Count > 0)
        {
            //create chart
            Chart chart = new Chart();
            chart.Titles.Add("Температура");

            //AdjustAppearence(chart);
            chart.Width = 850;
            chart.Height = 400;
            chart.Titles[0].Font = new Font("Arial", 14);
            chart.BackColor = Color.AliceBlue;

            chart.ChartAreas.Add(new ChartArea("Температура"));
            chart.ChartAreas[0].AxisX.LabelStyle.Format = "dd MMM\nHH:mm";// "g";
            chart.ChartAreas[0].AxisX.TitleFont = new Font("Arial", 11, FontStyle.Bold);
            chart.ChartAreas[0].AxisX.TitleForeColor = Color.Red;
            chart.ID = "004";
            chart.Legends.Add("Температура");
            chart.Legends["Температура"].IsDockedInsideChartArea = false;
            chart.Legends["Температура"].DockedToChartArea = "Температура";
            //
            chart.ChartAreas[0].AxisY.Title = "°C";

            _charts.Add(chart);

            //add series
            //foreach (var t in _temperConsumers)
            //{
            AddTemperSeries(chart);
            //}

            //set AxisX title
            //if (_temperConsumers.Count == 1 && _temperConsumers[0].Data.Count > 0)
            //chart.ChartAreas[0].AxisX.Title = chart.Series[0].Points.Last().YValues[0].ToString();
            ////if ((_temperConsumers.Count == 1) && (te.Rows.Count > 0))
            ////    chart.ChartAreas[0].AxisX.Title = string.Format("קריאה אחרונה: {0:0.#} מעלות", te.Rows[te.Rows.Count - 1][1].ToString());
        }
        #endregion
    }

    public List<string> GetCounterValues(string CounterType)
    {
        List<string> list = new List<string>();
        list.Add("מונה\\תעריף,קריאה עדכנית,נכון לשעה");
        foreach (tbl_Counter c in _counters)
            try
            {
                if (c.tbl_CounterType.sql_description.Trim() == CounterType)
                    list.Add(
                        (c.ShortName == "" || c.ShortName == null ?
                            c.T_Tariff.TariffName :
                            c.ShortName) 
                        + "," +
                        (_countersLastVal[_counters.IndexOf(c)] == null ?
                            "," :
                            _countersLastVal[_counters.IndexOf(c)].Value.ToString() + "," +
                            _countersLastVal[_counters.IndexOf(c)].Timestamp.ToString("yyyy-MM-dd HH:mm")
                            )
                        );
            }
            catch { }
        return list;
    }

    private void AdjustAppearence(Chart chart, int LocalLang)
    {
        //chart.CssClass = "Chart";
        int Rez = (int)drRes[1];
        //chart.Width = 940;
        chart.Height = 400;
        chart.Titles[0].Font = new Font("Arial", 14, FontStyle.Bold);
        chart.BackColor = Color.AliceBlue;

        chart.ChartAreas.Add(new ChartArea(NameV));
        chart.ChartAreas.Add(new ChartArea(NameC));
        if (Rez > 1000)
        {
            chart.ChartAreas[0].AxisX.LabelStyle.Format = "dd/MM\nyyyy";
            chart.ChartAreas[1].AxisX.LabelStyle.Format = "dd/MM\nyyyy";
        }
        else
        {
            chart.ChartAreas[0].AxisX.LabelStyle.Format = "dd/MM\nHH:mm";// "g";
            chart.ChartAreas[1].AxisX.LabelStyle.Format = "dd/MM\nHH:mm";// "g";
        }
        chart.ChartAreas[0].AxisX.TitleFont = new Font("Arial", 11, FontStyle.Bold);
        chart.ChartAreas[1].AxisX.TitleFont = new Font("Arial", 11, FontStyle.Bold);
        chart.ChartAreas[0].AxisX.TitleForeColor = Color.Red;
        chart.ChartAreas[1].AxisX.TitleForeColor = Color.Red;
        //chart.ChartAreas[0].AxisY.Title = c.tbl_CounterType.sql_meser_unit;

        if (LocalLang == 0)
        {
            chart.ChartAreas[1].AxisY.Title = "ש\"ח";
        }
        if (LocalLang == 1)
        {
            chart.ChartAreas[1].AxisY.Title = "$";
        }
        if (LocalLang == 2)
        {
            chart.ChartAreas[1].AxisY.Title = "₽";
        }

        chart.ChartAreas[0].AxisY.TitleFont = new Font("Arial", 12, FontStyle.Bold);
        chart.ChartAreas[1].AxisY.TitleFont = new Font("Arial", 12, FontStyle.Bold);

        chart.Legends.Add(NameV);
        chart.Legends.Add(NameC);

        chart.Legends[NameV].IsDockedInsideChartArea = false;
        chart.Legends[NameV].DockedToChartArea = NameV;
        chart.Legends[NameV].LegendItemOrder = LegendItemOrder.ReversedSeriesOrder;
        chart.Legends[NameV].TitleFont = chart.ChartAreas[0].AxisY.TitleFont;
        chart.Legends[NameC].IsDockedInsideChartArea = false;
        chart.Legends[NameC].DockedToChartArea = NameC;
        chart.Legends[NameC].LegendItemOrder = LegendItemOrder.ReversedSeriesOrder;
        if (LocalLang == 0)
        {
            chart.Legends[NameC].Title = "עלות, " + chart.ChartAreas[1].AxisY.Title;
        }
        if (LocalLang == 1)
        {
            chart.Legends[NameC].Title = "Cost, " + chart.ChartAreas[1].AxisY.Title;
        }
        if (LocalLang == 2)
        {
            chart.Legends[NameC].Title = "Стоимость, " + chart.ChartAreas[1].AxisY.Title;
        }
        
        chart.Legends[NameC].TitleFont = chart.Legends[NameV].TitleFont;

        LegendCellColumn colTotal = new LegendCellColumn();
        colTotal.Text = "#TOTAL{N2}";
        chart.Legends[NameV].CellColumns.Add(colTotal);
        chart.Legends[NameC].CellColumns.Add(colTotal);

        LegendCellColumn secondColumn = new LegendCellColumn();
        secondColumn.ColumnType = LegendCellColumnType.Text;
        //secondColumn.HeaderText = "Name";
        secondColumn.Text = "#LEGENDTEXT";
        secondColumn.HeaderBackColor = Color.WhiteSmoke;
        chart.Legends[NameV].CellColumns.Add(secondColumn);
        chart.Legends[NameC].CellColumns.Add(secondColumn);

        LegendCellColumn firstColumn = new LegendCellColumn();
        firstColumn.ColumnType = LegendCellColumnType.SeriesSymbol;
        //firstColumn.HeaderText = "Color";
        firstColumn.HeaderBackColor = Color.WhiteSmoke;
        chart.Legends[NameV].CellColumns.Add(firstColumn);
        chart.Legends[NameC].CellColumns.Add(firstColumn);


        if (LocalLang == 1)
        {
            for (int i = 0; i < 2; i++)
            {
                chart.Legends[i].CellColumns[0].Alignment = ContentAlignment.BottomLeft;
                chart.Legends[i].CellColumns[1].Alignment = ContentAlignment.BottomLeft;
            }
        }
    }

    private void AddTemperSeries( Chart chart)
    {
        Series series = new Series("טמפרטורה");
        series.ChartType = SeriesChartType.Line;
        series.BorderWidth = 3;//set line width
        series.ChartArea = chart.ChartAreas[0].Name;
        series.XValueType = ChartValueType.DateTime;
        series.ToolTip = "#VALX{g} - #VALY{0}°C";
        series.IsVisibleInLegend = true;
        series.Legend = chart.Legends[0].Name;

        chart.Series.Add(series);

        for (int i = 0; i < te.Rows.Count; i++)
        {
            series.Points.AddXY((DateTime)te.Rows[i][0], Convert.ToDouble(te.Rows[i][1]));
        }

        Series ss = new Series("צריכת חשמל");
        ss.YAxisType = AxisType.Secondary;
        chart.ChartAreas[0].AxisY2.Title = "קווט''ש";
        chart.ChartAreas[0].AxisY.TitleFont = new Font("Arial", 12, FontStyle.Bold);
        chart.ChartAreas[0].AxisY2.TitleFont = new Font("Arial", 12, FontStyle.Bold);
        chart.ChartAreas[0].AxisY2.TextOrientation = TextOrientation.Horizontal;
        //chart.ChartAreas[0].AxisY2.TitleAlignment = StringAlignment.Center;
        ss.ChartType = SeriesChartType.Line;
        ss.BorderWidth = 3;//set line width
        ss.Color = Color.Brown;
        ss.BorderColor = Color.Brown;
        ss.ChartArea = chart.ChartAreas[0].Name;
        ss.XValueType = ChartValueType.DateTime;
        ss.ToolTip = "#VALX{g} - #VALY{0.##}";
        ss.IsVisibleInLegend = true;
        ss.Legend = chart.Legends[0].Name;

        chart.Series.Add(ss);

        for (int i = 0; i < te.Rows.Count; i++)
        {
            ss.Points.AddXY((DateTime)te.Rows[i][0], Convert.ToDouble(te.Rows[i][2]));
        }
    }

    private void AddSeries(Chart chart, int LoadID, DataTable dt)
   {
        string[] LoadT = { "פסגה", "גבע", "שפל" };
        int Rez = (int)drRes[1];
        
        Series series1 = new Series("צריכה" + LoadID.ToString());
        series1.ChartType = SeriesChartType.StackedColumn;
        series1.ChartArea = chart.ChartAreas[0].Name;
        series1.XValueType = ChartValueType.DateTime;
        series1.ToolTip = "#VALX{g} - #VALY{0.###}";
        series1.IsVisibleInLegend = true;
        series1.Legend = chart.Legends[0].Name;
        

        chart.Series.Add(series1);

        Series series2 = new Series("עלות" + LoadID.ToString());
        series2.ChartType = SeriesChartType.StackedColumn;
        series2.ChartArea = chart.ChartAreas[1].Name;
        series2.XValueType = ChartValueType.DateTime;
        series2.ToolTip = "#VALX{g} - #VALY{0.##}";
        series2.LabelFormat = "0.##";
        series2.Legend = chart.Legends[1].Name;

        chart.Series.Add(series2);

        {
            series1.LegendText = LoadT[LoadID];
            series2.LegendText = LoadT[LoadID];
            series1.Name += "(" + series1.LegendText + ")";
            series2.Name += "(" + series2.LegendText + ")";
            if (LoadID == 0)//pisga
            {
                series1.Color = Color.Red;
                series2.Color = Color.Red;
            }
            else if (LoadID == 1)//geva
            {
                series1.Color = Color.Gold;
                series2.Color = Color.Gold;
            }
            else //shefel
            {
                series1.Color = Color.Green;
                series2.Color = Color.Green;
            }
        }
        LoadID++;

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            series1.Points.AddXY((DateTime)dt.Rows[i][0], Convert.ToDouble(dt.Rows[i][LoadID * 2 - 1]));
            series2.Points.AddXY((DateTime)dt.Rows[i][0], Convert.ToDouble(dt.Rows[i][LoadID * 2]));
        }
    }

    private void AddAirSeries(Chart chart)
    {
        int Lid = 0;
        Series series = new Series("ספיקת אוויר");
        series.ChartType = SeriesChartType.StackedColumn;
        series.BorderWidth = 2;//set line width
        series.ChartArea = chart.ChartAreas[0].Name;
        series.XValueType = ChartValueType.DateTime;
        series.ToolTip = "#VALX{g} - #VALY{0}Nm³";
        series.IsVisibleInLegend = true;
        series.Legend = chart.Legends[0].Name;

        chart.Series.Add(series);

        te = AllFunctions.Populate("EXEC  [dbo].[S_CalculateAir] N'" + DateFrom.ToString("yyyy-MM-dd HH:mm:ss") + "', N'" + DateUntil.ToString("yyyy-MM-dd HH:mm:ss") + "', '" + ListIDs + "'" );

        for (int i = 0; i < te.Rows.Count; i++)
        {
            series.Points.AddXY((DateTime)te.Rows[i][0], Convert.ToDouble(te.Rows[i][1]));
        }

        string SS = AllFunctions.GetScalar("SELECT ISNULL([ConsumerEnergy], -1) AS CE FROM [dbo].[tbl_Consumers_0] WHERE [sql_Consumer_ID] = " + ListIDs);
        if (SS == "-1")
        {
            return;
        }
        DataSet dst = new DataSet();
        SqlDataAdapter sda = new SqlDataAdapter("", "");
        DataTable[] dtb = new DataTable[5];

        TableVal = null;
        TableCos = null;
        dst = new DataSet();
        sda = new SqlDataAdapter("", "");
        //DataTable[] dtb = new DataTable[5];

        string SQL = "EXEC [dbo].[S_CalculateCompoundCounters] N'" + DateFrom.ToString("yyyy/MM/dd HH:mm:ss") + "', N'" + DateUntil.ToString("yyyy/MM/dd HH:mm:ss") + "', N'" + SS + "', 0";

        SqlConnection CN = new SqlConnection(AllFunctions.ConnectStr);

        SqlCommand selectCMD = new SqlCommand(SQL, CN);
        selectCMD.CommandTimeout = 0;

        sda.SelectCommand = selectCMD;

        CN.Open();

        sda.Fill(dst, "Customers");

        CN.Close();

        dtb[0] = dst.Tables[0];
        TableVal = dtb[1] = dst.Tables[1];
        TableCos = dtb[2] = dst.Tables[2];
        dtb[3] = dst.Tables[3];
        Resol = "רזולוציה " + dtb[3].Rows[0][0].ToString();
        dtb[4] = dst.Tables[4];
        LowerTable = dtb[4];

        DataRow drValue = dtb[1].Rows[0];
        DataRow drCost = dtb[2].Rows[0];
        NameV = "ValueTaoz";
        NameC = "CostTaoz";

        drValue = dtb[1].Rows[0];
        drCost = dtb[2].Rows[0];
        drRes = dtb[3].Rows[0];

        #region AdjustAppearence

        chart.ChartAreas.Add(new ChartArea(NameV));
        //chart.ChartAreas.Add(new ChartArea(NameC));
        //chart.ChartAreas[0].AxisX.LabelStyle.Format = "dd/MM\nHH:mm";// "g";
        //chart.ChartAreas[0].AxisX.TitleFont = new Font("Arial", 11, FontStyle.Bold);
        //chart.ChartAreas[0].AxisX.TitleForeColor = Color.Red;
        chart.ChartAreas[1].AxisX.LabelStyle.Format = "dd/MM\nHH:mm";// "g";
        chart.ChartAreas[1].AxisX.TitleFont = new Font("Arial", 11, FontStyle.Bold);
        chart.ChartAreas[1].AxisX.TitleForeColor = Color.Red;
        //chart.ChartAreas[0].AxisY.Title = c.tbl_CounterType.sql_meser_unit;

        //chart.ChartAreas[0].AxisY.TitleFont = new Font("Arial", 12, FontStyle.Bold);
        chart.ChartAreas[1].AxisY.TitleFont = new Font("Arial", 12, FontStyle.Bold);

        chart.Legends.Add(NameV);
        //chart.Legends.Add(NameC);

        chart.Legends[NameV].IsDockedInsideChartArea = false;
        chart.Legends[NameV].DockedToChartArea = NameV;
        chart.Legends[NameV].LegendItemOrder = LegendItemOrder.ReversedSeriesOrder;
        chart.Legends[NameV].TitleFont = chart.ChartAreas[0].AxisY.TitleFont;
        //chart.Legends[NameC].IsDockedInsideChartArea = false;
        //chart.Legends[NameC].DockedToChartArea = NameC;
        //chart.Legends[NameC].LegendItemOrder = LegendItemOrder.ReversedSeriesOrder;
        //chart.Legends[NameC].TitleFont = chart.Legends[NameV].TitleFont;

        LegendCellColumn colTotal = new LegendCellColumn();
        colTotal.Text = "#TOTAL{N2}";
        chart.Legends[NameV].CellColumns.Add(colTotal);
        //chart.Legends[NameC].CellColumns.Add(colTotal);

        LegendCellColumn secondColumn = new LegendCellColumn();
        secondColumn.ColumnType = LegendCellColumnType.Text;
        //secondColumn.HeaderText = "Name";
        secondColumn.Text = "#LEGENDTEXT";
        secondColumn.HeaderBackColor = Color.WhiteSmoke;
        chart.Legends[NameV].CellColumns.Add(secondColumn);
        //chart.Legends[NameC].CellColumns.Add(secondColumn);

        LegendCellColumn firstColumn = new LegendCellColumn();
        firstColumn.ColumnType = LegendCellColumnType.SeriesSymbol;
        //firstColumn.HeaderText = "Color";
        firstColumn.HeaderBackColor = Color.WhiteSmoke;
        chart.Legends[NameV].CellColumns.Add(firstColumn);
        //chart.Legends[NameC].CellColumns.Add(firstColumn);

        #endregion

        //AdjustAppearence(chart, 0);
        //chart.ID = "002";
        chart.ChartAreas[1].AxisY.Title = "קוט''ש";
        //chart.Legends[NameV].Title = "צריכה, קוט''ש";
        //_charts.Add(chart);

        string[] LoadT = { "Pisga", "Geva", "Shefel" };
        int Rez = (int)drRes[1];

        for (int LoadID = 0; LoadID < 3; LoadID++)
        {

            Series series1 = new Series("צריכה" + LoadID.ToString());
            series1.ChartType = SeriesChartType.StackedColumn;
            series1.ChartArea = chart.ChartAreas[1].Name;
            series1.XValueType = ChartValueType.DateTime;
            series1.ToolTip = "#VALX{g} - #VALY{0.###}";
            series1.IsVisibleInLegend = true;
            series1.Legend = chart.Legends[1].Name;


            chart.Series.Add(series1);

            //Series series2 = new Series("עלות" + LoadID.ToString());
            //series2.ChartType = SeriesChartType.StackedColumn;
            //series2.ChartArea = chart.ChartAreas[1].Name;
            //series2.XValueType = ChartValueType.DateTime;
            //series2.ToolTip = "#VALX{g} - #VALY{0.##}";
            //series2.LabelFormat = "0.##";
            //series2.Legend = chart.Legends[1].Name;

            //chart.Series.Add(series2);

            {
                series1.LegendText = LoadT[LoadID];
                //series2.LegendText = LoadT[LoadID];
                series1.Name += "(" + series1.LegendText + ")";
                //series2.Name += "(" + series2.LegendText + ")";
                if (LoadID == 0)//pisga
                {
                    series1.Color = Color.Red;
                    //series2.Color = Color.Red;
                }
                else if (LoadID == 1)//geva
                {
                    series1.Color = Color.Gold;
                    //series2.Color = Color.Gold;
                }
                else //shefel
                {
                    series1.Color = Color.Green;
                    //series2.Color = Color.Green;
                }
            }
            Lid = LoadID + 1;

            for (int i = 0; i < dtb[0].Rows.Count; i++)
            {
                series1.Points.AddXY((DateTime)dtb[0].Rows[i][0], Convert.ToDouble(dtb[0].Rows[i][Lid * 2 - 1]));
                //series2.Points.AddXY((DateTime)dt.Rows[i][0], Convert.ToDouble(dt.Rows[i][LoadID * 2]));
            }

        }








    }

    private void AddSteamSeries(Chart chart)
    {
        Series series = new Series("ספיקת קיתור");
        series.ChartType = SeriesChartType.Line;
        series.BorderWidth = 2;//set line width
        series.ChartArea = chart.ChartAreas[0].Name;
        series.XValueType = ChartValueType.DateTime;
        series.ToolTip = "#VALX{g} - #VALY{0}kG/h";
        series.IsVisibleInLegend = true;
        series.Legend = chart.Legends[0].Name;

        chart.Series.Add(series);

        te = AllFunctions.Populate("EXEC  [dbo].[S_CalculateSteam] N'" + DateFrom.ToString("yyyy-MM-dd HH:mm:ss") + "', N'" + DateUntil.ToString("yyyy-MM-dd HH:mm:ss") + "', '" + ListIDs + "'" );

        for (int i = 0; i < te.Rows.Count; i++)
        {
            series.Points.AddXY((DateTime)te.Rows[i][0], Convert.ToDouble(te.Rows[i][1]));
        }
    }

    private void AddSeriesEn(Chart chart, int LoadID, DataTable dt)
    {
        double Rate = Convert.ToDouble(System.Configuration.ConfigurationManager.AppSettings["DollarExchangeRate"]);

        string[] LoadT = {"Peak", "Mid Peak", "Off Peak"};
        int Rez = (int)drRes[1];

        Series series1 = new Series("Consuming" + LoadID.ToString());
        series1.ChartType = SeriesChartType.StackedColumn;
        series1.ChartArea = chart.ChartAreas[0].Name;
        series1.XValueType = ChartValueType.DateTime;
        series1.ToolTip = "#VALX{g} - #VALY{0.###}";
        series1.IsVisibleInLegend = true;
        series1.Legend = chart.Legends[0].Name;


        chart.Series.Add(series1);

        Series series2 = new Series("Cost" + LoadID.ToString());
        series2.ChartType = SeriesChartType.StackedColumn;
        series2.ChartArea = chart.ChartAreas[1].Name;
        series2.XValueType = ChartValueType.DateTime;
        series2.ToolTip = "#VALX{g} - #VALY{0.##}";
        series2.LabelFormat = "0.##";
        series2.Legend = chart.Legends[1].Name;

        chart.Series.Add(series2);

        //if (Counter.TaozCounter.T_TaozLoad != null)
        {
            series1.LegendText = LoadT[LoadID];
            series2.LegendText = LoadT[LoadID];
            series1.Name += "(" + series1.LegendText + ")";
            series2.Name += "(" + series2.LegendText + ")";
            if (LoadID == 0)//pisga
            {
                series1.Color = Color.Red;
                series2.Color = Color.Red;
            }
            else if (LoadID == 1)//geva
            {
                series1.Color = Color.Gold;
                series2.Color = Color.Gold;
            }
            else //shefel
            {
                series1.Color = Color.Green;
                series2.Color = Color.Green;
            }
        }
        LoadID++;

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            series1.Points.AddXY((DateTime)dt.Rows[i][0], Convert.ToDouble(dt.Rows[i][LoadID * 2 - 1]));
            series2.Points.AddXY((DateTime)dt.Rows[i][0], Convert.ToDouble(dt.Rows[i][LoadID * 2]) / Rate);
        }
    }

    private void AddSeriesRu(Chart chart, int LoadID, DataTable dt)
    {
        double Rate = Convert.ToDouble(System.Configuration.ConfigurationManager.AppSettings["DollarExchangeRate"]);

        string[] LoadT = { "Пиковая зона", "Полупиковая", "Ночная зона" };
        int Rez = (int)drRes[1];

        Series series1 = new Series("Потребление " + LoadID.ToString());
        series1.ChartType = SeriesChartType.StackedColumn;
        series1.ChartArea = chart.ChartAreas[0].Name;
        series1.XValueType = ChartValueType.DateTime;
        series1.ToolTip = "#VALX{g} - #VALY{0.###}";
        series1.IsVisibleInLegend = true;
        series1.Legend = chart.Legends[0].Name;


        chart.Series.Add(series1);

        Series series2 = new Series("Стоимость" + LoadID.ToString());
        series2.ChartType = SeriesChartType.StackedColumn;
        series2.ChartArea = chart.ChartAreas[1].Name;
        series2.XValueType = ChartValueType.DateTime;
        series2.ToolTip = "#VALX{g} - #VALY{0.##}";
        series2.LabelFormat = "0.##";
        series2.Legend = chart.Legends[1].Name;

        chart.Series.Add(series2);

        //if (Counter.TaozCounter.T_TaozLoad != null)
        {
            series1.LegendText = LoadT[LoadID];
            series2.LegendText = LoadT[LoadID];
            series1.Name += "(" + series1.LegendText + ")";
            series2.Name += "(" + series2.LegendText + ")";
            if (LoadID == 0)//pisga
            {
                series1.Color = Color.Red;
                series2.Color = Color.Red;
            }
            else if (LoadID == 1)//geva
            {
                series1.Color = Color.Gold;
                series2.Color = Color.Gold;
            }
            else //shefel
            {
                series1.Color = Color.Green;
                series2.Color = Color.Green;
            }
        }
        LoadID++;

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            series1.Points.AddXY((DateTime)dt.Rows[i][0], Convert.ToDouble(dt.Rows[i][LoadID * 2 - 1]));
            series2.Points.AddXY((DateTime)dt.Rows[i][0], Convert.ToDouble(dt.Rows[i][LoadID * 2]) / Rate);
        }
    }

    public void FillTimestamps(List<DataSample> Data)
    {
        foreach (DataSample sample in Data)
        {
            if ((from s in _emptyPoints where s.Timestamp == sample.Timestamp select s).Count() < 1)
                _emptyPoints.Add(new DataSample(sample.Timestamp, 0, 0));
        }
    }

    public List<Chart> Charts
    {
        get { return _charts; }
    }
}
