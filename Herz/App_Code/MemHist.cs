﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Class to store paging history of MemberPages...
/// </summary>
public class MemHist
{
	public MemHist()
	{
        
    }
    static public bool byArrow = false;

    static private int PagesHist;
    static private int CurHist;
    static private List<string> Hists = new List<string>();

    static public int HistNow
    {
        get { return CurHist; }
        set
        {
            if ((value == -1) && (CurHist > 1))
            {
                CurHist--;
            }
            else
            {
                if ((value == 1) && (CurHist < PagesHist - 1))
                {
                    CurHist++;
                }
            } 
        }
    }
    static public string HistPage
    {
        get { return Hists[CurHist]; }
        set 
        { 
            Hists.Add( value);
            PagesHist++;
            CurHist = PagesHist - 1;
            byArrow = false;
        }
    }

    static public void MemClean()
    {
        Hists.Clear();
    }
}