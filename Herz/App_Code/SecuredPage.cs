﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SecuredPage
/// </summary>
public class SecuredPage : System.Web.UI.Page
{
    protected T_User _user;
    protected DataClassesDataContext _db;
    protected byte _permission;

    public SecuredPage()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["user"] == null)
            Response.Redirect("~/Login.aspx");
        
        _user = Session["user"] as T_User;

        if (!IsPostBack)
        {
            if ((_permission = CheckPermission()) == 0)
                Response.Redirect(@"~/MemberPages/DeniedM.aspx");
        }
    }

    private byte CheckPermission()
    //returns 0-deny access, 1-allow view, 2-allow change
    {
        if (_db == null)
            _db = new DataClassesDataContext();

        string[] tree = Request.FilePath.Split('/');
        //check permission of the user for the page
        var upPerm = from p in _db.T_UserPermissions
                     where p.UserID == _user.ID &&
                          p.T_RestrictedPath.Type == 'P' &&
                          //Request.RawUrl.Contains(p.T_RestrictedPath.Name)
                          p.T_RestrictedPath.Name == tree[tree.Length - 1]
                     select p.Permission;
        if (upPerm.Count() > 0)
            return upPerm.First();

        //check permission of the user for the folder
        var ufPerm = from p in _db.T_UserPermissions
                     where p.UserID == _user.ID &&
                          p.T_RestrictedPath.Type == 'F' &&
                          //Request.RawUrl.Contains(p.T_RestrictedPath.Name)
                          p.T_RestrictedPath.Name == tree[tree.Length - 1]
                     select p.Permission;
        if (ufPerm.Count() > 0)
            return ufPerm.First();

        //check permission of the user's role for the page
        var uRoles = from r in _db.T_RoleUsers
                     where r.UserID == _user.ID
                     select r.RoleID;
        var rpPerm = from p in _db.T_RolePermissions
                     where uRoles.Contains(p.RoleID) &&
                          p.T_RestrictedPath.Type == 'P' &&
                          //Request.RawUrl.Contains(p.T_RestrictedPath.Name)
                          p.T_RestrictedPath.Name == tree[tree.Length - 2]
                     select p.Permission;
        if (rpPerm.Count() > 0)
        {
            if (rpPerm.Contains((byte)2))
                return 2;
            else if (rpPerm.Contains((byte)1))
                return 1;
            else return 0;
        }

        //check permission of the user's role for the folder
        var rfPerm = from p in _db.T_RolePermissions
                     where uRoles.Contains(p.RoleID) &&
                          p.T_RestrictedPath.Type == 'F' &&
                          //Request.RawUrl.Contains(p.T_RestrictedPath.Name)
                          p.T_RestrictedPath.Name == tree[tree.Length - 2]
                     select p.Permission;
        if (rfPerm.Count() > 0)
        {
            if (rfPerm.Contains((byte)2))
                return 2;
            else if (rfPerm.Contains((byte)1))
                return 1;
            else return 0;
        }

        //if no records found, deny access
        return 0;
    }
}
