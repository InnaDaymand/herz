﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for taozTariff
/// </summary>
public class taozTariff
{
    DataClassesDataContext db;
	public taozTariff()
	{
        db=new DataClassesDataContext();
	}
    public float GetTariff(Voltage voltage, Season season, consumptionType consumption)
    {
        /*var tariff = from rate in db.Taoz__rates
                     where rate.TaozType_Id == (int)voltage &&
                           rate.Season_Id == (int)season &&
                           rate.UsageType_Id == (int)consumption
                     select rate.Taoz__rate1;
        return Convert.ToSingle(tariff.First());*/

        var tstep = (from step in db.T_TaozTariffs
                     where step.TaozVoltage == (int)voltage
                         && step.TaozSeason == (int)season
                         && step.TaozLoad == (int)consumption
                     select step).First();

        double tariffVal = (from tariffValues in db.T_TariffStepValues
                            where tariffValues.TariffStepID == tstep.TariffStepID
                            orderby tariffValues.ValidFromDate descending
                            select tariffValues.Value).First();

        return (float)tariffVal;
    }
}
