﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CalendarEN.ascx.cs" Inherits="Controls_CalendarEN" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajx" %>

<%@ Register assembly="eWorld.UI, Version=2.0.6.2393, Culture=neutral, PublicKeyToken=24d65337282035f2" namespace="eWorld.UI" tagprefix="ew" %>

<style type="text/css">
    .auto-style1 {
        width: 20px;
    }
</style>

<div>
    <table cellpadding="5px">
        <tr valign="middle">
            <td>
    <asp:Literal Text="From:" runat="server" ID="litFrom" />
    <asp:TextBox ID="txtDateFrom" runat="server" Width="80px" style="margin-right: 0px"></asp:TextBox>
    <asp:Image ID="Image1" runat="server" ImageUrl="~/images/SmallCalendar.gif" />
    <ajx:CalendarExtender ID="txtDateFrom_CalendarExtender" runat="server" 
        Enabled="True" TargetControlID="txtDateFrom" PopupButtonID="Image1" 
                    Format="yyyy-MM-dd">
    </ajx:CalendarExtender>
    <ew:TimePicker ID="TimePicker1" runat="server" Width="40px" NumberOfColumns="4" PopupLocation="Bottom" Scrollable="False" ImageUrl="~/images/timepicker.gif" 
                    MilitaryTime="True" PopupHeight="180px" >
        <ButtonStyle BorderStyle="None" Width="25px" />
    </ew:TimePicker>
            </td>
            <td>
    <asp:Literal Text="To:" runat="server" ID="litTo" />
    <asp:TextBox ID="txtDateTo" runat="server" Width="80px" style="margin-right: 4px"></asp:TextBox>
    <asp:Image ID="Image2" runat="server" ImageUrl="~/images/SmallCalendar.gif" />
    <ajx:CalendarExtender ID="txtDateTo_CalendarExtender" runat="server" 
        Enabled="True" TargetControlID="txtDateTo" PopupButtonID="Image2" Format="yyyy-MM-dd">
    </ajx:CalendarExtender>
    <ew:TimePicker ID="TimePicker2" runat="server" Width="40px" NumberOfColumns="4" PopupLocation="Bottom" Scrollable="False" ImageUrl="~/images/timepicker.gif" 
                    MilitaryTime="True" PopupHeight="180px" >
        <ButtonStyle BorderStyle="None" Width="25px" />
    </ew:TimePicker>
            </td>
            <td>
                <asp:Button ID="bGraph" runat="server" Text="Show Chart" OnClick="bGraph_Click" />
            </td>
            <td class="auto-style1">
                 <asp:Button ID="bPrint" runat="server" Text="Show table" OnClick="bPrint_Click" />
<%--           <asp:ImageButton ID="btnPrint" runat="server" OnClick="ibtExcel_Click"  
                    ImageUrl="~/images/ImgBtnPrint.png" />--%>

            </td>      
<%--            <td>
                <asp:ImageButton runat="server" 
                    ImageUrl="~/images/MonthlyPrnS.png" OnClientClick="OpenMonthes()" />
            </td>                              --%>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <asp:Label ID ="lblDateState" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
</div>