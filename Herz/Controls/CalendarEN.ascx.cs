﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Controls_CalendarEN : System.Web.UI.UserControl
{
    public event System.EventHandler ClickGraph;
    public event System.EventHandler ClickTable;
    public int Lang = 1;
    //public string btnName0 = "Show chart";
    //public string btnName1 = "Show table";

    public DateTime FromDate
    {
        get {return DateTime.Parse(txtDateFrom.Text).Date + TimePicker1.SelectedTime.TimeOfDay; }
        set
        { txtDateFrom_CalendarExtender.SelectedDate = value;
            TimePicker1.SelectedTime = value;
        }
    }

    public DateTime TillDate
    {
        get {return DateTime.Parse(txtDateTo.Text).Date + TimePicker2.SelectedTime.TimeOfDay;}
        set { txtDateTo_CalendarExtender.SelectedDate = value;
            TimePicker2.SelectedTime = value; }
    }
       

    protected void Page_Load(object sender, EventArgs e)
    {
        switch (Lang)
        {
            case 1:
                litFrom.Text = "From";
                litTo.Text = "To";
                bGraph.Text = "Show Chart";
                bPrint.Text = "Show table";
                break;
            case 2:
                litFrom.Text = "От";
                litTo.Text = "До";
                bGraph.Text = "Отчет";
                bPrint.Text = "Подробный отчет";
                break;
            default:
                break;
        }
    }

    protected void bPrint_Click(object sender, EventArgs e)
    {
        DateTime d0 = DateTime.Parse(txtDateFrom.Text).Date + TimePicker1.SelectedTime.TimeOfDay;
        DateTime d1 = DateTime.Parse(txtDateTo.Text).Date + TimePicker2.SelectedTime.TimeOfDay;
        if (d0 >= d1)
        {
            lblDateState.Text = "תאריך התחלה גדולה מתאריך סיום!";
            return;
        }

        ClickTable(this, e);
    }

    protected void bGraph_Click(object sender, EventArgs e)
    {
        DateTime d0 = DateTime.Parse(txtDateFrom.Text).Date + TimePicker1.SelectedTime.TimeOfDay;
        DateTime d1 = DateTime.Parse(txtDateTo.Text).Date + TimePicker2.SelectedTime.TimeOfDay;
        if (d0 >= d1)
        {
            lblDateState.Text = "תאריך התחלה גדולה מתאריך סיום!";
            return;
        }
        ClickGraph(this, e);
    }
}