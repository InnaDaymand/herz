﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CalendarMD.ascx.cs" Inherits="Controls_CalendarMD" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajx" %>

<%@ Register assembly="eWorld.UI, Version=2.0.6.2393, Culture=neutral, PublicKeyToken=24d65337282035f2" namespace="eWorld.UI" tagprefix="ew" %>

<style type="text/css">
    .auto-style1 {
        height: 38px;
    }
</style>

<div dir="rtl">
    <table cellpadding="5px">
        <tr valign="middle">
            <td class="auto-style1">
    מ-
    <asp:TextBox ID="txtDateFrom" runat="server" Width="80px" style="margin-right: 0px"></asp:TextBox>
    <asp:Image ID="Image1" runat="server" ImageUrl="~/images/SmallCalendar.gif" />
    <ajx:CalendarExtender ID="txtDateFrom_CalendarExtender" runat="server" 
        Enabled="True" TargetControlID="txtDateFrom" PopupButtonID="Image1" 
                    Format="yyyy-MM-dd">
    </ajx:CalendarExtender>
    <ew:TimePicker ID="TimePicker1" runat="server" Width="40px" 
        MinuteInterval="OneHour" NumberOfColumns="3" PopupLocation="Bottom" 
        PopupWidth="120px" Scrollable="False" ImageUrl="~/images/timepicker.gif" 
                    MilitaryTime="True" >
        <ButtonStyle BorderStyle="None" Width="25px" />
    </ew:TimePicker>
            </td>
            <td class="auto-style1">
    עד-
    <asp:TextBox ID="txtDateTo" runat="server" Width="80px" style="margin-right: 4px"></asp:TextBox>
    <asp:Image ID="Image2" runat="server" ImageUrl="~/images/SmallCalendar.gif" />
    <ajx:CalendarExtender ID="txtDateTo_CalendarExtender" runat="server" 
        Enabled="True" TargetControlID="txtDateTo" PopupButtonID="Image2" Format="yyyy-MM-dd">
    </ajx:CalendarExtender>
    <ew:TimePicker ID="TimePicker2" runat="server" Width="40px" 
        MinuteInterval="OneHour" NumberOfColumns="3" PopupLocation="Bottom" 
        PopupWidth="120px" Scrollable="False" ImageUrl="~/images/timepicker.gif" 
                    MilitaryTime="True" >
        <ButtonStyle BorderStyle="None" Width="25px" />
    </ew:TimePicker>
            </td>
            <td class="auto-style1">
                <asp:Button ID="btnGraph" runat="server" Text="הצג גרף" OnClick="btnGraph_Click" />
            </td>
            <td class="auto-style1">
                <asp:Button ID="bPrint" runat="server" Text="הצג דוח" OnClick="bPrint_Click" />
            </td>      
            <td class="auto-style1">
                <asp:Button ID="btnA0" runat="server" Text="" OnClick="btnA0_Click" Visible="False" />
           </td>                              
            <td class="auto-style1">
                <asp:Button ID="btnA1" runat="server" Text="" OnClick="btnA1_Click" Visible="False" />
           </td>                              
        </tr>
    </table>
</div>