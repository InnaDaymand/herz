﻿using System;

public partial class Controls_CalendarMD : System.Web.UI.UserControl
{
    public event System.EventHandler ClickGraph;
    public event System.EventHandler ClickTable;
    public event System.EventHandler ClickA0;
    public event System.EventHandler ClickA1;

    public bool ForTime1 = true;
    public bool ForTime2 = true;
    
    public string btnName0 = "הצג גרף";
    public string btnName1 = "הצג דוח";
    public string btnName2 = "";
    public string btnName3 = "";

    public bool btnShow0 = true;
    public bool btnShow1 = true;
    public bool btnShow2 = false;
    public bool btnShow3 = false;

    public DateTime FromDate
    {
        get { return DateTime.Parse(txtDateFrom.Text).Date + TimePicker1.SelectedTime.TimeOfDay; }
        set
        {
            txtDateFrom_CalendarExtender.SelectedDate = value;
            TimePicker1.SelectedTime = value;
        }
    }

    public DateTime TillDate
    {
        get { return DateTime.Parse(txtDateTo.Text).Date + TimePicker2.SelectedTime.TimeOfDay; }
        set
        {
            txtDateTo_CalendarExtender.SelectedDate = value;
            TimePicker2.SelectedTime = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        btnGraph.Text = btnName0;
        btnGraph.Visible = btnShow0;
        bPrint.Text = btnName1;
        bPrint.Visible = btnShow1;
        btnA0.Text = btnName2;
        btnA0.Visible = btnShow2;
        btnA1.Text = btnName3;
        btnA1.Visible = btnShow3;

        TimePicker1.SelectedTime = new DateTime(1900, 1, 1, 0, 0, 0);
        TimePicker2.SelectedTime = new DateTime(1900, 1, 1, 0, 0, 0);

        TimePicker1.Visible = ForTime1;
        TimePicker2.Visible = ForTime2;

    }

    protected void bPrint_Click(object sender, EventArgs e)
    {
        ClickTable(this, e);
    }

    protected void btnGraph_Click(object sender, EventArgs e)
    {
        ClickGraph(this, e);
    }

    protected void btnA0_Click(object sender, EventArgs e)
    {
        ClickA0(this, e);
    }

    protected void btnA1_Click(object sender, EventArgs e)
    {
        ClickA1(this, e);
    }
}