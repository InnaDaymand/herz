﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Help/HelpPage.master" AutoEventWireup="true" CodeFile="HClients.aspx.cs" Inherits="Help_HClients" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    Clients Page
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    לקוחות
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">
    <table>
        <tr>
            <td style="width: 100px; font-weight: bold">דף:
            </td>
            <td>לקוחות
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold">תפקיד:
            </td>
            <td>תצוגת רשימת לקוחות קיימים ועידכון פרטיהם, הגדרת לקוח חדש, מחיקת לקוח.
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold">שימוש ע"י:
            </td>
            <td>מנהל המערכת.
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold; vertical-align: top">תאור:
            </td>
            <td>
                הדף מחולק לשני חלקים. בחלק ימני מוצגת רשימה של לקוחות קיימים עם מספר מצומצם של פרטים. בחלק שמאלי מופיעים כל הפרטים של הקוח מסוים.<br />
                ע"מ לבחור לקוח יש ללחוץ על "בחר" בשורה רצויה ברשימת הלקוחות.<br />
                לעריכת פרטי הלקוח יש ללחוץ "עריכה". למחיקת הלקוח יש ללחוץ "מחק".<br />
                <u>הערה:</u> עם ללקוח שברצונך למחוק מוגדרים צרכנים, פעולת המחיקה לא תתבצע. במקרה זה עליר קודם למחוק את כל הצרכנים השייכים ללקוח.<br />
                ע"מ להגדיר לקוח חדש יש ללחוץ "חדש". בסיום הכנסת נתונים יש ללחוץ "שמור".<br />
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold; vertical-align: top">פרטים:
            </td>
            <td>
                <b>מס' לקוח</b> - ID של לקוח במערכת (שדה חובה).<br /> <u>הערה:</u> בגרסא הבאה של המערכת יהפוך להיות מספר אוטומטי 
                ולא יוכנס ע&quot;י משתמש.<br />
                <b>שם</b> - השם של הלקוח (שדה חובה)<br />
                <b>ישוב</b> - ישוב בו נמצא בית או משרד של הלקוח<br />
                <b>כתובת</b> - כתובת למשלוחים<br />
                <b>מיקוד</b> - מיקוד הדואר של הלקוח.<br />
                <b>מדינה</b> - מדינה בה נמצא בית או משרד של הלקוח<br />
                <b>טלפון</b> - מספר הטלפון של הלקוח<br />
                <b>פקס</b> - מספר הפקס של הלקוח<br />
                <b>משתמש ראשי</b> - המשתמש העיקרי של הלקוח. המשתמש הזה יקבל הודעות מן המערכת. <br />
                <b>משתמשים מורשים</b> - רשימת משתמשים נוספים שרשאים לראות נתוני הלקוח.<br />
            </td>
        </tr>
    </table>
</asp:Content>

