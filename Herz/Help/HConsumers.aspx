﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Help/HelpPage.master" AutoEventWireup="true" CodeFile="HConsumers.aspx.cs" Inherits="Help_HConsumers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    Consumers Page
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    צרכנים
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">
    <table>
        <tr>
            <td style="width: 100px; font-weight: bold">דף:
            </td>
            <td>צרכנים
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold">תפקיד:
            </td>
            <td>תצוגת רשימת צרכנים קיימים ועידכון פרטיהם, הגדרת צרכן חדש, מחיקת צרכן.
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold">שימוש ע"י:
            </td>
            <td>מנהל המערכת.
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold; vertical-align: top">תאור:
            </td>
            <td>
                הדף מחולק לשני חלקים. בחלק ימני מוצגת רשימה של צרכנים קיימים עם מספר מצומצם של פרטים. בחלק שמאלי מופיעים כל הפרטים של צרכן מסוים.<br />
                ע"מ לבחור צרכן יש ללחוץ על "בחר" בשורה רצויה ברשימת הצרכנים.<br />
                לעריכת פרטי הצרכן יש ללחוץ "עריכה". למחיקת הצרכן יש ללחוץ "מחק".<br />
                ע"מ להגדיר צרכן חדש יש ללחוץ "חדש". בסיום הכנסת נתונים יש ללחוץ "שמור".<br />
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold; vertical-align: top">פרטים:
            </td>
            <td>
                <b>מס' צרכן</b> - ID של צרכן במערכת (שדה חובה).<br /> <u>הערה:</u> בגרסא הבאה של המערכת יהפוך להיות מספר אוטומטי 
                ולא יוכנס ע&quot;י משתמש.<br />
                <b>שם</b> - השם של הצרכן (שדה חובה)<br />
                <b>יצרן</b> - שם היצרן של הצרכן<br />
                <b>דגם</b> - הדגם של הצרכן<br />
                <b>צריכה</b> - צריכת אנרגיה של הצרכן לפי נתוני היצרן<br />
                <b>פעיל</b> - מצבו של הצרכן במערכת (פעיל \ לא פעיל)<br />
                <b>לקוח</b> - שם הלקוח לו שייך הצרכן<br />
            </td>
        </tr>
    </table>
</asp:Content>

