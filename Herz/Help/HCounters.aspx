﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Help/HelpPage.master" AutoEventWireup="true" CodeFile="HCounters.aspx.cs" Inherits="Help_HCounters" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    Counters Page
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    מונים
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">
    <table>
        <tr>
            <td style="width: 100px; font-weight: bold">דף:
            </td>
            <td>מונים
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold">תפקיד:
            </td>
            <td>תצוגת רשימת מונים קיימים ועידכון פרטיהם, הגדרת מונה חדש, מחיקת מונה.
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold">שימוש ע"י:
            </td>
            <td>מנהל המערכת.
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold; vertical-align: top">תאור:
            </td>
            <td>
                הדף מחולק לשני חלקים. בחלק ימני מוצגת רשימה של מונים קיימים עם מספר מצומצם של פרטים. בחלק שמאלי מופיעים כל הפרטים של מונה מסוים.<br />
                ע"מ לבחור מונה יש ללחוץ על "בחר" בשורה רצויה ברשימת המונים.<br />
                לעריכת פרטי המונה יש ללחוץ "עריכה". למחיקת המונה יש ללחוץ "מחק".<br />
                ע"מ להגדיר מונה חדש יש ללחוץ "חדש". בסיום הכנסת נתונים יש ללחוץ "שמור".<br />
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold; vertical-align: top">פרטים:
            </td>
            <td>
                <b>מס' מונה</b> - ID של מונה במערכת (שדה חובה).<br /> <u>הערה:</u> בגרסא הבאה של המערכת יהפוך להיות מספר אוטומטי 
                ולא יוכנס ע&quot;י משתמש.<br />
                <b>שם קצר</b> - שם או קינוי של המונה (עד 10 תווים)<br />
                <b>סוג</b> - סוג האנרגיה&nbsp; שהמונה מודד<br />
                <b>צרכן</b> - שם הצרכן שהמונה שייך לו.<br />
                <b>מס&#39; סידורי</b> - המספר הסידורי הרשום על המונה<br />
                <b>תעריף</b> - שמ התעריף לפיו נעשה חישוב עלות.<br />
                <b>פעימה</b> - שווי של פעימת המונה ביחידות מידה של סוג האנרגיה (שדה חובה).<br />
                <b>מכפיל תצוגה</b> - ערך של סיפרה בצג המונה ביחידות מידה של סוג האנרגיה. לדוגמה, עם מונה מציג מגה ווט אז המכפיל שווה 1000.<br />
                <b>פעיל</b> - מצבו של המונה במערכת (פעיל \ לא פעיל)<br />
                <b>מפוצל</b> - מגדיר האם המונה משמש כמקור פולסים למונים וירטואלים.<br />
                <b>מקדם תעריף</b> - בעמצאות מקדם זה ניתן לתקן תעריף כללי במיוחד עבור המונה (התעריף יוכפל במקדם).<br />
            </td>
        </tr>
    </table>
</asp:Content>

