﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Help/HelpPage.master" AutoEventWireup="true" CodeFile="HPermissions.aspx.cs" Inherits="Help_HPermissions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    Permissions Page
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    הרשאות
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">
    <table>
        <tr>
            <td style="width: 100px; font-weight: bold">דף:
            </td>
            <td>הרשאות
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold">תפקיד:
            </td>
            <td>ניהול הרשאות.
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold">שימוש ע"י:
            </td>
            <td>מנהל המערכת.
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold; vertical-align: top">תאור:
            </td>
            <td>
                הרשאות נתנות למשתמשים בודדים או לקבוצות משתמשים עבור משאבי המערכת שונים (דפים מסוימים או תיקיות שלמות)<br />
                הדף מכיל שלוש לשוניות שנותנות חתחים שונים של הרשאות <br />
                בלשונית <b>הגבלות</b> מוצגים דפים ותיקיות של המערכת שעבורם מגבילים או מרחיבים גישה למשתמש או קבוצה.<br />
                בלשונית <b>סוגי משתמשים</b> מוצגים קבוצות משתמשים עם רשימת חברים והרשאות.<br />
                בלשונית <b>משתמשים</b> מוצגים משתמשים, שייכות שלהם לקבוצות והרשאות אישיות.<br />
            </td>
        </tr>
    </table>
</asp:Content>

