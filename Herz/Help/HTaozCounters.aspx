﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Help/HelpPage.master" AutoEventWireup="true" CodeFile="HTaozCounters.aspx.cs" Inherits="Help_HTaozCounters" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    Taoz Counters Page
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    מוני תעו"ז
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">
    <table>
        <tr>
            <td style="width: 100px; font-weight: bold">דף:
            </td>
            <td>מוני תעו"ז
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold">תפקיד:
            </td>
            <td>קישור מונים וירטואלים למונים ראשיים לפי עומסים.
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold">שימוש ע"י:
            </td>
            <td>מנהל המערכת.
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold; vertical-align: top">תאור:
            </td>
            <td>
                הדף מחולק לשני חלקים. בחלק ימני מוצגת רשימה של מונים ראשיים אשר סופרים פולסים 
                בלי להתחשב בזמני עומסים. בחלק שמאלי מוצגת רשימה של מונים וירטואלים שסופרים פולסים 
                לפי זמני עומסים.<br />
                ע&quot;מ לבחור מונה ראשי יש ללחוץ על &quot;בחר&quot; בשורה רצויה ברשימה. רשימת מונים וירטואלים 
                מתעדכנת בהתאם.<br />
                מתחת לרשימת מוני עןמס יש כלים להוספהת מונה עומס.<br />
                כדי להוסיף מונה עומס יש לבחור עומס מרשמת עומסים ולבחור מונה וירטואלי מרשימת 
                מונים וללחוץ "הוסף".<br />
                ע"מ להסיר מונה עומס יש ללחוץ "מחק" בשורה רצויה.<br />
            </td>
        </tr>
    </table>
</asp:Content>

