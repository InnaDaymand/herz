﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Help/HelpPage.master" AutoEventWireup="true" CodeFile="HTariffs.aspx.cs" Inherits="Help_HTariffs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    Tariffs Page
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    תעריפים
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">
    <table>
        <tr>
            <td style="width: 100px; font-weight: bold">דף:
            </td>
            <td>תעריפים
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold">תפקיד:
            </td>
            <td>עידכון תעריפים לחישוב עלות אנרגיה.
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold">שימוש ע"י:
            </td>
            <td>מנהל המערכת.
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold; vertical-align: top">תאור:
            </td>
            <td>
                הדף מחולק לשלושה חלקים. בחלק ימני מוצגת רשימה של תעריפים מוגדרים במערכת. בחלק 
                אמצעי מופיעה רשימה של מדרגות או שלבים של אותו תעריף (לכל תעריף יש לפחות 
                מדרגה\שלב אחת). בחלק שמאלי מופיעים ערכים של תעריפים בשקלים חדשים יחד עם תאריך שהערך נכנס לתוקף.<br />
                כאשר ספק אנרגיה מעדכן תעריף, צריך להוסיף את הערך החדש יחד עם תאריך העדכון למערכת.<br />
                ע"מ לבצע עידכון, תחילה צריך לבחור תעריף רצוי ע"י לחיצת "בחר" ברשימת התעריפים. לאחר מכן יש לבחור מדרגה או שלב של התעריף. סביר להניח שצריך לעדכן כל המדרגות או שלבים של התעריף.
                אחר כך יש להזין את הערך החדש ואת התאריך כניסתו לתוקף וללחוץ &quot;הוסף&quot;.<br />
                כמו כן יש גם אפשרות לתקן ערך קיים ישר ברשימת הערכים ע&quot;י לחיצת &quot;עריכה&quot; בשורה 
                רצויה. יחד עם זאת אין אפשרות לשנות תאריך של הערך.</td>
        </tr>
    </table>
</asp:Content>

