﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Help/HelpPage.master" AutoEventWireup="true" CodeFile="HUsers.aspx.cs" Inherits="Help_HUsers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    Users Page
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    משתמשים
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">
    <table>
        <tr>
            <td style="width: 100px; font-weight: bold">דף:
            </td>
            <td>משתמשים
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold">תפקיד:
            </td>
            <td>תצוגת רשימת משתמשים קיימים ועידכון פרטיהם, הגדרת משתמש חדש, מחיקת משתמש.
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold">שימוש ע"י:
            </td>
            <td>מנהל המערכת.
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold; vertical-align: top">תאור:
            </td>
            <td>
                הדף מחולק לשני חלקים. בחלק ימני מוצגת רשימה של משתמשים קיימים עם מספר מצומצם של פרטים. בחלק שמאלי מופיעים כל הפרטים של משתמש מסוים.<br />
                ע"מ לבחור משתמש יש ללחוץ על "בחר" בשורה רצויה ברשימת משתמשים.<br />
                לעריכת פרטי המשתמש יש ללחוץ "עריכה". למחיקת המשתמש יש ללחוץ "מחק".<br />
                <u>הערה:</u> לפני מחיקת משתמש יש למחוק כל היחסים שלו כמו שייכות לקבוצות והרשאות אישיות.<br />
                ע"מ להגדיר משתמש חדש יש ללחוץ "חדש". בסיום הכנסת נתונים יש ללחוץ "שמור".<br />
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold; vertical-align: top">פרטים:
            </td>
            <td>
                <b>מס' משתמש</b> - ID של משתמש במערכת (שדה חובה).<br /> <u>הערה:</u> בגרסא הבאה של המערכת יהפוך להיות מספר אוטומטי 
                ולא יוכנס ע&quot;י משתמש.<br />
                <b>שם פרטי</b> - השם הפרטי של המשתמש (שדה חובה)<br />
                <b>שם משפחה</b> - שם המשפחה של המשתמש (שדה חובה)<br />
                <b>נייד</b> - מספר הטלפון הנייד של המשתמש<br />
                <b>דוא"ל</b> - דואר אלקטרוני של המשתמש<br />
                <b>שם משתמש</b> - שם משתמש לכניסה למערכת (שדה חובה)<br />
                <b>סיסמה</b> - סיסמת כניסה למערכת<br />
                <b>רשאי לראות לקוחות</b> - רשימת לקוחות שהמשתמש רשאי לראות נתוניהם (אם המשתמש הינו משתמש ראשי של אחד הלקוחות, אין צורך להוסיף את הלקוח לרשימה).<br />
            </td>
        </tr>
    </table>
</asp:Content>

