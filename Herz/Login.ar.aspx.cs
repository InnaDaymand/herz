﻿using System;
using System.Linq;
using System.Web;
using System.IO;

public partial class Login : System.Web.UI.Page
{
    DataClassesDataContext db = new DataClassesDataContext();
    //tbl_Client client;
    T_User _user;

    protected void Page_Load(object sender, EventArgs e)
    {
        string currentPageFileName = new FileInfo(this.Request.Url.LocalPath).Name;
        string Lang = currentPageFileName.Substring(currentPageFileName.Length - 7, 2);
        if (Lang == "in") Lang = "HE";
        string SQL = "", SS = "";
        DataClassesDataContext db = new DataClassesDataContext();
        if (AllFunctions.ConnectStr == "")
        {
            AllFunctions.ConnectStr = db.Connection.ConnectionString;
        }


        SQL = "SELECT [" + Lang + "] FROM Util_ResLang WHERE ID = 50009";
        SS = AllFunctions.GetScalar(SQL);
        if (SS == "")
        {
            SS = "50009";
        }
        NAM.Text = SS;

        SQL = "SELECT [" + Lang + "] FROM Util_ResLang WHERE ID = 50010";
        SS = AllFunctions.GetScalar(SQL);
        if (SS == "")
        {
            SS = "50010";
        }
        PWD.Text = SS;

        SQL = "SELECT [" + Lang + "] FROM Util_ResLang WHERE ID = 50011";
        SS = AllFunctions.GetScalar(SQL);
        if (SS == "")
        {
            SS = "50011";
        }
        chkRemember.Text = SS;

        SQL = "SELECT [" + Lang + "] FROM Util_ResLang WHERE ID = 50012";
        SS = AllFunctions.GetScalar(SQL);
        if (SS == "")
        {
            SS = "50012";
        }
        lblError.Text = SS;

        SQL = "SELECT [" + Lang + "] FROM Util_ResLang WHERE ID = 50013";
        SS = AllFunctions.GetScalar(SQL);
        if (SS == "")
        {
            SS = "50013";
        }
        ForBtn.Text = SS;


        SQL = "SELECT [" + Lang + "] FROM Util_ResLang WHERE ID = 50001";
        SS = AllFunctions.GetScalar(SQL);
        if (SS == "")
        {
            SS = "50001";
        }
        Home.Text = SS;

        SQL = "SELECT [" + Lang + "] FROM Util_ResLang WHERE ID = 50002";
        SS = AllFunctions.GetScalar(SQL);
        if (SS == "")
        {
            SS = "50002";
        }
        AboutUs.Text = SS;

        SQL = "SELECT [" + Lang + "] FROM Util_ResLang WHERE ID = 50003";
        SS = AllFunctions.GetScalar(SQL);
        if (SS == "")
        {
            SS = "50003";
        }
        Services.Text = SS;

        SQL = "SELECT [" + Lang + "] FROM Util_ResLang WHERE ID = 50004";
        SS = AllFunctions.GetScalar(SQL);
        if (SS == "")
        {
            SS = "50004";
        }
        Products.Text = SS;

        SQL = "SELECT [" + Lang + "] FROM Util_ResLang WHERE ID = 50005";
        SS = AllFunctions.GetScalar(SQL);
        if (SS == "")
        {
            SS = "50005";
        }
        EnterSystem.Text = SS;

        SQL = "SELECT [" + Lang + "] FROM Util_ResLang WHERE ID = 50006";
        SS = AllFunctions.GetScalar(SQL);
        if (SS == "")
        {
            SS = "50006";
        }
        Contact.Text = SS;

        SQL = "SELECT [" + Lang + "] FROM Util_ResLang WHERE ID = 50007";
        SS = AllFunctions.GetScalar(SQL);
        if (SS == "")
        {
            SS = "50007";
        }
        AllRights.Text = SS;



        Session["Resolution"] = "0";
        if (!IsPostBack)
        {
            HttpCookie user = Request.Cookies["user"];
            HttpCookie pass = Request.Cookies["pass"];
            if (user != null && pass != null)
                if (LoginUser(user.Value, pass.Value))
                {
                    //renew cookies
                    user.Expires = DateTime.Now.AddDays(14);
                    pass.Expires = DateTime.Now.AddDays(14);
                    Response.Cookies.Set(user);
                    Response.Cookies.Set(pass);

                    StartPage();
                }
        }

        //this.UserName.Focus();
        txtUser.Focus();
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        if (!LoginUser(txtUser.Text, txtPass.Text))
        {
            lblError.Visible = true;
            return;
        }

        if (chkRemember.Checked)
            SaveCookie();

        StartPage();
    }
    private bool LoginUser(string user, string pass)
    {
        try
        {
            //client = db.tbl_Clients.First(c => c.sql_username == user && c.sql_password == pass);
            _user = db.T_Users.First(u => u.UserName == user && u.Password == pass);

            //AllFunctions.ConnectStr = db.Connection.ConnectionString;

            //Session["client"] = client;
            //Session["ClientID"] = client.sql_clientID;
            Session["user"] = _user;
            Session["UID"] = _user.ID;

            return true;
        }
        catch (Exception ex)
        {
            //lblError.Visible = true;
            return false;
        }
    }
    private void SaveCookie()
    {
        HttpCookie user = new HttpCookie("user", txtUser.Text);
        user.Expires = DateTime.Now.AddDays(14);
        //user.Secure = true;
        HttpCookie pass = new HttpCookie("pass", txtPass.Text);
        pass.Expires = DateTime.Now.AddDays(14);
        //pass.Secure = true;

        Response.Cookies.Add(user);
        Response.Cookies.Add(pass);
    }
    private void StartPage()
    {
        Session["DateFrom"] = DateTime.Today.AddDays(-1).ToString("yyyy-MM-dd HH:mm:ss");
        Session["DateTill"] = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

        //if (client.sql_role)//admin
        if (_user.IsAdmin())//admin
            Response.Redirect(@"~/AdminPages/Clients.aspx", false);
        else
        {
            Response.Redirect(@"~/MemberPages/ConsumersList.aspx", false);
        }
    }
    protected void ImageButton1_Click(object sender, System.Web.UI.ImageClickEventArgs e)
    {
        if (!LoginUser(txtUser.Text, txtPass.Text))
        {
            lblError.Visible = true;
            return;
        }

        if (chkRemember.Checked)
            SaveCookie();

        StartPage();
    }
}