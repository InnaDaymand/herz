﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SeenergePublicPage.master" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="SeenrgLogin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="StyleSheet.css" rel="Stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
    <div align="center" style="height:30px; align-content:center;"></div>
    <div align="center"  style="align-content:center; align-items:center; align-self:center; height:360px;">
            <asp:Panel ID="Panel2" runat="server" Width="560px" Height="360px" 
                 BackColor="WhiteSmoke">
                <br />
                <br />

                <asp:Panel ID="Panel3" runat="server" BackColor="#818285" CssClass="GreyText" ForeColor="White" >
<%--                    <asp:Label ID="Label1" runat="server" Font-Names="HelveticaNeueCyr" Font-Size="24px" ForeColor="White" Text="" Height="36px" HorizontalAlign="Center" >
                    </asp:Label>--%>

                    <asp:Literal Text="כניסה למערכת" runat="server"/>

                </asp:Panel>

                <asp:Table ID="Table1" runat="server" Width="480px" Height="240px" HorizontalAlign="Center"
                    ForeColor="#f1592a" CssClass="OrangeText" BackColor="White" >
                    <asp:TableRow Height="12px" Font-Size="12px">
                        <asp:TableCell ColumnSpan="5"></asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow Height="40px">
                        <asp:TableCell Width="20px"></asp:TableCell>
                        <asp:TableCell Width="140px" HorizontalAlign="Left" >
                            <asp:literal text="שם משתמש:" runat="server" />
                        </asp:TableCell><asp:TableCell Width="10px" >&nbsp</asp:TableCell><asp:TableCell Width="220px" HorizontalAlign="Justify">
                            <asp:textbox ID="txtUser" runat="server" Width="100%" BorderColor="#f1592a" BorderStyle="Solid" BorderWidth="1px" Height="32px" />
                        </asp:TableCell><asp:TableCell></asp:TableCell></asp:TableRow><asp:TableRow Height="40px">
                            <asp:TableCell Width="20px"></asp:TableCell><asp:TableCell  Width="140px" HorizontalAlign="Left" >
                                <asp:literal text="סיסמא:" runat="server" />
                            </asp:TableCell><asp:TableCell >&nbsp</asp:TableCell><asp:TableCell  Width="220px" HorizontalAlign="Justify">
                                <asp:textbox ID="txtPass" runat="server" Width="100%" TextMode="Password"
                                    BorderColor="#f1592a" BorderStyle="Solid" BorderWidth="1px" Height="32px" />
                            </asp:TableCell><asp:TableCell></asp:TableCell></asp:TableRow><asp:TableRow Height="30px">
                        <asp:TableCell ColumnSpan="3"></asp:TableCell><asp:TableCell ColumnSpan="2" Font-Size="18px" HorizontalAlign="Right">
                            <asp:checkbox ID="chkRemember" text="שמור סיסמא" runat="server" />
                        </asp:TableCell></asp:TableRow><asp:TableRow Height="10px" Font-Size="8px">
                            <asp:TableCell ColumnSpan="5" HorizontalAlign="Center">
                 <asp:Label ID="lblError" runat="server" EnableViewState="False" ForeColor="Blue"
                 Text="שם משתמש ו\או סיסמה שגויים" Visible="False" Font-Bold="True" Font-Names="Arial" Font-Size="14px" Width="60%" />
                            </asp:TableCell></asp:TableRow><asp:TableRow Height="40px">
                        <asp:TableCell ColumnSpan="4" HorizontalAlign="Left" >
                            <asp:imagebutton imageurl="~/SeenergImg/button-he.png" runat="server"
                                OnClick="Input_Click"/></asp:TableCell>
                        <asp:TableCell></asp:TableCell></asp:TableRow></asp:Table></asp:Panel></div><div style="height:50px;"></div>


<%-- BorderColor="#f1592a" BorderWidth="2px"--%>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>

