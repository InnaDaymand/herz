﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SeenrgLogin : System.Web.UI.Page
{
    DataClassesDataContext db = new DataClassesDataContext();
    T_User _user;

    protected void Page_Load(object sender, EventArgs e)
    {
        string SQL = "", SS = "";
        AllFunctions.LangCur = 0;
        //DataClassesDataContext db = new DataClassesDataContext();
        if (AllFunctions.ConnectStr == "")
        {
            AllFunctions.ConnectStr = db.Connection.ConnectionString;
        }


        Session["Resolution"] = "0";
        if (!IsPostBack)
        {
            HttpCookie user = Request.Cookies["user"];
            HttpCookie pass = Request.Cookies["pass"];
            if (user != null && pass != null)
                if (LoginUser(user.Value, pass.Value))
                {
                    //renew cookies
                    user.Expires = DateTime.Now.AddDays(14);
                    pass.Expires = DateTime.Now.AddDays(14);
                    Response.Cookies.Set(user);
                    Response.Cookies.Set(pass);

                    StartPage();
                }
        }

        txtUser.Focus();
    
    }

    private bool LoginUser(string user, string pass)
    {
        string II = "-1";
        string uName = "";
        try
        {
            uName = "SELECT [ID] FROM [dbo].[T_User] WHERE [UserName] = N'" + user + "' AND [Password] = N'" + pass + "'";
            II = AllFunctions.GetScalar(uName);
            uName = "SELECT [FirstName] + ' ' + [LastName] FROM [dbo].[T_User] WHERE [ID] = " + II;
            uName = AllFunctions.GetScalar(uName);


            _user = db.T_Users.First(u => u.UserName == user && u.Password == pass);
            Session["user"] = _user;
            //Session["UID"] = _user.ID;
            //Session["user"] = uName;
            Session["UID"] = II;
            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
    }

    private void StartPage()
    {
        Session["DateFrom"] = DateTime.Today.AddDays(-1).ToString("yyyy-MM-dd HH:mm:ss");
        Session["DateTill"] = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

        //if (client.sql_role)//admin
        if (_user.IsAdmin())//admin
            Response.Redirect(@"~/AdminPages/Clients.aspx", false);
        else
        {
            Response.Redirect(@"~/MemberPages/ConsumersList.aspx", false);
        }
    }

    protected void Input_Click(object sender, System.Web.UI.ImageClickEventArgs e)
    {
        if (!LoginUser(txtUser.Text, txtPass.Text))
        {
            lblError.Visible = true;
            return;
        }

        if (chkRemember.Checked)
            SaveCookie();

        StartPage();
    }

    private void SaveCookie()
    {
        HttpCookie user = new HttpCookie("user", txtUser.Text);
        user.Expires = DateTime.Now.AddDays(14);
        HttpCookie pass = new HttpCookie("pass", txtPass.Text);
        pass.Expires = DateTime.Now.AddDays(14);
        Response.Cookies.Add(user);
        Response.Cookies.Add(pass);
    }

}