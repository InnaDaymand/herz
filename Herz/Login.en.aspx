﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.en.aspx.cs" Inherits="Login_en" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <title>Login</title>
    <link href="StyleSheet.css" rel="Stylesheet" type="text/css" />
    <style type="text/css">
        .style1
        {
            width: 27px;
            height: 21px;
        }
        .style4
        {
            height: 25px;
        }
        .style9
        {
            font-family: Arial;
            font-size: 18px;
            font-weight: normal;
            text-decoration: none;
            margin-right: 23px;
            height: 22px;
            width: 132px;
        }
        .style10
        {
            width: 33px;
            height: 22px;
        }
        .style11
        {
            width: 172px;
            height: 22px;
        }
        .style12
        {
            font-family: Arial;
            font-size: 18px;
            font-weight: normal;
            text-decoration: none;
            margin-right: 23px;
            height: 24px;
            width: 132px;
        }
        .style13
        {
            width: 172px;
            height: 24px;
        }
        .style14
        {
            height: 24px;
        }
        .style15
        {
            width: 33px;
            height: 24px;
        }
    </style>
</head>
<body>
<form id="form1" runat="server" style="text-align: center; vertical-align: middle">
    <div align="center">
        <br />
        <asp:Panel ID="Panel1" runat="server" Width="940px" BackImageUrl="SeenergImg/background.jpg">
        <br />
        <br />
        <br />
        <div style="align-content:center">
                <img src="SeenergImg/pics.png" alt="Pics" />&nbsp;&nbsp;&nbsp;
                <asp:ImageButton ImageUrl="SeenergImg/logo.png" runat="server" PostBackUrl="http://seener-g.com" />
        </div>
        <br />

            <div align="center" style="background-color:#f1592a;">
                <ul id="menu-mainmenu" style="vertical-align: middle; text-align: left" dir="ltr">
               <li id="menu-item-21" ><a class="AonLi" href="http://seener-g.com">
                <asp:Literal ID="Home" runat="server" Text="Home"></asp:Literal>
                </a></li>
                <li id="menu-item-23" ><a class="AonLi" href="http://seener-g.com/what-we-do">
                <asp:Literal ID="Services" runat="server" Text="Solutions"></asp:Literal>
                </a></li>
                <li id="menu-item-24" ><a class="AonLi" href="http://seener-g.com/products">
                <asp:Literal ID="Products" runat="server" Text="Products"></asp:Literal>
                </a></li>
                <li id="menu-item-33" ><a class="AonLi" href="http://seener-g.com/applications">
                <asp:Literal ID="Applicats" runat="server" Text="Applications"></asp:Literal>
                </a></li>
                <li id="menu-item-22" ><a class="AonLi" href="http://seener-g.com/about-us-2">
                <asp:Literal ID="AboutUs" runat="server" Text="About Us"></asp:Literal>
                </a></li>
                <li id="menu-item-25" class="InLi">
<%--                    <a href="#" style="font-family: Arial, Helvetica, sans-serif; font-size: 21px; text-decoration: none; color: #00337A">--%>
                <asp:Literal ID="EnterSystem" runat="server" Text="Login" ></asp:Literal>
                </a></li>
                <li id="menu-item-26" >
                    <a class="AonLi" href="http://seener-g.com/contact-us">
                <asp:Literal ID="Contact" runat="server" Text="Contact Us"></asp:Literal>
                </a></li>
                </ul>
                </div>
    <div align="center" style="height: 34px">
        <img src="Eng/Titles/Log in.png" alt="Login" height="39" />
    </div>
    <div align="center" style="height: 12px">
    </div>
    <div align="center">
    <asp:Panel runat="server" Height="246px" Width="407px" 
        ID="Pnl0" HorizontalAlign="Center" CssClass="panLogin" 
        BorderColor="#14499A" BorderStyle="Solid" BorderWidth="2px" 
                Direction="LeftToRight">

            <table dir="ltr" style="text-align:left; width: 400px; left: 0px; top: 0px; height: 281px;" >
                <tr><td colspan="4" class="style1"></td></tr>
                <tr>
                    <td class="style9" dir="ltr" align="left">
                        <asp:Literal ID="NAM" Text="50009" runat="server" />
                    </td>
                    <td class="style11"></td>
                     <td class="style14" dir="ltr">
                        <asp:TextBox ID="txtUser" runat="server" Width="220px" 
                            CssClass="RoundCornerFontBlack" BorderStyle="Solid" Height="24px" 
                            BorderColor="#CDCDCD" BorderWidth="2" Font-Names="Arial" Font-Size="16px" />
                    </td>
                    <td class="style10"></td>
                </tr>
                <tr><td colspan="4" class="style1"></td></tr>
                <tr>
                     <td class="style12" dir="ltr" align="left">
                        <asp:Literal ID="PWD" Text="50010" runat="server" />
                    </td>
                        <td class="style13"></td>
                   <td class="style14" dir="ltr">
                        <asp:TextBox ID="txtPass" runat="server" Width="220px" TextMode="Password"
                        CssClass="RoundCornerFontBlack" BorderStyle="Solid" Height="24px" Font-Names="Arial" Font-Size="16px"  
                            BorderColor="#CDCDCD" BorderWidth="2" />
                    </td>
                    <td class="style15"></td>
                </tr>
         <tr>
         <td colspan="4" align="left" class="style4">&nbsp;<asp:CheckBox ID="chkRemember" 
                 runat="server" Text="50011" 
                 Font-Bold="False" Font-Names="Arial" Font-Size="15px" Width="330px" />
             </td>
         </tr>
         <tr><td colspan="4" align="center" class="style4">
             <asp:Label ID="lblError" runat="server" EnableViewState="False" ForeColor="Red" 
                 Text="50012"
                  Visible="False" Font-Bold="False" 
                 Font-Names="Arial" Font-Size="15px" Width="60%" />
                </td></tr>
                <tr><td colspan="4">
                     <asp:ImageButton ID="ImageButton2" runat="server" ImageAlign="Left" 
                         ImageUrl="~/Eng/Titles/enter.png" onclick="ImageButton1_Click" Width="60px" Height="60px" />
                    </td></tr>
                <tr>
                    <td align="left" colspan="3" dir="ltr" height="32px" class="NewFont2" >
                    <asp:Label runat="server" Text="50013" ID="ForBtn" Visible="false"></asp:Label>
                     </td>
                    <td align="right" dir="ltr" height="32px" ></td>
                </tr>
            </table>
            </asp:Panel>
    </div>


            <div style="height: 24px" >   
            </div>
            <div align="center" dir="ltr" class="FooterMenu" >
               <br />
                    All rights reserved to <a href="http://www.thertz-eng.com/eng" style="color: #f1592a; text-decoration: none;">T. Hertz Consulting Engineers Ltd</a>
               <br />
            </div>



             </asp:Panel>
       </div>






    </form>
</body>
</html>
