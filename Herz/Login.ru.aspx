﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.ru.aspx.cs" Inherits="Login_ru" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <title>Login</title>
    <link href="StyleSheet.css" rel="Stylesheet" type="text/css" />
    <style type="text/css">
        .style1
        {
            width: 27px;
            height: 21px;
        }
        .style4
        {
            height: 25px;
        }
        .style9
        {
            font-family: Arial;
            font-size: 18px;
            font-weight: normal;
            text-decoration: none;
            margin-right: 23px;
            height: 22px;
            width: 132px;
        }
        .style10
        {
            width: 33px;
            height: 22px;
        }
        .style11
        {
            width: 172px;
            height: 22px;
        }
        .style12
        {
            font-family: Arial;
            font-size: 18px;
            font-weight: normal;
            text-decoration: none;
            margin-right: 23px;
            height: 24px;
            width: 132px;
        }
        .style13
        {
            width: 172px;
            height: 24px;
        }
        .style14
        {
            height: 24px;
        }
        .style15
        {
            width: 33px;
            height: 24px;
        }
        .auto-style1 {
            width: 235px;
            height: 27px;
        }
        .auto-style3 {
            width: 400px;
            left: 0px;
            top: 0px;
            height: 227px;
        }
    </style>
</head>

<body>
    
<form id="form1" runat="server" style="text-align: center; vertical-align: middle">
    <div align="center">
        <br />
        <asp:Panel ID="Panel2" runat="server" Width="940px" BackImageUrl="SeenergImg/background.jpg">
        <br />
        <br />
        <br />
        <div style="align-content:center">
                <img src="SeenergImg/pics.png" alt="Pics" />&nbsp;&nbsp;&nbsp;
                <asp:ImageButton ImageUrl="SeenergImg/logo.png" runat="server" PostBackUrl="http://seener-g.com" />
        </div>
        <br />

           <div align="center" style="background-color:#f1592a;">
                <ul id="menu-mainmenu" style="vertical-align: middle; text-align: left" dir="ltr">
               <li id="menu-item-21" ><a class="AonLi" href="http://seener-g.com">
                <asp:Literal ID="Home" runat="server" Text="50001"></asp:Literal>
                </a></li>
                <li id="menu-item-23" ><a class="AonLi" href="http://seener-g.com/what-we-do">
                <asp:Literal ID="Services" runat="server" Text="50003"></asp:Literal>
                </a></li>
                <li id="menu-item-24" ><a class="AonLi" href="http://seener-g.com/products">
                <asp:Literal ID="Products" runat="server" Text="50004"></asp:Literal>
                </a></li>
<%--                <li id="menu-item-33" ><a class="AonLi" href="http://seener-g.com/applications">
                <asp:Literal ID="Applicats" runat="server" Text="Applications"></asp:Literal>
                </a></li>--%>
                <li id="menu-item-22" ><a class="AonLi" href="http://seener-g.com/about-us-2">
                <asp:Literal ID="AboutUs" runat="server" Text="50002"></asp:Literal>
                </a></li>
                <li id="menu-item-25" class="InLi"> 
                       <a href="#" style="font-family: Arial, Helvetica, sans-serif; font-size: 21px; text-decoration: none; color: #00337A">
                <asp:Literal ID="EnterSystem" runat="server" Text="50005"></asp:Literal>
                </a></li>
                <li id="menu-item-26" >
                    <a class="AonLi" href="http://seener-g.com/contact-us">
                <asp:Literal ID="Contact" runat="server" Text="50006"></asp:Literal>
                </a></li>
                </ul>
                </div>

<input type="hidden" value="" name="clientScreenWidth" id="clientScreenWidth" />
 <script type="text/javascript">
    document.getElementById("clientScreenWidth").value = screen.width;
</script>





    <div align="center" style="height: 34px">
        <img alt="Вход" class="auto-style1" src="Rus/Titles/Login__ru[391].png" />
    </div>
    <div align="center" style="height: 12px">
    </div>
    <div align="center">
    <asp:Panel runat="server" Height="230px" Width="407px" 
        ID="Pnl0" HorizontalAlign="Center" CssClass="panLogin" 
        BorderColor="#14499A" BorderStyle="Solid" BorderWidth="2px" 
                Direction="LeftToRight">

            <table dir="ltr" style="text-align:left; " class="auto-style3" >
                <tr><td colspan="4" class="style1"></td></tr>
                <tr>
                    <td class="style9" dir="ltr" align="left">
                        <asp:Literal ID="NAM" Text="50009" runat="server" />
                    </td>
                    <td class="style11"></td>
                     <td class="style14" dir="ltr">
                        <asp:TextBox ID="txtUser" runat="server" Width="220px" 
                            CssClass="RoundCornerFontBlack" BorderStyle="Solid" Height="24px" 
                            BorderColor="#CDCDCD" BorderWidth="2" Font-Names="Arial" Font-Size="16px" />
                    </td>
                    <td class="style10"></td>
                </tr>
                <tr><td colspan="4" class="style1"></td></tr>
                <tr>
                     <td class="style9" dir="ltr" align="left">
                        <asp:Literal ID="PWD" Text="50010" runat="server" />
                    </td>
                        <td class="style13"></td>
                   <td class="style14" dir="ltr">
                        <asp:TextBox ID="txtPass" runat="server" Width="220px" TextMode="Password"
                        CssClass="RoundCornerFontBlack" BorderStyle="Solid" Height="24px" Font-Names="Arial" Font-Size="16px"  
                            BorderColor="#CDCDCD" BorderWidth="2" />
                    </td>
                    <td class="style15"></td>
                </tr>
         <tr>
         <td colspan="4" align="left" class="style4">&nbsp;<asp:CheckBox ID="chkRemember" 
                 runat="server" Text="50011" 
                 Font-Bold="False" Font-Names="Arial" Font-Size="15px" Width="330px" />
             </td>
         </tr>
         <tr><td colspan="4" align="center" class="style4">
             <asp:Label ID="lblError" runat="server" EnableViewState="False" ForeColor="Red" 
                 Text="50012"
                  Visible="False" Font-Bold="False" 
                 Font-Names="Arial" Font-Size="15px" Width="60%" />
                </td></tr>
<%--                <tr><td colspan="4"></td></tr>--%>
                <tr>
                    <td align="left" colspan="3" dir="ltr" height="48px" class="NewFont2" >
                        <asp:ImageButton ImageUrl="Rus/enter_%20ru[389].png" runat="server" Height="48px" ID="GoIn" OnClick="ImageButton1_Click" />
                    <asp:Label runat="server" Text="50013" ID="ForBtn" Visible="False"></asp:Label>
                     </td>
                    <td align="right" dir="ltr" height="32px" ></td>
                </tr>
            </table>
            </asp:Panel>
    </div>
            <div style="height: 24px" >   
            </div>
            <div align="center" dir="ltr" class="FooterMenu" >
               <br />
                <asp:Literal ID="AllRights" runat="server" Text="50007" 
                    ViewStateMode="Enabled"></asp:Literal>
               <br />
            </div>
             </asp:Panel>
       </div>
    </form>
</body>
</html>
