﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MdLogin.aspx.cs" Inherits="Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login</title>
    <link href="StyleSheet.css" rel="Stylesheet" type="text/css" />
    <style type="text/css">
        .style1
        {
            width: 27px;
            height: 21px;
        }
        .style4
        {
            height: 25px;
        }
        .style9
        {
            font-family: Arial;
            font-size: 18px;
            font-weight: normal;
            text-decoration: none;
            margin-right: 23px;
            height: 22px;
            width: 132px;
        }
        .style10
        {
            width: 33px;
            height: 22px;
        }
        .style11
        {
            width: 172px;
            height: 22px;
        }
        .style12
        {
            font-family: Arial;
            font-size: 18px;
            font-weight: normal;
            text-decoration: none;
            margin-right: 23px;
            height: 24px;
            width: 132px;
        }
        .style13
        {
            width: 172px;
            height: 24px;
        }
        .style14
        {
            height: 24px;
        }
        .style15
        {
            width: 33px;
            height: 24px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server" style="text-align: center">
        <div style="margin: 0px; height: 20px; background-color: #d3d3d3;" ></div>
        <div align="center" class="bgUpperMaster">
            <asp:Panel ID="Panel1" runat="server" Width="940px" CssClass="Panel3s" HorizontalAlign="Center" Direction="RightToLeft">
            <img src="images/header_left.jpg" alt="THertz" width="690px" height="180"/>
            <img src="images/md_big_logo.png" alt="md2000" width="240" height="180"/>

            <div class="GreenMenu">
                <ul id="menu-mainmenu" 
                    style="vertical-align: middle; text-align:right; height: 25px;">
                <li id="menu-item-21" ><a class="AtoGreenMenu" href="http://md2000.net/">
                    <asp:Literal ID="Home" runat="server" Text="50001"></asp:Literal>
                </a></li>
                <li id="menu-item-22" ><a class="AtoGreenMenu" href="http://md2000.net/?page_id=72">
                    <asp:Literal ID="AboutUs" runat="server" Text="50002"></asp:Literal>
                </a></li>
                <li id="menu-item-23" ><a class="AtoGreenMenu" href="http://md2000.net/?page_id=66">
                    <asp:Literal ID="Services" runat="server" Text="50003"></asp:Literal>
                </a></li>
                <li id="menu-item-24" ><a class="AtoGreenMenu" href="http://md2000.net/?page_id=84">
                    <asp:Literal ID="Products" runat="server" Text="50004"></asp:Literal>
                </a></li>
                <li id="menu-item-25" >
                    <a href="#" style="font-family: Arial, Helvetica, sans-serif; font-size: 21px; text-decoration: none; color: #00337A">
                <asp:Literal ID="EnterSystem" runat="server" Text="50005"></asp:Literal>
                </a></li>
                <li id="menu-item-26" ><a class="AtoGreenMenu" href="http://md2000.net/?page_id=68" dir="rtl">
                    <asp:Literal ID="Contact" runat="server" Text="50006"></asp:Literal>
                </a></li>
                </ul>			
            </div>
            <a href="http://md2000.net/?page_id=101English">		
                <img class="style1" src="images/gb.png" alt="heb" style="position:relative; top:-230px; left: 380px;"/></a>	
            <a href="http://md2000.net/?page_id=108/Arabian">		
                <img class="style1" src="images/ar.jpg" style="position: relative; top: -230px; left: 380px" /></a>
            <a href="http://md2000.net/?page_id=110/Russian">		
                <img class="style1" src="images/ru.jpg"  style="position: relative; top: -230px;  left: 380px" /></a>


            <div align="center" style="height: 34px">
                <img src="images/Titles/title-login.png" alt="כניסה למערכת" />
            </div>
            <div align="center" style="height: 12px">
            </div>
            <div align="center">
            <asp:Panel runat="server" Height="230px" Width="407px" 
                ID="Pnl0" HorizontalAlign="Center" CssClass="panLogin" 
                BorderColor="#14499A" BorderStyle="Solid" BorderWidth="2px" 
                        Direction="RightToLeft">

            <table style="text-align:right; width: 400px;" >
                <tr><td colspan="4" class="style1"></td></tr>
                <tr>
                    <td class="NewFont1" dir="rtl" align="left" width="132">  
                        <asp:Literal ID="NAM" Text="50009" runat="server" />
                    </td>
                    <td class="style6"></td>
                    <td>
                        <asp:TextBox ID="txtUser" runat="server" Width="220px" 
                            CssClass="RoundCornerFontBlack" BorderStyle="Solid" Height="24px" 
                            BorderColor="#CDCDCD" BorderWidth="2" />
                    </td>
                    <td style="width: 33px"></td>
                </tr>
                <tr><td colspan="4" class="style1"></td></tr>
                <tr>
                    <td class="NewFont1" dir="rtl" align="left" width="132" height="24">
                        <asp:Literal ID="PWD" Text="50010" runat="server" />
                    </td>
                        <td class="style6"></td>
                    <td>
                        <asp:TextBox ID="txtPass" runat="server" Width="220px" TextMode="Password"
                        CssClass="RoundCornerFontBlack" BorderStyle="Solid" Height="24px" 
                            BorderColor="#CDCDCD" BorderWidth="2" />
                    </td>
                    <td style="width: 33px"></td>
                </tr>
         <tr>
         <td colspan="4" align="right" class="style4">&nbsp;&nbsp;&nbsp;&nbsp;
             <asp:CheckBox ID="chkRemember" runat="server" Text="50011" 
                 Font-Bold="False" Font-Names="Arial" Font-Size="15px" Width="330px" />
             </td>
         </tr>
         <tr><td colspan="4" align="center" class="style4">
             <asp:Label ID="lblError" runat="server" EnableViewState="False" ForeColor="Red" 
                 Text="50012"
                  Visible="False" Font-Bold="False" 
                 Font-Names="Arial" Font-Size="15px" Width="60%" />
                </td></tr>
                <tr><td colspan="4"></td></tr>
                <tr>
                    <td align="left" colspan="3" dir="rtl" height="32px" class="NewFont2" >
                    <asp:Label runat="server" Text="50013" ID="ForBtn"></asp:Label>
                        <asp:ImageButton 
                         ID="ImageButton2" runat="server" ImageAlign="Middle" 
                         ImageUrl="~/images/OnOff.png" onclick="ImageButton1_Click" />
                     </td>
                     <td></td>
                </tr>
            </table>
               </asp:Panel>
               </div>


            <div style="height: 24px" >   
            </div>
            <div align="center" dir="ltr" class="FooterMenu" >
               <br />
                <asp:Literal ID="AllRights" runat="server" Text="50007" 
                    ViewStateMode="Enabled"></asp:Literal>
               <br />
            </div>
            </asp:Panel>    
        </div>
    </form>
</body>
</html>
