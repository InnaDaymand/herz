﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class MemberPages_AlarmDef : SecuredPage
{
    private T_Alarm alarm;
    private T_AlarmTriggerCondition trigger;
    int alarmId = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        System.Web.UI.WebControls.Image ti = (System.Web.UI.WebControls.Image)Master.FindControl("TitleImage");
        ti.ImageUrl = "~/SeenergImg/Titles/Title-9.png";
        alarmId = int.Parse(Request.QueryString["AlarmID"]);
        DataClassesDataContext db = new DataClassesDataContext();
        if (AllFunctions.ConnectStr == "")
        {
            AllFunctions.ConnectStr = db.Connection.ConnectionString;
        }

        if (!_user.IsAdmin())
        {
            string userWhere = Session["userWhere"].ToString();
            LinqConsumers.Where = _user.IsAdmin() ? "" : userWhere;
        }

        if (alarmId > 0)
        {
            alarm = (from a in db.T_Alarms
                     where a.AlarmID == alarmId
                     select a).First();

            if (alarm.TriggerConditionID.HasValue)
            {
                trigger = (from t in db.T_AlarmTriggerConditions
                           where t.ID == alarm.TriggerConditionID.Value
                           select t).First();
            }
            
            if (!IsPostBack)
            {
                txtName.Text = alarm.Name;
                ConsumerCombo.SelectedValue = alarm.tbl_Counter.sql_Consumer_ID.ToString();
                CounterCombo.SelectedValue = alarm.CounterID.ToString();
                PeriodCombo.SelectedValue = alarm.Period.ToString();
                SignCombo.SelectedValue = alarm.Sign;
                txtThreshold.Text = alarm.BaseValue.ToString();
                //ToleranceCombo.SelectedValue = alarm.Tolerance;
                ToleranceCombo.SelectedValue = alarm.Tolerance.ToString();
                chkSms.Checked = alarm.SendSms;
                chkEmail.Checked = alarm.SendEmail;
                txtDateFrom.Text = alarm.CheckTime.Date.ToString("yyyy-MM-dd");
                TimePicker1.SelectedTime = alarm.CheckTime;
                CheckIntervalCombo.SelectedValue = alarm.CheckInterval.ToString();
                if (alarm.StopTime.HasValue)
                    TimePicker2.SelectedTime = alarm.StopTime.Value;
                chkActive.Checked = alarm.IsActive;

                if (alarm.TriggerConditionID.HasValue)
                {
                    chkDependent.Checked = true;
                    cmbMainConsumer.SelectedValue = trigger.tbl_Counter.sql_Consumer_ID.ToString();
                    cmbMainConsumerCounter.SelectedValue = trigger.CounterID.ToString();
                    cmbMainConsumerSign.SelectedValue = trigger.Sign;
                    txtMainConsumerThreshold.Text = trigger.BaseValue.ToString();
                    cmbMainConsumerTolerance.SelectedValue = trigger.Tolerance.ToString();
                }
            }
        }
        else
        {
            alarm = new T_Alarm();
            if (!IsPostBack)
            {
                txtDateFrom.Text = DateTime.Now.ToString("yyyy-MM-dd");
                PeriodCombo.SelectedValue = "24";
                CheckIntervalCombo.SelectedValue = "24";
            }

        }
            //btnDel.Enabled = false;

    }

    protected void Page_PreRenderComplete(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            CounterCombo.SelectedValue = alarm.CounterID.ToString();

            if (alarm.TriggerConditionID.HasValue)
                cmbMainConsumerCounter.SelectedValue = trigger.CounterID.ToString();
        }
        string SQL = "SELECT MailID FROM T_MailsAlarm WHERE AlarmID = " + alarm.AlarmID.ToString();

        DataClassesDataContext db = new DataClassesDataContext();
        string CS = db.Connection.ConnectionString;
        using (SqlConnection CN = new SqlConnection(CS))
        {
            CN.Open();
            SqlCommand CMD = new SqlCommand(SQL, CN);
            SqlDataReader dr = CMD.ExecuteReader();
            while (dr.Read())
            {
                for (int i = 0; i < ListMails.Items.Count; i++)
                {
                    if (ListMails.Items[i].Value.ToString() == dr[0].ToString())
                    {
                        ListMails.Items[i].Selected = true;
                    }
                }
            }
            dr.Close();

            SQL = "SELECT PhoneID FROM dbo.T_PhoneNumbersAlarms WHERE AlarmID = " + alarm.AlarmID.ToString();
            SqlCommand CMQ = new SqlCommand(SQL, CN);
            SqlDataReader da = CMQ.ExecuteReader();
            while (da.Read())
            {
                for (int i = 0; i < ListSMS.Items.Count; i++)
                {
                    if (ListSMS.Items[i].Value.ToString() == da[0].ToString())
                    {
                        ListSMS.Items[i].Selected = true;
                    }
                }
            }









        }

    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        string TriggerCond = "NULL";
        if (chkDependent.Checked)
        {
            if (trigger == null)
                trigger = new T_AlarmTriggerCondition();

            trigger.CounterID = int.Parse(cmbMainConsumerCounter.SelectedValue);
            trigger.Sign = cmbMainConsumerSign.SelectedValue;
            trigger.BaseValue = double.Parse(txtMainConsumerThreshold.Text);
            trigger.Tolerance = double.Parse(cmbMainConsumerTolerance.SelectedValue);
            if (trigger.Sign.Contains('<'))
            {
                trigger.LowerLimit = 0;
                trigger.UpperLimit = trigger.BaseValue * (1 - trigger.Tolerance);
            }
            else
            {
                trigger.LowerLimit = trigger.BaseValue * (1 + trigger.Tolerance);
                trigger.UpperLimit = double.MaxValue;
            }

            if(trigger.ID==0)//new
            {
                db.T_AlarmTriggerConditions.InsertOnSubmit(trigger);
                db.SubmitChanges();
            }

            TriggerCond = trigger.ID.ToString();
            alarm.TriggerConditionID = trigger.ID;
        }
        else
        {
            if (alarm.TriggerConditionID.HasValue)
            {
                TriggerCond = "NULL";                //delete
                //db.T_AlarmTriggerConditions.DeleteOnSubmit(trigger);
                alarm.TriggerConditionID = null;
            }
        }

        string S0 = ""; string S1 = ""; string S3 = "";

        if (TimePicker2.SelectedValue.ToString() == "")
        {
            S3 = "NULL";
        }
        else
	    {
            S3 = "N'" + TimePicker2.SelectedValue.Value.ToShortTimeString() + "'";
	    }

        if (SignCombo.SelectedValue == "<")
        {
            S0 = "0";
            S1 = (alarm.BaseValue * (1 - alarm.Tolerance)).ToString();
        }
        else
        {
            S0 = (alarm.BaseValue * (1 + alarm.Tolerance)).ToString();
            S1 = "10E +100" ;
        }
        string SQL = "";
        if (alarm.AlarmID == 0)
        {
            //Prepare the Insert Statement

            SQL = "INSERT INTO dbo.T_Alarm (Name, CounterID, Period, [Sign], BaseValue, Tolerance, UpperLimit, LowerLimit, " 
            + "IsActive, SendEmail, SendSms, CheckTime, CheckInterval, StopTime, TriggerConditionID) VALUES (N'" + txtName.Text.Trim() + "', "
            + CounterCombo.SelectedValue + ", " + PeriodCombo.SelectedValue + ", N'" + SignCombo.SelectedValue +"', " + txtThreshold.Text + ", " + ToleranceCombo.SelectedValue
            + ", " + S1 + ", " + S0 + ", " + Convert.ToByte(chkActive.Checked).ToString() + ", " + Convert.ToByte(chkEmail.Checked).ToString() + ", " + Convert.ToByte(chkSms.Checked).ToString() + ", '"
            + DateTime.Parse(txtDateFrom.Text).ToString("yyyy-MM-dd") + " "  + TimePicker1.SelectedTime.TimeOfDay + "', " + CheckIntervalCombo.SelectedValue + ", " + S3 
            + ", " + TriggerCond + ")";
        }
        else
        {
            //Prepare the Update Statement

            SQL = "UPDATE dbo.T_Alarm SET Name = N'" + txtName.Text.Trim() + "', CounterID = "
                   + CounterCombo.SelectedValue + ", Period = " + PeriodCombo.SelectedValue + ", Sign = N'"
                   + SignCombo.SelectedValue + "', BaseValue = " + txtThreshold.Text + ", Tolerance = " + ToleranceCombo.SelectedValue + ", UpperLimit = "
                   + S1 + ", LowerLimit = " + S0 + ", IsActive = " + Convert.ToByte(chkActive.Checked).ToString() + ", SendEmail = " +  Convert.ToByte(chkEmail.Checked).ToString()+ ", SendSms = "
                   + Convert.ToByte(chkSms.Checked).ToString() + ", CheckTime = '" + DateTime.Parse(txtDateFrom.Text).ToString("yyyy-MM-dd") + " " + TimePicker1.SelectedTime.TimeOfDay + "', CheckInterval = "
                   + CheckIntervalCombo.SelectedValue + ", StopTime = " + S3 + ", TriggerConditionID = " + TriggerCond
                   + " WHERE AlarmID = " + alarmId.ToString();
        }
        ExecSQL(SQL);

/*
        alarm.Name = txtName.Text.Trim();
        alarm.CounterID = int.Parse(CounterCombo.SelectedValue);
        alarm.Period = int.Parse(PeriodCombo.SelectedValue);
        alarm.Sign = SignCombo.SelectedValue;
        alarm.BaseValue = double.Parse(txtThreshold.Text);
        //alarm.Tolerance = ToleranceCombo.SelectedValue;
        alarm.Tolerance = double.Parse(ToleranceCombo.SelectedValue);
        if (alarm.Sign.Contains('<'))
        {
            alarm.LowerLimit = 0;
            //alarm.UpperLimit = alarm.BaseValue * (1 - double.Parse(alarm.Tolerance));
            alarm.UpperLimit = alarm.BaseValue * (1 - alarm.Tolerance);
        }
        else
        {
            //alarm.LowerLimit = alarm.BaseValue * (1 + double.Parse(alarm.Tolerance));
            alarm.LowerLimit = alarm.BaseValue * (1 + alarm.Tolerance);
            alarm.UpperLimit = double.MaxValue;
        }
        alarm.IsActive = chkActive.Checked;
        alarm.SendEmail = chkEmail.Checked;
        alarm.SendSms = chkSms.Checked;
        alarm.CheckTime = DateTime.Parse(txtDateFrom.Text).Date + TimePicker1.SelectedTime.TimeOfDay;
        alarm.CheckInterval = int.Parse(CheckIntervalCombo.SelectedValue);
        alarm.StopTime = TimePicker2.SelectedValue;

        //if (chkDependent.Checked)
        //    alarm.TriggerConditionID = trigger.ID;

        //if (Request.QueryString["AlarmID"] == "0")

        if (alarm.AlarmID == 0)
            db.T_Alarms.InsertOnSubmit(alarm);

        db.SubmitChanges();
*/
        #region Adding Mail and SMS
        
        if (ListMails.SelectedIndex > -1)
        {
            if (alarm.AlarmID != 0)
            {
                SQL = "DELETE FROM T_MailsAlarm WHERE AlarmID = " + alarm.AlarmID.ToString();
                ExecSQL(SQL);

                for (int i = 0; i < ListMails.Items.Count; i++)
                {
                    if (ListMails.Items[i].Selected)
                    {
                        SQL = "INSERT INTO T_MailsAlarm (MailID, AlarmID, HAlarmID) VALUES (" 
                            + ListMails.Items[i].Value.ToString() + ", " 
                            + alarm.AlarmID.ToString() + ", 0)";
                        ExecSQL(SQL);
                    }
                }
            }
        }

        if (ListSMS.SelectedIndex > -1)
        {
            if (alarm.AlarmID != 0)
            {
                SQL = "DELETE FROM dbo.T_PhoneNumbersAlarms WHERE AlarmID = " + alarm.AlarmID.ToString();
                ExecSQL(SQL);
                for (int i = 0; i < ListSMS.Items.Count; i++)
                {
                    if (ListSMS.Items[i].Selected)
                    {
                        SQL = "INSERT INTO dbo.T_PhoneNumbersAlarms (PhoneID, AlarmID) VALUES (" 
                            + ListSMS.Items[i].Value.ToString() + ", " 
                            + alarm.AlarmID.ToString() + ")"; 
                        ExecSQL(SQL);
                    }
                }
            }
        }
        
        #endregion

        Response.Redirect("AlarmsList.aspx");
    }

    protected void btnDel_Click(object sender, EventArgs e)
    {
        //DataClassesDataContext db = new DataClassesDataContext();
        //if (alarm.TriggerConditionID.HasValue)
        //    db.T_AlarmTriggerConditions.DeleteOnSubmit(trigger);

        //db.T_Alarms.DeleteOnSubmit(alarm);

        //try
        //{
        //    db.SubmitChanges();
        //}
        //catch
        //{
        //}
        string SQL = "DELETE FROM  dbo.T_Alarm WHERE  AlarmID = " + alarmId.ToString();
        ExecSQL(SQL);

        Response.Redirect("AlarmsList.aspx");
    }

    protected void ExecSQL(string SQL)
    {

        using (SqlConnection CN = new SqlConnection(AllFunctions.ConnectStr))
        {
            CN.Open();
            SqlCommand CMD = new SqlCommand(SQL, CN);
            CMD.ExecuteNonQuery();
            CN.Close();
        }

    }
}
