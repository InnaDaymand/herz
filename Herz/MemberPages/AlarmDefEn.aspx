﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MemberPages/MemberEn.master" AutoEventWireup="true" CodeFile="AlarmDefEn.aspx.cs" Inherits="MemberPages_AlarmDef" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register assembly="eWorld.UI, Version=2.0.6.2393, Culture=neutral, PublicKeyToken=24d65337282035f2" namespace="eWorld.UI" tagprefix="ew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style4
        {
            width: 215px;
        }
        .auto-style2 {
            height: 26px;
        }
        .auto-style3 {
            width: 354px;
            align-items:flex-end;
        }
        .auto-style4 {
            width: 354px;
            align-items: flex-end;
            height: 26px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="based" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <div align="center" dir="ltr">
    <table width="100%" style="align-content:center; align-items:center; align-self:center">
        <tr>
            <td>

            <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </ajaxToolkit:ToolkitScriptManager>
                <table style="align-content:center; align-items:center; align-self:center">
                    <tr>
                        <td valign="top" width="50%">
                            <asp:SqlDataSource ID="SqlCounters" runat="server" ConnectionString="Data Source=127.0.0.1;Initial Catalog=md2000Net;User ID=sa;password=poShSp4kD4sd" SelectCommand="SELECT 620 AS ID, enDesc  AS Name FROM tbl_CounterTypes"></asp:SqlDataSource>
                            <table class="DetailsFormViewLayout" >
                                <tr class="DetailsFormViewHeader">
                                    <td colspan="2" align="left">Fault</td>
                                </tr>
                                <tr>
                                    <td class="auto-style3">
                                        Name:
                                    </td>
                                    <td align="left" dir="ltr">
                                        <asp:Textbox ID="txtName" runat="server" Width="300px" />
                                        <asp:RequiredFieldValidator runat="server" ControlToValidate="txtName" 
                                            ErrorMessage="הכנס שם" Display="Dynamic" Text="*" 
                                            ValidationGroup="AllValidators" ID="RequiredFieldValidator1"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style3">
                                        Consumer:
                                    </td>
                                    <td class="auto-style2"  align="left">
                                        <asp:DropDownList ID="ConsumerCombo" runat="server" AutoPostBack="true" 
                                            DataTextField="ConsumerName" 
                                            DataValueField="sql_Consumer_ID" Width="140px" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style3">
                                        Meter type and rate:
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="CounterCombo" runat="server"
                                            DataSourceID="SqlCounters" 
                                            DataTextField="Name" 
                                            DataValueField="ID" Width="140px" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style3">
                                        Consumption period:
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="PeriodCombo" runat="server" Width="140px" >
                                            <asp:ListItem Value="1">1 Hour</asp:ListItem>
                                            <asp:ListItem Value="12">12 Hours</asp:ListItem>
                                            <asp:ListItem Value="24">24 Hours</asp:ListItem>
                                            <asp:ListItem Value="168">7 Days</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style3">
                                        If the consumption:
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="SignCombo" runat="server" Width="50px">
                                            <asp:ListItem>&gt;</asp:ListItem>
                                            <asp:ListItem>&lt;</asp:ListItem>
<%--                                            <asp:ListItem>&lt;=</asp:ListItem>
                                            <asp:ListItem>&gt;=</asp:ListItem>--%>
                                        </asp:DropDownList>
                                        <asp:TextBox ID="txtThreshold" runat="server" />
                                        <asp:RequiredFieldValidator runat="server" ControlToValidate="txtThreshold" 
                                            ErrorMessage="הכנס סף" Display="Dynamic" Text="*" 
                                            ValidationGroup="AllValidators" ID="RequiredFieldValidator2"></asp:RequiredFieldValidator>
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" 
                                            FilterType="Numbers,Custom" ValidChars="."
                                            runat="server" Enabled="True" TargetControlID="txtThreshold">
                                        </cc1:FilteredTextBoxExtender>
                                        &nbsp; -
                                        <asp:DropDownList ID="ToleranceCombo" runat="server">
                                            <asp:ListItem Text="30%" Value="0.3" />
                                            <asp:ListItem Text="50%" Value="0.5" />
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <%-- && IsSplitted == false --%>
                                <tr class="DetailsFormViewHeader">
                                    <td colspan="2" align="left">Condition</td>
                                </tr>
                                <tr>
                                    <td class="auto-style3">
                                        If the consumer depends on other consumer:
                                    </td>
                                    <td align="left">
                                        <asp:CheckBox ID="chkDependent" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style4">
                                        Consumer:
                                    </td>
                                    <td align="left" class="auto-style2">
                                        <asp:DropDownList ID="cmbMainConsumer" runat="server" AutoPostBack="true" 
                                            DataTextField="ConsumerName" 
                                            DataValueField="sql_Consumer_ID" Width="140px" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style3">
                                        Meter type and rate:
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="cmbMainConsumerCounter" runat="server"
                                            DataSourceID="SqlCounters" 
                                            DataTextField="Name" 
                                            DataValueField="ID" Width="140px" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style3">
                                        If the
                                        consumption in the same period:
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="cmbMainConsumerSign" runat="server" Width="50px">
                                            <asp:ListItem>&gt;</asp:ListItem>
                                            <asp:ListItem>&lt;</asp:ListItem>
<%--                                            <asp:ListItem>&lt;=</asp:ListItem>
                                            <asp:ListItem>&gt;=</asp:ListItem>--%>
                                        </asp:DropDownList>
                                        <asp:TextBox ID="txtMainConsumerThreshold" runat="server" />
                                        <asp:RequiredFieldValidator runat="server" ControlToValidate="txtThreshold" 
                                            ErrorMessage="הכנס סף" Display="Dynamic" Text="*" 
                                            ValidationGroup="AllValidators" ID="RequiredFieldValidator3"></asp:RequiredFieldValidator>
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" 
                                            FilterType="Numbers,Custom" ValidChars="."
                                            runat="server" Enabled="True" TargetControlID="txtThreshold">
                                        </cc1:FilteredTextBoxExtender>
                                        &nbsp; -
                                        <asp:DropDownList ID="cmbMainConsumerTolerance" runat="server">
                                            <asp:ListItem Text="30%" Value="0.3" />
                                            <asp:ListItem Text="50%" Value="0.5" />
                                        </asp:DropDownList>
                                    </td>
                                </tr>                                <tr class="DetailsFormViewHeader">
                                    <td colspan="2" align="left">Alert</td>
                                </tr>
                                <tr>
                                    <td class="auto-style3">
                                        Type of message:
                                    </td>
                                    <td align="left">
                                        <asp:CheckBox ID="chkSms" runat="server" Text="SMS" />
                                        &nbsp;
                                        <asp:CheckBox ID="chkEmail" runat="server" Text='Mail' />
                                    </td>
                                </tr>
                                <tr class="DetailsFormViewHeader">
                                    <td colspan="2" align="left">Timing</td>
                                </tr>
                                <tr>
                                    <td class="auto-style3">
                                        First check on:
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtDateFrom" runat="server" Width="100px" ></asp:TextBox>
                                        <asp:Image ID="Image1" runat="server" ImageUrl="~/images/SmallCalendar.gif" />
                                        <cc1:CalendarExtender ID="txtDateFrom_CalendarExtender" runat="server" 
                                            Enabled="True" TargetControlID="txtDateFrom" PopupButtonID="Image1" Format="yyyy-MM-dd">
                                        </cc1:CalendarExtender>
                                        <ew:TimePicker ID="TimePicker1" runat="server" Width="60px" NumberOfColumns="4" PopupLocation="Bottom" Scrollable="False" ImageUrl="~/images/timepicker.gif" 
                                            MilitaryTime="True" PopupHeight="180px" >
                                            <ButtonStyle BorderStyle="None" Width="25px" />
                                        </ew:TimePicker>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style3">
                                        Check every:                                     
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="CheckIntervalCombo" runat="server" >
                                            <asp:ListItem Value="1">1 Hour</asp:ListItem>
                                            <asp:ListItem Value="12">12 Hours</asp:ListItem>
                                            <asp:ListItem Value="24">24 Hours</asp:ListItem>
                                            <asp:ListItem Value="168">7 Days</asp:ListItem>
                                     </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style3">
                                        Until:
                                    </td>
                                    <td align="left">
                                        <ew:TimePicker ID="TimePicker2" runat="server" Nullable="true" 
                                            ShowClearTime="true" ClearTimeText="מחק" Width="120px" NumberOfColumns="4" PopupLocation="Bottom" Scrollable="False" ImageUrl="~/images/timepicker.gif" 
                                            MilitaryTime="True" PopupHeight="180px" >
                                            <ButtonStyle BorderStyle="None" Width="25px" />
                                        </ew:TimePicker>
                                    </td>
                                </tr>
                                <tr class="DetailsFormViewHeader">
                                    <td colspan="2" align="left">State</td>
                                </tr>
                                <tr>
                                    <td class="auto-style3">
                                        Active:
                                    </td>
                                    <td align="left">
                                        <asp:CheckBox ID="chkActive" runat="server" />
                                    </td>
                                </tr>

<%--                                <tr class="DetailsFormViewHeader">
                                    <td colspan="2" align ="left">Mail</td>
                                </tr>
                                <tr align="center">
                                    <td colspan="2">
                                        <asp:ListBox ID="ListMails" runat="server"  SelectionMode="Multiple"
                                            DataSourceID="SqlDataSource1" 
                                            DataTextField="Email" 
                                            DataValueField="ID">
                                        </asp:ListBox>

                                        <asp:SqlDataSource runat="server"
                                            ConnectionString="<%$ ConnectionStrings:Money_DisplayConnectionString3 %>" 
                                            ID="SqlDataSource1" 
                                            SelectCommand="SELECT T_Mails.ID,  T_User.FirstName + ' ' + T_User.LastName + ' ==> ' + T_Mails.Email AS Email FROM T_Mails INNER JOIN T_User ON T_Mails.UserID = T_User.ID ORDER BY T_User.FirstName + ' ' + T_User.LastName" >
                                        </asp:SqlDataSource>
                                    </td>
                                </tr>
                                <tr class="DetailsFormViewHeader">
                                    <td colspan="2" align="left">SMS</td>
                                </tr>
                                <tr align="center">
                                    <td colspan="2">
                                        <asp:ListBox ID="ListSMS" runat="server"  SelectionMode="Multiple"
                                            DataSourceID="DataSourceSMS" 
                                            DataTextField="TEL" 
                                            DataValueField="ID">
                                        </asp:ListBox>

                                        <asp:SqlDataSource runat="server"
                                            ConnectionString="<%$ ConnectionStrings:Money_DisplayConnectionString3 %>"
                                            ID="DataSourceSMS" 
                                            SelectCommand="SELECT FirstName + ' ' + LastName + ' ==> ' + TFN.TelNo AS TEL, TFN.ID FROM T_PhoneNumbers AS TFN INNER JOIN T_User ON TFN.UserID = T_User.ID">
                                        </asp:SqlDataSource>
                                    </td>
                                </tr>--%>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>        
                            <asp:Button ID="btnDel" Text="Delete" runat="server" onclick="btnDel_Click" OnClientClick="return confirm('?למחוק את הרשומה')"/>&nbsp;
                            <asp:Button ID="btnSave" Text="Save" runat="server" ValidationGroup="AllValidators" onclick="btnSave_Click"/>&nbsp;
                            <asp:Button ID="btnCancel" Text="Cancel" runat="server" PostBackUrl="~/MemberPages/AlarmsListEn.aspx"/>&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
        </div>
</asp:Content>

