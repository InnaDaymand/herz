﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MemberPages_AlarmLog : SecuredPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        System.Web.UI.WebControls.Image ti = (System.Web.UI.WebControls.Image)Master.FindControl("TitleImage");
        ti.ImageUrl = "~/Eng/Titles/error log.png";
        if (!IsPostBack)
        {
            if (!_user.IsAdmin())//not admin
            {
                //GridView1.Columns[5].Visible = false;

                cmbClients.Visible = false;
                lblFilter.Visible = false;
            }
            else
            {
                cmbClients.DataSource = LinqClients;
                cmbClients.DataBind();
                cmbClients.Items.Insert(0, "כל הלקוחות");
            }
        }
        if (!_user.IsAdmin())//not admin
        {
            //LinqAlarms.Where = "T_Alarm.tbl_Counter.tbl_Consumers_0.tbl_Client.sql_clientID == @ClientID";
            //LinqAlarms.WhereParameters.Add("ClientID", TypeCode.Int32, _client.sql_clientID.ToString());
            //LinqAlarms.Where = "T_Alarm.tbl_Counter.tbl_Consumers_0.T_Client.ID == @ClientID";
            //LinqAlarms.WhereParameters.Add("ClientID", TypeCode.Int32, _client.sql_clientID.ToString());
            DataClassesDataContext db = new DataClassesDataContext();
            GridView1.DataSourceID = "";
            var clientIDs = DataClassLoader.ClientsAllowedForUser(db, _user.ID).Select(x => x.ID);

            //string SQL = "SELECT Name AS name, "
            //    + " (SELECT sql_Consumer_name FROM tbl_Consumers_0 INNER JOIN tbl_Counters ON tbl_Counters.sql_Consumer_ID = [dbo].[tbl_Consumers_0].sql_Consumer_ID "
            //    + " WHERE tbl_Counters.sql_counter_ID = CounterID) AS consumer, "
            //    + " (SELECT[enDesc] FROM[dbo].[tbl_CounterTypes] INNER JOIN[dbo].[tbl_Counters] ON[tbl_Counters].[sql_Type_ID] = [tbl_CounterTypes].sql_Type_ID "
            //    + " WHERE[tbl_Counters].sql_counter_ID = CounterID ) AS counter, CONVERT(VARCHAR, Tolerance *100) +'%' AS Tolerance, IsActive AS Value, "
            //    + " (SELECT[Name] FROM[dbo].[T_Client] INNER JOIN[tbl_Consumers_0] ON[T_Client].ID = [tbl_Consumers_0].sql_clientID "
            //    + " INNER JOIN[tbl_Counters] ON tbl_Counters.sql_Consumer_ID = [dbo].[tbl_Consumers_0].sql_Consumer_ID WHERE tbl_Counters.sql_counter_ID = CounterID) AS client "
            //    + " FROM T_Alarm WHERE(AlarmID IN (" + clientIDs.ToString() + ")";

            //GridView1.DataSource = AllFunctions.Populate(SQL);






            GridView1.DataSource = from alarm
                                       in db.T_AlarmLogs.Where(c => clientIDs.Contains(c.T_Alarm.tbl_Counter.tbl_Consumers_0.sql_clientID))
                                   orderby alarm.CheckTime descending
                                   select new
                                   {
                                       name = alarm.T_Alarm.Name,
                                       consumer = alarm.T_Alarm.tbl_Counter.tbl_Consumers_0.sql_Consumer_name,
                                       counter = alarm.T_Alarm.tbl_Counter.tbl_CounterType.sql_description + '-' + alarm.T_Alarm.tbl_Counter.T_Tariff.TariffName,
                                       alarm.CheckTime,
                                       alarm.Value,
                                       alarm.T_Alarm.Tolerance,
                                       client = alarm.T_Alarm.tbl_Counter.tbl_Consumers_0.T_Client.Name
                                   };
            GridView1.PageIndexChanging += new GridViewPageEventHandler(GridView1_PageIndexChanging);
        }
    }
    void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataBind();
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if((double)DataBinder.Eval(e.Row.DataItem,"Tolerance")<.5)
                e.Row.Cells[4].BackColor = System.Drawing.Color.Yellow;
            else
                e.Row.Cells[4].BackColor = System.Drawing.Color.Tomato;
        }
    }
    protected void cmbClients_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (cmbClients.SelectedIndex == 0)//no filter
            LinqAlarms.Where = "";
        else
        {
            //LinqAlarms.Where = "T_Alarm.tbl_Counter.tbl_Consumers_0.tbl_Client.sql_clientID == @ClientID";
            LinqAlarms.Where = "T_Alarm.tbl_Counter.tbl_Consumers_0.sql_clientID == @ClientID";
            LinqAlarms.WhereParameters.Clear();
            LinqAlarms.WhereParameters.Add("ClientID", TypeCode.Int32, cmbClients.SelectedValue);
        }
        GridView1.DataBind();
    }
}
