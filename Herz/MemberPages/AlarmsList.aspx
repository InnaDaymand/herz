﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MemberPages/MemberSg.master" AutoEventWireup="true" CodeFile="AlarmsList.aspx.cs" Inherits="MemberPages_AlarmsList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style4
        {
            height: 62px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="based" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
<div  align="center">
    <table width="640px">
        <tr align="center">
            <td class="style4">
                <asp:Label ID="lblFilter" runat="server" Text="סינון לפי לקוח" />
                <asp:DropDownList ID="cmbClients" runat="server" AutoPostBack="True" 
                    DataTextField="clientName" DataValueField="ID" 
                    onselectedindexchanged="cmbClients_SelectedIndexChanged">
                </asp:DropDownList>
                <asp:LinqDataSource ID="LinqClients" runat="server" 
                    ContextTypeName="DataClassesDataContext" 
                    Select="new (ID, Name as clientName)" 
                    TableName="T_Clients">
                </asp:LinqDataSource>
            </td>
        </tr>
        <tr>
            <td align="center">
             <br />
             <asp:LinqDataSource ID="LinqAlarms" runat="server" 
                    ContextTypeName="DataClassesDataContext" 
                    Select="new (
                            AlarmID, 
                            Name,                                     
                            tbl_Counter.tbl_Consumers_0.sql_Consumer_name as consumer,
                            tbl_Counter.T_Tariff.TariffName as counter,
                            Tolerance,
                            IsActive,
                            tbl_Counter.tbl_Consumers_0.T_Client.Name as client)"
                    TableName="T_Alarms" >
                </asp:LinqDataSource>
                <asp:GridView ID="grdAlarms" runat="server" 
                    AutoGenerateColumns="False" DataKeyNames="AlarmID" DataSourceID="LinqAlarms" 
                    onselectedindexchanged="grdAlarms_SelectedIndexChanged" Font-Names="Arial" 
                    Font-Size="13pt" Width="560px" BackColor="White">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" SelectText="עריכה" ButtonType="Button" />
                        <asp:BoundField DataField="AlarmID" HeaderText="קוד" Visible="False" />
                        <asp:BoundField DataField="Name" HeaderText="שם" />
                        <asp:BoundField DataField="consumer" HeaderText="צרכן" />
                        <asp:BoundField DataField="counter" HeaderText="תעריף" />
                        <asp:BoundField DataField="Tolerance" HeaderText="רמה" DataFormatString="{0:P0}" />
                        <asp:CheckBoxField DataField="IsActive" HeaderText="פעיל" />
                        <asp:BoundField DataField="client" HeaderText="לקוח" />
                    </Columns>
                    <EditRowStyle BackColor="#F7F6F3" />
                    <HeaderStyle CssClass="DetailsFormViewHeader"></HeaderStyle>
                </asp:GridView>
                <asp:Button ID="btnNew" runat="server" Text="חדש" 
                    PostBackUrl="~/MemberPages/AlarmDef.aspx?AlarmID=0" CssClass="ButtonRes" />
            </td>
        </tr>
    </table>
</div>
</asp:Content>

