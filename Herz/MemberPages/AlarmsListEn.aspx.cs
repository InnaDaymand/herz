﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MemberPages_AlarmsListEn : SecuredPage
{
    //DataClassLoader _dLoader = new DataClassLoader();
    //tbl_Client _client;

    protected void Page_Load(object sender, EventArgs e)
    {
        System.Web.UI.WebControls.Image ti = (System.Web.UI.WebControls.Image)Master.FindControl("TitleImage");
        ti.ImageUrl = "~/Eng/Titles/alerts.png";
        if (!MemHist.byArrow)
        {
            MemHist.HistPage = Request.RawUrl;
        }
        else
        {
            MemHist.byArrow = false;
        }
        if (!IsPostBack)
        {
            //tbl_Client _client = Session["client"] as tbl_Client;
            //if (!_client.sql_role)//not admin
            //T_User user = Session["user"] as T_User;
            if (!_user.IsAdmin())//not admin
            {
                //LinqAlarms.Where = "tbl_Counter.tbl_Consumers_0.tbl_Client.sql_clientID == @ClientID";
                //LinqAlarms.WhereParameters.Add("ClientID", TypeCode.Int32, _client.sql_clientID.ToString());
                DataClassesDataContext db = new DataClassesDataContext();
                grdAlarms.DataSourceID = "";
                string UID = _user.ID.ToString();
                //var clientIDs = DataClassLoader.ClientsAllowedForUser(db, _user.ID).Select(x => x.ID);
                //grdAlarms.DataSource = from alarm
                //                           in db.T_Alarms.Where(c => clientIDs.Contains(c.tbl_Counter.tbl_Consumers_0.sql_clientID))
                //                       select new
                //                       {
                //                           alarm.AlarmID,
                //                           alarm.Name,
                //                           consumer = alarm.tbl_Counter.tbl_Consumers_0.sql_Consumer_name,
                //                           counter = alarm.tbl_Counter.tbl_CounterType.sql_description + '-' + alarm.tbl_Counter.T_Tariff.TariffName,
                //                           alarm.Tolerance,
                //                           alarm.IsActive,
                //                           client = alarm.tbl_Counter.tbl_Consumers_0.T_Client.Name
                //                       };
                //grdAlarms.Columns[7].Visible = false;

                string SQL = "SELECT AlarmID, Name AS name, "
                    + " (SELECT sql_Consumer_name FROM tbl_Consumers_0 INNER JOIN tbl_Counters ON tbl_Counters.sql_Consumer_ID = [dbo].[tbl_Consumers_0].sql_Consumer_ID "
                    + " WHERE tbl_Counters.sql_counter_ID = CounterID) AS consumer, "
                    + " (SELECT[enDesc] FROM[dbo].[tbl_CounterTypes] INNER JOIN[dbo].[tbl_Counters] ON[tbl_Counters].[sql_Type_ID] = [tbl_CounterTypes].sql_Type_ID "
                    + " WHERE[tbl_Counters].sql_counter_ID = CounterID ) AS counter, CONVERT(VARCHAR, Tolerance *100) +'%' AS Tolerance, IsActive, "
                    + " (SELECT[Name] FROM[dbo].[T_Client] INNER JOIN[tbl_Consumers_0] ON[T_Client].ID = [tbl_Consumers_0].sql_clientID "
                    + " INNER JOIN[tbl_Counters] ON tbl_Counters.sql_Consumer_ID = [dbo].[tbl_Consumers_0].sql_Consumer_ID WHERE tbl_Counters.sql_counter_ID = CounterID) AS client "
                    + " FROM T_Alarm WHERE([CounterID] IN(SELECT CountID FROM [dbo].[V_CountersByUsers] WHERE [UserID] = " + UID + "))";

                grdAlarms.DataSource = AllFunctions.Populate(SQL);
                grdAlarms.DataBind();
                cmbClients.Visible = false;
                lblFilter.Visible = false;
            }
            else
            {
                cmbClients.DataSource = LinqClients;
                cmbClients.DataBind();
                cmbClients.Items.Insert(0, "כל הלקוחות");
            }
        }
    }
    protected void grdAlarms_SelectedIndexChanged(object sender, EventArgs e)
    {
        Response.Redirect("AlarmDefEn.aspx?AlarmID=" + grdAlarms.SelectedDataKey.Value.ToString());
    }
    protected void cmbClients_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (cmbClients.SelectedIndex == 0)//no filter
            LinqAlarms.Where = "";
        else
        {
            LinqAlarms.Where = "tbl_Counter.tbl_Consumers_0.T_Client.ID == @ClientID";
            LinqAlarms.WhereParameters.Clear();
            LinqAlarms.WhereParameters.Add("ClientID", TypeCode.Int32, cmbClients.SelectedValue);
        }
        grdAlarms.DataBind();
    }
}
