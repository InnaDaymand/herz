﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MemberPages/MemberSg.master" AutoEventWireup="true" CodeFile="CalculateEnergyStore.aspx.cs" Inherits="MemberPages_CompareChart" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register assembly="eWorld.UI, Version=2.0.6.2393, Culture=neutral, PublicKeyToken=24d65337282035f2" namespace="eWorld.UI" tagprefix="ew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style2
        {
            height: 74px;
        }
        </style>
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="based" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
<ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</ajaxToolkit:ToolkitScriptManager>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">
    <asp:Panel runat="server" ID="PanelChart">
        <div align="center">
            <table width="640px">
                <tr valign="middle">
                    <td align="center">
                        מ-
                        <asp:TextBox ID="txtDateFrom" runat="server" Width="80px" style="margin-right: 0px"></asp:TextBox>
                        <asp:Image ID="Image1" runat="server" ImageUrl="~/images/SmallCalendar.gif" />
                        <cc1:CalendarExtender ID="txtDateFrom_CalendarExtender" runat="server" 
                            Enabled="True" TargetControlID="txtDateFrom" PopupButtonID="Image1" 
                            Format="yyyy-MM-dd">
                        </cc1:CalendarExtender>
                     </td>
                </tr>
                <tr >
                    <td align="center">
                        <asp:ListBox ID="chbCons" runat="server" AutoPostBack="True" BackColor="Silver" 
                            DataTextField="sql_Consumer_name" DataValueField="sql_Consumer_ID" 
                            style="margin-right: 0px" Width="205px"></asp:ListBox>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                    <asp:Button ID="btnShow" runat="server" Text="חשב" onclick="btnShow_Click" />
                </td>
            </tr>
        </table>
    </div>
        <br />
    <div align="center">
        <asp:Label Text="0 שעות 0 דקות" runat="server" Font-Names="Arial" 
            Font-Size="24px" ID="lbZmn" Font-Bold="true" ForeColor="Black" />
    </div>
        <br />
    <div align="center">
        <asp:Label Text="0.00 ₪" runat="server" Font-Names="Arial" 
            Font-Size="24px" ID="lbRes" Font-Bold="true" ForeColor="Black"  />
    </div>

    </asp:Panel>
</asp:Content>

