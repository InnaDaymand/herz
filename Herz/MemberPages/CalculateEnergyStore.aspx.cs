﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web.UI.DataVisualization.Charting;

public partial class MemberPages_CompareChart : SecuredPage
{
    //DateTime _from, _to;
    string CS = "";
    string ShS = "₪";
   protected void Page_Load(object sender, EventArgs e)
    {
        System.Web.UI.WebControls.Image ti = (System.Web.UI.WebControls.Image)Master.FindControl("TitleImage");
        ti.ImageUrl = "~/SeenergImg/Titles/CalculateEnergyStore.png";

        DataClassesDataContext dc = new DataClassesDataContext();
        CS = dc.Connection.ConnectionString;
        if (IsPostBack)
        {
            AllFunctions.DateForStore = DateTime.Parse(txtDateFrom.Text).Date;
            //txtDateTo_CalendarExtender.SelectedDate = DateTime.Today;
            //txtDateFrom_CalendarExtender.SelectedDate = DateTime.Today.AddDays(-1);

            //string Uid = Session["UID"].ToString();
            ////string SQL = "SELECT ID, NAME FROM [md2000Net].[dbo].[T_Client] WHERE [PowerUserID] = "
            ////    + Uid + " UNION ALL SELECT ID, NAME FROM [md2000Net].[dbo].[T_Client] WHERE ID IN "
            ////    + "(SELECT [ClientID] FROM [md2000Net].[dbo].[T_ClientUser] WHERE [UserID] = " + Uid + ") ORDER BY NAME";

            //string SQL = "EXEC S_GetConsumersForCalculateSleepingStore " + Session["UID"];
            ////string SQL = "SELECT [sql_Consumer_ID], [sql_Consumer_name] FROM [tbl_Consumers_0] WHERE [sql_Consumer_ID] IN "
            ////+ "(SELECT [ConsumerID]   FROM [md2000Net].[dbo].[T_CounterModesParams] WHERE [ParamES1] <> 0 ) ";

            //SqlConnection CN = new SqlConnection(CS);
            //CN.Open();

            //SqlCommand command = new SqlCommand(SQL, CN);
            //SqlDataAdapter adapter = new SqlDataAdapter();
            //adapter.SelectCommand = command;

            //DataTable table = new DataTable();
            //adapter.Fill(table);
            //chbCons.DataSource = table;
            //chbCons.DataTextField = "sql_Consumer_name";
            //chbCons.DataValueField = "sql_Consumer_ID";
            //chbCons.DataBind();
            //CN.Close();
        }
        else
        {
            //_to = DateTime.Today;
            //_from = DateTime.Today.AddDays(-1);
            
            txtDateFrom_CalendarExtender.SelectedDate = DateTime.Today.AddDays(-1); 
            AllFunctions.DateForStore = DateTime.Today.AddDays(-1); 
            txtDateFrom_CalendarExtender.SelectedDate = DateTime.Today.AddDays(-1);

            string Uid = Session["UID"].ToString();

            string SQL = "EXEC S_GetConsumersForCalculateSleepingStore " + Session["UID"];

            SqlConnection CN = new SqlConnection(CS);
            CN.Open();

            SqlCommand command = new SqlCommand(SQL, CN);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = command;

            DataTable table = new DataTable();
            adapter.Fill(table);
            chbCons.DataSource = table;
            chbCons.DataTextField = "sql_Consumer_name";
            chbCons.DataValueField = "sql_Consumer_ID";
            chbCons.DataBind();
            CN.Close();
            //_from = DateTime.Parse(txtDateFrom.Text).Date ..+ TimePicker1.SelectedTime.TimeOfDay;
            //.._to = DateTime.Parse(txtDateTo.Text).Date + TimePicker2.SelectedTime.TimeOfDay;
        }

        txtDateFrom_CalendarExtender.SelectedDate = AllFunctions.DateForStore;
        //txtDateTo_CalendarExtender.SelectedDate = _to;
        //TimePicker1.SelectedTime = _from;
        //TimePicker2.SelectedTime = _to;

    }

    protected void btnShow_Click(object sender, EventArgs e)
    {

        if ((txtDateFrom.Text.Length == 0) || (chbCons.SelectedIndex == -1))
        {
            return;
        }

        string SQL = "EXEC [dbo].[S_GetCalculationCostSleepingStore] @ConsID = " + chbCons.SelectedValue.ToString() + ", @DateID = '" 
        + DateTime.Parse(txtDateFrom.Text).Date.ToString("yyyy-MM-dd") + "'";

        DataSet dst = new DataSet();
        SqlDataAdapter sda = new SqlDataAdapter("", "");
        DataTable dtb = new DataTable();

        dst = new DataSet();
        sda = new SqlDataAdapter("", "");

        SqlConnection CN = new SqlConnection(CS);

        SqlCommand selectCMD = new SqlCommand(SQL, CN);
        selectCMD.CommandTimeout = 0;

        sda.SelectCommand = selectCMD;

        CN.Open();

        sda.Fill(dst, "Results");

        CN.Close();

        dtb = dst.Tables[0];
        
        lbRes.Text = dtb.Rows[0][0].ToString() + " " + ShS; 

        lbZmn.Text = dtb.Rows[0][1].ToString() + " " + "שעות" + " "  + dtb.Rows[0][2].ToString() + " " + "דקות";




        //DateTime dt0 = DateTime.Parse(txtDateFrom.Text).Date + TimePicker1.SelectedTime.TimeOfDay;
        //DateTime dt1 = DateTime.Parse(txtDateTo.Text).Date + TimePicker2.SelectedTime.TimeOfDay;
        //if (dt0 > dt1)
        //{
        //    txtDateTo.Text = "";
        //    return;
        //}
        //string SQL = "";
        //string S = CollectData();
        ////string SQL = "EXEC [dbo].[S_CalculateCosts] N'" + dt0.ToString("yyyy-MM-dd HH:mm:00") + "', '" + dt1.ToString("yyyy-MM-dd HH:mm:ss") + "', '" + S + "', '', 0";
        //    //+ Session["Client"].ToString() + ", 0";

        //DataSet dst = new DataSet();
        //SqlDataAdapter sda = new SqlDataAdapter("", "");

        ////string SS = "";
        //SqlConnection CN = new SqlConnection(CS);

        //SqlCommand selectCMD = new SqlCommand(SQL, CN);
        //selectCMD.CommandTimeout = 0;

        //sda.SelectCommand = selectCMD;

        //CN.Open();

        //sda.Fill(dst, "Customers");

        //CN.Close();

        //DataTable dNames = dst.Tables[0];
        //int rr = dNames.Rows.Count;

        //for (int i = 1; i <= rr; i++) 
        //{
        //    Chart ct = new Chart();
        //    ct.ChartAreas.Add(dNames.Rows[i - 1][0].ToString());
        //    ct.Width = 850;
        //    ct.Height = 200;
        //    //ct.Titles[0].Font = new Font("Arial", 14);
        //    ct.BackColor = Color.AliceBlue;

        //    PanelChart.Controls.Add(ct);

        //    AddSeries(ct, ct.ChartAreas[0], dst.Tables[i]); 
        //}

    }

    protected string CollectData()
    {
        string S = "";
        for (int i = 0; i < chbCons.Items.Count; i++)
        {
            if (chbCons.Items[i].Selected)
            {
                S += chbCons.Items[i].Value.ToString() + ", ";
            }
        }

        S = S.Substring(0, (S.Length - 2));

        return S;
    }

    private void AddSeries(Chart chart, /* int LoadID, */ ChartArea ca, DataTable dt)
    {
        double TOT = 0;
       
        string[] LoadT = { "פסגה", "גבע", "שפל" };

        Title t = new System.Web.UI.DataVisualization.Charting.Title();
        t.Font = new Font("Arial", 14, FontStyle.Bold); 
        t.Text = ca.Name;
        t.DockedToChartArea = ca.Name;
        chart.Titles.Add(t);

        ca.AxisY.Title = "ש\"ח";

        chart.Legends.Add(ca.Name);
        chart.Legends[ca.Name].IsDockedInsideChartArea = false;
        chart.Legends[ca.Name].DockedToChartArea = ca.Name;
        chart.Legends[ca.Name].Docking = Docking.Bottom;
        chart.Legends[ca.Name].Alignment = StringAlignment.Center;
        chart.Legends[ca.Name].LegendItemOrder = LegendItemOrder.ReversedSeriesOrder;
        chart.Legends[ca.Name].Title = "";
        //chart.Legends[ca.Name].Title = "עלות, " + ca.AxisY.Title;
        chart.Legends[ca.Name].TitleFont = ca.AxisY.TitleFont;


        LegendCellColumn colTotal = new LegendCellColumn();
        colTotal.Text = "#TOTAL{N2}";
        chart.Legends[ca.Name].CellColumns.Add(colTotal);

        LegendCellColumn firstColumn = new LegendCellColumn();
        firstColumn.ColumnType = LegendCellColumnType.SeriesSymbol;
        //firstColumn.HeaderText = "Color";
        firstColumn.HeaderBackColor = Color.WhiteSmoke;
        chart.Legends[ca.Name].CellColumns.Add(firstColumn);

        LegendCellColumn secondColumn = new LegendCellColumn();
        secondColumn.ColumnType = LegendCellColumnType.Text;
        //secondColumn.HeaderText = "Name";
        secondColumn.Text = "#LEGENDTEXT";
        secondColumn.HeaderBackColor = Color.WhiteSmoke;
        chart.Legends[ca.Name].CellColumns.Add(secondColumn);


        Series srs = new Series(LoadT[0]);
        srs.ChartType = SeriesChartType.StackedColumn;
        srs.ChartArea = ca.Name;
        srs.XValueType = ChartValueType.DateTime;
        srs.ToolTip = "#VALX{g} - #VALY{0.##}";
        srs.LabelFormat = "0.##";
        //srs.Legend = chart.Legends[1].Name;

        chart.Series.Add(srs);

        Series sr1 = new Series(LoadT[1]);
        sr1.ChartType = SeriesChartType.StackedColumn;
        sr1.ChartArea = ca.Name;
        sr1.XValueType = ChartValueType.DateTime;
        sr1.ToolTip = "#VALX{g} - #VALY{0.##}";
        sr1.LabelFormat = "0.##";
        //sr1.Legend = chart.Legends[1].Name;

        chart.Series.Add(sr1);

        Series sr2 = new Series(LoadT[2]);
        sr2.ChartType = SeriesChartType.StackedColumn;
        sr2.ChartArea = ca.Name;
        sr2.XValueType = ChartValueType.DateTime;
        sr2.ToolTip = "#VALX{g} - #VALY{0.##}";
        sr2.LabelFormat = "0.##";
        //srs.Legend = chart.Legends[1].Name;

        chart.Series.Add(sr2);

        double cdd = 0;
        srs.Color = Color.Red;
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            cdd += Convert.ToDouble(dt.Rows[i][1]);
            srs.Points.AddXY((DateTime)dt.Rows[i][0], Convert.ToDouble(dt.Rows[i][1]));
        }

        TOT = cdd;
        cdd = 0;
        sr1.Color = Color.Gold;
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            cdd += Convert.ToDouble(dt.Rows[i][1]);
            sr1.Points.AddXY((DateTime)dt.Rows[i][0], Convert.ToDouble(dt.Rows[i][2]));
        }

        TOT += cdd;
        cdd = 0;
        sr2.Color = Color.Green;
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            cdd += Convert.ToDouble(dt.Rows[i][1]);
            sr2.Points.AddXY((DateTime)dt.Rows[i][0], Convert.ToDouble(dt.Rows[i][3]));
        }
        TOT += cdd;

        LegendItem footer = new LegendItem();
        footer.Cells.Add(LegendCellType.Text, String.Format("{0:#,##0.##}", TOT), ContentAlignment.BottomCenter);
        footer.Cells.Add(LegendCellType.Text, "סה\"כ", ContentAlignment.BottomCenter);
        footer.Cells.Add(LegendCellType.Text, "", ContentAlignment.BottomCenter);
        footer.Cells[1].CellSpan = 2;
        chart.Legends[ca.Name].CustomItems.Add(footer);
    }

    protected void cmbClients_SelectedIndexChanged(object sender, EventArgs e)
    {
        //Session["Client"] = cmbClients.SelectedValue.ToString();
    }
}