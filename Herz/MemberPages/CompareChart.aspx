﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MemberPages/MemberSg.master" AutoEventWireup="true" CodeFile="CompareChart.aspx.cs" Inherits="MemberPages_CompareChart" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register assembly="eWorld.UI, Version=2.0.6.2393, Culture=neutral, PublicKeyToken=24d65337282035f2" namespace="eWorld.UI" tagprefix="ew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="../StyleSheet.css" rel="Stylesheet" type="text/css" />
    <style type="text/css">
        .style1
        {
            width: 258px;
        }
        .style4
        {
            width: 213px;
        }
        </style>
    </asp:Content>
<asp:Content ID="MyMenu" ContentPlaceHolderID="ForMenu" runat="server">

	<div align="center" class="UnprintedItem">
		<table style="padding:5px;">
			<tr style="vertical-align:middle;">
				<td class="auto-style1">
					מ-
					<asp:TextBox ID="txtDateFrom" runat="server" Width="80px" style="margin-right: 0px"></asp:TextBox>
					<asp:Image ID="Image1" runat="server" ImageUrl="~/images/SmallCalendar.gif" />
					<cc1:CalendarExtender ID="txtDateFrom_CalendarExtender" runat="server" 
						Enabled="True" TargetControlID="txtDateFrom" PopupButtonID="Image1" 
						Format="yyyy-MM-dd">
					</cc1:CalendarExtender>
					<ew:TimePicker ID="TimePicker1" runat="server" Width="40px" NumberOfColumns="4" PopupLocation="Bottom" Scrollable="False" ImageUrl="~/images/timepicker.gif" 
									MilitaryTime="True" PopupHeight="180px" >
						<ButtonStyle BorderStyle="None" Width="25px" />
					</ew:TimePicker>
				</td>
				<td>
					עד-
					<asp:TextBox ID="txtDateTo" runat="server" Width="80px" style="margin-right: 4px"></asp:TextBox>
					<asp:Image ID="Image2" runat="server" ImageUrl="~/images/SmallCalendar.gif" />
					<cc1:CalendarExtender ID="txtDateTo_CalendarExtender" runat="server" 
						Enabled="True" TargetControlID="txtDateTo" PopupButtonID="Image2" Format="yyyy-MM-dd">
					</cc1:CalendarExtender>
					<ew:TimePicker ID="TimePicker2" runat="server" Width="40px" NumberOfColumns="4" PopupLocation="Bottom" Scrollable="False" ImageUrl="~/images/timepicker.gif" 
									MilitaryTime="True" PopupHeight="180px" >
						<ButtonStyle BorderStyle="None" Width="25px" />
					</ew:TimePicker>
				</td>
				<td >
					<asp:Button ID="btnShow" runat="server" Text="הצג גרף" onclick="btnShow_Click" />
				</td>
				<td >
					<asp:Button ID="btnPrint" runat="server" Text="הצג דו''ח" OnClick="btnPrint_Click" />
				</td>
				<td >
					<asp:Button Text="בחירה חדשה" runat="server" ID="Button1" OnClick="ToUpper_Click" />
				</td>
			</tr>
            <tr>
                <td colspan="5" align="center">
                    <asp:Label ID ="lblDateState" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
		</table>
	</div>

        <asp:MultiView ID="mvCC" runat="server" ActiveViewIndex="0">
        <asp:View runat="server" ID="ViewMenu">
             <div align="center" class="auto-style1" dir="rtl">
                <table >
                    <tr>
                        <td align="right" colspan="3" style="align-content:flex-start; align-items:flex-start; align-self:center; 
                                            background-color:silver; border:solid;">
                            <asp:CheckBox Text="בחר הכל" runat="server" ID="SelAll" OnCheckedChanged="SelAll_CheckedChanged" AutoPostBack="True" />
                        </td>
                    </tr>
                    <tr >
                        <td align="center" colspan="3" style="align-content:center; align-items:center; align-self:center;">
                            <asp:CheckBoxList ID="chbCons" runat="server" AutoPostBack="false" 
                                DataTextField="sql_Consumer_name" BackColor="Silver" 
                                DataValueField="sql_Consumer_ID">
                            </asp:CheckBoxList>
                        </td>
                    </tr>
                </table>
            </div>

        </asp:View>

        <asp:View runat="server" ID="ShowData">
            <div align="center">
                <asp:Panel runat="server" ID="PanelChart" HorizontalAlign="Center">
                </asp:Panel>
            </div>
        </asp:View>

        <asp:View runat="server" ID="PrintData">

        <div align="center">
           <asp:Panel ID="Panel1" runat="server" Width="950px" CssClass="Panel3s">
                <a href="javascript:print()"><asp:Image ID="Image3" ImageUrl="~/PrintPages/NewLog.jpg" runat="server" BorderStyle="None" Width="940px" Height="120px"/></a>
                <p align="center" lang="he" style="font-family: Aharoni; font-size: 22px" class="UnprintedItem">
                    <a href="javascript:print()" dir="rtl">הדפס</a>
                </p>
            </asp:Panel>
        </div>

        <div align="center">
        <br />
        <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="X-Large" Text="עלות, ש&quot;ח"></asp:Label>
        <br />
            <p dir='rtl' lang="he" style="text-align: center">
                <asp:GridView ID="GridCompare" runat="server" Font-Names="Calibri" Font-Size="Large">
                </asp:GridView>
            </p>
        </div>

        </asp:View>
    </asp:MultiView>


</asp:Content>
    <asp:Content ID="Content2" ContentPlaceHolderID="based" Runat="Server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
    </asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">
</asp:Content>

