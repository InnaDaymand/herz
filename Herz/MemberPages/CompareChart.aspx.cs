﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;

public partial class MemberPages_CompareChart : System.Web.UI.Page
{
    DateTime _from, _to;
    string CS = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        System.Web.UI.WebControls.Image ti = (System.Web.UI.WebControls.Image)Master.FindControl("TitleImage");
        ti.ImageUrl = "~/SeenergImg/Titles/Title-CD.png";

        bool sgb = false;
        DataClassesDataContext dc = new DataClassesDataContext();
        CS = dc.Connection.ConnectionString;
        if (!IsPostBack)
        {
            AllFunctions.ConsID = "";
            _from = Convert.ToDateTime(Session["DateFrom"]);
            _to = Convert.ToDateTime(Session["DateTill"]);

            string Uid = Session["UID"].ToString();

            string SQL = "EXEC	[dbo].[S_GetAllConsumersByUser] " + Uid + ", 0, 0";
            chbCons.DataSource = AllFunctions.Populate(SQL);
            chbCons.DataTextField = "Name";
            chbCons.DataValueField = "ID";
            chbCons.DataBind();
            sgb = false;
        }
        else
        {
            DateTime dt0 = DateTime.Parse(txtDateFrom.Text).Date + TimePicker1.SelectedTime.TimeOfDay;
            DateTime dt1 = DateTime.Parse(txtDateTo.Text).Date + TimePicker2.SelectedTime.TimeOfDay;
            if ((_from != dt0) || (_to != dt1))
            {
                Session["DateFrom"] = dt0.ToString("yyyy-MM-dd HH:mm:ss");
                Session["DateTill"] = dt1.ToString("yyyy-MM-dd HH:mm:ss");

                _from = Convert.ToDateTime(Session["DateFrom"]);
                _to = Convert.ToDateTime(Session["DateTill"]);
            }
            AllFunctions.IDforGraphDoh = "";
            sgb = true;
            if (_from > _to)
            {
                lblDateState.Text = "תאריך התחלה גדולה מתאריך סיום!";
                return;
            }
        }

        txtDateFrom_CalendarExtender.SelectedDate = _from;
        txtDateTo_CalendarExtender.SelectedDate = _to;
        TimePicker1.SelectedTime = _from;
        TimePicker2.SelectedTime = _to;

        if ((sgb) && (AllFunctions.ConsID != "")) ShowGraph(_from, _to);
    }

    protected void btnShow_Click(object sender, EventArgs e)
    {
        if (txtDateFrom.Text.Length == 0)
        {
            return;
        }

        DateTime dt0 = DateTime.Parse(txtDateFrom.Text).Date + TimePicker1.SelectedTime.TimeOfDay;
        DateTime dt1 = DateTime.Parse(txtDateTo.Text).Date + TimePicker2.SelectedTime.TimeOfDay;
        if (dt0 > dt1)
        {
            txtDateTo.Text = "";
            return;
        }

        if (AllFunctions.ConsID == "")
        {
            string nid = "";
            foreach (ListItem item in chbCons.Items)
            {
                if (item.Selected) nid += item.Value.ToString() + ", ";
            }
            AllFunctions.ConsID = nid.Substring(0, nid.Length - 2);
        }

        if ((AllFunctions.ConsID != "")) ShowGraph(dt0, dt1);
    }

    protected string CollectData()
    {
        string S = "";
        return S;
    }

    private void AddSeries(Chart chart, /* int LoadID, */ ChartArea ca, DataTable dt)
    {
        double TOT = 0;

        string[] LoadT = { "פסגה", "גבע", "שפל" };

        Title t = new System.Web.UI.DataVisualization.Charting.Title();
        t.Font = new Font("Arial", 14, FontStyle.Bold);
        t.Text = ca.Name;
        t.DockedToChartArea = ca.Name;
        chart.Titles.Add(t);

        ca.AxisY.Title = "ש\"ח";
        ca.AxisX.LabelStyle.Format = "dd/MM\nHH:mm";

        chart.Legends.Add(ca.Name);
        chart.Legends[ca.Name].IsDockedInsideChartArea = false;
        chart.Legends[ca.Name].DockedToChartArea = ca.Name;
        chart.Legends[ca.Name].Docking = Docking.Bottom;
        chart.Legends[ca.Name].Alignment = StringAlignment.Center;
        chart.Legends[ca.Name].LegendItemOrder = LegendItemOrder.ReversedSeriesOrder;
        chart.Legends[ca.Name].Title = "";
        //chart.Legends[ca.Name].Title = "עלות, " + ca.AxisY.Title;
        chart.Legends[ca.Name].TitleFont = ca.AxisY.TitleFont;


        LegendCellColumn colTotal = new LegendCellColumn();
        colTotal.Text = "#TOTAL{N2}";
        chart.Legends[ca.Name].CellColumns.Add(colTotal);

        LegendCellColumn firstColumn = new LegendCellColumn();
        firstColumn.ColumnType = LegendCellColumnType.SeriesSymbol;
        //firstColumn.HeaderText = "Color";
        firstColumn.HeaderBackColor = Color.WhiteSmoke;
        chart.Legends[ca.Name].CellColumns.Add(firstColumn);

        LegendCellColumn secondColumn = new LegendCellColumn();
        secondColumn.ColumnType = LegendCellColumnType.Text;
        //secondColumn.HeaderText = "Name";
        secondColumn.Text = "#LEGENDTEXT";
        secondColumn.HeaderBackColor = Color.WhiteSmoke;
        chart.Legends[ca.Name].CellColumns.Add(secondColumn);


        Series srs = new Series(LoadT[0]);
        srs.ChartType = SeriesChartType.StackedColumn;
        srs.ChartArea = ca.Name;
        srs.XValueType = ChartValueType.DateTime;
        srs.ToolTip = "#VALX{g} - #VALY{0.##}";
        srs.LabelFormat = "0.##";
        //srs.Legend = chart.Legends[1].Name;

        chart.Series.Add(srs);

        Series sr1 = new Series(LoadT[1]);
        sr1.ChartType = SeriesChartType.StackedColumn;
        sr1.ChartArea = ca.Name;
        sr1.XValueType = ChartValueType.DateTime;
        sr1.ToolTip = "#VALX{g} - #VALY{0.##}";
        sr1.LabelFormat = "0.##";
        //sr1.Legend = chart.Legends[1].Name;

        chart.Series.Add(sr1);

        Series sr2 = new Series(LoadT[2]);
        sr2.ChartType = SeriesChartType.StackedColumn;
        sr2.ChartArea = ca.Name;
        sr2.XValueType = ChartValueType.DateTime;
        sr2.ToolTip = "#VALX{g} - #VALY{0.##}";
        sr2.LabelFormat = "0.##";
        //srs.Legend = chart.Legends[1].Name;

        chart.Series.Add(sr2);

        double cdd = 0;
        srs.Color = Color.Red;
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            cdd += Convert.ToDouble(dt.Rows[i][1]);
            srs.Points.AddXY((DateTime)dt.Rows[i][0], Convert.ToDouble(dt.Rows[i][1]));
        }

        TOT = cdd;
        cdd = 0;
        sr1.Color = Color.Gold;
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            cdd += Convert.ToDouble(dt.Rows[i][2]);
            sr1.Points.AddXY((DateTime)dt.Rows[i][0], Convert.ToDouble(dt.Rows[i][2]));
        }

        TOT += cdd;
        cdd = 0;
        sr2.Color = Color.Green;
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            cdd += Convert.ToDouble(dt.Rows[i][3]);
            sr2.Points.AddXY((DateTime)dt.Rows[i][0], Convert.ToDouble(dt.Rows[i][3]));
        }
        TOT += cdd;

        LegendItem footer = new LegendItem();
        footer.Cells.Add(LegendCellType.Text, String.Format("{0:#,##0.##}", TOT), ContentAlignment.BottomCenter);
        footer.Cells.Add(LegendCellType.Text, "סה\"כ", ContentAlignment.BottomCenter);
        footer.Cells.Add(LegendCellType.Text, "", ContentAlignment.BottomCenter);
        footer.Cells[1].CellSpan = 2;
        chart.Legends[ca.Name].CustomItems.Add(footer);
    }

    protected void cmbClients_SelectedIndexChanged(object sender, EventArgs e)
    {
        //Session["Client"] = cmbClients.SelectedValue.ToString();
    }

    private void ShowGraph(DateTime dt0, DateTime dt1)
    {
        PanelChart.Controls.Clear();
        string S = AllFunctions.ConsID;
        //string S = CollectData();
        string SQL = "EXEC [dbo].[S_CalculateCosts] N'" + dt0.ToString("yyyy-MM-dd HH:mm:00") + "', '" + dt1.ToString("yyyy-MM-dd HH:mm:ss") + "', '" + S + "', '', 0";
        AllFunctions.ForCompareSQL = SQL;
        //+ Session["Client"].ToString() + ", 0";

        DataSet dst = new DataSet();
        SqlDataAdapter sda = new SqlDataAdapter("", "");

        //string SS = "";
        SqlConnection CN = new SqlConnection(CS);

        SqlCommand selectCMD = new SqlCommand(SQL, CN);
        selectCMD.CommandTimeout = 0;

        sda.SelectCommand = selectCMD;

        CN.Open();

        sda.Fill(dst, "Customers");

        CN.Close();

        try
        {
            DataTable dNames = dst.Tables[0];
            int rr = dNames.Rows.Count;

            for (int i = 1; i <= rr; i++)
            {
                Chart ct = new Chart();
                ct.ChartAreas.Add(dNames.Rows[i - 1][0].ToString());
                ct.Width = AllFunctions.ChartWidth;
                ct.Height = 200;
                //ct.Titles[0].Font = new Font("Arial", 14);
                ct.BackColor = Color.AliceBlue;

                PanelChart.Controls.Add(ct);

                AddSeries(ct, ct.ChartAreas[0], dst.Tables[i]);
            }


        }
        catch (Exception)
        {

            //throw;
        }
        mvCC.ActiveViewIndex = 1;
    }

    protected void Users_MenuItemClick(object sender, MenuEventArgs e)
    {
        if (e.Item.ChildItems.Count > 0) return;
        ListItem lt = new ListItem(e.Item.Text, e.Item.Value);
        lt.Selected = true;
        chbCons.Items.Add(lt);
    }

    protected void ToUpper_Click(object sender, EventArgs e)
    {
        AllFunctions.ConsID = "";
        mvCC.ActiveViewIndex = 0;
    }

    protected void btnPrint_Click(object sender, EventArgs e)
    {
        if (AllFunctions.ForCompareSQL == "")
        {
            return;
        }
        string SQL = AllFunctions.ForCompareSQL + ", 1";
        GridCompare.DataSource = AllFunctions.Populate(SQL);
        GridCompare.DataBind();
        mvCC.ActiveViewIndex = 2;
    }

    protected void SelAll_CheckedChanged(object sender, EventArgs e)
    {
        bool rf = SelAll.Checked;
        foreach (ListItem item in chbCons.Items)
        {
            item.Selected = rf;
        }
        mvCC.ActiveViewIndex = 0;
    }
}