﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MemberPages/MemberEn.master" AutoEventWireup="true" CodeFile="CompareChartEn.aspx.cs" Inherits="MemberPages_CompareChartEn" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register assembly="eWorld.UI, Version=2.0.6.2393, Culture=neutral, PublicKeyToken=24d65337282035f2" namespace="eWorld.UI" tagprefix="ew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style1
        {
            width: 258px;
        }
        .style4
        {
            width: 213px;
        }
        </style>
    </asp:Content>
<asp:Content ID="MyMenu" ContentPlaceHolderID="ForMenu" runat="server">
     <div align="center">
        <table width="940px" style="align-content:center; align-items:center; align-self:center">
            <tr>
                <td align="center">
                    <table cellpadding="5px" dir="ltr">
                        <tr valign="middle">
                            <td class="style1">
                                From:
                                <asp:TextBox ID="txtDateFrom" runat="server" Width="80px" style="margin-right: 0px"></asp:TextBox>
                                <asp:Image ID="Image1" runat="server" ImageUrl="~/images/SmallCalendar.gif" />
                                <cc1:CalendarExtender ID="txtDateFrom_CalendarExtender" runat="server" 
                                    Enabled="True" TargetControlID="txtDateFrom" PopupButtonID="Image1" 
                                    Format="yyyy-MM-dd">
                                </cc1:CalendarExtender>
                                <ew:TimePicker ID="TimePicker1" runat="server" Width="40px" 
                                    NumberOfColumns="4" PopupLocation="Bottom" 
                                    PopupWidth="140px" Scrollable="False" ImageUrl="~/images/timepicker.gif" 
                                                MilitaryTime="True" PopupHeight="180px">
                                    <ButtonStyle BorderStyle="None" Width="25px" />
                                </ew:TimePicker>
                            </td>
                            <td class="style4">
                                To:
                                <asp:TextBox ID="txtDateTo" runat="server" Width="80px" style="margin-right: 4px"></asp:TextBox>
                                <asp:Image ID="Image2" runat="server" ImageUrl="~/images/SmallCalendar.gif" />
                                <cc1:CalendarExtender ID="txtDateTo_CalendarExtender" runat="server" 
                                    Enabled="True" TargetControlID="txtDateTo" PopupButtonID="Image2" Format="yyyy-MM-dd">
                                </cc1:CalendarExtender>
                                <ew:TimePicker ID="TimePicker2" runat="server" Width="40px" 
                                    NumberOfColumns="4" PopupLocation="Bottom" PopupHeight="180px" 
                                    PopupWidth="140px" Scrollable="False" ImageUrl="~/images/timepicker.gif" 
                                                MilitaryTime="True" >
                                    <ButtonStyle BorderStyle="None" Width="25px" />
                                </ew:TimePicker>
                            </td>
                            <td align="center">
                                <asp:Button ID="btnShow" runat="server" Text="Send" onclick="btnShow_Click" />
                            </td>

                            <td align="center">
                                <asp:Button Text="New Choice" runat="server" ID="ToUpper" OnClick="ToUpper_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <asp:MultiView ID="mvCC" runat="server" ActiveViewIndex="0">
        <asp:View runat="server" ID="ViewMenu">
            <div align="center">
                <table>
                    <tr >
                        <td colspan="3" align="center">
                            <asp:CheckBoxList ID="chbCons" runat="server" AutoPostBack="false" 
                                DataTextField="sql_Consumer_name" BackColor="Silver" 
                                DataValueField="sql_Consumer_ID">
                            </asp:CheckBoxList>
                        </td>
                    </tr>
                </table>
            </div>
        </asp:View>

        <asp:View runat="server" ID="ShowData">
            <div align="center">
                <asp:Panel runat="server" ID="PanelChart" HorizontalAlign="Center">
                </asp:Panel>
            </div>
<%--            <div align="center">
                <asp:Button Text="New Choice" runat="server" ID="ToUpper" OnClick="ToUpper_Click" />
            </div>--%>
        </asp:View>
    </asp:MultiView>

<%--    <asp:Menu ID="Users" runat="server" Orientation="Horizontal" BorderColor="Gray" BorderStyle="None" DisappearAfter="2000" 
        MaximumDynamicDisplayLevels="1" StaticDisplayLevels="1" ItemWrap="True" OnMenuItemClick="Users_MenuItemClick">
        <StaticMenuItemStyle BackColor="#f1592a" BorderColor="#f1592a" BorderStyle="Solid" ForeColor="White"></StaticMenuItemStyle>
        <DynamicMenuItemStyle BackColor="Silver" BorderStyle="Solid" BorderColor="Silver" BorderWidth="1" 
            VerticalPadding="5" HorizontalPadding="10" ForeColor="#f1592a"/>
        <DynamicItemTemplate >
            <asp:CheckBox Text='<%# Eval("Text") %>' runat="server" AutoPostBack="false" />
        </DynamicItemTemplate>
    </asp:Menu>--%>
</asp:Content>
    <asp:Content ID="Content2" ContentPlaceHolderID="based" Runat="Server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
    </asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
<%--    <div align="center">
        <table width="640px" style="align-content:center; align-items:center; align-self:center">
            <tr>
                <td align="center">
                    <table cellpadding="5px">
                        <tr valign="middle">
                            <td class="style1">
                                מ-
                                <asp:TextBox ID="txtDateFrom" runat="server" Width="80px" style="margin-right: 0px"></asp:TextBox>
                                <asp:Image ID="Image1" runat="server" ImageUrl="~/images/SmallCalendar.gif" />
                                <cc1:CalendarExtender ID="txtDateFrom_CalendarExtender" runat="server" 
                                    Enabled="True" TargetControlID="txtDateFrom" PopupButtonID="Image1" 
                                    Format="yyyy-MM-dd">
                                </cc1:CalendarExtender>
                                <ew:TimePicker ID="TimePicker1" runat="server" Width="40px" 
                                    MinuteInterval="OneHour" NumberOfColumns="3" PopupLocation="Bottom" 
                                    PopupWidth="120px" Scrollable="False" ImageUrl="~/images/timepicker.gif" 
                                                MilitaryTime="True" >
                                    <ButtonStyle BorderStyle="None" Width="25px" />
                                </ew:TimePicker>
                            </td>
                            <td class="style4">
                                עד-
                                <asp:TextBox ID="txtDateTo" runat="server" Width="80px" style="margin-right: 4px"></asp:TextBox>
                                <asp:Image ID="Image2" runat="server" ImageUrl="~/images/SmallCalendar.gif" />
                                <cc1:CalendarExtender ID="txtDateTo_CalendarExtender" runat="server" 
                                    Enabled="True" TargetControlID="txtDateTo" PopupButtonID="Image2" Format="yyyy-MM-dd">
                                </cc1:CalendarExtender>
                                <ew:TimePicker ID="TimePicker2" runat="server" Width="40px" 
                                    MinuteInterval="OneHour" NumberOfColumns="3" PopupLocation="Bottom" 
                                    PopupWidth="120px" Scrollable="False" ImageUrl="~/images/timepicker.gif" 
                                                MilitaryTime="True" >
                                    <ButtonStyle BorderStyle="None" Width="25px" />
                                </ew:TimePicker>
                            </td>
                        </tr>
                        <tr >
                            <td colspan="3" align="center">
                                <asp:CheckBoxList ID="chbCons" runat="server" AutoPostBack="True" 
                                    DataTextField="sql_Consumer_name" BackColor="Silver" 
                                    DataValueField="sql_Consumer_ID">
                                </asp:CheckBoxList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="3" align="center">
                    <asp:Button ID="btnShow" runat="server" Text="שלח" onclick="btnShow_Click" />
                </td>
            </tr>
        </table>
    </div>--%>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">
<%--    <asp:Panel runat="server" ID="PanelChart">
    </asp:Panel>--%>
</asp:Content>

