﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

public partial class MemberPages_ConsumersList : SecuredPage
{
    //TreeView tv = null;
    //DataClassLoader _dLoader = new DataClassLoader();
    //tbl_Client _client;
    //T_User _user;
    DataClassesDataContext db = new DataClassesDataContext();
    //string userWhere;
    protected void Page_Unload(object sender, EventArgs e)
    {
        MemHist.byArrow = false;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        System.Web.UI.WebControls.Image ti = (System.Web.UI.WebControls.Image)Master.FindControl("TitleImage");
        ti.ImageUrl = "~/SeenergImg/Titles/Title-1.png";

        
        //tv = (TreeView)Master.FindControl("TreeView1");

        if (!MemHist.byArrow)
        {
            MemHist.HistPage = Request.RawUrl;
        }
        //else
        //{
        //    MemHist.byArrow = false;
        //}
        if (!IsPostBack)
        {
            AllFunctions.QtyMenu = 0;
            for (int i = 0; i < 50; i++)
			    {
                    AllFunctions.ConsMenu[i, 0] = "";
                    AllFunctions.ConsMenu[i, 1] = "";
			    }        
            AllFunctions.EnchParam = false;
            Notes.Text = "";
            Shem.Visible = false;
            Notes.Visible = false;
            //_client = Session["client"] as tbl_Client;
            _user = Session["user"] as T_User;

            //if (!_client.sql_role)//not admin
            if (!_user.IsAdmin())//not admin
            {
                //lblName.Text = _client.sql_first_name + " " + _client.sql_last_name;
                //lblName.Text = _user.FirstName + " " + _user.LastName;

                //var x =_user.AllowedClients();
                //LinqConsumers.Where = "sql_clientID == @ClientID";
                //LinqConsumers.WhereParameters.Add("ClientID", TypeCode.Int32, _client.sql_clientID.ToString());
                //LinqConsumers.Where = "_user.AllowedClients().Contains( sql_clientID )";
                //LinqConsumers.WhereParameters.Add("ClientID", TypeCode.Int32, _client.sql_clientID.ToString());

                //DataClassesDataContext db = new DataClassesDataContext();
                //grdConsumers.DataSourceID = "";
                var allowedClients = from client
                                         in DataClassLoader.ClientsAllowedForUser(db, _user.ID)
                                     select new
                                     {
                                         client.ID,
                                         clientName = client.Name
                                     };
                var clientIDs = allowedClients.Select(x => x.ID);
                /*grdConsumers.DataSource = from client
                                              in db.tbl_Consumers_0s.Where(c => clientIDs.Contains(c.sql_clientID))
                                          select new
                                          {
                                              client.sql_Consumer_ID,
                                              client.sql_Consumer_name,
                                              client.sql_Consumer_Model,
                                              client.sql_manufacturer,
                                              client.sql_Consumer_active,
                                              client.IsSwitchedOff,
                                              client = client.T_Client.Name
                                          };*/
                //userWhere = string.Format("sql_clientID == {0}", clientIDs.First());
                string userWhere = "";
                foreach (var cId in clientIDs)
                    userWhere += string.Format("sql_clientID == {0} || ", cId);
                userWhere = userWhere.Remove(userWhere.Length - 4);
                //LinqConsumers.Where = userWhere;
                Session["userWhere"] = userWhere;
                //grdConsumers.Columns[7].Visible = false;
                if (clientIDs.Count() > 0)
                {
                    //lblName.Text = "סינון לפי לקוח";

                    cmbClients.DataSource = allowedClients;
                    cmbClients.DataBind();
                    cmbClients.Items.Insert(0, "בחר שם לקוח");
                    cmbClients.SelectedIndex = 0;
                    //cmbClients.Text = "";
                }
                else
                {
                    //lblName.Text = _user.FirstName + " " + _user.LastName;
                    cmbClients.Visible = false;
                }
            }
            else
            {
                //lblName.Text = "סינון לפי לקוח";

                cmbClients.DataSource = LinqClients;
                cmbClients.DataBind();
                cmbClients.Items.Insert(0, "כל הלקוחות");
            }
        }
        else
        //if(IsPostBack)
        {
            lblError.Visible = false;
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        string ids = "";
        string names = "";
        bool state = false;
        int Qtty = 0;
        int R = -1;

        //AllFunctions.EnchParam = true;

        foreach (GridViewRow row in grdConsumers.Rows)
        {
            AllFunctions.ConsMenu[row.RowIndex, 0] = grdConsumers.DataKeys[row.RowIndex].Value.ToString();
            AllFunctions.ConsMenu[row.RowIndex, 1] = grdConsumers.Rows[row.RowIndex].Cells[2].Text.Trim().Replace("&quot;", "''");
            AllFunctions.ConsMenu[row.RowIndex, 2] = grdConsumers.DataKeys[row.RowIndex].Values[2].ToString();
            CheckBox chk = (CheckBox)row.FindControl("chkSelector");
            if (chk != null && chk.Checked)
            {
                R = row.RowIndex;
                ids += "," + grdConsumers.DataKeys[R].Value.ToString();
                names += ", " + grdConsumers.Rows[R].Cells[2].Text.Trim().Replace("&quot;", "''");
                state = false; //((CheckBox)grdConsumers.Rows[row.RowIndex].Cells[6].Controls[0]).Checked;
                Qtty++;
            }
        }
        string SS = "", SID = "";
        AllFunctions.QtyMenu = grdConsumers.Rows.Count;
        if (ids != "")
        {
            names = names.Substring(1);
            //remove first comma
            ids = ids.Substring(1);
            if (Qtty == 1)
            {
                SS = grdConsumers.DataKeys[R].Values[2].ToString();
                SID = grdConsumers.DataKeys[R].Values[3].ToString();
                AllFunctions.IDforEP = SID;
                ////DataClassesDataContext dc = new DataClassesDataContext();
                ////string CS = dc.Connection.ConnectionString;
                //////string SQL = "SELECT COUNT(*) FROM dbo.T_FactorData WHERE ConsumerID = " + ids;
                ////string SQL = "SELECT dbo.GetConsumerIDforPowerData(" + ids + ")";

                ////using (SqlConnection CN = new SqlConnection(CS))
                ////{
                ////    SqlCommand command = new SqlCommand(SQL, CN);
                ////    try
                ////    {
                ////        CN.Open();
                ////        SS = command.ExecuteScalar().ToString();
                ////        //AllFunctions.ConsID = ids;
                ////        AllFunctions.ConsName = names;
                ////    }
                ////    catch
                ////    {
                ////        SS = "0";
                ////    }
                ////}
                ////if (SS != "0")
                ////{
                ////    SS = "1";
                ////    AllFunctions.EnchParam = true;
                ////    AllFunctions.ConsID = SS;
                ////}
            }
            string SQL = "SELECT SUM(POWER(2, TID)) FROM (SELECT ([TypeFC]) as TID FROM [dbo].[V_ConsumersType] WHERE  [ConsID] IN (" + ids + ") GROUP BY [TypeFC]) AS TB ";
            int II = Convert.ToInt32(AllFunctions.GetScalar(SQL));
            AllFunctions.HorMenuNo2 = (bool)(II == 4);


            Response.Redirect(String.Format("DataChartTaoz.aspx?ConsumersList={0}&Names={1}&State={2}&EPID={3}", ids, names, SS, SID));
        }
        else
            lblError.Visible = true;
    }

    protected void cmbClients_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["Client"] = cmbClients.SelectedItem.Text; 
        DataClassesDataContext dc = new DataClassesDataContext();
        string CS = dc.Connection.ConnectionString;
        string SQD = "EXEC [dbo].[S_GetParametersOfClient] ";

        grdConsumers.Visible = true;
        btnSubmit.Visible = true;
        if (cmbClients.SelectedIndex == 0) //no filter
        {
            if (_user.IsAdmin())
            {
                SQD += " 0";
            }
            else
            {
                SQD += " 0";
            }
        }
        else
        {
                SQD += " " + cmbClients.SelectedValue.ToString();

            string SS = "";
            string SQL = "SELECT ClientNote FROM dbo.T_ClientNotes WHERE ClientID = " + cmbClients.SelectedValue.ToString();
            AllFunctions.IDforGraphDoh = cmbClients.SelectedValue.ToString();
            using (SqlConnection CN = new SqlConnection(CS))
            {
                SqlCommand command = new SqlCommand(SQL, CN);
                try
                {
                    CN.Open();
                    SS = command.ExecuteScalar().ToString();
                }
                catch 
                {
                }
            }
            if (SS != "")
            {
                Notes.Text = SS;
                Shem.Visible = true;
                Notes.Visible = true;
            }
            else
            {
                Shem.Visible = false;
                Notes.Visible = false;

            }
        }




            using (SqlConnection CN = new SqlConnection(CS))
            {

                CN.Open();
                SqlCommand command = new SqlCommand(SQD, CN);
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = command;

                DataTable te = new DataTable("Consumers");
                adapter.Fill(te);

                grdConsumers.DataSource = te;
                CN.Close();

                grdConsumers.DataBind();

        }        
    }

    protected void grdConsumers_SelectedIndexChanged(object sender, EventArgs e)
    {
        int SelRows = 0;
        CheckBox cx = (CheckBox)sender;
        if (!cx.Checked)
        {

            foreach (GridViewRow row in grdConsumers.Rows)
            {
                CheckBox chk = (CheckBox)row.FindControl("chkSelector");
                if (chk != null && chk.Checked)
                {
                    SelRows++;
                }
            }

            if (SelRows == 0)
            {
                for (int i = 0; i < grdConsumers.Rows.Count; i++)
                {
                    grdConsumers.Rows[i].Enabled = true;
                }
            }
        }
        else
        {
            GridViewRow row = (GridViewRow)cx.NamingContainer;
            string nmn = grdConsumers.DataKeys[row.RowIndex].Values[1].ToString();
            //string nmn = row.Cells[8].Text;
            for (int i = 0; i < grdConsumers.Rows.Count; i++)
            {
                grdConsumers.Rows[i].Enabled = (grdConsumers.DataKeys[i].Values[1].ToString() == nmn);
            }

        }
    }
}

