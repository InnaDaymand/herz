﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MemberPages/MemberRu.master" AutoEventWireup="true" CodeFile="ConsumersListRu.aspx.cs" Inherits="MemberPages_ConsumersList" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="based" Runat="Server">
    <div align="center" >
        <table width="420px" dir="ltr">
            <tr>
                <td align="center">
                    <asp:DropDownList ID="cmbClients" runat="server" AutoPostBack="True" 
                        DataTextField="clientName" DataValueField="ID" 
                        onselectedindexchanged="cmbClients_SelectedIndexChanged" Visible="True" 
                        Font-Names="Arial" Font-Size="15pt" ForeColor="Black" style="margin-right: 0px" 
                        Width="360px">
                    </asp:DropDownList>
                    <asp:LinqDataSource ID="LinqClients" runat="server" 
                        ContextTypeName="DataClassesDataContext" 
                        Select="new (ID, Name as clientName)" 
                        TableName="T_Clients">
                    </asp:LinqDataSource>
                </td>
            </tr>
            <tr>
                <td align="center">
                   <asp:GridView ID="grdConsumers" runat="server" AutoGenerateColumns="False" 
                    DataKeyNames="CID,NC,EP,ID" Visible="False" 
                        Width="360px" onselectedindexchanged="grdConsumers_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField HeaderText="Выбор">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkSelector" runat="server" OnCheckedChanged="grdConsumers_SelectedIndexChanged"
                                     AutoPostBack="true" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="CID" HeaderText="Код" Visible="False" />
                            <asp:BoundField DataField="NM" HeaderText="Потребитель" />
                            <asp:BoundField DataField="TP" HeaderText="Тип" />
                            <asp:BoundField DataField="NC" HeaderText="" Visible="false" />
                            <asp:BoundField DataField="EP" HeaderText="" Visible="false" />
                            <asp:BoundField DataField="ID" HeaderText="" Visible="false" />
                        </Columns>
                        
                        <HeaderStyle CssClass="DetailsFormViewHeader"></HeaderStyle>
                        <RowStyle ForeColor="#333333" CssClass="DetailsFormViewRow"></RowStyle>
                        <AlternatingRowStyle></AlternatingRowStyle>
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                <asp:Label ID="lblError" runat="server" ForeColor="Red" Text="Вы должны выбрать потребителя" Visible="False" ></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="center">
                <asp:Button ID="btnSubmit" runat="server" Text="Просмотр данных" 
                        OnClick="btnSubmit_Click" Visible="False" Height="29px" 
                        CssClass="ButtonRes" />
                </td>
            </tr>
            <tr style="height:24" >
                <td>
                </td>
            </tr>
        </table>
        <p dir="ltr" runat="server" style="text-align: left; vertical-align: top">
                 <asp:Label ID="Shem" runat="server" Text="Комментарии:" Font-Names="Arial" 
                        Font-Size="14pt" ForeColor="DarkBlue" Font-Bold="True" 
                        Font-Underline="True"></asp:Label>
       <br style="text-align: right; vertical-align: top" />
                <asp:Label ID="Notes" runat="server" Text="Примечания к составным потребителям" Font-Names="Arial" 
                        Font-Size="14pt" ForeColor="Black" Font-Bold="False"></asp:Label>
         </p>
    </div>  
</asp:Content>
