﻿<%@ Page Language="C#" MasterPageFile="~/MemberPages/MemberSg.master" AutoEventWireup="true" CodeFile="DataChartTaoz.aspx.cs" Inherits="MemberPages_DataChart" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register assembly="EO.Web" namespace="EO.Web" tagprefix="eo" %>

<%@ Register assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>


<%@ Register assembly="eWorld.UI, Version=2.0.6.2393, Culture=neutral, PublicKeyToken=24d65337282035f2" namespace="eWorld.UI" tagprefix="ew" %>




<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style4
        {
            width: 213px;
        }
        .style5
        {
            height: 20px;
        }
        .style6
        {
            height: 80px;
            width: 300px;
        }
     </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="based" Runat="Server">
    <div>
        <table width="100%">
            <tr align="center">
                <td><asp:Label ID="lblConsumer" runat="server" Font-Bold="True" Font-Names="Arial" 
                        Font-Size="16pt" ForeColor="#0066CC" /></td>
            </tr>
            <tr>
                <td height="30" width="100%" align="center" valign="middle">
                <asp:Label ID="lblTotal" runat="server" Font-Bold="True" Font-Names="Arial" 
                        Font-Size="15pt" BorderStyle="Solid" 
                        ForeColor="#0066CC" BorderColor="#0099FF" BorderWidth="1px" 
                        style="margin-bottom: 0px" Width="100%" CssClass="RoundCornerFontBlack" />
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table cellpadding="5px">
                        <tr valign="middle">
                            <td>
                    מ-
                    <asp:TextBox ID="txtDateFrom" runat="server" Width="80px" style="margin-right: 0px"></asp:TextBox>
                    <asp:ImageButton ID="Image1" runat="server" ImageUrl="~/images/SmallCalendar.gif" TabIndex="1" />
                    <cc1:CalendarExtender ID="txtDateFrom_CalendarExtender" runat="server" 
                        Enabled="True" TargetControlID="txtDateFrom" PopupButtonID="Image1" 
                                    Format="yyyy-MM-dd" PopupPosition="Left">
                    </cc1:CalendarExtender>
                    <ew:TimePicker ID="TimePicker1" runat="server" Width="40px" NumberOfColumns="4" PopupLocation="Bottom" 
                        Scrollable="False" ImageUrl="~/images/timepicker.gif" 
                                    MilitaryTime="True" TabIndex="2" PopupHeight="180px" >
                        <ButtonStyle BorderStyle="None" Width="25px" />
                    </ew:TimePicker>
                            </td>
                            <td class="style4">
                    עד-
                    <asp:TextBox ID="txtDateTo" runat="server" Width="80px" style="margin-right: 4px" TabIndex="3"></asp:TextBox>
                    <asp:ImageButton ID="Image2" runat="server" ImageUrl="~/images/SmallCalendar.gif" TabIndex="4" />
                    <cc1:CalendarExtender ID="txtDateTo_CalendarExtender" runat="server" 
                        Enabled="True" TargetControlID="txtDateTo" PopupButtonID="Image2" Format="yyyy-MM-dd">
                    </cc1:CalendarExtender>
                    <ew:TimePicker ID="TimePicker2" runat="server" Width="40px" NumberOfColumns="4" PopupLocation="Bottom" 
                        Scrollable="False" ImageUrl="~/images/timepicker.gif" 
                                    MilitaryTime="True" TabIndex="5" PopupHeight="180px" >
                        <ButtonStyle BorderStyle="None" Width="25px" />
                    </ew:TimePicker>
                            </td>
                            <td>
                                <asp:Button ID="btnShow" runat="server" Text="הצג גרף" OnClick="btnShow_Click" TabIndex="6" />
                            </td>
                            <td>
                            <asp:Button ID="btnPrint" runat="server" Text="הצג דו''ח" OnClick="btnPrint_Click" TabIndex="7"/>
                           </td>      
                           <td>
                               <asp:Button runat="server"  Text="דו''ח חודשי" ID="btnPrintMonthes" OnClick="btnPrintMonthes_Click" TabIndex="8"/>
                           </td>                              
                            <td>
                                <asp:Button Text="הפעלה"  runat="server" ID="SendOn"  Visible="false" OnClick="SendOn_Click" TabIndex="9" />
                            </td>
                            <td>
                                <asp:Button Text="כיבוי" runat="server" ID="SendOff" Visible="false" OnClick="SendOff_Click" TabIndex="10" />
                            </td>
         
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <%--            <tr>
                <td class="style5">
               </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID ="lblSwitchState" runat="server" Visible="False"></asp:Label>
                    <asp:Button ID="btnSwitch" runat="server" Width="50" Text="ניתוק" 
                        onclick="btnSwitch_Click" 
                        OnClientClick="return confirm('לשנות את מצב הצרכן?');" Visible="False" />
                </td>
            </tr>--%>
            <td align="center">
                    <asp:Label ID ="lblDateState" runat="server" ForeColor="Red"></asp:Label>
                    <asp:Label ID ="lblRes" runat="server" Visible="False" >רזולוציה רבע שעה</asp:Label>
            </td>
            </tr>
<%--            <tr>
                <td class="style5">
               </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID ="lblSwitchState" runat="server" Visible="False"></asp:Label>
                    <asp:Button ID="btnSwitch" runat="server" Width="50" Text="ניתוק" 
                        onclick="btnSwitch_Click" 
                        OnClientClick="return confirm('לשנות את מצב הצרכן?');" Visible="False" />
                </td>
            </tr>--%>
        </table>
    </div>
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">
    <div align="center">
        <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
            <asp:View ID="GrapfData" runat="server">
                <cc1:TabContainer ID="TabContainer1" runat="server" BorderStyle="None" 
                    CssClass="MyTabStyle" BorderWidth="1" ActiveTabIndex="0">
                    <cc1:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                    </cc1:TabPanel>
                </cc1:TabContainer>
            </asp:View>

            <asp:View runat="server" ID="PrintData">
                <div align="center">
                   <asp:Panel ID="Panel1" runat="server" Width="950px">
                        <a href="javascript:print()">
                            <asp:Image ID="Image3" ImageUrl="~/PrintPages/NewLog.jpg" runat="server" 
                                BorderStyle="None" Width="940px" Height="120px"/></a>
                        <p align="center" lang="he" style="font-family: Aharoni; font-size: 22px" class="UnprintedItem">
                            <a href="javascript:print()" dir="rtl">הדפס</a>
                        </p>
                    </asp:Panel>
                </div>

                <div align="center">
                <br />
                    <p dir='rtl' lang="he" style="text-align: center">
                        <asp:Label Text="ראטקא" runat="server" ID="lb1" Font-Size="36px" 
                            Font-Bold="True" ForeColor="#0033CC" Font-Names="Calibri" />
                        <br />
                        <br />
                        <asp:Label Text="ראגדה" runat="server" ID="lb2" Font-Size="30px" 
                            Font-Bold="True" ForeColor="#0066FF" Font-Names="Calibri" />
                    </p>
                    <br />
                    <p dir='rtl' lang="he" style="text-align: center">
                        <asp:Label ID="lbDates" runat="server" Text="From Date To Date" 
                            Font-Bold="True" Font-Names="Times New Roman" Font-Size="24px"></asp:Label>
                    </p>
                <br />
                    <div dir='rtl' lang="he" style="text-align: center" align="center">
                    <table id="TaData" align='center' width='450' border='1' 
                        runat="server" dir='rtl' class="NewFont1" style="margin: auto">
                    <tr>
                        <th colspan='2'>סך הכל</th>
                    </tr>
                    <tr>
                        <th>
                            <asp:Label  runat="server" ID="TypData"/>
                        </th>
                        <th>
                            <asp:Label runat="server" ID="Alut0"/>
                        </th>
                    </tr>
                    <tr>
                        <td align='center'>
                            <asp:Label runat="server" ID="V0" />
                        </td>
                        <td align='center'>
                            <asp:Label runat="server" ID="C0" />
                        </td>
                    </tr>
                    <tr>
                        <th colspan='2'>
                            <asp:Label runat="server" ID="Pisga"/>
                        </th>
                   </tr><tr>
                        <th>
                            <asp:Label  runat="server" ID="TypData1"/>
                        </th>
                        <th>
                            <asp:Label runat="server" ID="Alut1"/>
                        </th>
                    </tr><tr>
                        <td align='center'>
                            <asp:Label runat="server" ID="V1" />
                        </td>
                        <td align='center'>
                            <asp:Label runat="server" ID="C1" />
                        </td>
                    </tr>
                    <tr>
                        <th colspan='2'>
                            <asp:Label runat="server" ID="Geva"/>
                        </th>
                    </tr>
                    <tr>
                        <th>
                            <asp:Label  runat="server" ID="TypData2"/>
                        </th>
                        <th>
                            <asp:Label runat="server" ID="Alut2"/>
                        </th>
                    </tr><tr>
                        <td align='center'>
                            <asp:Label runat="server" ID="V2" />
                        </td>
                        <td align='center'>
                            <asp:Label runat="server" ID="C2" />
                        </td>
                    </tr>

                    <tr>
                        <th colspan='2'>
                            <asp:Label runat="server" ID="Shefel"/>
                        </th>
                    </tr><tr>
                        <th>
                            <asp:Label  runat="server" ID="TypData3"/>
                        </th>
                        <th>
                            <asp:Label runat="server" ID="Alut3"/>
                        </th>
                    </tr><tr>
                        <td align='center'>
                            <asp:Label runat="server" ID="V3" />
                        </td>
                        <td align='center'>
                            <asp:Label runat="server" ID="C3" />
                        </td>
                    </tr>
                </table>
                </div>        
                </div>
            </asp:View>

            <asp:View runat="server" ID="MonthData">
                <div align="center">
                   <asp:Panel ID="Panel5" runat="server" Width="950px" >
                        <a href="javascript:print()">
                            <asp:Image ID="Image4" ImageUrl="~/PrintPages/NewLog.jpg" 
                                runat="server" BorderStyle="None" Width="940px" Height="120px"/></a>
                        <p align="center" lang="he" style="font-family: Aharoni; font-size: 22px" class="UnprintedItem">
                            <a href="javascript:print()" dir="rtl">הדפס</a>
                        </p>
                    </asp:Panel>
                    <p dir='rtl' lang="he" style="text-align: center">
                        <asp:Label Text="ראטקא" runat="server" ID="lb11" Font-Size="36px" 
                            Font-Bold="True" ForeColor="#0033CC" Font-Names="Calibri" />
                        <br />
                        <asp:Label Text="ראגדה" runat="server" ID="lb22" Font-Size="30px" 
                            Font-Bold="True" ForeColor="#0066FF" Font-Names="Calibri" />
                    </p>
                </div>
                <div>
                    <table id="ChTab" class="UnprintedItem" dir="rtl" style="align-items:center; align-content:center; align-self:center;">
                        <tr>
                            <th colspan="6" style="width:100%;"></th>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label Text="מחודש" runat="server" />
                            </td>
                            <td>
                                <asp:DropDownList runat="server" ID="fromMM">
                                    <asp:ListItem Text="ינואר" Value="1" />
                                    <asp:ListItem Text="פברואר" Value="2" />
                                    <asp:ListItem Text="מרץ" Value="3" />
                                    <asp:ListItem Text="אפריל" Value="4" />
                                    <asp:ListItem Text="מאי" Value="5" />
                                    <asp:ListItem Text="יוני" Value="6" />
                                    <asp:ListItem Text="יולי" Value="7" />
                                    <asp:ListItem Text="אוגוסט" Value="8" />
                                    <asp:ListItem Text="ספטמבר" Value="9" />
                                    <asp:ListItem Text="אוקטובר" Value="10" />
                                    <asp:ListItem Text="נובמבר" Value="11" />
                                    <asp:ListItem Text="דצמבר" Value="12" />
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:DropDownList runat="server" ID="fromYY">
                                    <asp:ListItem Text="2012" />
                                    <asp:ListItem Text="2013" />
                                    <asp:ListItem Text="2014" />
                                    <asp:ListItem Text="2015" />
                                    <asp:ListItem Text="2016" />
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:Label ID="Label2" Text="לחודש" runat="server" />
                            </td>
                            <td>
                                <asp:DropDownList runat="server" ID="tillMM">
                                    <asp:ListItem Text="ינואר" Value="1" />
                                    <asp:ListItem Text="פברואר" Value="2" />
                                    <asp:ListItem Text="מרץ" Value="3" />
                                    <asp:ListItem Text="אפריל" Value="4" />
                                    <asp:ListItem Text="מאי" Value="5" />
                                    <asp:ListItem Text="יוני" Value="6" />
                                    <asp:ListItem Text="יולי" Value="7" />
                                    <asp:ListItem Text="אוגוסט" Value="8" />
                                    <asp:ListItem Text="ספטמבר" Value="9" />
                                    <asp:ListItem Text="אוקטובר" Value="10" />
                                    <asp:ListItem Text="נובמבר" Value="11" />
                                    <asp:ListItem Text="דצמבר" Value="12" />
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:DropDownList runat="server" ID="tillYY">
                                    <asp:ListItem Text="2012" />
                                    <asp:ListItem Text="2013" />
                                    <asp:ListItem Text="2014" />
                                    <asp:ListItem Text="2015" />
                                    <asp:ListItem Text="2016" />
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </div>
                <br />
                <asp:GridView ID="PreviewMonthes" runat="server" BackColor="#CCCCCC" OnPageIndexChanging="PreviewMonthes_PageIndexChanging"
                BorderColor="#999999" BorderStyle="Solid" BorderWidth="3px" CellPadding="6" 
                CellSpacing="2" ForeColor="Black" Font-Names="Courier New" Font-Size="20px" Font-Bold="true" 
                HorizontalAlign="Center" Width="100%" AllowPaging="True" PageSize="24" >
                    <FooterStyle BackColor="#CCCCCC" />
                    <HeaderStyle BackColor="Silver" Font-Bold="True" ForeColor="Black" Font-Size="X-Large" />
                    <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                    <RowStyle BackColor="White" />
                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle BackColor="#808080" />
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle BackColor="#383838" />
                </asp:GridView>
                <p class="UnprintedItem">
                    <asp:ImageButton ImageUrl="~/images/ShowMe.png" runat="server" 
                        ID="MonPrint" onclick="MonPrint_Click" />
                </p>
            </asp:View>
         </asp:MultiView>
      <table border="0" cellpadding="0" cellspacing="0" width="940px" dir="rtl" id="LowerTab">
            <tr id="LowerRow">
                <td style="width: 313px;">
                    <asp:Panel ID="Panel2" runat="server" Width="100%" BorderColor="#0066CC" BorderStyle="Solid" BorderWidth="1" HorizontalAlign="Center">

                        <asp:Label Text="text" runat="server" ID="RUP" BackColor="#00ADEF" 
                            ForeColor="#BDEBFA" Width= "100%" Font-Names="Arial" Font-Size="14pt" 
                            Font-Underline="True" />
                        <asp:Label Text="text" runat="server"  ID="RU1" Width= "160px" Font-Names="Arial" Font-Size="14"/>
                        <asp:Label Text="text" runat="server"  ID="RU2" Width= "160px" Font-Names="Arial" Font-Size="14"/>
                        <asp:Label Text="text" runat="server"  ID="RU3" Width= "160px" Font-Names="Arial" Font-Size="14"/>
                    </asp:Panel>
                </td>
                <td style="width: 314px;">
                    <asp:Panel ID="Panel3" runat="server" Width="100%" BorderColor="#0066CC" BorderStyle="Solid" BorderWidth="1" HorizontalAlign="Center">

                        <asp:Label runat="server" ID="MUP" BackColor="#00ADEF" 
                            ForeColor="#BDEBFA" Width= "100%" Font-Names="Arial" Font-Size="14pt" 
                            Font-Underline="True"  />
                        <asp:Label Text="" runat="server" Width="100%" ID="MU1" Font-Names="Arial" Font-Size="14pt" />
                        <asp:Label Text="" runat="server" Width="100%" ID="MU2" Font-Names="Arial" Font-Size="14pt" />
                        <asp:Label Text="" runat="server" Width="100%" ID="MU3" Font-Names="Arial" Font-Size="14pt" />
                   </asp:Panel>
                </td>
                <td style="width: 313px;">
                    <asp:Panel ID="Panel4" runat="server" Width="100%" BorderColor="#0066CC" BorderStyle="Solid" BorderWidth="1" HorizontalAlign="Center">
                        <asp:Label runat="server"  ID="LUP" BackColor="#00ADEF" ForeColor="#BDEBFA" 
                            Width= "100%" Font-Names="Arial" Font-Size="14pt" Font-Underline="True" />
                        <asp:Label Text="" runat="server"  ID="LU1" Width= "160px" Font-Names="Arial" Font-Size="14"/>
                        <asp:Label Text="" runat="server"  ID="LU2" Width= "160px" Font-Names="Arial" Font-Size="14"/>
                        <asp:Label Text="" runat="server"  ID="LU3" Width= "160px" Font-Names="Arial" Font-Size="14"/>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
    <script type="text/javascript">
        function OpenNew() 
        {
            window.showModalDialog('../PrintPages/PrintPeriod.aspx', '', "dialogWidth=950px, dialogHeight=100%");
        }
    </script>
    <script type="text/javascript">
        function OpenMonthes() 
        {
            window.showModalDialog('../PrintPages/PrintMonthes.aspx', '', "dialogWidth=950px, dialogHeight=100%");
        }
    </script>
</asp:Content>
