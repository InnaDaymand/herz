﻿using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MemberPages_DataChart : SecuredPage
{
    DateTime _from, _to;
    string _consumerIdList;

    GraphBuilder _gBuilder = new GraphBuilder();

    protected void Page_Load(object sender, EventArgs e)
    {
        _consumerIdList = Request.QueryString["ConsumersList"];
        string SQL = "", SSS = "";

        SQL = "SELECT TCL.Name FROM tbl_Consumers_0 AS TCR INNER JOIN "
            + "T_Client AS TCL ON TCR.sql_clientID = TCL.ID WHERE sql_Consumer_ID IN (" + _consumerIdList + ")";
        lb1.Text = lb11.Text = AllFunctions.GetScalar(SQL);
        btnPrintMonthes.Enabled = (bool)(_consumerIdList.IndexOf(",") < 0);


        #region Prepare SMS buttons
        if (_consumerIdList.IndexOf(",") < 0)
        {
            SQL = "SELECT COUNT(*) FROM [dbo].[T_OnOffConsumer] WHERE [ConsID] IN (" + _consumerIdList + ")";
            SSS = AllFunctions.GetScalar(SQL);
            if (SSS == "1")
            {
                SendOff.Visible = true;
                SendOn.Visible = true;

               SQL = "SELECT [CurrStatus] FROM [dbo].[T_OnOffConsumer] WHERE [ConsID] IN (" + _consumerIdList + ")";
               SSS = AllFunctions.GetScalar(SQL);

                switch (SSS)
                {
                    case "0":
                        SendOff.Enabled = false;
                        SendOn.Enabled = true;
                        break;
                    case "1":
                        SendOn.Enabled = false;
                        SendOff.Enabled = true;
                        break;
                    default:
                        break;
                }
            }
        }
        #endregion
        
        SQL = "SELECT TOP 1 ConsumerType FROM tbl_Consumers_0 WHERE sql_Consumer_ID IN (" + _consumerIdList + ")";
        AllFunctions.ConsumerType = SSS = AllFunctions.GetScalar(SQL);
        System.Web.UI.WebControls.Image ti = (System.Web.UI.WebControls.Image)Master.FindControl("TitleImage");

        if (SSS == "0")
        {
            ti.ImageUrl = "~/SeenergImg/Titles/Title-3.png";
        }
        if (SSS == "3")
        {
            ti.ImageUrl = "~/SeenergImg/Titles/Title-3E.png";
        }
        if (SSS == "7")
        {
            ti.ImageUrl = "~/SeenergImg/Titles/Air.png";
            btnPrint.Visible = false;
            btnPrintMonthes.Visible = false;
        }
        if (SSS == "8")
        {
            ti.ImageUrl = "~/SeenergImg/Titles/Steam.png";
            btnPrint.Visible = false;
            btnPrintMonthes.Visible = false;
        }
        Control cause = null;

        _from = Convert.ToDateTime(Session["DateFrom"]);
        _to = Convert.ToDateTime(Session["DateTill"]);

        lb2.Text = lb22.Text = lblConsumer.Text = Request.QueryString["Names"];
        AllFunctions.IDforEP = Request.QueryString["EPID"];

        AllFunctions.ConsID = _consumerIdList;
        AllFunctions.ConsName = lblConsumer.Text;

        #region (Not In USE) For State
        //string S1 = Request.QueryString["State"];
        //if (S1 == "1")
        //{
        //    Menu mn = (Menu) Master.FindControl("HorMenu");
        //    if (mn != null)
        //    {
        //        MenuItem mi = mn.Items[1];
        //        mi.Selectable = true;
        //        AllFunctions.EnchParam = true;
        //    }
        //}
        //else
        //{
        //    Menu mn = (Menu) Master.FindControl("HorMenu");
        //    if (mn != null)
        //    {
        //        MenuItem mi = mn.Items[1];
        //        mi.Selectable = false;
        //        AllFunctions.EnchParam = false;
        //    }
        //}
        #endregion

        if (!MemHist.byArrow)
        {
            MemHist.HistPage = Request.RawUrl;
        }
        else
        {
            MemHist.byArrow = false;
        }
        if (IsPostBack)
        {
            DateTime dt0 = DateTime.Parse(txtDateFrom.Text).Date + TimePicker1.SelectedTime.TimeOfDay;
            DateTime dt1 = DateTime.Parse(txtDateTo.Text).Date + TimePicker2.SelectedTime.TimeOfDay;
            if ((_from != dt0) || (_to != dt1))
            {
                Session["DateFrom"] = dt0.ToString("yyyy-MM-dd HH:mm:ss");
                Session["DateTill"] = dt1.ToString("yyyy-MM-dd HH:mm:ss");

                _from = Convert.ToDateTime(Session["DateFrom"]);
                _to = Convert.ToDateTime(Session["DateTill"]);
            }

            //_from = DateTime.Parse(txtDateFrom.Text).Date + TimePicker1.SelectedTime.TimeOfDay;
            //_to = DateTime.Parse(txtDateTo.Text).Date + TimePicker2.SelectedTime.TimeOfDay;
            if (_from > _to)
            {
                lblTotal.Visible = false;
                lblDateState.Text = "תאריך התחלה גדולה מתאריך סיום!";
            }
            else
            {
                lblTotal.Visible = true;
            }
            cause = GetPostBackControl( Page);
        }
        else
        {
            TabContainer1.ActiveTabIndex = 0;
            MultiView1.ActiveViewIndex = 0;
        }

        txtDateFrom_CalendarExtender.SelectedDate = _from;
        txtDateTo_CalendarExtender.SelectedDate = _to;
        TimePicker1.SelectedTime = _from;
        TimePicker2.SelectedTime = _to;

        #region (Not In USE) Counter Gap (To show elements that were not worked) 

        /*
            DataClassesDataContext dc = new DataClassesDataContext();
            string CS = dc.Connection.ConnectionString;
            string SQL = "EXEC dbo.S_CompoundCounterGap N'" + _from.ToString() + "', '" + _to.ToString() + "', '" + _consumerIdList + "'";

            using (SqlConnection CN = new SqlConnection(CS))
            {
                SqlCommand command = new SqlCommand(SQL, CN);
                try
                {
                    CN.Open();
                    SS = command.ExecuteScalar().ToString();
                }
                catch (Exception ex)
                {
                    //System.Windows.Forms.MessageBox.Show(ex.Message);
                    //lblWarning.ForeColor = System.Drawing.Color.Red;
                    //lblWarning.Text = "בעיה בשרת SQL";
                    //return;
                }
                if (SS != "")
                {
                    Session["popCons"] = SS;
                    Response.Write("<SCRIPT>window.open('popConsumers.aspx', 'form1', 'scrollbars=no, toolbar=no, location=no, directories=no, status=no, menubar=no, width=400, height=200', true)</SCRIPT>");
                    //btnSwitch.Text = "בחר שוב";
                    ////txtDateTo.Text = Convert.ToDateTime(SS).ToShortDateString();
                    ////TimePicker2.SelectedTime = Convert.ToDateTime(SS);
                    ////return;
                }
            }
            //Response.Write("<SCRIPT>window.open('popConsumers.aspx', false)</SCRIPT>"); 
        */
        #endregion

        #region (Not In USE) Check "תנור כשת" (Temporarily from 10/09/2013)

        //if ((_consumerIdList == "30") || (_consumerIdList == "64"))
        //{
        //    if (_from <= DateTime.Parse("19/08/13 17:00:00"))
        //    {
        //        lblDateState.Visible = true;
        //        lblDateState.Text = "ליפני 19/08/2013 נתונים תנור קשת+תנור דלי לא כוללים תנור דלי!";
        //    }
        //}
        #endregion

        #region (Not In USE) Interval & Switch
        //IntervalType interval;
        //switch (_gBuilder.Resol)
        //{
        //    case "רזולוציה יממה":
        //        interval = IntervalType.Days;
        //        break;
        //    case "רזולוציה שבוע":
        //        interval = IntervalType.Weeks;
        //        break;
        //    default:
        //        interval = IntervalType.Hours;
        //        break;
        //}

        /*
                foreach (AjaxControlToolkit.TabPanel panel in TabContainer1.Tabs)
                {
                    System.Web.UI.DataVisualization.Charting.Chart chart = panel.Controls[0] as System.Web.UI.DataVisualization.Charting.Chart;
                }

                if (!NotShow)
                {
                    if (_consumerIdList.Contains(','))
                        btnSwitch.Enabled = false;
                    else
                    {
                        if (_permission == 1)
                            btnSwitch.Enabled = false;

                        if (Request.QueryString["State"].ToLower() == "true")
                        {
                            lblSwitchState.Text = "הצרכן מנותק";
                            lblSwitchState.ForeColor = Color.Red;
                            btnSwitch.Text = "חיבור";
                        }
                        else
                        {
                            lblSwitchState.Text = "הצרכן מחובר";
                            lblSwitchState.ForeColor = Color.Green;
                            //btnSwitch.Text = "חיבור";
                        }
                    }
                }
        */
        #endregion
        //PrepareReport();
        ShowGraph();
    }

    protected void btnSwitch_Click(object sender, EventArgs e)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        tbl_Consumers_0 consumer = db.tbl_Consumers_0s.First(c => c.sql_Consumer_ID == int.Parse(_consumerIdList));
        //consumer.IsSwitchedOff = !consumer.IsSwitchedOff; //when NULL this does not work
        consumer.IsSwitchedOff = !(bool.Parse(Request.QueryString["State"]));
        db.SubmitChanges();
        Response.Redirect("ConsumersList.aspx");
    }

    public static Control GetPostBackControl( System.Web.UI.Page page)
    {
        Control postbackControlInstance = null;

        string postbackControlName = page.Request.Params.Get("__EVENTTARGET");
        if (postbackControlName != null && postbackControlName != string.Empty)
        {
            postbackControlInstance = page.FindControl(postbackControlName);
        }
        else
        {
            // handle the Button control postbacks
            for (int i = 0; i < page.Request.Form.Keys.Count; i++)
            {
                postbackControlInstance = page.FindControl(page.Request.Form.Keys[i]);
                if (postbackControlInstance is System.Web.UI.WebControls.Button)
                {
                    return postbackControlInstance;
                }
            }
        }

        // handle the ImageButton postbacks
        if (postbackControlInstance == null)
        {
            for (int i = 0; i < page.Request.Form.Count; i++)
            {
                if ((page.Request.Form.Keys[i].EndsWith(".x")) || (page.Request.Form.Keys[i].EndsWith(".y")))
                {
                    postbackControlInstance = page.FindControl(page.Request.Form.Keys[i].Substring(0, page.Request.Form.Keys[i].Length - 2));
                    return postbackControlInstance;
                }
            }
        }
        return postbackControlInstance;
    }

    private void PrepareReport()
    {
        string SData = "מ-" + _from.ToString("dd/MM/yyyy") + " עד-" + _to.ToString("dd/MM/yyyy");

        DataTable tv = _gBuilder.TableVal;
        DataTable tc = _gBuilder.TableCos;
        //string SCont = "";
        string SID = TabContainer1.Tabs[0].ID;
        string SIT = "";
        if (SID == "003")
        {
            SIT = "מ``ק/שעה";
        }
        else
            SIT = "קוט``ש";
        string SN = "עלות בש''ח (ללא מע''מ)";

        string S0 = SIT;
        TypData.Text = S0;
        Alut0.Text = SN;
        lbDates.Text = SData;
        V0.Text = (tv.Rows[0][3]).ToString();
        C0.Text = (tc.Rows[0][3]).ToString();

        if (SID == "002")
        {
            TypData1.Text = S0;
            TypData2.Text = S0;
            TypData3.Text = S0;
            Alut1.Text = SN;
            Alut2.Text = SN;
            Alut3.Text = SN;
            Pisga.Text = "פסגה";
            Geva.Text = "גבע";
            Shefel.Text = "שפל";
            V1.Text = (tv.Rows[0][0]).ToString();
            V2.Text = (tv.Rows[0][1]).ToString();
            V3.Text = (tv.Rows[0][2]).ToString();
            C1.Text = (tc.Rows[0][0]).ToString();
            C2.Text = (tc.Rows[0][1]).ToString();
            C3.Text = (tc.Rows[0][2]).ToString();
        }
        else
        {
            TypData1.Text = "";
            TypData2.Text = "";
            TypData3.Text = "";
            Alut1.Text = "";
            Alut2.Text = "";
            Alut3.Text = "";
            Pisga.Text = "";
            Geva.Text = "";
            Shefel.Text = "";
            V1.Text = "";
            V2.Text = "";
            V3.Text = "";
            C1.Text = "";
            C2.Text = "";
            C3.Text = "";
        }

/*
        if (SID == "002")
        {
            AllFunctions.ForPrint[0] = SID;
            AllFunctions.ForPrint[1] = SIT;
            AllFunctions.ForPrint[2] = SS;
            AllFunctions.ForPrint[3] = lblConsumer.Text;
            AllFunctions.ForPrint[4] = SData;
            AllFunctions.ForPrint[5] = (tv.Rows[0][3]).ToString();
            AllFunctions.ForPrint[6] = (tc.Rows[0][3]).ToString();
            AllFunctions.ForPrint[7] = (tv.Rows[0][0]).ToString();
            AllFunctions.ForPrint[8] = (tc.Rows[0][0]).ToString();
            AllFunctions.ForPrint[9] = (tv.Rows[0][1]).ToString();
            AllFunctions.ForPrint[10] = (tc.Rows[0][1]).ToString();
            AllFunctions.ForPrint[11] = (tv.Rows[0][2]).ToString();
            AllFunctions.ForPrint[12] = (tc.Rows[0][2]).ToString();
        }
        else
        {
            //SCont = ", '', '', '', '', '', '";
            AllFunctions.ForPrint[0] = SID;
            AllFunctions.ForPrint[1] = SIT;
            AllFunctions.ForPrint[2] = SS;
            AllFunctions.ForPrint[3] = lblConsumer.Text;
            AllFunctions.ForPrint[4] = SData;
            AllFunctions.ForPrint[5] = (tv.Rows[0][0]).ToString();
            AllFunctions.ForPrint[6] = (tc.Rows[0][0]).ToString();
            AllFunctions.ForPrint[7] = "";
            AllFunctions.ForPrint[8] = "";
            AllFunctions.ForPrint[9] = "";
            AllFunctions.ForPrint[10] = "";
            AllFunctions.ForPrint[11] = "";
            AllFunctions.ForPrint[12] = "";
        }
 */

    }

    protected void SendSMS_Click(object sender, EventArgs e)
    {

    }

    protected void btnShow_Click(object sender, EventArgs e)
    {
        //Page_Load(this, e);
        //ShowGraph();
        MultiView1.ActiveViewIndex = 0;
    }

    protected void btnPrint_Click(object sender, EventArgs e)
    {
        PrepareReport();
        MultiView1.ActiveViewIndex = 1;
    }

    protected void btnPrintMonthes_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 2;
    }

    protected void MonPrint_Click(object sender, ImageClickEventArgs e)
    {
        string SQL = "";
        SQL = "EXEC [dbo].[S_prepareToPrintMonthes] @StartMonth = " + fromMM.SelectedValue.ToString() + ", "
            + "@StartYear = " + fromYY.SelectedValue.ToString() + ", "
            + "@FinMonth = " + tillMM.SelectedValue.ToString() + ", "
            + "@FinYear = " + tillYY.SelectedValue.ToString() + ", "
            + "@Consumers = N'" + AllFunctions.ConsID + "'";

        PreviewMonthes.DataSource = AllFunctions.Populate(SQL);
        PreviewMonthes.DataBind();
    }

    protected void PreviewMonthes_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        PreviewMonthes.PageIndex = e.NewPageIndex;
        ImageClickEventArgs ie = new ImageClickEventArgs(0, 0);
        MonPrint_Click(this, ie);
    }

    private void ShowGraph()
    {
        TabContainer1.Tabs.Clear();
        string SS = "", SQL = "";
        //bool NotShow = false;

        #region Check the definition of <Consumer start date>
        /*
         * Check the the definition of <Consumer start date>:
         * EXEC dbo.S_CompoundCounterGap @StartDate, @EndDate, @ConsID
        */

        SQL = "EXEC [dbo].[S_CompareDate] N'" + _from.ToString("yyyy/MM/dd HH:mm:ss") + "', '" + _consumerIdList + "'";
        SS = AllFunctions.GetScalar(SQL);
        if ((SS != "OK") || (!lblTotal.Visible))
        {
            if (SS != "OK") lblDateState.Text = "אין נתונים לפני תאריך  " + SS;
            //NotShow = true;
            TabContainer1.Visible = false;
            lblTotal.Visible = false;
        }
        else
        {
            TabContainer1.Visible = true;
            //NotShow = false;
            lblDateState.Text = "";
            lblTotal.Visible = true;
        }

        #endregion

        _gBuilder.ListIDs = _consumerIdList;
        _gBuilder.sResol = (string)Session["Resolution"];
        _gBuilder.DateFrom = _from;
        _gBuilder.DateUntil = _to;

        _gBuilder.GetCounters(_consumerIdList);
        _gBuilder.GetTemperConsumers(_consumerIdList);
        _gBuilder.GetTemperatureData(_from, _to);
        _gBuilder.Build();

        if (AllFunctions.ConsumerType == "3")
        {
            lblTotal.Text = "עלות היצור הכוללת של היצרנים לתקופה:" + String.Format("{0:0.##} ש\"ח", _gBuilder.OverallCost);
        }
        if (AllFunctions.ConsumerType == "0")
        {
            lblTotal.Text = "עלות הצריכה הכוללת של הצרכנים לתקופה:" + String.Format("{0:0.##} ש\"ח", _gBuilder.OverallCost);
        }

        lblRes.Text = "";

        try
        {
            foreach (var chart in _gBuilder.Charts)
            {
                chart.Width = 940;

                AjaxControlToolkit.TabPanel tab = new AjaxControlToolkit.TabPanel();
                tab.HeaderText = chart.Titles[0].Text;
                TabContainer1.Tabs.Add(tab);
                tab.ID = chart.ID;
                tab.BorderWidth = 1;
                tab.BorderStyle = System.Web.UI.WebControls.BorderStyle.Solid;
                tab.Controls.Add(chart);

                if ((_gBuilder.LowerTable != null) && (_gBuilder.LowerTable.Rows.Count > 1))
                {
                    System.Data.DataTable dd = _gBuilder.LowerTable;
                    Panel2.Visible = true;
                    Panel3.Visible = true;
                    Panel4.Visible = true;

                    RUP.Text = dd.Rows[0][0].ToString();
                    RU1.Text = dd.Rows[1][0].ToString();
                    RU2.Text = dd.Rows[2][0].ToString();
                    RU3.Text = dd.Rows[3][0].ToString();

                    MUP.Text = dd.Rows[0][1].ToString();
                    MU1.Text = dd.Rows[1][1].ToString();
                    MU2.Text = dd.Rows[2][1].ToString();
                    MU3.Text = dd.Rows[3][1].ToString();

                    LUP.Text = dd.Rows[0][2].ToString();
                    LU1.Text = dd.Rows[1][2].ToString();
                    LU2.Text = dd.Rows[2][2].ToString();
                    LU3.Text = dd.Rows[3][2].ToString();
                }
                else
                {

                    Panel2.Visible = false;
                    Panel3.Visible = false;
                    Panel4.Visible = false;
                }
            }
        }
        catch (Exception ex)
        {

            System.Windows.Forms.MessageBox.Show(ex.Message);
        }

    }

    protected void SendOn_Click(object sender, EventArgs e)
    {
        string SQL = "SELECT TelNoOn FROM [dbo].[T_OnOffConsumer] WHERE [ConsID] IN (" + _consumerIdList + ")";
        string sL = AllFunctions.GetScalar(SQL);
        SQL = "SELECT TextOn FROM [dbo].[T_OnOffConsumer] WHERE [ConsID] IN (" + _consumerIdList + ")";
        string sT = AllFunctions.GetScalar(SQL);

        if (PrepareSMS(sL, sT))
        {
            SQL = "UPDATE [dbo].[T_OnOffConsumer] SET [CurrStatus] = 1, [LastRequest] = GETDATE() WHERE [ConsID] IN (" + _consumerIdList + ")";
            AllFunctions.DoExec(SQL);
            SendOff.Enabled = true;
            SendOn.Enabled = false;
        }
        TabContainer1.Focus();
    }

    protected void SendOff_Click(object sender, EventArgs e)
    {
        string SQL = "SELECT TelNoOff FROM [dbo].[T_OnOffConsumer] WHERE [ConsID] IN (" + _consumerIdList + ")";
        string sL = AllFunctions.GetScalar(SQL);
        SQL = "SELECT TextOff FROM [dbo].[T_OnOffConsumer] WHERE [ConsID] IN (" + _consumerIdList + ")";
        string sT = AllFunctions.GetScalar(SQL);

        if (PrepareSMS(sL, sT))
        {
            SQL = "UPDATE [dbo].[T_OnOffConsumer] SET [CurrStatus] = 0, [LastRequest] = GETDATE() WHERE [ConsID] IN (" + _consumerIdList + ")";
            AllFunctions.DoExec(SQL);
            SendOff.Enabled = false;
            SendOn.Enabled = true;
        }
        TabContainer1.Focus();
    }

    private bool PrepareSMS(string Telefon, string Poslanie)
    {
        //set password, user name, message text, semder name and number
        string userName = "hazat47";
        string password = "9977572";
        string messageText = Poslanie;
        string senderName = "MD2000";
        string senderNumber = "0000000";
        //set phone numbers
        string phonesList = Telefon;
        //set additional parameters
        string timeToSend = DateTime.Now.AddMinutes(10).ToShortDateString();
        // create XML
        StringBuilder sbXml = new StringBuilder();
        sbXml.Append("<Inforu>");
        sbXml.Append("<User>");
        sbXml.Append("<Username>" + userName + "</Username>");
        sbXml.Append("<Password>" + password + "</Password>");
        sbXml.Append("</User>");
        sbXml.Append("<Content Type=\"sms\">");
        sbXml.Append("<Message>" + messageText + "</Message>");
        sbXml.Append("</Content>");
        sbXml.Append("<Recipients>");
        sbXml.Append("<PhoneNumber>" + phonesList + "</PhoneNumber>");
        sbXml.Append("</Recipients>");
        sbXml.Append("<Settings>");
        sbXml.Append("<SenderName>" + senderName + "</SenderName>");
        sbXml.Append("<SenderNumber>" + senderNumber + "</SenderNumber>");
        sbXml.Append("<MessageInterval>0</MessageInterval>");
        sbXml.Append("<TimeToSend>" + timeToSend + "</TimeToSend>");
        sbXml.Append("</Settings>");
        sbXml.Append("</Inforu >");
        string strXML = HttpUtility.UrlEncode(sbXml.ToString(), System.Text.Encoding.UTF8);
        string result = PostDataToURL("http://api.inforu.co.il/SendMessageXml.ashx", "InforuXML=" + strXML);
        return true;
    }

    static string PostDataToURL(string szUrl, string szData)
    {
        //Setup the web request
        string szResult = string.Empty;
        WebRequest Request = WebRequest.Create(szUrl);
        Request.Timeout = 30000;
        Request.Method = "POST";
        Request.ContentType = "application/x-www-form-urlencoded";
        //Set the POST data in a buffer
        byte[] PostBuffer;
        try
        {
            // replacing " " with "+" according to Http post RPC
            szData = szData.Replace(" ", "+");
            //Specify the length of the buffer
            PostBuffer = Encoding.UTF8.GetBytes(szData);
            Request.ContentLength = PostBuffer.Length;
            //Open up a request stream
            Stream RequestStream = Request.GetRequestStream();
            //Write the POST data
            RequestStream.Write(PostBuffer, 0, PostBuffer.Length);
            //Close the stream
            RequestStream.Close();
            //Create the Response object
            WebResponse Response;
            Response = Request.GetResponse();
            //Create the reader for the response
            StreamReader sr = new StreamReader(Response.GetResponseStream(), Encoding.UTF8);
            //Read the response
            szResult = sr.ReadToEnd();
            //Close the reader, and response  \\inetpub\\vhosts\\md2000.com\\vault_scripts\\
            sr.Close();
            Response.Close();

            return szResult;
        }
        catch (Exception e)
        {
            string lines = "Catch at " + DateTime.Now.ToLongDateString() + " \nwith message: " + e.Message;
            //System.IO.StreamWriter file = new System.IO.StreamWriter("C:\\SendingSMS\\SendingSMS\\bin\\Release\\SendingSMS.txt", true);
            //file.WriteLine(lines);
            //file.Close();

            return szResult;
        }
        
    }

    protected void Dtp_SelectionChanged(object sender, EventArgs e)
    {

    }
}
