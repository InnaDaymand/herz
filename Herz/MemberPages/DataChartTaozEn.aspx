﻿<%@ Page Language="C#" MasterPageFile="~/MemberPages/MemberEn.master" AutoEventWireup="true" CodeFile="DataChartTaozEn.aspx.cs" Inherits="MemberPages_DataChartEn" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>

<%@ Register assembly="eWorld.UI, Version=2.0.6.2393, Culture=neutral, PublicKeyToken=24d65337282035f2" namespace="eWorld.UI" tagprefix="ew" %>

<%@ Register Src="~/Controls/CalendarEN.ascx" TagName="CalendarEN" TagPrefix="cen" %>





<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style4
        {
            width: 213px;
        }
        .style5
        {
            height: 20px;
        }
        .style6
        {
            height: 80px;
            width: 300px;
        }
        </style>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="based" Runat="Server">
    <div>
        <table width="800px">
            <tr align="center">
                <td><asp:Label ID="lblConsumer" runat="server" Font-Bold="True" Font-Names="Arial" 
                        Font-Size="16pt" ForeColor="#0066CC" /></td>
            </tr>
            <tr>
                <td height="30" width="100%" align="center" valign="middle">
                <asp:Label ID="lblTotal" runat="server" Font-Bold="True" Font-Names="Arial" 
                        Font-Size="15pt" BorderStyle="Solid" 
                        ForeColor="#0066CC" BorderColor="#0099FF" BorderWidth="1px" 
                        style="margin-bottom: 0px" Width="100%" CssClass="RoundCornerFontBlack" />
                </td>
            </tr>
            <tr>
                <td align="center" dir="ltr">
                    <cen:CalendarEN runat="server" ID="Dates" OnClickTable="Dates_ClickTable" OnClickGraph="Dates_ClickGraph" />

                </td>
            </tr>
            <tr>
            <td align="center" class="style5">
                    <asp:Label ID ="lblDateState" runat="server" ForeColor="Red"></asp:Label>
<%--                    <asp:Label ID ="lblRes" runat="server" Visible="False" >רזולוציה רבע שעה</asp:Label>--%>
            </td>
            </tr>
        </table>
    </div>
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</ajaxToolkit:ToolkitScriptManager>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">
    <div align="center">
        <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex ="0">
            <asp:View ID="GrapfData" runat="server">
                <cc1:TabContainer ID="TabContainer1" runat="server" BorderStyle="None" 
                    CssClass="MyTabStyle" BorderWidth="1" ActiveTabIndex="0">
                </cc1:TabContainer>
            </asp:View>

            <asp:View runat="server" ID="PrintData">
                <div align="center">
                   <asp:Panel ID="Panel1" runat="server" Width="950px">
                        <a href="javascript:print()">
                            <asp:Image ID="Image3" ImageUrl="~/Eng/11.jpg" runat="server" 
                                BorderStyle="None" Width="940px" Height="120px"/></a>
                        <p align="center" lang="he" style="font-family: Aharoni; font-size: 22px" class="UnprintedItem">
                            <a href="javascript:print()" dir="ltr">Print</a>
                        </p>
                    </asp:Panel>
                </div>

                <div align="center">
                <br />
                    <p dir='ltr' lang="en" style="text-align: center">
                        <asp:Label Text="LB1" runat="server" ID="lb1" Font-Size="36px" 
                            Font-Bold="True" ForeColor="#0033CC" Font-Names="Calibri" />
                        <br />
                        <br />
                        <asp:Label Text="LB2" runat="server" ID="lb2" Font-Size="30px" 
                            Font-Bold="True" ForeColor="#0066FF" Font-Names="Calibri" />
                    </p>
                    <br />
                    <p dir='ltr' lang="en" style="text-align: center">
                        <asp:Label ID="lbDates" runat="server" Text="From Date To Date" 
                            Font-Bold="True" Font-Names="Times New Roman" Font-Size="24px"></asp:Label>
                    </p>
                <br />
                    <div dir='ltr' lang="en" style="text-align: center" align="center">
                    <table id="TaData" align='center' width='450' border='1' 
                        runat="server" dir='ltr' class="NewFont1" style="margin: auto">
                    <tr>
                        <th colspan='2'>Total</th>
                    </tr>
                    <tr>
                        <th>
                            <asp:Label  runat="server" ID="TypData"/>
                        </th>
                        <th>
                            <asp:Label runat="server" ID="Alut0"/>
                        </th>
                    </tr>
                    <tr>
                        <td align='center'>
                            <asp:Label runat="server" ID="V0" />
                        </td>
                        <td align='center'>
                            <asp:Label runat="server" ID="C0" />
                        </td>
                    </tr>
                    <tr>
                        <th colspan='2'>
                            <asp:Label runat="server" ID="Pisga"/>
                        </th>
                   </tr><tr>
                        <th>
                            <asp:Label  runat="server" ID="TypData1"/>
                        </th>
                        <th>
                            <asp:Label runat="server" ID="Alut1"/>
                        </th>
                    </tr><tr>
                        <td align='center'>
                            <asp:Label runat="server" ID="V1" />
                        </td>
                        <td align='center'>
                            <asp:Label runat="server" ID="C1" />
                        </td>
                    </tr>
                    <tr>
                        <th colspan='2'>
                            <asp:Label runat="server" ID="Geva"/>
                        </th>
                    </tr>
                    <tr>
                        <th>
                            <asp:Label  runat="server" ID="TypData2"/>
                        </th>
                        <th>
                            <asp:Label runat="server" ID="Alut2"/>
                        </th>
                    </tr><tr>
                        <td align='center'>
                            <asp:Label runat="server" ID="V2" />
                        </td>
                        <td align='center'>
                            <asp:Label runat="server" ID="C2" />
                        </td>
                    </tr>

                    <tr>
                        <th colspan='2'>
                            <asp:Label runat="server" ID="Shefel"/>
                        </th>
                    </tr><tr>
                        <th>
                            <asp:Label  runat="server" ID="TypData3"/>
                        </th>
                        <th>
                            <asp:Label runat="server" ID="Alut3"/>
                        </th>
                    </tr><tr>
                        <td align='center'>
                            <asp:Label runat="server" ID="V3" />
                        </td>
                        <td align='center'>
                            <asp:Label runat="server" ID="C3" />
                        </td>
                    </tr>
                </table>
                </div>        
                </div>
            </asp:View>




<%--       <table border="0" cellpadding="0" cellspacing="0" width="940px" dir="ltr" id="LowerTab">
            <tr id="LowerRow">
                <td style="width: 313px;">
                    <asp:Panel ID="Panel2" runat="server" Width="100%" BorderColor="#0066CC" BorderStyle="Solid" BorderWidth="1" HorizontalAlign="Center">

                        <asp:Label Text="text" runat="server" ID="RUP" BackColor="#00ADEF" 
                            ForeColor="#BDEBFA" Width= "100%" Font-Names="Arial" Font-Size="14pt" 
                            Font-Underline="True" />
                        <asp:Label Text="text" runat="server"  ID="RU1" Width= "160px" Font-Names="Arial" Font-Size="14"/>
                        <asp:Label Text="text" runat="server"  ID="RU2" Width= "160px" Font-Names="Arial" Font-Size="14"/>
                        <asp:Label Text="text" runat="server"  ID="RU3" Width= "160px" Font-Names="Arial" Font-Size="14"/>
                    </asp:Panel>
                </td>
                <td style="width: 314px;">
                    <asp:Panel ID="Panel3" runat="server" Width="100%" BorderColor="#0066CC" BorderStyle="Solid" BorderWidth="1" HorizontalAlign="Center">

                        <asp:Label runat="server" ID="MUP" BackColor="#00ADEF" 
                            ForeColor="#BDEBFA" Width= "100%" Font-Names="Arial" Font-Size="14pt" 
                            Font-Underline="True"  />
                        <asp:Label Text="" runat="server" Width="100%" ID="MU1" Font-Names="Arial" Font-Size="14pt" />
                        <asp:Label Text="" runat="server" Width="100%" ID="MU2" Font-Names="Arial" Font-Size="14pt" />
                        <asp:Label Text="" runat="server" Width="100%" ID="MU3" Font-Names="Arial" Font-Size="14pt" />
                   </asp:Panel>
                </td>
                <td style="width: 313px;">
                    <asp:Panel ID="Panel4" runat="server" Width="100%" BorderColor="#0066CC" BorderStyle="Solid" BorderWidth="1" HorizontalAlign="Center">
                        <asp:Label runat="server"  ID="LUP" BackColor="#00ADEF" ForeColor="#BDEBFA" 
                            Width= "100%" Font-Names="Arial" Font-Size="14pt" Font-Underline="True" />
                        <asp:Label Text="" runat="server"  ID="LU1" Width= "160px" Font-Names="Arial" Font-Size="14"/>
                        <asp:Label Text="" runat="server"  ID="LU2" Width= "160px" Font-Names="Arial" Font-Size="14"/>
                        <asp:Label Text="" runat="server"  ID="LU3" Width= "160px" Font-Names="Arial" Font-Size="14"/>
                    </asp:Panel>
                </td>
            </tr>
        </table>--%>

        </asp:MultiView>
    </div>

    <script type="text/javascript">
        function OpenNew() 
        {
            window.showModalDialog('../PrintPages/PrintPeriod.aspx', '', "dialogWidth=950px, dialogHeight=100%");
        }
    </script>
    <script type="text/javascript">
        function OpenMonthes() 
        {
            window.showModalDialog('../PrintPages/PrintMonthes.aspx', '', "dialogWidth=950px, dialogHeight=100%");
        }
    </script>
</asp:Content>
