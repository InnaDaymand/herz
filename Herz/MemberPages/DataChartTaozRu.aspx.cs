﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MemberPages_DataChartRu : SecuredPage
{
    DateTime _from, _to;
    string _consumerIdList;

    GraphBuilder _gBuilder = new GraphBuilder();
    
    protected void Page_Load(object sender, EventArgs e) 
    {
        //System.Web.UI.WebControls.Panel pt = (System.Web.UI.WebControls.Panel)Master.FindControl("Panel1");

        //double wt = document.documentElement.clientWidth

        //Page.ClientScript.RegisterStartupScript(typeof(double), "MWD", "MyWidth();");
        //int WT = System.Web.Configuration.HttpCapabilitiesBase.ScreenPixelsWidth;

        Dates.Lang = 2;
        TabContainer1.Tabs.Clear();
        Dates.ClickGraph += ibtExcel_Click;
        Dates.ClickTable += ibtExcel_Click;
        _consumerIdList = Request.QueryString["ConsumersList"];

        string SWQ = "SELECT TCL.RusName FROM tbl_Consumers_0 AS TCR INNER JOIN "
            + "T_Client AS TCL ON TCR.sql_clientID = TCL.ID WHERE sql_Consumer_ID IN (" + _consumerIdList + ")";
        lb1.Text = AllFunctions.GetScalar(SWQ);



        string str = "SELECT TOP 1 ConsumerType FROM tbl_Consumers_0 WHERE sql_Consumer_ID = " + _consumerIdList;
        AllFunctions.ConsumerType = str = AllFunctions.GetScalar(str);
        System.Web.UI.WebControls.Image ti = (System.Web.UI.WebControls.Image)Master.FindControl("TitleImage");

        if (str == "0")
        {
            ti.ImageUrl = "~/Rus/Titles/energy consumption_ru.png";
        }
        if (str == "3")
        {
            ti.ImageUrl = "~/Rus/Titles/energy consumption_ru.png";
        }
        Control cause = null;
        //bool NotShow = false;


        _from = Convert.ToDateTime(Session["DateFrom"]);
        _to = Convert.ToDateTime(Session["DateTill"]);

        AllFunctions.ConsName = lb2.Text = lblConsumer.Text = Request.QueryString["Names"];
        AllFunctions.IDforEP = Request.QueryString["EPID"];

        AllFunctions.ConsID = _consumerIdList;
        //AllFunctions.ConsName = lblConsumer.Text;
        string S1 = Request.QueryString["State"];
        if (S1 == "1")
        {
            Menu mn = (Menu) Master.FindControl("HorMenu");
            if (mn != null)
            {
                MenuItem mi = mn.Items[1];
                mi.Selectable = true;
                AllFunctions.EnchParam = true;
            }
        }
        else
        {
            Menu mn = (Menu) Master.FindControl("HorMenu");
            if (mn != null)
            {
                MenuItem mi = mn.Items[1];
                mi.Selectable = false;
                AllFunctions.EnchParam = false;
            }
        }

        if (!MemHist.byArrow)
        {
            MemHist.HistPage = Request.RawUrl;
        }
        else
        {
            MemHist.byArrow = false;
        }
        if (IsPostBack)
        {
            DateTime dt0 = Dates.FromDate;
            DateTime dt1 = Dates.TillDate;
            if ((_from != dt0) || (_to != dt1))
            {
                Session["DateFrom"] = dt0.ToString("yyyy-MM-dd HH:mm:ss");
                Session["DateTill"] = dt1.ToString("yyyy-MM-dd HH:mm:ss");

                _from = Convert.ToDateTime(Session["DateFrom"]);
                _to = Convert.ToDateTime(Session["DateTill"]);
            }

            if (_from > _to)
            {
                lblTotal.Visible = false;
                lblDateState.Text = "Дата начала больше даты окончания!";
            }
            else
            {
                lblTotal.Visible = true;
            }
            cause = GetPostBackControl(Page);
        }
        else
            MultiView1.ActiveViewIndex = 0;
        Dates.FromDate = _from;
        Dates.TillDate = _to;

        #region Counter Gap (To show elements that were not worked)

        string SS = "";
        DataClassesDataContext dc = new DataClassesDataContext();
        string CS = dc.Connection.ConnectionString;
        string SQL = "EXEC dbo.S_CompoundCounterGap N'" + _from.ToString() + "', '" + _to.ToString() + "', '" + _consumerIdList + "'";
        #endregion

        #region Check the definition of <Consumer start date>
        SQL = "EXEC [dbo].[S_CompareDate] N'" + _from.ToString("yyyy/MM/dd HH:mm:ss") + "', '" + _consumerIdList + "'";
        using (SqlConnection CN = new SqlConnection(CS))
        {
            SqlCommand command = new SqlCommand(SQL, CN);
            try
            {
                CN.Open();
                SS = command.ExecuteScalar().ToString();
            }
            catch (Exception)
            {
            }
            if ((SS != "OK") || (!lblTotal.Visible))
            {
                //lblDateState.Text = "הצרכן התחיל " + SS;
                if (SS != "OK") lblDateState.Text = "Нет данных до этой даты  " + SS;
                //NotShow = true;
                TabContainer1.Visible = false;
                lblTotal.Visible = false;
                ////return;
            }
            else
            {
                TabContainer1.Visible = true;
                //NotShow = false;
                lblDateState.Text = "";
                lblTotal.Visible = true;
            }
        }
        #endregion

        //double tw = lblTotal.Width.Value;

        _gBuilder.ListIDs = _consumerIdList;
        _gBuilder.sResol = (string)Session["Resolution"];
        _gBuilder.DateFrom = _from;
        _gBuilder.DateUntil = _to;

        _gBuilder.GetCounters(_consumerIdList);
        _gBuilder.GetTemperConsumers(_consumerIdList);
        _gBuilder.GetTemperatureData(_from, _to);
        _gBuilder.BuildRu();

        if (AllFunctions.ConsumerType == "3")
        {
            lblTotal.Text = "Общая стоимость для производителей: " + String.Format("{0: 0.##} ₽", _gBuilder.OverallCost);
        }
        if (AllFunctions.ConsumerType == "0")
        {
            lblTotal.Text = "Общее потребление энергии за период: " + String.Format("{0: 0.##} ₽", _gBuilder.OverallCost);
        }
        try
        {
            foreach (var chart in _gBuilder.Charts)
            {
                AjaxControlToolkit.TabPanel tab = new AjaxControlToolkit.TabPanel();
                tab.HeaderText = chart.Titles[0].Text;
                TabContainer1.Tabs.Add(tab);
                tab.ID = chart.ID;
                tab.BorderWidth = 0;
                tab.BorderStyle = System.Web.UI.WebControls.BorderStyle.None;
                double wsc = Convert.ToDouble(AllFunctions.BrotherWidth);
                wsc = wsc * 0.78;
                chart.Width = new Unit(wsc, UnitType.Pixel); 
                tab.Controls.Add(chart);
            }
        }
        catch (Exception ex)
        {
            System.Windows.Forms.MessageBox.Show(ex.Message);
        }
        if (!IsPostBack)
        {
            TabContainer1.ActiveTabIndex = 0;
        }
        foreach (AjaxControlToolkit.TabPanel panel in TabContainer1.Tabs)
        {
            System.Web.UI.DataVisualization.Charting.Chart chart = panel.Controls[0] as System.Web.UI.DataVisualization.Charting.Chart;
        }
        PrepareReport();
    }

    protected void ibtExcel_Click(object sender, EventArgs e)
    {
        PrepareReport();
    }

    protected void btnSwitch_Click(object sender, EventArgs e)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        tbl_Consumers_0 consumer = db.tbl_Consumers_0s.First(c => c.sql_Consumer_ID == int.Parse(_consumerIdList));
        //consumer.IsSwitchedOff = !consumer.IsSwitchedOff; //when NULL this does not work
        consumer.IsSwitchedOff = !(bool.Parse(Request.QueryString["State"]));
        db.SubmitChanges();
        Response.Redirect("ConsumersListRu.aspx");
    }

    public static Control GetPostBackControl(Page page)
    {
        Control postbackControlInstance = null;

        string postbackControlName = page.Request.Params.Get("__EVENTTARGET");
        if (postbackControlName != null && postbackControlName != string.Empty)
        {
            postbackControlInstance = page.FindControl(postbackControlName);
        }
        else
        {
            // handle the Button control postbacks
            for (int i = 0; i < page.Request.Form.Keys.Count; i++)
            {
                postbackControlInstance = page.FindControl(page.Request.Form.Keys[i]);
                if (postbackControlInstance is System.Web.UI.WebControls.Button)
                {
                    return postbackControlInstance;
                }
            }
        }

        // handle the ImageButton postbacks
        if (postbackControlInstance == null)
        {
            for (int i = 0; i < page.Request.Form.Count; i++)
            {
                if ((page.Request.Form.Keys[i].EndsWith(".x")) || (page.Request.Form.Keys[i].EndsWith(".y")))
                {
                    postbackControlInstance = page.FindControl(page.Request.Form.Keys[i].Substring(0, page.Request.Form.Keys[i].Length - 2));
                    return postbackControlInstance;
                }
            }
        }
        return postbackControlInstance;
    }

    private void PrepareReport()
    {
        double Rate = Convert.ToDouble(System.Configuration.ConfigurationManager.AppSettings["DollarExchangeRate"]);
        string SData = "От " + _from.ToString("dd/MM/yyyy") + " до " + _to.ToString("dd/MM/yyyy");
        DataTable tv = _gBuilder.TableVal;
        DataTable tc = _gBuilder.TableCos;
        string SID = TabContainer1.Tabs[0].ID;
        string SIT = "";
        if (SID == "003")
        {
            SIT = "М³/час";
        }
        else
            SIT = "кВт⋅ч";
        string SN = "Стоимость ₽";

        string S0 = SIT;
        TypData.Text = S0;
        Alut0.Text = SN;
        lbDates.Text = SData;

        if (SID == "002")
        {
            V0.Text = (tv.Rows[0][3]).ToString();
            C0.Text = ((double)tc.Rows[0][3] / Rate).ToString("#0.0000");
            TypData1.Text = S0;
            TypData2.Text = S0;
            TypData3.Text = S0;
            Alut1.Text = SN;
            Alut2.Text = SN;
            Alut3.Text = SN;
            Pisga.Text = "Пиковая зона";
            Geva.Text = "Полупиковая зона";
            Shefel.Text = "Ночная зона";
            V1.Text = (tv.Rows[0][0]).ToString();
            V2.Text = (tv.Rows[0][1]).ToString();
            V3.Text = (tv.Rows[0][2]).ToString();
            C1.Text = ((double)tc.Rows[0][0] / Rate).ToString("#0.0000");
            C2.Text = ((double)tc.Rows[0][1] / Rate).ToString("#0.0000");
            C3.Text = ((double)tc.Rows[0][2] / Rate).ToString("#0.0000");
        }
        else
        {
            V0.Text = (tv.Rows[0][0]).ToString();
            C0.Text = ((double)tc.Rows[0][0] / Rate).ToString("#0.0000");
            Pisga.Text = "";
            Geva.Text = "";
            Shefel.Text = "";
            TypData1.Text = "";
            TypData2.Text = "";
            TypData3.Text = "";
            Alut1.Text = "";
            Alut2.Text = "";
            Alut3.Text = "";
            Pisga.Text = "";
            Geva.Text = "";
            Shefel.Text = "";
            V1.Text = "";
            V2.Text = "";
            V3.Text = "";
            C1.Text = "";
            C2.Text = "";
            C3.Text = "";
        }

    }

    protected void Dates_ClickTable(object sender, EventArgs e)
    {
        PrepareReport();
        MultiView1.ActiveViewIndex = 1;
    }

    protected void Dates_ClickGraph(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 0;
    }
}
