﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MemberPages/MemberSg.master" AutoEventWireup="true" CodeFile="DaysParameters.aspx.cs" Inherits="MemberPages_EnchancedParameters" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>

<%@ Register assembly="eWorld.UI, Version=2.0.6.2393, Culture=neutral, PublicKeyToken=24d65337282035f2" namespace="eWorld.UI" tagprefix="ew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
   <style type="text/css">
        .style4
        {
            width: 213px;
        }
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="based" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <div align="center">
        <table width="100%">
            <tr align="center">
                <td><asp:Label ID="lblConsumer" runat="server" Font-Bold="True" Font-Names="Arial" 
                        Font-Size="16pt" ForeColor="#0066CC" /></td>
            </tr>
            <tr>
                <td height="30" width="100%" align="center" valign="middle">
                <asp:Label ID="lblTotal" runat="server" Font-Bold="True" Font-Names="Arial" 
                        Font-Size="15pt" BorderStyle="Solid" 
                        ForeColor="#0066CC" BorderColor="#0099FF" BorderWidth="1px" 
                        style="margin-bottom: 0px" Width="100%" CssClass="RoundCornerFontBlack" />
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table cellpadding="5px">
                        <tr valign="middle">
                  <td class="style4">
                      יממה-
                    <asp:TextBox ID="txtDateTo" runat="server" Width="80px" style="margin-right: 4px"></asp:TextBox>
                    <asp:Image ID="Image2" runat="server" ImageUrl="~/images/SmallCalendar.gif" />
                    <cc1:CalendarExtender ID="txtDateTo_CalendarExtender" runat="server" 
                        Enabled="True" TargetControlID="txtDateTo" PopupButtonID="Image2" Format="yyyy-MM-dd">
                    </cc1:CalendarExtender>
                    
                 </td>
                 <td>
                    <asp:Button ID="Button1" runat="server" Text="שלח" />
                 </td>
<%--                            <td>

                            <asp:ImageButton ID="btnPrint" runat="server" OnClick="ibtExcel_Click"  
                                    ImageUrl="~/images/ImgBtnPrint.png" />

                           </td>                                    
 <script type="text/javascript" language="javascript">
     function CallPrint(rl0, rl1, arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10) {
         if (rl1 == "קוט``ש") rl1 = "קוט''ש";
         if (rl1 == "מ``ק/שעה") rl1 = "מ''ק/שעה";
         var WinPrint = window.open('', '', '');
         WinPrint.document.write("<html><head><LINK rel=\"stylesheet\" type\"text/css\" href=\"css/print.css\" media=\"print\"><LINK rel=\"stylesheet\" type\"text/css\" href=\"css/print.css\" media=\"screen\"></head><body>");
         WinPrint.document.write("<p dir='rtl'><CENTER><h1>");
         WinPrint.document.write(arg0);
         WinPrint.document.write("</h1><br/><h2>");
         WinPrint.document.write(arg1);
         WinPrint.document.write("</h2><br/><h3>");
         WinPrint.document.write(arg2);
         WinPrint.document.write("</h3></CENTER><p>");
         WinPrint.document.write("<table align='center' width='450' cellspacing='0' border='1' cols='2' dir='rtl'><tr>");
         WinPrint.document.write("<th colspan='2'>סך הכל</th>");
         WinPrint.document.write("</tr><tr>");
         WinPrint.document.write("<th>" + rl1 + "</th>");
         WinPrint.document.write("<th>עלות בש''ח (ללא מע''מ)</th>");
         WinPrint.document.write("</tr><tr>");
         WinPrint.document.write("<td align='center'>" + arg3 + "</td>");
         WinPrint.document.write("<td align='center'>" + arg4 + "</td>");
         WinPrint.document.write("</tr>");

         if (rl0 == "002") {
             WinPrint.document.write("<tr><th colspan='2'>פסגה</th>");
             WinPrint.document.write("</tr><tr>");
             WinPrint.document.write("<th>" + rl1 + "</th>");
             WinPrint.document.write("<th>עלות בש''ח (ללא מע''מ)</th>");
             WinPrint.document.write("</tr><tr>");
             WinPrint.document.write("<td align='center'>" + arg5 + "</td>");
             WinPrint.document.write("<td align='center'>" + arg6 + "</td>");
             WinPrint.document.write("</tr>");

             WinPrint.document.write("<tr><th colspan='2'>גבע</th>");
             WinPrint.document.write("</tr><tr>");
             WinPrint.document.write("<th>" + rl1 + "</th>");
             WinPrint.document.write("<th>עלות בש''ח (ללא מע''מ)</th>");
             WinPrint.document.write("</tr><tr>");
             WinPrint.document.write("<td align='center'>" + arg7 + "</td>");
             WinPrint.document.write("<td align='center'>" + arg8 + "</td>");
             WinPrint.document.write("</tr>");

             WinPrint.document.write("<tr><th colspan='2'>שפל</th>");
             WinPrint.document.write("</tr><tr>");
             WinPrint.document.write("<th>" + rl1 + "</th>");
             WinPrint.document.write("<th>עלות בש''ח (ללא מע''מ)</th>");
             WinPrint.document.write("</tr><tr>");
             WinPrint.document.write("<td align='center'>" + arg9 + "</td>");
             WinPrint.document.write("<td align='center'>" + arg10 + "</td>");
             WinPrint.document.write("</tr>");
         }
         WinPrint.document.write("</table></body></html>");
         WinPrint.document.close();
         WinPrint.focus();
         WinPrint.print();
         WinPrint.close();

         return false;
     }
</script>--%>
                        </tr>
                    </table>
                </td>
            </tr>
       </table>
    </div>

<ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</ajaxToolkit:ToolkitScriptManager>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">
    <div runat="server" align="center">
<%--        <asp:MultiView ID="MultiView1" runat="server">
        </asp:MultiView>--%>
        <cc1:TabContainer ID="TabContEnc" runat="server" BorderStyle="None" CssClass="MyTabStyle">
        </cc1:TabContainer>
    </div>
</asp:Content>

