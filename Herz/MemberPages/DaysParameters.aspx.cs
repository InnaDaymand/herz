﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web.UI.DataVisualization.Charting;

public partial class MemberPages_EnchancedParameters : System.Web.UI.Page
{
    DateTime _from, _to;
    string _consumerIdList;

    protected void Page_Load(object sender, EventArgs e)
    {
        System.Web.UI.WebControls.Image ti = (System.Web.UI.WebControls.Image)Master.FindControl("TitleImage");
        ti.ImageUrl = "~/SeenergImg/Titles/Title-Day.png";

        _from = Convert.ToDateTime(Session["DateTill"]);
        _from = _from.Date;
        _to = _from.AddDays(1);
        _to = _to.AddMinutes(-14);

        _consumerIdList = AllFunctions.IDforEP;
        lblConsumer.Text = AllFunctions.ConsName;

        if (IsPostBack)
        {

            DateTime dt0 = DateTime.Parse(txtDateTo.Text).Date;
            DateTime dt1 = dt0.AddDays(1);
            _from = dt0;
            _to = _from.AddDays(1);
            _to = _to.AddMinutes(-14);
            lblTotal.Visible = true;
        }
        txtDateTo_CalendarExtender.SelectedDate = _to;

        DataClassesDataContext dc = new DataClassesDataContext();
        string CS = dc.Connection.ConnectionString;
        DataSet dst = new DataSet();
        SqlDataAdapter sda = new SqlDataAdapter("", "");
        DataTable[] dtb = new DataTable[12];

        string SQL = "EXEC [dbo].[S_CalculateFactors] N'" + _from.ToString("yyyy/MM/dd HH:mm:ss") + "', N'" + _to.ToString("yyyy/MM/dd HH:mm:ss") + "', N'" + _consumerIdList + "', 0";
        SqlConnection CN = new SqlConnection(CS);

        SqlCommand selectCMD = new SqlCommand(SQL, CN);
        selectCMD.CommandTimeout = 0;
        sda.SelectCommand = selectCMD;
        CN.Open();
        sda.Fill(dst);
        CN.Close();

        dtb[0] = dst.Tables[0];
        dtb[1] = dst.Tables[1];
        dtb[2] = dst.Tables[2];
        dtb[3] = dst.Tables[3];
        dtb[4] = dst.Tables[4];
        dtb[5] = dst.Tables[5];
        dtb[6] = dst.Tables[6];
        dtb[7] = dst.Tables[7];
        dtb[8] = dst.Tables[8];
        dtb[9] = dst.Tables[9];
        dtb[10] = dst.Tables[10];
        dtb[11] = dst.Tables[11];

        string S0, S1;

        if (Convert.ToSingle(dtb[1].Rows[0][0].ToString()) == 0)
        {
            S0 = S1 = "צרכן לא עבד";
        }
        else
        {
            S0 = dtb[1].Rows[0][0].ToString();
            S1 = dtb[1].Rows[0][1].ToString();
        }
        string SS = "מתח: מקסימלי - " + S0 + "; מינימלי: " + S1;
        InputChart(SS, "010", dtb[0], "וולט", Color.Blue);                  //"Voltage (kv)"

        if (dtb[3].Rows[0][0].ToString() == "0")
        {
            S0 = "צרכן לא עבד";
            InputChart("זרם 0 א'", "011", dtb[2], "א'", Color.HotPink);                 //Current 1 (amp)
        }
        else
        {
            S0 = "1 - " + dtb[3].Rows[0][0].ToString() + " א'; " + "2 - " + dtb[9].Rows[0][0].ToString() + "א'; " + "3 - " + dtb[11].Rows[0][0].ToString() + " א'" ;
            InputCurrentChart(S0, dtb[2], dtb[8], dtb[10], "Amper");
        }

        if (dtb[5].Rows[0][0].ToString() == "0")
        {
            S0 = "צרכן לא עבד";
        }
        else
        {
            S0 = dtb[5].Rows[0][0].ToString();
        }
        SS = "הספק: מקסימלי - " + S0;
        InputChart(SS, "012", dtb[4], "קו''ט", Color.Green);                //Power (MW.)

        if (dtb[7].Rows[0][0].ToString() == "0")
        {
            S0 = "צרכן לא עבד";
        }
        else
        {
            S0 = dtb[7].Rows[0][0].ToString();
        }
        SS = "מקדם הספק: ממוצע - " + S0;
        InputChart(SS, "013", dtb[6], "מקדם הספק", Color.Brown);           //Power Factor ()
    }

    protected void InputChart(string NewName, string NewID, DataTable dt, string SerName, Color clr)
    {
        Chart chart = new Chart();
        chart.Titles.Add(NewName);

        chart.Width = 850;
        chart.Height = 400;
        chart.Titles[0].Font = new Font("Arial", 14);
        chart.BackColor = Color.AliceBlue;

        chart.ChartAreas.Add(new ChartArea(SerName));
        chart.ChartAreas[0].AxisX.LabelStyle.Format = "dd/MM\nHH:mm";// "g";
        chart.ChartAreas[0].AxisY.TitleFont = new Font("Arial", 12, FontStyle.Bold);
        chart.ChartAreas[0].AxisX.TitleFont = new Font("Arial", 11, FontStyle.Bold);
        chart.ChartAreas[0].AxisY.Title = SerName;
        chart.ChartAreas[0].AxisX.TitleForeColor = Color.Red;
        chart.ID = NewID;
        InputSeries(chart, dt, SerName, clr);

        AjaxControlToolkit.TabPanel tab = new AjaxControlToolkit.TabPanel();
        tab.HeaderText = chart.Titles[0].Text;
        tab.ID = chart.ID;
        tab.BorderStyle = System.Web.UI.WebControls.BorderStyle.None;
        tab.Controls.Add(chart);
        TabContEnc.Tabs.Add(tab);

    }

    protected void InputSeries(Chart ct, DataTable dt, string SeriaName, Color cr)
    {
        Series s1 = new Series(SeriaName);
        s1.ChartType = SeriesChartType.Line;
        s1.BorderWidth = 3;//set line width
        s1.Color = cr;
        s1.ChartArea = ct.ChartAreas[0].Name;
        s1.XValueType = ChartValueType.DateTime;
        s1.ToolTip = "#VALX{g} - #VALY{0.0} " + SeriaName;
        s1.IsVisibleInLegend = true;

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            s1.Points.AddXY((DateTime)dt.Rows[i][0], Convert.ToDouble(dt.Rows[i][1]));
        }

        ct.Series.Add(s1);
    }

    protected void InputCurrentChart(string NewName, DataTable dt, DataTable dt2, DataTable dt3, string SerName)
    {
        Chart chart = new Chart();
        chart.Titles.Add("זרם: מקסימלי" + ": " + NewName);

        chart.Width = 850;
        chart.Height = 400;
        chart.Titles[0].Font = new Font("Arial", 14);
        chart.BackColor = Color.AliceBlue;

        chart.ChartAreas.Add(new ChartArea(SerName));
        chart.ChartAreas[0].AxisX.LabelStyle.Format = "dd/MM\nHH:mm";// "g";
        chart.ChartAreas[0].AxisY.TitleFont = new Font("Arial", 12, FontStyle.Bold);
        chart.ChartAreas[0].AxisX.TitleFont = new Font("Arial", 11, FontStyle.Bold);
        chart.ChartAreas[0].AxisY.Title = "א'";
        chart.ChartAreas[0].AxisX.TitleForeColor = Color.Red;
        chart.ChartAreas[0].Area3DStyle.Enable3D = true;
        chart.ID = "123";
        //InputSeries(chart, dt, SerName, clr);

        Series s1 = new Series(SerName + " 1");

        s1.ChartType = SeriesChartType.Line;
        s1.BorderWidth = 3;//set line width
        s1.Color = Color.DarkGreen;
        s1.ChartArea = chart.ChartAreas[0].Name;
        s1.XValueType = ChartValueType.DateTime;
        s1.ToolTip = "#VALX{g} - #VALY{0.0} " + " א ";
        s1.IsVisibleInLegend = true;

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            s1.Points.AddXY((DateTime)dt.Rows[i][0], Convert.ToDouble(dt.Rows[i][1]));
        }

        chart.Series.Add(s1);

        Series s2 = new Series(SerName + " 2");
        s2.ChartType = SeriesChartType.Line;
        s2.BorderWidth = 3;//set line width
        s2.Color = Color.LightBlue;
        s2.ChartArea = chart.ChartAreas[0].Name;
        s2.XValueType = ChartValueType.DateTime;
        s2.ToolTip = "#VALX{g} - #VALY{0.0} " + " א ";
        s2.IsVisibleInLegend = true;

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            s2.Points.AddXY((DateTime)dt2.Rows[i][0], Convert.ToDouble(dt2.Rows[i][1]));
        }

        chart.Series.Add(s2);

        Series s3 = new Series(SerName + " 3");
        s3.ChartType = SeriesChartType.Line;
        s3.BorderWidth = 3;//set line width
        s3.Color = Color.DarkRed;
        s3.ChartArea = chart.ChartAreas[0].Name;
        s3.XValueType = ChartValueType.DateTime;
        s3.ToolTip = "#VALX{g} - #VALY{0.0} " + " א ";
        s3.IsVisibleInLegend = true;

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            s3.Points.AddXY((DateTime)dt3.Rows[i][0], Convert.ToDouble(dt3.Rows[i][1]));
        }

        chart.Series.Add(s3);

        AjaxControlToolkit.TabPanel tab = new AjaxControlToolkit.TabPanel();
        tab.HeaderText = "זרם";
        tab.ID = chart.ID;
        tab.BorderStyle = System.Web.UI.WebControls.BorderStyle.None;
        tab.Controls.Add(chart);
        TabContEnc.Tabs.Add(tab);
    }


}