﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web.UI.DataVisualization.Charting;

public partial class MemberPages_EnchancedParametersEn : System.Web.UI.Page
{

    DateTime _from, _to;
    string _consumerIdList;

    //DateTime _from = Convert.ToDateTime("2013-07-22 00:00:00");                         // DateTime.Today.AddDays(-2);
    //DateTime _to = Convert.ToDateTime("2013-07-23 00:00:00");                           //DateTime.Today.AddDays(-1);

    protected void Page_Load(object sender, EventArgs e)
    {
        System.Web.UI.WebControls.Image ti = (System.Web.UI.WebControls.Image)Master.FindControl("TitleImage");
        ti.ImageUrl = "~/Eng/Titles/Daily Technical Data.png";

        //_from = DateTime.Today.AddDays(-1);
        //_to = DateTime.Now;
        //_from = Convert.ToDateTime(Session["DateFrom"]);
        //_to = _from.AddDays(7);
        _from = Convert.ToDateTime(Session["DateTill"]);
        _from = _from.Date;
        _to = _from.AddDays(1);
        _to = _to.AddMinutes(-14);

        _consumerIdList = AllFunctions.IDforEP;
        lblConsumer.Text = AllFunctions.ConsName;

        if (IsPostBack)
        {
            DateTime dt0 = DateTime.Parse(txtDateTo.Text).Date;
            DateTime dt1 = dt0.AddDays(1);
            _from = dt0;
            _to = _from.AddDays(1);
            _to = _to.AddMinutes(-14);
            lblTotal.Visible = true;
        }
        txtDateTo_CalendarExtender.SelectedDate = _to;

        DataClassesDataContext dc = new DataClassesDataContext();
        string CS = dc.Connection.ConnectionString;
        DataSet dst = new DataSet();
        SqlDataAdapter sda = new SqlDataAdapter("", "");
        DataTable[] dtb = new DataTable[8];

        string SQL = "EXEC [dbo].[S_CalculateFactors] N'" + _from.ToString("yyyy/MM/dd HH:mm:ss") + "', N'" + _to.ToString("yyyy/MM/dd HH:mm:ss") + "', N'" + _consumerIdList + "', 0";
        SqlConnection CN = new SqlConnection(CS);

        SqlCommand selectCMD = new SqlCommand(SQL, CN);
        selectCMD.CommandTimeout = 0;
        sda.SelectCommand = selectCMD;
        CN.Open();
        sda.Fill(dst);
        CN.Close();

        dtb[0] = dst.Tables[0];
        dtb[1] = dst.Tables[1];
        dtb[2] = dst.Tables[2];
        dtb[3] = dst.Tables[3];
        dtb[4] = dst.Tables[4];
        dtb[5] = dst.Tables[5];
        dtb[6] = dst.Tables[6];
        dtb[7] = dst.Tables[7];

        string S0, S1;

        if (Convert.ToSingle(dtb[1].Rows[0][0].ToString()) == 0)
        {
            S0 = S1 = "not active";
        }
        else
        {
            S0 = dtb[1].Rows[0][0].ToString();
            S1 = dtb[1].Rows[0][1].ToString();
        }
        string SS = "Voltage: max - " + S0 + "; min - " + S1;
        InputChart(SS, "010", dtb[0], "volt", Color.Blue);                  //"Voltage (kv)"

        if (dtb[3].Rows[0][0].ToString() == "0")
        {
            S0 = "Consumer not active";
        }
        else
        {
            S0 = dtb[3].Rows[0][0].ToString();
        }
        SS = "Current: max - " + S0;
        InputChart(SS, "011", dtb[2], "A", Color.HotPink);                 //Current (amp)

        if (dtb[5].Rows[0][0].ToString() == "0")
        {
            S0 = "Consumer not active";
        }
        else
        {
            S0 = dtb[5].Rows[0][0].ToString();
        }
        SS = "Power: max -" + S0;
        InputChart(SS, "012", dtb[4], "kw.", Color.Green);                //Power (MW.)

        S0 = dtb[7].Rows[0][0].ToString().Trim();
        if ((S0 == "0") || (S0 == ""))
        {
            S0 = "not active";
        }
        SS = "Power factor: average -" + S0;
        InputChart(SS, "013", dtb[6], "Power factor", Color.Brown);           //Power Factor ()




        //add series
        //AddParamsSeries(chart, 28);
    }

    protected void InputChart(string NewName, string NewID, DataTable dt, string SerName, Color clr)
    {
        //create chart
        Chart chart = new Chart();
        chart.Titles.Add(NewName);

        chart.Width = 940; 
        chart.Height = 400;
        chart.Titles[0].Font = new Font("Arial", 14);
        chart.BackColor = Color.AliceBlue;

        chart.ChartAreas.Add(new ChartArea(SerName));
        chart.ChartAreas[0].AxisX.LabelStyle.Format = "dd/MM\nHH:mm";// "g";
        chart.ChartAreas[0].AxisY.TitleFont = new Font("Arial", 12, FontStyle.Bold);
        chart.ChartAreas[0].AxisX.TitleFont = new Font("Arial", 11, FontStyle.Bold);
        chart.ChartAreas[0].AxisY.Title = SerName;
        chart.ChartAreas[0].AxisX.TitleForeColor = Color.Red;
        chart.ID = NewID;
        //chart.Legends.Add("Parameters");
        //chart.Legends["Parameters"].IsDockedInsideChartArea = false;
        //chart.Legends["Parameters"].DockedToChartArea = "Parameters";
        InputSeries(chart, dt, SerName, clr);

        AjaxControlToolkit.TabPanel tab = new AjaxControlToolkit.TabPanel();
        tab.HeaderText = chart.Titles[0].Text;
        tab.ID = chart.ID;
        tab.BorderStyle = System.Web.UI.WebControls.BorderStyle.None;
        tab.Controls.Add(chart);
        TabContEnc.Tabs.Add(tab);

    }

    protected void InputSeries(Chart ct, DataTable dt, string SeriaName, Color cr)
    {
        Series s1 = new Series(SeriaName);
        s1.ChartType = SeriesChartType.Line;
        s1.BorderWidth = 3;//set line width
        s1.Color = cr;
        s1.ChartArea = ct.ChartAreas[0].Name;
        s1.XValueType = ChartValueType.DateTime;
        s1.ToolTip = "#VALX{g} - #VALY{0.0} " + SeriaName;
        s1.IsVisibleInLegend = true;
        //s1.Legend = ct.Legends[0].Name;

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            s1.Points.AddXY((DateTime)dt.Rows[i][0], Convert.ToDouble(dt.Rows[i][1]));
        }

        ct.Series.Add(s1);

    }
}