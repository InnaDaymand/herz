﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MemberPages/MemberSg.master" AutoEventWireup="true" CodeFile="DeniedM.aspx.cs" Inherits="MemberPages_DeniedM" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="based" Runat="Server">
    <table width="100%">
        <tr>
            <td align="center">
                <asp:Label Text="אינך רשאי לראות מידע זה!" runat="server" ForeColor="Red" Font-Bold="True" Font-Names="Arial" Font-Size="XX-Large" />
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">
</asp:Content>

