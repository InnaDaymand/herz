﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web.UI.DataVisualization.Charting;

public partial class MemberPages_EnchancedParameters : System.Web.UI.Page
{

    DateTime _from, _to;
    string _consumerIdList;

    //DateTime _from = Convert.ToDateTime("2013-07-22 00:00:00");                         // DateTime.Today.AddDays(-2);
    //DateTime _to = Convert.ToDateTime("2013-07-23 00:00:00");                           //DateTime.Today.AddDays(-1);

    protected void Page_Load(object sender, EventArgs e)
    {
        System.Web.UI.WebControls.Image ti = (System.Web.UI.WebControls.Image)Master.FindControl("TitleImage");
        ti.ImageUrl = "~/SeenergImg/Titles/Title-E.png";

        //_from = DateTime.Today.AddDays(-1);
        //_to = DateTime.Now;
        //_from = Convert.ToDateTime(Session["DateFrom"]);
        //_to = _from.AddDays(7);
        _to = Convert.ToDateTime(Session["DateTill"]);
        _from = _to.AddDays(-7);

        _consumerIdList = AllFunctions.IDforEP;
        lblConsumer.Text = AllFunctions.ConsName;

        if (IsPostBack)
        {
            //_from = DateTime.Parse(txtDateFrom.Text).Date + TimePicker1.SelectedTime.TimeOfDay;
            //_to = DateTime.Parse(txtDateTo.Text).Date + TimePicker2.SelectedTime.TimeOfDay;

            //DateTime dt0 = DateTime.Parse(txtDateFrom.Text).Date + TimePicker1.SelectedTime.TimeOfDay;
            //DateTime dt1 = dt0.AddDays(7);
            DateTime dt1 = DateTime.Parse(txtDateTo.Text).Date + TimePicker2.SelectedTime.TimeOfDay;
            DateTime dt0 = dt1.AddDays(-7);
            if ((_from != dt0) || (_to != dt1))
            {
                Session["DateFrom"] = dt0.ToString("yyyy-MM-dd HH:mm:ss");
                Session["DateTill"] = dt1.ToString("yyyy-MM-dd HH:mm:ss");

                _to = dt1;
                _from = _to.AddDays(-7);

            }

            if (_from > _to)
            {
                lblTotal.Visible = false;
                //lblDateState.Text = "תאריך התחלה גדולה מתאריך סיום!";
            }
            else
            {
                lblTotal.Visible = true;
            }
            //cause = GetPostBackControl( Page);
        }
        //txtDateFrom_CalendarExtender.SelectedDate = _from;
        txtDateTo_CalendarExtender.SelectedDate = _to;
        //TimePicker1.SelectedTime = _from;
        TimePicker2.SelectedTime = _to;

        DataClassesDataContext dc = new DataClassesDataContext();
        string CS = dc.Connection.ConnectionString;
        DataSet dst = new DataSet();
        SqlDataAdapter sda = new SqlDataAdapter("", "");
        DataTable[] dtb = new DataTable[12];

        string SQL = "EXEC [dbo].[S_CalculateFactors] N'" + _from.ToString("yyyy/MM/dd HH:mm:ss") + "', N'" + _to.ToString("yyyy/MM/dd HH:mm:ss") + "', N'" + _consumerIdList + "', 0";
        SqlConnection CN = new SqlConnection(CS);

        SqlCommand selectCMD = new SqlCommand(SQL, CN);
        selectCMD.CommandTimeout = 0;
        sda.SelectCommand = selectCMD;
        CN.Open();
        sda.Fill(dst);
        CN.Close();

        dtb[0] = dst.Tables[0];
        dtb[1] = dst.Tables[1];
        dtb[2] = dst.Tables[2];
        dtb[3] = dst.Tables[3];
        dtb[4] = dst.Tables[4];
        dtb[5] = dst.Tables[5];
        dtb[6] = dst.Tables[6];
        dtb[7] = dst.Tables[7];
        dtb[8] = dst.Tables[8];
        dtb[9] = dst.Tables[9];
        dtb[10] = dst.Tables[10];
        dtb[11] = dst.Tables[11];

        string S0, S1;

        if (Convert.ToSingle(dtb[1].Rows[0][0].ToString()) == 0)
        {
            S0 = S1 = "צרכן לא עבד";
        }
        else
        {
            S0 = dtb[1].Rows[0][0].ToString();
            S1 = dtb[1].Rows[0][1].ToString();
        }
        string SS = "מתח: מקסימלי - " + S0 + "; מינימלי: " + S1;
        InputChart(SS, "010", dtb[0], "וולט", Color.Blue);                  //"Voltage (kv)"

        if (dtb[3].Rows[0][0].ToString() == "0")
        {
            S0 = "צרכן לא עבד";
        }
        else
        {
            S0 = dtb[3].Rows[0][0].ToString();
        }
        SS = "זרם פזה 1: מקסימלי - " + S0;
        InputChart(SS, "011", dtb[2], "א'", Color.HotPink);                 //Current 1(Amp)


        if (dtb[9].Rows[0][0].ToString() == "0")
        {
            S0 = "צרכן לא עבד";
        }
        else
        {
            S0 = dtb[9].Rows[0][0].ToString();
        }
        SS = "זרם פזה 2: מקסימלי - " + S0;
        InputChart(SS, "014", dtb[8], "א'", Color.HotPink);                 //Current 2(Amp)


        if (dtb[11].Rows[0][0].ToString() == "0")
        {
            S0 = "צרכן לא עבד";
        }
        else
        {
            S0 = dtb[11].Rows[0][0].ToString();
        }
        SS = "זרם פזה 3: מקסימלי - " + S0;
        InputChart(SS, "015", dtb[10], "א'", Color.HotPink);                 //Current 2(Amp)


        if (dtb[5].Rows[0][0].ToString() == "0")
        {
            S0 = "צרכן לא עבד";
        }
        else
        {
            S0 = dtb[5].Rows[0][0].ToString();
        }
        SS = "הספק: מקסימלי - " + S0;
        InputChart(SS, "012", dtb[4], "קו''ט", Color.Green);                //Power (MW.)

        if (dtb[7].Rows[0][0].ToString() == "0")
        {
            S0 = "צרכן לא עבד";
        }
        else
        {
            S0 = dtb[7].Rows[0][0].ToString();
        }
        SS = "מקדם הספק: ממוצע - " + S0;
        InputChart(SS, "013", dtb[6], "מקדם הספק", Color.Brown);           //Power Factor ()

    }

    protected void InputChart(string NewName, string NewID, DataTable dt, string SerName, Color clr)
    {
        //create chart
        Chart chart = new Chart();
        chart.Titles.Add(NewName);

        chart.Width = 850;
        chart.Height = 400;
        chart.Titles[0].Font = new Font("Arial", 14);
        chart.BackColor = Color.AliceBlue;

        chart.ChartAreas.Add(new ChartArea(SerName));
        chart.ChartAreas[0].AxisX.LabelStyle.Format = "dd/MM\nHH:mm";// "g";
        chart.ChartAreas[0].AxisY.TitleFont = new Font("Arial", 12, FontStyle.Bold);
        chart.ChartAreas[0].AxisX.TitleFont = new Font("Arial", 11, FontStyle.Bold);
        chart.ChartAreas[0].AxisY.Title = SerName;
        chart.ChartAreas[0].AxisX.TitleForeColor = Color.Red;
        chart.ID = NewID;
        //chart.Legends.Add("Parameters");
        //chart.Legends["Parameters"].IsDockedInsideChartArea = false;
        //chart.Legends["Parameters"].DockedToChartArea = "Parameters";
        InputSeries(chart, dt, SerName, clr);

        AjaxControlToolkit.TabPanel tab = new AjaxControlToolkit.TabPanel();
        tab.HeaderText = chart.Titles[0].Text;
        tab.ID = chart.ID;
        tab.BorderStyle = System.Web.UI.WebControls.BorderStyle.None;
        tab.Controls.Add(chart);
        TabContEnc.Tabs.Add(tab);

    }

    protected void InputSeries(Chart ct, DataTable dt, string SeriaName, Color cr)
    {
        Series s1 = new Series(SeriaName);
        s1.ChartType = SeriesChartType.Line;
        s1.BorderWidth = 3;//set line width
        s1.Color = cr;
        s1.ChartArea = ct.ChartAreas[0].Name;
        s1.XValueType = ChartValueType.DateTime;
        s1.ToolTip = "#VALX{g} - #VALY{0.0} " + SeriaName;
        s1.IsVisibleInLegend = true;
        //s1.Legend = ct.Legends[0].Name;

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            s1.Points.AddXY((DateTime)dt.Rows[i][0], Convert.ToDouble(dt.Rows[i][1]));
        }

        ct.Series.Add(s1);

    }
}