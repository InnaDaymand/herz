﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MemberPages/MemberEn.master" AutoEventWireup="true" CodeFile="EnchancedParametersEn.aspx.cs" Inherits="MemberPages_EnchancedParametersEn" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>

<%@ Register assembly="eWorld.UI, Version=2.0.6.2393, Culture=neutral, PublicKeyToken=24d65337282035f2" namespace="eWorld.UI" tagprefix="ew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style4
        {
            width: 213px;
        }
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="based" Runat="Server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <div align="center">
        <table width="100%">
            <tr align="center">
                <td><asp:Label ID="lblConsumer" runat="server" Font-Bold="True" Font-Names="Arial" 
                        Font-Size="16pt" ForeColor="#0066CC" /></td>
            </tr>
            <tr>
                <td height="30" width="100%" align="center" valign="middle">
                <asp:Label ID="lblTotal" runat="server" Font-Bold="True" Font-Names="Arial" 
                        Font-Size="15pt" BorderStyle="Solid" 
                        ForeColor="#0066CC" BorderColor="#0099FF" BorderWidth="1px" 
                        style="margin-bottom: 0px" Width="100%" CssClass="RoundCornerFontBlack" />
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table cellpadding="5px" dir="ltr">
                        <tr valign="middle">
                            <td>One week before</td>
                             <td class="style4">
                                <asp:TextBox ID="txtDateTo" runat="server" Width="80px" style="margin-right: 4px"></asp:TextBox>
                                <asp:Image ID="Image2" runat="server" ImageUrl="~/images/SmallCalendar.gif" />
                                <cc1:CalendarExtender ID="txtDateTo_CalendarExtender" runat="server" 
                                    Enabled="True" TargetControlID="txtDateTo" PopupButtonID="Image2" Format="yyyy-MM-dd">
                                </cc1:CalendarExtender>
                                <ew:TimePicker ID="TimePicker2" runat="server" Width="40px" 
                                    NumberOfColumns="4" PopupLocation="Bottom" 
                                    PopupHeight="180px" Scrollable="False" ImageUrl="~/images/timepicker.gif" 
                                                MilitaryTime="True" >
                                    <ButtonStyle BorderStyle="None" Width="25px" />
                                </ew:TimePicker>
                             </td>
                             <td>
                                <asp:Button ID="Button1" runat="server" Text="Send" />
                             </td>
                        </tr>
                    </table>
                </td>
            </tr>
       </table>
    </div>


</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">
    <div align="center" runat="server">
<%--        <asp:MultiView ID="MultiView1" runat="server">
        </asp:MultiView>--%>
        <cc1:TabContainer ID="TabContEnc" runat="server" BorderStyle="None" CssClass="MyTabStyle">
        </cc1:TabContainer>
    </div>
</asp:Content>

