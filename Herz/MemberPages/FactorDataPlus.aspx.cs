﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web.UI.DataVisualization.Charting;

public partial class MemberPages_FactorDataPlus : System.Web.UI.Page
{

    DateTime _to;
    string _consumerIdList;
    string SetIdList;

    protected void Page_Load(object sender, EventArgs e)
    {

        _consumerIdList = Request.QueryString["Cons"];
        SetIdList = Request.QueryString["Set"];

        if ((AllFunctions.IDforEP == "") && (SetIdList == "0") && (_consumerIdList == "0"))
        {
            Response.Redirect("ConsumersList.aspx");
        }

        System.Web.UI.WebControls.Image ti = (System.Web.UI.WebControls.Image)Master.FindControl("TitleImage");
        ti.ImageUrl = "~/SeenergImg/Titles/CumulativePoweFactor.png";

        if (SetIdList == "0")
        {
            if (_consumerIdList == "0")
            {
                _consumerIdList = AllFunctions.IDforEP;
                lblConsumer.Text = AllFunctions.ConsName;
            }
            else
                lblConsumer.Text = AllFunctions.GetScalar("SELECT [sql_Consumer_name] FROM [dbo].[tbl_Consumers_0] WHERE [sql_Consumer_ID] = " + _consumerIdList);
        }
        else
        {
            lblConsumer.Text = AllFunctions.GetScalar("SELECT SetName FROM [dbo].[T_SummaryFactorSets] WHERE [SetID] = " + SetIdList);
        }

        _to = Convert.ToDateTime(Session["DateTill"]);


        if (IsPostBack)
        {
            DateTime dt1 = DateTime.Parse(txtDateTo.Text).Date; // + TimePicker2.SelectedTime.TimeOfDay;
            if (_to != dt1)
            {
                _to = dt1;
            }
        }
        txtDateTo_CalendarExtender.SelectedDate = _to;

        DataClassesDataContext dc = new DataClassesDataContext();
        string CS = dc.Connection.ConnectionString;
        DataSet dst = new DataSet();
        SqlDataAdapter sda = new SqlDataAdapter("", "");
        DataTable dtb = new DataTable();

        string SQL = "EXEC [dbo].[S_PowerFactorPlus] @ConsS = " +  _consumerIdList + ", @LastDate = " 
            + "N'" + _to.ToString("yyyy/MM/dd HH:mm:ss") + "', @SetNo = " + SetIdList ;
        SqlConnection CN = new SqlConnection(CS);

        SqlCommand selectCMD = new SqlCommand(SQL, CN);
        selectCMD.CommandTimeout = 0;
        sda.SelectCommand = selectCMD;
        CN.Open();
        sda.Fill(dst);
        CN.Close();

        dtb = dst.Tables[0];
        string SS = "מקדם הספק מצתבר";
        InputChart(SS, "010", dtb, "", Color.Green);                  

    }

    protected void InputChart(string NewName, string NewID, DataTable dt, string SerName, Color clr)
    {
        //create chart
        Chart chart = new Chart();
        //chart.Titles.Add(NewName);

        chart.Width = 850;
        chart.Height = 400;
        //chart.Titles[0].Font = new Font("Arial", 14);
        chart.BackColor = Color.AliceBlue;

        chart.ChartAreas.Add(new ChartArea(SerName));
        chart.ChartAreas[0].AxisX.LabelStyle.Format = "dd/MM\nyyyy";// "g";
        chart.ChartAreas[0].AxisY.TitleFont = new Font("Arial", 12, FontStyle.Bold);
        chart.ChartAreas[0].AxisX.TitleFont = new Font("Arial", 11, FontStyle.Bold);
        chart.ChartAreas[0].AxisY.Title = SerName;
        chart.ChartAreas[0].AxisX.TitleForeColor = Color.Red;
        chart.ID = NewID;
        //chart.Legends.Add("Parameters");
        //chart.Legends["Parameters"].IsDockedInsideChartArea = false;
        //chart.Legends["Parameters"].DockedToChartArea = "Parameters";
        if (dt.Rows.Count > 1)
        {
            InputSeries(chart, dt, SerName, clr);
        }
        
        AjaxControlToolkit.TabPanel tab = new AjaxControlToolkit.TabPanel();
        tab.HeaderText = NewName;
        //tab.HeaderText = chart.Titles[0].Text;
        tab.ID = chart.ID;
        tab.BorderStyle = System.Web.UI.WebControls.BorderStyle.None;
        tab.Controls.Add(chart);
        TabContEnc.Tabs.Add(tab);

    }

    protected void InputSeries(Chart ct, DataTable dt, string SeriaName, Color cr)
    {
        Series s1 = new Series(SeriaName);
        s1.ChartType = SeriesChartType.Line;
        s1.BorderWidth = 3;//set line width
        s1.Color = cr;
        s1.ChartArea = ct.ChartAreas[0].Name;
        s1.XValueType = ChartValueType.DateTime;
         //s1.ToolTip = "#VALY{0.0000} " + SeriaName;
        s1.ToolTip = "#VALX{g} - #VALY{0.0000} " + SeriaName;
        s1.IsVisibleInLegend = true;
        //s1.Legend = ct.Legends[0].Name;

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            s1.Points.AddXY((DateTime)dt.Rows[i][0], Convert.ToDouble(dt.Rows[i][1]));
        }

        ct.Series.Add(s1);

    }

    protected void Button1_Click(object sender, EventArgs e)
    {

    }
}