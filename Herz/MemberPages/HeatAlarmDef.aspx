﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MemberPages/MemberSg.master" AutoEventWireup="true" CodeFile="HeatAlarmDef.aspx.cs" Inherits="MemberPages_HeatAlarmDef" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register assembly="eWorld.UI, Version=2.0.6.2393, Culture=neutral, PublicKeyToken=24d65337282035f2" namespace="eWorld.UI" tagprefix="ew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="based" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <%--<script language="javascript">
        function ConfirmDelete() 
        {
            return confirm('למחוק את הרשומה?');
        }
    </script>--%>
    <div align="center">
    <table width="640px">
<%--        <tr>
            <th class="HeaderStyle">הגדרת התראה</th>
        </tr>
--%>        <tr>
            <td>
<ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</ajaxToolkit:ToolkitScriptManager>
                <table>
                    <tr>
                        <td valign="top" width="50%">
                            <asp:LinqDataSource ID="LinqConsumers" runat="server" 
                                ContextTypeName="DataClassesDataContext" 
                                Select="new (sql_Consumer_ID, sql_Consumer_name + sql_manufacturer as ConsumerName)" 
                                TableName="tbl_Consumers_0s"
                                Where="HasTemperature == true" >
                            </asp:LinqDataSource>
                            <asp:LinqDataSource ID="LinqCounters" runat="server" 
                                ContextTypeName="DataClassesDataContext" 
                                Select="new (sql_counter_ID, tbl_CounterType.sql_description + '-' + T_Tariff.TariffName as counterType)" 
                                TableName="tbl_Counters" 
                                Where="sql_Consumer_ID == @ConsumerID"><%-- && IsSplitted == false --%>
                                <WhereParameters>
                                    <asp:ControlParameter ControlID="ConsumerCombo" PropertyName="SelectedValue"
                                        Type="Int32" DefaultValue="0" Name="ConsumerID" />
                                </WhereParameters>
                            </asp:LinqDataSource>
                            <asp:LinqDataSource ID="LinqPeriods" runat="server" 
                                ContextTypeName="DataClassesDataContext" 
                                TableName="T_TimePeriods">
                            </asp:LinqDataSource>
                            <table class="DetailsFormViewLayout" >
                                <tr class="DetailsFormViewHeader">
                                    <td colspan="2">תקלה</td>
                                </tr>
                                <tr>
                                    <td>
                                        שם\תאור:
                                    </td>
                                    <td>
                                        <asp:Textbox ID="txtName" runat="server" Width="200px" />
                                        <asp:RequiredFieldValidator runat="server" ControlToValidate="txtName" 
                                            ErrorMessage="הכנס שם" Display="Dynamic" Text="*" 
                                            ValidationGroup="AllValidators" ID="RequiredFieldValidator1"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        צרכן:
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ConsumerCombo" runat="server" AutoPostBack="true"
                                            DataSourceID="LinqConsumers" 
                                            DataTextField="ConsumerName" 
                                            DataValueField="sql_Consumer_ID" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        סוג מונה ותעריף:
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="CounterCombo" runat="server"
                                            DataSourceID="LinqCounters" 
                                            DataTextField="counterType" 
                                            DataValueField="sql_counter_ID" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        שינוי טמפרטורה (חימום\קירור) ב:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtTempChange" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator runat="server" ControlToValidate="txtTempChange" 
                                            ErrorMessage="הכנס שינוי טמפרטורה" Display="Dynamic" Text="*" 
                                            ValidationGroup="AllValidators" ID="RequiredFieldValidator3"></asp:RequiredFieldValidator>
                                        מעלות
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        אמור לצרוך:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtThreshold" runat="server" />
                                        <asp:RequiredFieldValidator runat="server" ControlToValidate="txtThreshold" 
                                            ErrorMessage="הכנס סף" Display="Dynamic" Text="*" 
                                            ValidationGroup="AllValidators" ID="RequiredFieldValidator2"></asp:RequiredFieldValidator>
                                        קוט"ש
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        אם הצריכה:
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="SignCombo" runat="server" Width="50px">
                                            <asp:ListItem>&gt;</asp:ListItem>
                                            <asp:ListItem>&lt;</asp:ListItem>
<%--                                            <asp:ListItem>&lt;=</asp:ListItem>
                                            <asp:ListItem>&gt;=</asp:ListItem>--%>
                                        </asp:DropDownList>
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" 
                                            FilterType="Numbers,Custom" ValidChars="."
                                            runat="server" Enabled="True" TargetControlID="txtThreshold">
                                        </cc1:FilteredTextBoxExtender>
                                        &nbsp;מהצריכה הנורמלית&nbsp;ב-
                                        <asp:DropDownList ID="ToleranceCombo" runat="server">
                                            <asp:ListItem Text="30%" Value="0.3" />
                                            <asp:ListItem Text="50%" Value="0.5" />
                                        </asp:DropDownList>
                                    </td>
                                </tr><%-- 
                                <tr>
                                    <td>
                                        קריטיות:
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="PriorityCombo" runat="server">
                                            <asp:ListItem>נמוך</asp:ListItem>
                                            <asp:ListItem>בינוני</asp:ListItem>
                                            <asp:ListItem>גבוע</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>--%>
                                <tr class="DetailsFormViewHeader">
                                    <td colspan="2">התראה</td>
                                </tr>
                                <tr>
                                    <td>
                                        סוג הודעה:
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkSms" runat="server" Text="סמס" />
                                        &nbsp;
                                        <asp:CheckBox ID="chkEmail" runat="server" Text='דוא"ל' />
                                    </td>
                                </tr>
                                <tr class="DetailsFormViewHeader">
                                    <td colspan="2">תזמון</td>
                                </tr>
                                <tr>
                                    <td>
                                        בדיקה ראשונה ב:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtDateFrom" runat="server" Width="100px" ></asp:TextBox>
                                        <asp:Image ID="Image1" runat="server" ImageUrl="~/images/SmallCalendar.gif" />
                                        <cc1:CalendarExtender ID="txtDateFrom_CalendarExtender" runat="server" 
                                            Enabled="True" TargetControlID="txtDateFrom" PopupButtonID="Image1" Format="yyyy-MM-dd">
                                        </cc1:CalendarExtender>
                                        <ew:TimePicker ID="TimePicker1" runat="server" Width="60px" 
                                            MinuteInterval="OneHour" NumberOfColumns="3" PopupLocation="Bottom" 
                                            PopupWidth="120px" Scrollable="False" ImageUrl="~/images/timepicker.gif" 
                                            MilitaryTime="True" >
                                            <ButtonStyle BorderStyle="None" Width="25px" />
                                        </ew:TimePicker>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        לבדוק כל:                                     </td>
                                    <td>
                                        <asp:DropDownList ID="CheckIntervalCombo" runat="server"
                                            DataSourceID="LinqPeriods" 
                                            DataTextField="Name" 
                                            DataValueField="Hours" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        עד לשעה:
                                    </td>
                                    <td>
                                        <ew:TimePicker ID="TimePicker2" runat="server" Nullable="true" 
                                            ShowClearTime="true" ClearTimeText="מחק" Width="60px" 
                                            MinuteInterval="OneHour" NumberOfColumns="3" PopupLocation="Bottom" 
                                            PopupWidth="120px" Scrollable="False" ImageUrl="~/images/timepicker.gif" 
                                            MilitaryTime="True" >
                                            <ButtonStyle BorderStyle="None" Width="25px" />
                                        </ew:TimePicker>
                                    </td>
                                </tr>
                                <tr class="DetailsFormViewHeader">
                                    <td colspan="2">מצב</td>
                                </tr>
                                <tr>
                                    <td>
                                        פעיל:
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkActive" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>        
                            <asp:Button ID="btnDel" Text="מחק" runat="server" onclick="btnDel_Click" OnClientClick="return confirm('?למחוק את הרשומה')"/>&nbsp;
                            <asp:Button ID="btnSave" Text="שמור" runat="server" ValidationGroup="AllValidators" onclick="btnSave_Click"/>&nbsp;
                            <asp:Button ID="btnCancel" Text="בטל" runat="server" PostBackUrl="~/MemberPages/HeatAlarmsList.aspx"/>&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
        </div>
</asp:Content>

