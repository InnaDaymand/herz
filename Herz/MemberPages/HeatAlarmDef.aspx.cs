﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MemberPages_HeatAlarmDef : SecuredPage
{
    private DataClassesDataContext db = new DataClassesDataContext();
    private T_HeatAlarm alarm;

    protected void Page_Load(object sender, EventArgs e)
    {
        System.Web.UI.WebControls.Image ti = (System.Web.UI.WebControls.Image)Master.FindControl("TitleImage");
        ti.ImageUrl = "~/SeenergImg/Titles/Title-9.png";
        int alarmId = int.Parse(Request.QueryString["AlarmID"]);

        if (alarmId > 0)
        {
            alarm = (from a in db.T_HeatAlarms
                     where a.HAlarmID == alarmId
                     select a).First();

            if (!IsPostBack)
            {
                txtName.Text = alarm.Name;
                ConsumerCombo.SelectedValue = alarm.tbl_Counter.sql_Consumer_ID.ToString();
                CounterCombo.SelectedValue = alarm.CounterID.ToString();
                txtTempChange.Text = alarm.TemperChange.ToString();
                SignCombo.SelectedValue = alarm.Sign;
                txtThreshold.Text = alarm.BaseValue.ToString();
                //ToleranceCombo.SelectedValue = alarm.Tolerance;
                ToleranceCombo.SelectedValue = alarm.Tolerance.ToString();
                chkSms.Checked = alarm.SendSms;
                chkEmail.Checked = alarm.SendEmail;
                txtDateFrom.Text = alarm.CheckTime.Date.ToString("yyyy-MM-dd");
                TimePicker1.SelectedTime = alarm.CheckTime;
                CheckIntervalCombo.SelectedValue = alarm.CheckInterval.ToString();
                if (alarm.StopTime.HasValue)
                    TimePicker2.SelectedTime = alarm.StopTime.Value;
                chkActive.Checked = alarm.IsActive;
            }
        }
        else
        {
            alarm = new T_HeatAlarm();
            if (!IsPostBack)
            {
                txtDateFrom.Text = DateTime.Now.ToString("yyyy-MM-dd");
                //PeriodCombo.SelectedValue = "24";
                CheckIntervalCombo.SelectedValue = "24";
            }

            btnDel.Enabled = false;
        }

    }

    protected void Page_PreRenderComplete(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            CounterCombo.SelectedValue = alarm.CounterID.ToString();
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        //alarm = new T_Alarm();

        alarm.Name = txtName.Text.Trim();
        alarm.CounterID = int.Parse(CounterCombo.SelectedValue);
        alarm.TemperChange = int.Parse(txtTempChange.Text);
        alarm.Sign = SignCombo.SelectedValue;
        alarm.BaseValue = double.Parse(txtThreshold.Text);
        //alarm.Tolerance = ToleranceCombo.SelectedValue;
        alarm.Tolerance = double.Parse(ToleranceCombo.SelectedValue);
        if (alarm.Sign.Contains('<'))
        {
            alarm.LowerLimit = 0;
            //alarm.UpperLimit = alarm.BaseValue * (1 - double.Parse(alarm.Tolerance));
            alarm.UpperLimit = alarm.BaseValue * (1 - alarm.Tolerance);
        }
        else
        {
            //alarm.LowerLimit = alarm.BaseValue * (1 + double.Parse(alarm.Tolerance));
            alarm.LowerLimit = alarm.BaseValue * (1 + alarm.Tolerance);
            alarm.UpperLimit = double.MaxValue;
        }
        alarm.IsActive = chkActive.Checked;
        alarm.SendEmail = chkEmail.Checked;
        alarm.SendSms = chkSms.Checked;
        alarm.CheckTime = DateTime.Parse(txtDateFrom.Text).Date + TimePicker1.SelectedTime.TimeOfDay;
        alarm.CheckInterval = int.Parse(CheckIntervalCombo.SelectedValue);
        alarm.StopTime = TimePicker2.SelectedValue;

        //if (Request.QueryString["AlarmID"] == "0")
        if (alarm.HAlarmID == 0)
            db.T_HeatAlarms.InsertOnSubmit(alarm);

        db.SubmitChanges();

        Response.Redirect("HeatAlarmsList.aspx");
    }
    protected void btnDel_Click(object sender, EventArgs e)
    {
        db.T_HeatAlarms.DeleteOnSubmit(alarm);

        try
        {
            db.SubmitChanges();
        }
        catch
        {
        }

        Response.Redirect("HeatAlarmsList.aspx");
    }
}
