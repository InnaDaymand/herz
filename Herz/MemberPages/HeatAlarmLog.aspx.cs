﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MemberPages_HeatAlarmLog : SecuredPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        System.Web.UI.WebControls.Image ti = (System.Web.UI.WebControls.Image)Master.FindControl("TitleImage");
        ti.ImageUrl = "~/SeenergImg/Titles/Title-4.png";
        if (!IsPostBack)
        {
            if (!_user.IsAdmin())//not admin
            {
                //GridView1.Columns[5].Visible = false;

                cmbClients.Visible = false;
                lblFilter.Visible = false;
            }
            else
            {
                cmbClients.DataSource = LinqClients;
                cmbClients.DataBind();
                cmbClients.Items.Insert(0, "כל הלקוחות");
            }
        }
        if (!_user.IsAdmin())//not admin
        {
            DataClassesDataContext db = new DataClassesDataContext();
            GridView1.DataSourceID = "";
            var clientIDs = DataClassLoader.ClientsAllowedForUser(db, _user.ID).Select(x => x.ID);
            GridView1.DataSource = from alarm
                                       in db.T_HeatAlarmLogs.Where(c => clientIDs.Contains(c.T_HeatAlarm.tbl_Counter.tbl_Consumers_0.sql_clientID))
                                   orderby alarm.CheckTime descending
                                   select new
                                   {
                                       name = alarm.T_HeatAlarm.Name,
                                       consumer = alarm.T_HeatAlarm.tbl_Counter.tbl_Consumers_0.sql_Consumer_name,
                                       counter = alarm.T_HeatAlarm.tbl_Counter.tbl_CounterType.sql_description + '-' + alarm.T_HeatAlarm.tbl_Counter.T_Tariff.TariffName,
                                       alarm.CheckTime,
                                       alarm.Value,
                                       alarm.T_HeatAlarm.Tolerance,
                                       client = alarm.T_HeatAlarm.tbl_Counter.tbl_Consumers_0.T_Client.Name
                                   };
            GridView1.PageIndexChanging += new GridViewPageEventHandler(GridView1_PageIndexChanging);
        }
    }

    void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataBind();
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if((double)DataBinder.Eval(e.Row.DataItem,"Tolerance")<.5)
                e.Row.Cells[4].BackColor = System.Drawing.Color.Yellow;
            else
                e.Row.Cells[4].BackColor = System.Drawing.Color.Tomato;
        }
    }
    protected void cmbClients_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (cmbClients.SelectedIndex == 0)//no filter
            LinqAlarms.Where = "";
        else
        {
            //LinqAlarms.Where = "T_Alarm.tbl_Counter.tbl_Consumers_0.tbl_Client.sql_clientID == @ClientID";
            LinqAlarms.Where = "T_HeatAlarm.tbl_Counter.tbl_Consumers_0.sql_clientID == @ClientID";
            LinqAlarms.WhereParameters.Clear();
            LinqAlarms.WhereParameters.Add("ClientID", TypeCode.Int32, cmbClients.SelectedValue);
        }
        GridView1.DataBind();
    }
}
