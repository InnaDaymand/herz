﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MemberPages/MemberSg.master" AutoEventWireup="true" CodeFile="HeatAlarmsList.aspx.cs" Inherits="MemberPages_HeatAlarmsList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="based" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <div  align="center">
    <table width="640px">
        <tr>
            <td>
                <asp:Label ID="lblFilter" runat="server" Text="סינון לפי לקוח" />
                <asp:DropDownList ID="cmbClients" runat="server" AutoPostBack="True" 
                    DataTextField="clientName" DataValueField="ID" 
                    onselectedindexchanged="cmbClients_SelectedIndexChanged">
                </asp:DropDownList>
                <asp:LinqDataSource ID="LinqClients" runat="server" 
                    ContextTypeName="DataClassesDataContext" 
                    Select="new (ID, Name as clientName)" 
                    TableName="T_Clients">
                </asp:LinqDataSource>
            </td>
        </tr>
        <tr>
            <td  align="center" class="bgBoxTable" >
                <asp:LinqDataSource ID="LinqAlarms" runat="server" 
                    ContextTypeName="DataClassesDataContext" 
                    Select="new (
                            HAlarmID, 
                            Name,                                     
                            tbl_Counter.tbl_Consumers_0.sql_Consumer_name as consumer,
                            tbl_Counter.tbl_CounterType.sql_description + '-' + tbl_Counter.T_Tariff.TariffName as counter,
                            Tolerance,
                            IsActive,
                            tbl_Counter.tbl_Consumers_0.T_Client.Name as client)"
                    TableName="T_HeatAlarms" >
                </asp:LinqDataSource>
                <br />
                <asp:GridView ID="grdAlarms" runat="server" 
                    AutoGenerateColumns="False" DataKeyNames="HAlarmID" DataSourceID="LinqAlarms" 
                    onselectedindexchanged="grdAlarms_SelectedIndexChanged" Font-Names="Arial" 
                    Font-Size="13pt" Width="560px" BackColor="White">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" SelectText="בחר" />
                        <asp:BoundField DataField="HAlarmID" HeaderText="קוד" />
                        <asp:BoundField DataField="Name" HeaderText="שם" />
                        <asp:BoundField DataField="consumer" HeaderText="צרכן" />
                        <asp:BoundField DataField="counter" HeaderText="מונה ותעריף" />
                        <asp:BoundField DataField="Tolerance" HeaderText="רמה" DataFormatString="{0:P0}" />
                        <asp:CheckBoxField DataField="IsActive" HeaderText="פעיל" />
                        <asp:BoundField DataField="client" HeaderText="לקוח" />
                    </Columns>
                    <HeaderStyle CssClass="DetailsFormViewHeader"></HeaderStyle>
                    <RowStyle ForeColor="#333333"></RowStyle>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnNew" runat="server" Text="חדש" PostBackUrl="~/MemberPages/HeatAlarmDef.aspx?AlarmID=0"  CssClass="ButtonRes" />
            </td>
        </tr>
    </table>
        </div>
</asp:Content>

