﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MemberPages_HeatAlarmsList : SecuredPage
{
    //DataClassLoader _dLoader = new DataClassLoader();
    //tbl_Client _client;

    protected void Page_Load(object sender, EventArgs e)
    {
        System.Web.UI.WebControls.Image ti = (System.Web.UI.WebControls.Image)Master.FindControl("TitleImage");
        ti.ImageUrl = "~/SeenergImg/Titles/Title-8.png";
        if (!IsPostBack)
        {
            //tbl_Client _client = Session["client"] as tbl_Client;
            //if (!_client.sql_role)//not admin
            //T_User user = Session["user"] as T_User;
            if (!_user.IsAdmin())//not admin
            {
                //LinqAlarms.Where = "tbl_Counter.tbl_Consumers_0.tbl_Client.sql_clientID == @ClientID";
                //LinqAlarms.WhereParameters.Add("ClientID", TypeCode.Int32, _client.sql_clientID.ToString());
                DataClassesDataContext db = new DataClassesDataContext();
                grdAlarms.DataSourceID = "";
                var clientIDs = DataClassLoader.ClientsAllowedForUser(db, _user.ID).Select(x => x.ID);
                grdAlarms.DataSource = from alarm
                                           in db.T_HeatAlarms.Where(c => clientIDs.Contains(c.tbl_Counter.tbl_Consumers_0.sql_clientID))
                                       select new
                                       {
                                           alarm.HAlarmID,
                                           alarm.Name,
                                           consumer = alarm.tbl_Counter.tbl_Consumers_0.sql_Consumer_name,
                                           counter = alarm.tbl_Counter.tbl_CounterType.sql_description + '-' + alarm.tbl_Counter.T_Tariff.TariffName,
                                           alarm.Tolerance,
                                           alarm.IsActive,
                                           client = alarm.tbl_Counter.tbl_Consumers_0.T_Client.Name
                                       };
                //grdAlarms.Columns[7].Visible = false;

                cmbClients.Visible = false;
                lblFilter.Visible = false;
            }
            else
            {
                cmbClients.DataSource = LinqClients;
                cmbClients.DataBind();
                cmbClients.Items.Insert(0, "כל הלקוחות");
            }
        }
    }
    protected void grdAlarms_SelectedIndexChanged(object sender, EventArgs e)
    {
        Response.Redirect("HeatAlarmDef.aspx?AlarmID=" + grdAlarms.SelectedRow.Cells[1].Text);
    }
    protected void cmbClients_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (cmbClients.SelectedIndex == 0)//no filter
            LinqAlarms.Where = "";
        else
        {
            LinqAlarms.Where = "tbl_Counter.tbl_Consumers_0.tbl_Client.sql_clientID == @ClientID";
            LinqAlarms.WhereParameters.Clear();
            LinqAlarms.WhereParameters.Add("ClientID", TypeCode.Int32, cmbClients.SelectedValue);
        }
        grdAlarms.DataBind();
    }
}
