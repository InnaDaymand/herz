﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MemberPages_MemberRu : System.Web.UI.MasterPage
{
    string nop = "";

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["user"] == null)
            Response.Redirect("~/Login.ru.aspx");
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        T_User tu = (T_User)Session["user"];
        ImgAdmin.Visible = false;
        ImgAdmin.Visible = (bool)tu.IsAdmin();
        HorMenu.Items[2].Enabled = AllFunctions.HorMenuNo2;
        HorMenu.Items[6].Enabled = AllFunctions.HorMenuNo2;

        #region Building Tree View

        if (tvMenu.Nodes.Count == 0)
        {

            tvMenu.Nodes.Clear();
            string SQL = "EXEC [dbo].[S_GetUsersSubmenuItems] " + tu.ID.ToString() + ", 2";

            DataTable dtb = AllFunctions.Populate(SQL);
            if (dtb.Columns.Count == 5)
            {
                FillTree(SQL);
            }
            else
            {
                TreeNode tn = new TreeNode();
                TreeNode tm = new TreeNode();
                TreeNode tk = new TreeNode();

                string s1 = "";
                string s2 = "";
                string s3 = "";
                for (int i = 0; i < dtb.Rows.Count; i++)
                {
                    s1 = dtb.Rows[i][1].ToString().Trim();
                    s2 = dtb.Rows[i][0].ToString();
                    s3 = dtb.Rows[i][2].ToString();
                    tn = null;
                    tn = new TreeNode(s1, s2, "", "", "");
                    tvMenu.Nodes.Add(tn);
                }
            }
        }

        #endregion

        #region HorMenu

        if (!IsPostBack)
        {
            HorMenu.Items[1].Selectable = false;
            if (AllFunctions.EnchParam)
            {
                HorMenu.Items[1].Selectable = true;
            }
            if (tu.ID == 13)
            {
                HorMenu.Items[2].Enabled = false;
            }

            string SQL = "SELECT COUNT(*) FROM [ForKrakovich].[dbo].[ProfileReport] WHERE [UsID] = " + tu.ID.ToString();
            string Otvet = AllFunctions.GetScalar(SQL);

            if (!tu.IsAdmin() && Otvet != "0")
            {
                MenuItem mt = new MenuItem("Эффективность");  // חיסכון פוטנציאלי
                SQL = "SELECT [PrID], ISNULL([RName], [Name]) AS Name FROM [ForKrakovich].[dbo].[ProfileReport] WHERE [UsID] = " + tu.ID.ToString();
                DataTable dtb = AllFunctions.Populate(SQL);
                MenuItem[] mii = new MenuItem[dtb.Rows.Count];
                for (int i = 0; i < dtb.Rows.Count; i++)
                {
                    mii[i] = new MenuItem(dtb.Rows[i][1].ToString());
                    mii[i].NavigateUrl = "~/MemberPages/PotentialEconomyRu.aspx?prid=" + dtb.Rows[i][0].ToString();
                    mt.ChildItems.Add(mii[i]);
                }
                HorMenu.Items[6].ChildItems.Add(mt);
            }


            //if ((tu.ID != 6) && (tu.ID != 13))
            //{
            //    MenuItem mi = new MenuItem("יומן תקלות חימום", "halarmlog", "", "~/MemberPages/HeatAlarmLog.aspx", "");
            //    HorMenu.Items[4].ChildItems.Add(mi);
            //    MenuItem mt = new MenuItem("שינוי הרגלי צריכה", "taoz", "", "~/MemberPages/Taoz.aspx", "");
            //    HorMenu.Items[6].ChildItems.Add(mt);
            //}

        }
        #endregion

    }

    protected void btnLogout_Click(object sender, EventArgs e)
    {
        //clear cookies
        Response.Cookies["user"].Expires = DateTime.MinValue;
        Response.Cookies["pass"].Expires = DateTime.MinValue;
        MemHist.MemClean();
        Response.Redirect("~/Login.ru.aspx");
    }

    protected void btnForward_Click(object sender, ImageClickEventArgs e)
    {
        MemHist.HistNow = 1;
        MemHist.byArrow = true;
        string UF = MemHist.HistPage;
        Response.Redirect(UF);
    }

    protected void btnBack_Click(object sender, ImageClickEventArgs e)
    {
        MemHist.HistNow = -1;
        MemHist.byArrow = true;
        string UF = MemHist.HistPage;
        Response.Redirect(UF);
    }

    protected void HorMenu_MenuItemClick(object sender, MenuEventArgs e)
    {
        MenuItem t = e.Item;
        string s1 = t.Text;
        string s2 = t.Value.Substring(8);
        string s3 = t.Value.Substring(0, 1);
        if (s3 == "0")
        {
            AllFunctions.EnchParam = false;
        }
        else
        {
            AllFunctions.EnchParam = true;
            AllFunctions.ConsID = s2;
            AllFunctions.ConsName = s1;
        }
    }

    protected void tvMenu_SelectedNodeChanged(object sender, EventArgs e)
    {
        //AllFunctions.HorMenuNo2 = true;

        TreeNode tn = new TreeNode();
        TreeNode tm = new TreeNode();
        TreeNode tk = new TreeNode();

        string S0 = "";
        MenuItem smi0 = new MenuItem();
        MenuItem smi1 = new MenuItem();
        MenuItem mip = HorMenu.Items[0];
        string s1 = "";
        string s2 = "";
        string s3 = "";


        tn = tvMenu.SelectedNode;
        S0 = tn.Value.ToString();

        DataClassesDataContext dc = new DataClassesDataContext();
        string CS = dc.Connection.ConnectionString;
        string SQL = "EXEC dbo.S_GetConsumersByClientForSubmenu " + S0 + ", 2";

        DataSet dst = new DataSet();
        SqlDataAdapter sda = new SqlDataAdapter();
        DataTable dtb = new DataTable();

        dst = new DataSet();
        sda = new SqlDataAdapter();

        SqlConnection CN = new SqlConnection(CS);

        SqlCommand selectCMD = new SqlCommand(SQL, CN);
        selectCMD.CommandTimeout = 0;

        sda.SelectCommand = selectCMD;

        CN.Open();

        sda.Fill(dst, "ItemsMenu");

        CN.Close();

        dtb = dst.Tables[0];

        for (int i = 0; i < dtb.Rows.Count; i++)
        {
            s1 = dtb.Rows[i][0].ToString();
            s2 = dtb.Rows[i][1].ToString();
            s3 = dtb.Rows[i][2].ToString();
            tk = null;
            tk = new TreeNode(s2, s1, "", s3, "");
            tn.ChildNodes.Add(tk);
        }
        tn.ExpandAll();

    }

    void FillTree(string SQL)
    {
        string s0, s1, s3;
        DataTable dt = AllFunctions.Populate(SQL);
        tvMenu.Nodes.Clear();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            s0 = dt.Rows[i][0].ToString();
            s1 = dt.Rows[i][1].ToString();
            TreeNode n0 = new TreeNode(s1, s0);
            string un = dt.Rows[i][3].ToString();
            if (un == "0")
            {
                tvMenu.Nodes.Add(n0);
            }
            else
            {
                TreeNode nnt = GetNodeByID(un);
                if (dt.Rows[i][2].ToString() != "#")
                {
                    s3 = "DataChartTaozRu.aspx?ConsumersList=" + s0 + "&Names=" + s1 + "&State=0&EPID=" + s0;
                    n0.NavigateUrl = s3;
                }
                nnt.ChildNodes.Add(n0);
            }
        }
        tvMenu.ExpandDepth = 1;
        //tvMenu.ExpandAll();
    }

    TreeNode GetNodeByID(string sID)
    {
        TreeNode t0 = new TreeNode();
        string vps = "";
        for (int i = 0; i < tvMenu.Nodes.Count; i++)
        {
            nop = "";
            vps = CheckNode(tvMenu.Nodes[i], sID);
            if (vps != "")
            {
                t0 = tvMenu.FindNode(vps);
            }
        }

        return t0;
    }

    string CheckNode(TreeNode cTn, string sFF)
    {
        if (cTn.Value.ToString() == sFF)
        {
            nop = cTn.ValuePath;
        }
        else
        {
            for (int j = 0; j < cTn.ChildNodes.Count; j++)
            {
                if (CheckNode(cTn.ChildNodes[j], sFF) != "")
                {
                    break;
                }
            }
        }
        return nop;
    }
}

