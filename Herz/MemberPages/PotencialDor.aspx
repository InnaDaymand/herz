﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MemberPages/MemberSg.master" AutoEventWireup="true" CodeFile="PotencialDor.aspx.cs" Inherits="MemberPages_PotencialDor" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>

<%@ Register assembly="eWorld.UI, Version=2.0.6.2393, Culture=neutral, PublicKeyToken=24d65337282035f2" namespace="eWorld.UI" tagprefix="ew" %>

<%@ Register Src="~/Controls/CalendarMD.ascx" TagName="CalendarMD" TagPrefix="cem" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="based" Runat="Server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ForMenu" Runat="Server">
    <div align="center" dir="rtl" style="height:auto">
        <table>
            <tr>
                <td>
                    <asp:Label Text="שנה" runat="server" />
                </td>
                <td>
                    <asp:TextBox runat="server" Text="2017" ID="txtYear" />
                </td>
                <td>
                    <asp:Label Text="חודש" runat="server" />
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtMonth" Text="6" />
                </td>
                <td>
                    <asp:Button Text="הצג דו''ח" runat="server" ID="DorShow" OnClick="DorShow_Click" />
                </td>
            </tr>
        </table>
        <br />

        <asp:Panel ID="pnlDor" runat="server" Height="132px" Width="400px" Visible="false">
            <asp:Label Text="" runat="server" ID="Res" />
            <br />
            <asp:Label Text="" runat="server" ID="shefel" />
            <br />
            <asp:Label Text="" runat="server" ID="geva" />
            <br />
            <asp:Label Text="" runat="server" ID="pisga" />
            <br />
            <asp:Label Text="" runat="server" ID="power" />
            <br />
        </asp:Panel>
        <br />




        <asp:CheckBoxList runat="server" Width="100%" Height="102px" BorderStyle="Inset" ID="chbConsumers" RepeatColumns="4">
        </asp:CheckBoxList>
        <br />
        <asp:GridView ID="GV" runat="server">
        </asp:GridView>
        <br />
        <br />

    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">
</asp:Content>

