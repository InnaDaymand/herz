﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class MemberPages_PotencialDor : System.Web.UI.Page
{
    string PrID, typ;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            PrID = Request.QueryString["prid"];
            string SQL = "SELECT [TypeID] FROM [ForKrakovich].[dbo].[ProfileReport] WHERE PrID = " + PrID;
            typ = AllFunctions.GetScalar(SQL);
            

            if (typ == "5")
            {
                SQL = "SELECT [Consumers] AS ID, (SELECT [sql_Consumer_name] FROM [dbo].[tbl_Consumers_0] WHERE [sql_Consumer_ID] = [Consumers]) AS NAME "
                    + " FROM[ForKrakovich].[dbo].[SearchConsumers] WHERE[PrID] = "  + PrID + " ORDER BY NAME ";
                chbConsumers.DataSource = AllFunctions.Populate(SQL);
                chbConsumers.DataTextField = "NAME";
                chbConsumers.DataValueField = "ID";
                chbConsumers.DataBind();

                Res.Text = PrID;
            }
            else
            {
                pnlDor.Visible = true;
                chbConsumers.Visible = false;
                GV.Visible = false;
            }
        }
    }

    protected void DorShow_Click(object sender, EventArgs e)
    {
        string SS = "";
        string SQL = "";

        if (typ == "2")
        {
            SQL = "EXEC Util_GetTaozForKrakovich " + txtYear.Text + ", " + txtMonth.Text + ", '268, 269'";
            DataTable dt = AllFunctions.Populate(SQL);

            Res.Text = dt.Rows[0][0].ToString();
            shefel.Text = "שפל - " + dt.Rows[0][1].ToString() + " שעות ";
            geva.Text = "גבה - " + dt.Rows[0][2].ToString() + " שעות ";
            pisga.Text = "פסגה - " + dt.Rows[0][3].ToString() + " שעות ";
            power.Text = "פסגה - " + dt.Rows[0][4].ToString() + " קוט''ש ";
        }
        else
        {
            for (int i = 0; i < chbConsumers.Items.Count; i++)
            {
                if (chbConsumers.Items[i].Selected)
                {
                    SS += chbConsumers.Items[i].Value + ", ";
                }
            }
            SS = SS.Substring(0, SS.Length - 2);

            if (SS.Length <= 0)
            {
                return;
            }


            SQL = "EXEC [dbo].[Util_GetUnileverEconomy]	@YEAR = " + txtYear.Text + ",  @MONTH = " + txtMonth.Text + ", @ConsID = N'" + SS + "', @ProfileID = " + Res.Text ;
            GV.DataSource = AllFunctions.Populate(SQL);
            GV.DataBind();
        }
    }
}