﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MemberPages/MemberSg.master" AutoEventWireup="true" CodeFile="PotentialEconomy.aspx.cs" Inherits="MemberPages_PotentialEconomy" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>

<%@ Register assembly="eWorld.UI, Version=2.0.6.2393, Culture=neutral, PublicKeyToken=24d65337282035f2" namespace="eWorld.UI" tagprefix="ew" %>

<%@ Register Src="~/Controls/CalendarMD.ascx" TagName="CalendarMD" TagPrefix="cem" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="../StyleSheet.css" media="@media" rel="Stylesheet" type="text/css" />
    <style type="text/css">
        .auto-style2 {
            height: 20px;
        }
        .auto-style3 {
            height: 30px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="based" Runat="Server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
        <%--    <asp:MultiView ID="mvCC" runat="server" ActiveViewIndex="0">
        <asp:View runat="server" ID="ViewMenu">
        </asp:View>
    </asp:MultiView>--%>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ForMenu" Runat="Server">
    <div align="center" dir="rtl" style="height:auto">
        <table >
            <tr>
                <td align="center" colspan="3" style="align-content:flex-start; align-items:flex-start; align-self:center; ">
                    <asp:Image ImageUrl="~/PrintPages/NewLog.jpg" runat="server" CssClass="HiddenItem"  Height="120" Width="940" />
                </td>
            </tr>
            <tr align="center">
                <td class="auto-style2"><asp:Label ID="lblConsumer" runat="server" Font-Bold="True" Font-Names="Arial" 
                        Font-Size="18pt" ForeColor="#0066CC" CssClass="HiddenItem" >דוח חיסכון</asp:Label>
                </td>
            </tr>
            <tr>
                <td width="100%" align="center" valign="middle" class="auto-style3">
                    <asp:Label ID="lblTotal" runat="server" Font-Bold="True" Font-Names="Arial" 
                        Font-Size="15pt" CssClass="HiddenItem"  />
                </td>
            </tr>
            <tr>
                <td colspan="3" style="align-content:flex-start; align-items:flex-start; align-self:center; ">
                <p align="center" lang="he" style="font-family: Aharoni; font-size: 22px" class="UnprintedItem">
                    <a href="javascript:print()" dir="rtl">הדפס</a>
                </p>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="3" style="align-content:flex-start; align-items:flex-start; align-self:center; " class="UnprintedItem">
                    <cem:CalendarMD  runat="server" ID="Dates"  OnClickGraph="Dates_ClickGraph" OnClickTable="Dates_ClickTable" />
                </td>
            </tr>
            <tr>
                <td align="center" colspan="3" style="align-content:flex-start; align-items:flex-start; align-self:center;">
                    <asp:CheckBox Text="בחר הכל" runat="server" ID="SelAll" AutoPostBack="True" 
                        OnCheckedChanged="SelAll_CheckedChanged" CssClass="UnprintedItem"/>
                </td>
            </tr>
            <tr >
                <td align="center" colspan="3" style="align-content:center; align-items:center; align-self:center;" class="UnprintedItem">
                    <asp:CheckBoxList ID="clbCons" runat="server" DataTextField="Name" DataValueField="ID">
                    </asp:CheckBoxList>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="3" style="align-content:center; align-items:center; align-self:center;">
                    <asp:Label ID="DOH" runat="server" Font-Names="Arial" Font-Overline="False" Font-Size="X-Large" Font-Bold="True" />
                </td>
            </tr>
            <tr>
                <td align="center" colspan="3" style="align-content:center; align-items:center; align-self:center;">
                    <asp:GridView runat="server" ID="PriceGrid" Font-Names="Calibri" Font-Size="Large" >
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">
</asp:Content>

