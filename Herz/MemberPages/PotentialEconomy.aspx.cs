﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MemberPages_PotentialEconomy : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        System.Web.UI.WebControls.Image ti = (System.Web.UI.WebControls.Image)Master.FindControl("TitleImage");
        ti.ImageUrl = "~/SeenergImg/Titles/DohLeHisahon.png";

        Dates.btnName0 = "הצג דו''ח";
        Dates.btnName1 = "הצג דו''ח מפורט";
        Dates.ForTime1 = false;
        Dates.ForTime2 = false;

        if (!IsPostBack)
        {
            string SQL = "SELECT [Consumers] AS ID, (SELECT  [sql_Consumer_name] FROM [dbo].[tbl_Consumers_0] "
                + " WHERE [sql_Consumer_ID] = [Consumers]) AS Name FROM[ForKrakovich].[dbo].[SearchConsumers] WHERE [PrID] = " + Request.QueryString["prid"];
            clbCons.DataSource = AllFunctions.Populate(SQL);
            clbCons.DataTextField = "Name";
            clbCons.DataValueField = "ID";
            clbCons.DataBind();

            Dates.TillDate = DateTime.Today;
            Dates.FromDate = (DateTime.Today).AddDays(-7);
        }
        else
        {
            if (Dates.FromDate != Convert.ToDateTime(Session["FromDateDOH"]))
            {
                Session["FromDateDOH"] = Dates.FromDate.ToString("yyyy-MM-dd HH:mm:ss");
            }
            if (Dates.TillDate != Convert.ToDateTime(Session["TillDateDOH"]))
            {
                Session["TillDateDOH"] = Dates.TillDate.ToString("yyyy-MM-dd HH:mm:ss");
            }

            Dates.FromDate = Convert.ToDateTime(Session["FromDateDOH"]);
            Dates.TillDate = Convert.ToDateTime(Session["TillDateDOH"]);

        }
    }

    protected void Dates_ClickGraph(object sender, EventArgs e)
    {
        ////string nid = "";
        ////foreach (ListItem item in clbCons.Items)
        ////{
        ////    if (item.Selected) nid += item.Value.ToString() + ", ";
        ////}
        ////if (nid == "")
        ////{
        ////    return;
        ////}
        ////nid = nid.Substring(0, nid.Length - 2);
        ////DateTime dt0 = Dates.FromDate;
        ////DateTime dt1 = Dates.TillDate;

        ////string SQL = "EXEC [dbo].[Util_GetSpacesForKrakovich]  @FromDate = '" + dt0.ToString("yyyy-MM-dd") + "', @TillDate = '" 
        ////    + dt1.ToString("yyyy-MM-dd") + "', @IDs = N'" + nid + "', @ProfileID  = " + Request.QueryString["prid"];

        ////PriceGrid.DataSource = AllFunctions.Populate(SQL);
        ////PriceGrid.DataBind();

        ////lblTotal.Text = "מי " + dt0.ToString("yyyy-MM-dd") + " עד " + dt1.ToString("yyyy-MM-dd");
        string nid = "", nam = "";
        foreach (ListItem item in clbCons.Items)
        {
            if (item.Selected)
            {
                nid += item.Value.ToString() + ", ";
                nam += item.Text + ", ";
            }
        }
        if (nid == "")
        {
            return;
        }
        nid = nid.Substring(0, nid.Length - 2);
        nam = nam.Substring(0, nam.Length - 2);
        DateTime dt0 = Dates.FromDate;
        DateTime dt1 = Dates.TillDate;

        if (dt0 > dt1)
        {
            return;
        }


        string SQL = "EXEC [dbo].[Util_GetSpacesForKrakovich]  @FromDate = '" + dt0.ToString("yyyy-MM-dd") + "', @TillDate = '"
            + dt1.ToString("yyyy-MM-dd") + "', @IDs = N'" + nid + "', @ProfileID  = " + Request.QueryString["prid"];

        PriceGrid.DataSource = AllFunctions.Populate(SQL);
        PriceGrid.DataBind();


        if (PriceGrid.Rows.Count == 0)
        {
            return;
        }
        lblTotal.Text = "מי " + dt0.ToString("yyyy-MM-dd") + " עד " + dt1.ToString("yyyy-MM-dd");
        lblConsumer.Text = nam;
        int LastRow = PriceGrid.Rows.Count - 1;
        DOH.Text = "" + PriceGrid.Rows[LastRow].Cells[0].Text + ": " + PriceGrid.Rows[LastRow].Cells[PriceGrid.Rows[0].Cells.Count - 1].Text;

        PriceGrid.Visible = false;
        DOH.Visible = true;

    }

    protected void Dates_ClickTable(object sender, EventArgs e)
    {
        string nid = "", nam = "";
        foreach (ListItem item in clbCons.Items)
        {
            if (item.Selected)
            {
                nid += item.Value.ToString() + ", ";
                nam += item.Text + ", ";
            }
        }
        if (nid == "")
        {
            return;
        }
        nid = nid.Substring(0, nid.Length - 2);
        nam = nam.Substring(0, nam.Length - 2);
        DateTime dt0 = Dates.FromDate;
        DateTime dt1 = Dates.TillDate;

        if (dt0 > dt1)
        {
            return;
        }

        string SQL = "EXEC [dbo].[Util_GetSpacesForKrakovich]  @FromDate = '" + dt0.ToString("yyyy-MM-dd") + "', @TillDate = '"
            + dt1.ToString("yyyy-MM-dd") + "', @IDs = N'" + nid + "', @ProfileID  = " + Request.QueryString["prid"];

        PriceGrid.DataSource = AllFunctions.Populate(SQL);
        PriceGrid.DataBind();

        lblTotal.Text = "מי " + dt0.ToString("yyyy-MM-dd") + " עד " + dt1.ToString("yyyy-MM-dd");
        lblConsumer.Text = nam;

        //int LastRow = PriceGrid.Rows.Count - 1;
        //DOH.Text = "" + PriceGrid.Rows[LastRow].Cells[0].Text + ": " + PriceGrid.Rows[LastRow].Cells[PriceGrid.Rows[0].Cells.Count - 1].Text;

        PriceGrid.Visible = true;
        DOH.Visible = false;
    }

    protected void SelAll_CheckedChanged(object sender, EventArgs e)
    {
        bool rf = SelAll.Checked;
        foreach (ListItem item in clbCons.Items)
        {
            item.Selected = rf;
        }
        //mvCC.ActiveViewIndex = 0;
    }
}