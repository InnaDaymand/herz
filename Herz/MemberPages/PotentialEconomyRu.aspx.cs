﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MemberPages_PotentialEconomyRu : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        System.Web.UI.WebControls.Image ti = (System.Web.UI.WebControls.Image)Master.FindControl("TitleImage");
        ti.ImageUrl = "~/Rus/Titles/savIng_potential_RU.png";

        Dates.Lang = 2;

        if (!IsPostBack)
        {
            string SQL = "DECLARE @I INT; SELECT @I = [UsID] FROM[ForKrakovich].[dbo].[ProfileReport] WHERE [PrID] = " + Request.QueryString["prid"] + "; EXEC [dbo].[S_GetAllConsumersByUser] @I, 1, 2";
            clbCons.DataSource = AllFunctions.Populate(SQL);
            clbCons.DataTextField = "Name";
            clbCons.DataValueField = "OID";
            clbCons.DataBind();

            Dates.TillDate = DateTime.Today;
            Dates.FromDate = (DateTime.Today).AddDays(-7);
        }
        else
        {
            if (Dates.FromDate != Convert.ToDateTime(Session["FromDateDOH"]))
            {
                Session["FromDateDOH"] = Dates.FromDate.ToString("yyyy-MM-dd HH:mm:ss");
            }
            if (Dates.TillDate != Convert.ToDateTime(Session["TillDateDOH"]))
            {
                Session["TillDateDOH"] = Dates.TillDate.ToString("yyyy-MM-dd HH:mm:ss");
            }

            Dates.FromDate = Convert.ToDateTime(Session["FromDateDOH"]);
            Dates.TillDate = Convert.ToDateTime(Session["TillDateDOH"]);

        }
    }

    protected void Dates_ClickGraph(object sender, EventArgs e)
    {
        string nid = "", nam = "";
        foreach (ListItem item in clbCons.Items)
        {
            if (item.Selected)
            {
                nid += item.Value.ToString() + ", ";
                nam += item.Text + ", ";
            }
        }
        if (nid == "")
        {
            return;
        }
        nid = nid.Substring(0, nid.Length - 2);
        nam = nam.Substring(0, nam.Length - 2);
        DateTime dt0 = Dates.FromDate;
        DateTime dt1 = Dates.TillDate;

        if (dt0 > dt1)
        {
            return;
        }


        string SQL = "EXEC [dbo].[Util_GetSpacesForKrakovich]  @FromDate = '" + dt0.ToString("yyyy-MM-dd") + "', @TillDate = '"
            + dt1.ToString("yyyy-MM-dd") + "', @IDs = N'" + nid + "', @ProfileID  = " + Request.QueryString["prid"] + ", @Lang = 2";


        PriceGrid.DataSource = AllFunctions.Populate(SQL);
        PriceGrid.DataBind();

        for (int i = 0; i < PriceGrid.Rows.Count; i++)
        {
            PriceGrid.Rows[i].Visible = (bool)(PriceGrid.Rows[i].Cells[0].Text != "&nbsp;");
        }


        lblTotal.Text = "От " + dt0.ToString("yyyy-MM-dd") + " до " + dt1.ToString("yyyy-MM-dd");
        lblConsumer.Text = nam;
        PriceGrid.Visible = true;
        DOH.Visible = false;
    }

    protected void Dates_ClickTable(object sender, EventArgs e)
    {
        string nid = "", nam = "";
        foreach (ListItem item in clbCons.Items)
        {
            if (item.Selected)
            {
                nid += item.Value.ToString() + ", ";
                nam += item.Text + ", ";
            }
        }
        if (nid == "")
        {
            return;
        }
        nid = nid.Substring(0, nid.Length - 2);
        nam = nam.Substring(0, nam.Length - 2);
        DateTime dt0 = Dates.FromDate;
        DateTime dt1 = Dates.TillDate;

        if (dt0 > dt1)
        {
            return;
        }

        string SQL = "EXEC [dbo].[Util_GetSpacesForKrakovich]  @FromDate = '" + dt0.ToString("yyyy-MM-dd") + "', @TillDate = '"
            + dt1.ToString("yyyy-MM-dd") + "', @IDs = N'" + nid + "', @ProfileID  = " + Request.QueryString["prid"] + ", @Lang = 2";

        PriceGrid.DataSource = AllFunctions.Populate(SQL);
        PriceGrid.DataBind();

        lblTotal.Text = "מי " + dt0.ToString("yyyy-MM-dd") + " עד " + dt1.ToString("yyyy-MM-dd");
        lblConsumer.Text = nam;
        PriceGrid.Visible = true;
        DOH.Visible = false;
    }

    protected void SelAll_CheckedChanged(object sender, EventArgs e)
    {
        bool rf = SelAll.Checked;
        foreach (ListItem item in clbCons.Items)
        {
            item.Selected = rf;
        }
    }
}