﻿<%@ Page Language="C#" MasterPageFile="~/MemberPages/MemberSg.master" AutoEventWireup="true"  CodeFile="Taoz.aspx.cs" Inherits="_Default" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register Src="~/TableSeasonProfit.ascx" TagPrefix="uc" TagName="TableSeasonProfit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style4
        {
            height: 35px; 
        }
        .style5
        {
            background-color: #9498a1;
            x=#5d7b9d;
            color: White;
            height: 20px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="based" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <div align="center">
        <table width="640px">
            <tr>
                <td align="center" class="NewFont2">חישוב סידור עבודה אופטימלי</td>
            </tr>
            <tr>
                <td align="left">
                    <a href="javascript:tollbarPrintPage();" >
                        <img style="border-style:none" src="../images/btn_print.gif" onmouseover="this.src='../images/btn_printH.gif';" onmouseout="this.src='../images/btn_print.gif';" alt="Print" />
                    </a>
                </td>
            </tr>
            <tr>
                <td align="center">
<ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</ajaxToolkit:ToolkitScriptManager>
                    <cc1:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="2" Font-Names="Arial" 
                        Font-Size="16pt" CssClass="MyTabStyle" ForeColor="Black">
                        <cc1:TabPanel ID="TabPanel1" runat="server" HeaderText="סידור עבודה קיים" EnableTheming="True" Font-Italic="False" CssClass="MyTabStyle">
                            <ContentTemplate>
                            <asp:Table runat="server" Width="80%">
                                <asp:TableHeaderRow CssClass="TabHeaderStyle" runat="server" ClientIDMode="Inherit">
                                    <asp:TableHeaderCell Text="סידור עבודה קיים" BorderStyle="Solid" 
                                        BorderWidth="1px" runat="server" />
                                </asp:TableHeaderRow>
                                <asp:TableRow runat="server">
                                    <asp:TableCell HorizontalAlign="Center" runat="server">
                                        <asp:Table runat="server">
                                            <asp:TableRow runat="server">
                                                <asp:TableCell runat="server" Width="33%">
                                                <asp:RadioButton runat="server" GroupName="rdgVoltage" Text="מתח עליון" ID="rdbVoltageHigher" onclick="dispTaoz()"></asp:RadioButton>
                                                </asp:TableCell>
                                                <asp:TableCell runat="server" Width="33%">
                                                    <asp:Label runat="server" Text="הספק ממוצע, kW"></asp:Label>
                                                </asp:TableCell>
                                                <asp:TableCell runat="server" Width="34%"></asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow runat="server">
                                                <asp:TableCell runat="server">
                                                    <asp:RadioButton runat="server" GroupName="rdgVoltage" Checked="True" Text="מתח גבוה" ID="rdbVoltageHigh" onclick="dispTaoz()"></asp:RadioButton>
                                                </asp:TableCell>
                                                <asp:TableCell runat="server">
                                                    <asp:TextBox runat="server" ID="txtConsumption"></asp:TextBox>
                                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="txtConsumption" 
                                                        ErrorMessage="הכנס הספק" Display="Dynamic" Text="*" 
                                                        ValidationGroup="AllValidators" ID="RequiredFieldValidator1"></asp:RequiredFieldValidator>
                                                </asp:TableCell>
                                                <asp:TableCell runat="server">
                                                    <asp:CheckBox runat="server" AutoPostBack="False" Text="עבודה בשבת" ID="chkSabbat"></asp:CheckBox>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow runat="server">
                                                <asp:TableCell runat="server">
                                                    <asp:RadioButton runat="server" GroupName="rdgVoltage" Text="מתח נמוך" ID="rdbVoltageLow" onclick="dispTaoz()"></asp:RadioButton>
                                                </asp:TableCell>
                                                <asp:TableCell runat="server">
                                                    <asp:RangeValidator 
                                                        runat="server" MaximumValue="2147483648" MinimumValue="0" Type="Double" 
                                                        ControlToValidate="txtConsumption" ErrorMessage="הכנס מספר" Display="Dynamic" 
                                                        Text="נתונים שגויים" ValidationGroup="AllValidators" ID="RangeValidator1" />
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow runat="server">
                                    <asp:TableCell HorizontalAlign="Center" runat="server" CssClass="InputFormStyle">
                                        <asp:Table runat="server" ID="tblHours"></asp:Table>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow runat="server">
                                    <asp:TableCell HorizontalAlign="Center" runat="server">
                                        <asp:Button ID="btnCalc" runat="server" Text="חשב" ValidationGroup="AllValidators" CssClass="UnprintedItem" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                            </ContentTemplate>
                        </cc1:TabPanel>
                        <cc1:TabPanel ID="TabPanel2" runat="server" HeaderText="עלות החשמל">
                            <ContentTemplate>
                            <table class="ResultTable" >
                                <tr>
                                    <th class="TabHeaderStyle">עלות החשמל</th>
                                </tr>
                                <tr>
                                    <td style="background-color:White">
                                        <asp:Label ID="lblVoltage" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="background-color:White">
                                        <asp:Label ID="lblConsumption" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                    <table>
                                        <tr>
                                            <td style="min-width:200px;">
                                                </td>
                                            <td style="min-width:100px">
                                                קיץ</td>
                                            <td style="min-width:100px">
                                                חורף</td>
                                            <td style="min-width:100px">
                                                סתיו\אביב</td>
                                            <td style="min-width:100px">
                                                שנה</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                עלות החשמל במצב הנוכחי, ש"ח</td>
                                            <td>
                                                <asp:Label ID="lblCurSummer" runat="server" Text="0"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblCurWinter" runat="server" Text="0"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblCurSpring" runat="server" Text="0"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblCurYear" runat="server" Text="0"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                עלות החשמל במצב האופטימלי, ש&quot;ח</td>
                                            <td>
                                                <asp:Label ID="lblOptSummer" runat="server" Text="0"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblOptWinter" runat="server" Text="0"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblOptSpring" runat="server" Text="0"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblOptYear" runat="server" Text="0"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                חיסכון, ש&quot;ח</td>
                                            <td>
                                                <asp:Label ID="lblProfitSummer" runat="server" Text="0"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblProfitWinter" runat="server" Text="0"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblProfitSpring" runat="server" Text="0"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblProfitYear" runat="server" Text="0"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                    </td>
                                </tr>
                            </table>
                            </ContentTemplate>
                        </cc1:TabPanel>
                        <cc1:TabPanel ID="TabPanel3" runat="server" HeaderText="סידור עבודה מומלץ">
                            <ContentTemplate>
                                <cc1:Accordion ID="Accordion1" runat="server" HeaderCssClass="AccordionInactiveHeaderStyle" HeaderSelectedCssClass="AccordionActiveHeaderStyle">
                                    <Panes>
                                        <cc1:AccordionPane ID="AccordionPane1" runat="server">
                                            <Header>סידור עבודה מומלץ - קיץ
                                            </Header>
                                            <Content>
                                                <table class="ResultTable">
                                                <tr>
                                                    <td align="center" style="background-color:White" >
                                                        <asp:Table ID="tblHoursSummer" runat="server">
                                                        </asp:Table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center">
                                                        <uc:TableSeasonProfit ID="tblProfitSummer" runat="server" Season="summer" />
                                                    </td>
                                                </tr>
                                                </table>
                                            </Content>
                                        </cc1:AccordionPane>
                                        <cc1:AccordionPane ID="AccordionPane2" runat="server">
                                            <Header>סידור עבודה מומלץ - חורף
                                            </Header>
                                            <Content>
                                                <table class="ResultTable">
                                                <tr>
                                                    <td align="center" style="background-color:White">
                                                        <asp:Table ID="tblHoursWinter" runat="server">
                                                        </asp:Table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center">
                                                        <uc:TableSeasonProfit ID="tblProfitWinter" runat="server" Season="winter" />
                                                    </td>
                                                </tr>
                                                </table>
                                            </Content>
                                        </cc1:AccordionPane>
                                        <cc1:AccordionPane ID="AccordionPane3" runat="server">
                                            <Header>סידור עבודה מומלץ - סתיו\אביב
                                            </Header>
                                            <Content>
                                                <table class="ResultTable">
                                                <tr>
                                                    <td align="center" style="background-color:White">
                                                        <asp:Table ID="tblHoursSpring" runat="server">
                                                        </asp:Table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center">
                                                        <uc:TableSeasonProfit ID="tblProfitSpring" runat="server" Season="spring" />
                                                    </td>
                                                </tr>
                                                </table>
                                            </Content>
                                        </cc1:AccordionPane>
                                    </Panes>
                                </cc1:Accordion>
                            </ContentTemplate>
                        </cc1:TabPanel>
                        <cc1:TabPanel ID="TabPanel6" runat="server" HeaderText='הגדרות ומחירי התעו"ז'>
                            <ContentTemplate>
                            <table width="600px" >
                                <tr>
                                    <th class="style5">הגדרות ומחירי התעו"ז</th>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <img id="imgTaozHigher" src="https://www.iec.co.il/BusinessClients/PublishingImages/graph-taoz-metach-elion.gif" alt="פסגה" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <img id="imgTaozHigh" src="https://www.iec.co.il/BusinessClients/PublishingImages/graph-taoz-metach-gavoa.gif" alt="גבע" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <img id="imgTaozLow" src="https://www.iec.co.il/BusinessClients/PublishingImages/graph-taoz-metach-namuch.gif" alt="שפל" />
                                    </td>
                                </tr>
                            </table>
                            </ContentTemplate>
                        </cc1:TabPanel>
                    </cc1:TabContainer>
                </td>
            </tr>
        </table>
    </div>
    <script type="text/javascript" >
        function dispTaoz() {
            if (document.all.ctl00_ContentPlaceHolder2_TabContainer1_TabPanel1_rdbVoltageHigher.checked) {
                document.images.imgTaozHigher.style.display = 'block';
                document.images.imgTaozHigh.style.display = 'none';
                document.images.imgTaozLow.style.display = 'none';
            }
            else if (document.all.ctl00_ContentPlaceHolder2_TabContainer1_TabPanel1_rdbVoltageHigh.checked) {
                document.images.imgTaozHigher.style.display = 'none';
                document.images.imgTaozHigh.style.display = 'block';
                document.images.imgTaozLow.style.display = 'none';
            }
            else {
                document.images.imgTaozHigher.style.display = 'none';
                document.images.imgTaozHigh.style.display = 'none';
                document.images.imgTaozLow.style.display = 'block';
            }
        }
    </script>
    
    <script type="text/javascript">
        function tollbarPrintPage() {
            //var printElem = document.getElementById("printElem");
            var printElemCur = document.getElementById("ctl00_ContentPlaceHolder2_TabContainer1_TabPanel1");
            var printElemCost = document.getElementById("ctl00_ContentPlaceHolder2_TabContainer1_TabPanel2");
            //var printElemOpt = document.getElementById("TabContainer1_TabPanel3_Accordion1");
            var printElemSummerHeader = document.getElementById("ctl00_ContentPlaceHolder2_TabContainer1_TabPanel3_AccordionPane1_header");
            var printElemSummerContent = document.getElementById("ctl00_ContentPlaceHolder2_TabContainer1_TabPanel3_AccordionPane1_content");
            var printElemWinterHeader = document.getElementById("ctl00_ContentPlaceHolder2_TabContainer1_TabPanel3_AccordionPane2_header");
            var printElemWinterContent = document.getElementById("ctl00_ContentPlaceHolder2_TabContainer1_TabPanel3_AccordionPane2_content");
            var printElemSpringHeader = document.getElementById("ctl00_ContentPlaceHolder2_TabContainer1_TabPanel3_AccordionPane3_header");
            var printElemSpringContent = document.getElementById("ctl00_ContentPlaceHolder2_TabContainer1_TabPanel3_AccordionPane3_content");
            var printElemTaoz = document.getElementById("ctl00_ContentPlaceHolder2_TabContainer1_TabPanel6");

            if (null != printElemCur) {
                var headElem = document.getElementsByTagName("head");
                var headchangeRichTxtTBLStyle = "";
                if (null != headElem) {
                    headContent = headElem[0].innerHTML;
                }
                //var printTxt = printElemCur.innerHTML;

                //var params = "toolbar=yes, menubar=yes, location=no, scrollbars=auto";
                var params = "toolbar=1, menubar=1, location=0, scrollbars=yes";
                var printW = window.open("about:blank", "newW", params);

                printW.document.open("text/html", "replace");

                var windowContent = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">\n<html lang="he"><head>\n<meta http-equiv="Pragma" content="no-cache">';
                windowContent += headContent;

                windowContent += '</head>\n<body dir="rtl" onLoad="window.print();window.close();" style="text-align: center;margin-right: 40px">\n<table cellpadding="0" cellspacing="0" border="0" style="width: 600px"><tr><td>';
                //windowContent += '</head>\n<body dir="rtl" style="text-align: center; font-size:14px; margin-right: 40px">\n<table cellpadding="0" cellspacing="0" border="0" style="width: 600px"><tr><td>';
                windowContent += printElemCur.innerHTML; //printTxt;
                windowContent += '</td></tr><tr><td>';
                windowContent += printElemCost.innerHTML;
                //windowContent += '</td></tr><tr><td>';
                //windowContent += printElemOpt.innerHTML;
                windowContent += '</td></tr><tr><td><hr>';
                windowContent += printElemSummerHeader.outerHTML;
                windowContent += printElemSummerContent.innerHTML;
                windowContent += '</td></tr><tr><td><hr>';
                windowContent += printElemWinterHeader.outerHTML;
                windowContent += printElemWinterContent.innerHTML;
                windowContent += '</td></tr><tr><td><hr>';
                windowContent += printElemSpringHeader.outerHTML;
                windowContent += printElemSpringContent.innerHTML;
                windowContent += '</td></tr><tr><td><hr>';
                windowContent += printElemTaoz.innerHTML;
                windowContent += '</td></tr></table></body></html>';
                printW.document.write(windowContent);
                printW.document.close();
            }
        }
        function printPage() {
            var toolbarObj = document.getElementById("accessoryBar");

            if (null != toolbarObj) {
                toolbarObj.style.display = 'none';
            }
            window.print();
            window.close();
        }    
    </script>
</asp:Content>