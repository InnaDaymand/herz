﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : SecuredPage
{
    TextBox[, ,] daysParts;
    //Label[,,] allLabels;
    float powerKWH;
    Voltage v;
    bool[,] hours;
    int hoursWeek;
    int[,] begin = new int[7, 2];
    int[,] end = new int[7, 2];
    string[] dayNames = { "יום א", "יום ב", "יום ג", "יום ד", "יום ה", "יום ו", "שבת" };

    int monthsInSeason_Summer = 2;//-----------------------------------------------------
    int monthsInSeason_Winter = 3;// TableSeasonProfit user control has these params too
    int monthsInSeason_Spring = 7;//-----------------------------------------------------
    float weeksInMonth_Summer = (31.0F + 31) / 2 / 7;
    float weeksInMonth_Winter = (31.0F + 31 + 28) / 3 / 7;
    float weeksInMonth_Spring = (31.0F + 30 + 31 + 30 + 30 + 31 + 30) / 7 / 7;

    float MonthSummerPayment, MonthWinterPayment, MonthSpringPayment; //current monthly payment
    float paySummerOpt, payWinterOpt, paySpringOpt; //optimal monthly payments per season

    protected void Page_Load(object sender, EventArgs e)
    {
        /*hours = new bool[24, 7];
        for (int i = 0; i < 24; i++)
        {
            for (int j = 0; j < 7; j++)
                hours[i, j] = false;
        };*/

        // first index of array indicate begin or end of part day: 0 -begin , 1 - end of part
        // second index is point to day number;
        // three index is point to first or second part of day
        daysParts = new TextBox[2, 7, 2];
        //daysName = new string[2, 7, 2];
        //allLabels = new Label[2, 7, 2];

        for (int j = 0; j < 7; j++)
        {
            for (int k = 0; k < 2; k++)
            {
                TableRow row = new TableRow();
                tblHours.Rows.Add(row);

                TableCell celDay = new TableCell();
                row.Cells.Add(celDay);
                if (k == 0)
                {
                    Label lblDay = new Label();
                    lblDay.Text = dayNames[j];
                    lblDay.Font.Underline = true;
                    celDay.Controls.Add(lblDay);
                }

                for (int i = 1; i > -1; i--)
                {
                    TableCell celFrom = new TableCell();
                    row.Cells.Add(celFrom);
                    Label lblFrom = new Label();
                    if (i == 1)
                        lblFrom.Text = "משעה";
                    else
                        lblFrom.Text = "עד לשעה";
                    celFrom.Controls.Add(lblFrom);

                    TableCell celHour = new TableCell();
                    row.Cells.Add(celHour);
                    daysParts[i, j, k] = new TextBox();
                    daysParts[i, j, k].Text = "0";
                    /*if (!chkSabbat.Checked)
                        if (j == 6) 
                            daysParts[i, j, k].Enabled = false;*/
                    celHour.Controls.Add(daysParts[i, j, k]);
                }
            }
        }

        btnCalc.Click += new EventHandler(btnCalc_Click);
    }

    void btnCalc_Click(object sender, EventArgs e)
    {
        powerKWH = float.Parse(txtConsumption.Text);
        lblConsumption.Text = String.Format("הספק ממוצע: {0} kW", powerKWH);

        if (rdbVoltageHigher.Checked)
        {
            v = Voltage.Ilion;
            lblVoltage.Text = rdbVoltageHigher.Text;
        }
        else if (rdbVoltageHigh.Checked)
        {
            v = Voltage.High;
            lblVoltage.Text = rdbVoltageHigh.Text;
        }
        else
        {
            v = Voltage.Low;
            lblVoltage.Text = rdbVoltageLow.Text;
        }

        for (int j = 0; j < 7; j++)
        {
            for (int k = 0; k < 2; k++)
            {
                int start, stop;
                if (!int.TryParse(daysParts[1, j, k].Text, out start))
                {
                    daysParts[1, j, k].ForeColor = Color.Red;
                    daysParts[1, j, k].Focus();
                    RangeValidator1.IsValid = false;
                    return;
                }
                if (!int.TryParse(daysParts[0, j, k].Text, out stop))
                {
                    daysParts[0, j, k].ForeColor = Color.Red;
                    daysParts[0, j, k].Focus();
                    RangeValidator1.IsValid = false;
                    return;
                }
                if ((stop < start) || (stop < 0) || (start < 0))
                {
                    daysParts[1, j, k].ForeColor = Color.Red;
                    daysParts[0, j, k].ForeColor = Color.Red;
                    daysParts[1, j, k].Focus();
                    RangeValidator1.IsValid = false;
                    return;
                }
                daysParts[0, j, k].ForeColor = Color.Black;
                daysParts[1, j, k].ForeColor = Color.Black;
                begin[j, k] = start;
                end[j, k] = stop;
            }
        }

        hoursWeek = 0;
        // fill hours 
        for (int i = 0; i < 24; i++)
        {
            for (int j = 0; j < 7; j++)
            {
                if (((begin[j, 0] <= i) && (i < end[j, 0])) || ((begin[j, 1] <= i) && (i < end[j, 1])))
                    hoursWeek++;
                //hours[i, 6 - j] = true;
                //else hours[i, 6 - j] = false;
            };
        };

        MonthSummerPayment = weeksInMonth_Summer * powerKWH * CalcWeekPayment(Season.summer);
        MonthWinterPayment = weeksInMonth_Winter * powerKWH * CalcWeekPayment(Season.winter);
        MonthSpringPayment = weeksInMonth_Spring * powerKWH * CalcWeekPayment(Season.spring);
        float SeasonPayment_Summer = MonthSummerPayment * monthsInSeason_Summer;
        float SeasonPayment_Winter = MonthWinterPayment * monthsInSeason_Winter;
        float SeasonPayment_Spring = MonthSpringPayment * monthsInSeason_Spring;

        lblCurSummer.Text = String.Format("{0:#,##0}", SeasonPayment_Summer);
        lblCurWinter.Text = String.Format("{0:#,##0}", SeasonPayment_Winter);
        lblCurSpring.Text = String.Format("{0:#,##0}", SeasonPayment_Spring);
        lblCurYear.Text = String.Format("{0:#,##0}", SeasonPayment_Summer + SeasonPayment_Winter + SeasonPayment_Spring);

        //optimization
        paySummerOpt = weeksInMonth_Summer * powerKWH * CalcWeekPaymentOptimal(Season.summer);
        payWinterOpt = weeksInMonth_Winter * powerKWH * CalcWeekPaymentOptimal(Season.winter);
        paySpringOpt = weeksInMonth_Spring * powerKWH * CalcWeekPaymentOptimal(Season.spring);

        tblProfitSummer.PayCurMonth = MonthSummerPayment;
        tblProfitSummer.PayOptMonth = paySummerOpt;
        tblProfitWinter.PayCurMonth = MonthWinterPayment;
        tblProfitWinter.PayOptMonth = payWinterOpt;
        tblProfitSpring.PayCurMonth = MonthSpringPayment;
        tblProfitSpring.PayOptMonth = paySpringOpt;

        float SeasonPaymentOpt_Summer = paySummerOpt * monthsInSeason_Summer;
        float SeasonPaymentOpt_Winter = payWinterOpt * monthsInSeason_Winter;
        float SeasonPaymentOPt_Spring = paySpringOpt * monthsInSeason_Spring;

        lblOptSummer.Text = String.Format("{0:#,##0}", SeasonPaymentOpt_Summer);
        lblOptWinter.Text = String.Format("{0:#,##0}", SeasonPaymentOpt_Winter);
        lblOptSpring.Text = String.Format("{0:#,##0}", SeasonPaymentOPt_Spring);
        lblOptYear.Text = String.Format("{0:#,##0}", SeasonPaymentOpt_Summer + SeasonPaymentOpt_Winter + SeasonPaymentOPt_Spring);

        float profitSummer = SeasonPayment_Summer - SeasonPaymentOpt_Summer;
        float profitWinter = SeasonPayment_Winter - SeasonPaymentOpt_Winter;
        float profitSpring = SeasonPayment_Spring - SeasonPaymentOPt_Spring;
        lblProfitSummer.Text = String.Format("{0:#,##0}", profitSummer);
        lblProfitWinter.Text = String.Format("{0:#,##0}", profitWinter);
        lblProfitSpring.Text = String.Format("{0:#,##0}", profitSpring);
        lblProfitYear.Text = String.Format("{0:#,##0}", profitSummer + profitWinter + profitSpring);

        TabContainer1.ActiveTabIndex = 1;
    }

    private void BuildTable(Season season)
    {
        Table tbl;
        switch (season)
        {
            case Season.summer:
                tbl = tblHoursSummer;
                break;
            case Season.winter:
                tbl = tblHoursWinter;
                break;
            default:
                tbl = tblHoursSpring;
                break;
        }

        for (int j = 0; j < 7; j++)
        {
            List<Point> oneDay = FindPartsForDay(j + 1);
            for (int i = 0; i < oneDay.Count; i++)
            {
                TableRow row = new TableRow();
                tbl.Rows.Add(row);

                TableCell celDay = new TableCell();
                celDay.HorizontalAlign = HorizontalAlign.Right;
                celDay.Width = 70;
                row.Cells.Add(celDay);
                if (i == 0)
                {
                    Label lblDay = new Label();
                    lblDay.Text = dayNames[j];
                    lblDay.Font.Underline = true;
                    celDay.Controls.Add(lblDay);
                }

                TableCell celFrom = new TableCell();
                celFrom.HorizontalAlign = HorizontalAlign.Right;
                celFrom.Width = 100;
                row.Cells.Add(celFrom);
                Label lblFrom = new Label();
                lblFrom.Text = String.Format("משעה {0}", oneDay[i].X);
                celFrom.Controls.Add(lblFrom);

                TableCell celTo = new TableCell();
                celTo.HorizontalAlign = HorizontalAlign.Right;
                celTo.Width = 100;
                row.Cells.Add(celTo);
                Label lblTo = new Label();
                lblTo.Text = String.Format("עד לשעה {0}", oneDay[i].Y + 1);
                celTo.Controls.Add(lblTo);
            }
        }

        TableFooterRow footer = new TableFooterRow();
        tbl.Rows.Add(footer);

        TableCell celFooter1 = new TableCell();
        footer.Cells.Add(celFooter1);
        Label lblFooter1 = new Label();
        lblFooter1.Text = "סה''כ";
        celFooter1.Controls.Add(lblFooter1);

        TableCell celFooter2 = new TableCell();
        footer.Cells.Add(celFooter2);
        Label lblFooter2 = new Label();
        lblFooter2.Text = hoursWeek.ToString();
        celFooter2.Controls.Add(lblFooter2);

        TableCell celFooter3 = new TableCell();
        footer.Cells.Add(celFooter3);
        Label lblFooter3 = new Label();
        lblFooter3.Text = "שעות בשבוע";
        celFooter3.Controls.Add(lblFooter3);
    }

    float CalcWeekPayment(Season season)
    {
        taozTariff tariff = new taozTariff();
        int redHours = 0;
        int yellowHours = 0;
        int greenHours = 0;
        for (int i = 0; i < 7; i++)
            for (int k = 0; k < 2; k++)
            {
                redHours += AllFunctions.HowManyRedHours(season, i + 1, begin[i, k], end[i, k]);
                yellowHours += AllFunctions.HowManyYellowHours(season, i + 1, begin[i, k], end[i, k]);
                greenHours += AllFunctions.HowManyGreenHours(season, i + 1, begin[i, k], end[i, k]);
            }

        /*        float payment = redHours * AllFunctions.Tarrif(v, season, consumptionType.Pisgah) +
                           yellowHours * AllFunctions.Tarrif(v, season, consumptionType.Geva) +
                           greenHours * AllFunctions.Tarrif(v, season, consumptionType.Shefel);*/
        float payment = redHours * tariff.GetTariff(v, season, consumptionType.Pisgah) +
                   yellowHours * tariff.GetTariff(v, season, consumptionType.Geva) +
                   greenHours * tariff.GetTariff(v, season, consumptionType.Shefel);

        return payment;
    }

    /*    private int weekHours(bool[,] Hours)
        {
            int total = 0;
            for (int i = 0; i < 24; i++)
            {
                for (int j = 0; j < 7; j++)
                    if (Hours[i, j]) total++;
            };
            return total;
        }*/

    float CalcWeekPaymentOptimal(Season season)
    {
        taozTariff tariff = new taozTariff();
        //bool[,] Hours = new bool[24, 7];
        //int nWH = hoursWeek /*weekHours(hours)*/;

        /*for (int i = 0; i < 24; i++)
        {
            for (int j = 0; j < 7; j++)
            {
                hoursTemp[i, j] = false;
            }
        }*/

        hours = Optimize(hoursWeek, season);
        BuildTable(season);
        /*float totalMin = redHours(hours, season) * AllFunctions.Tarrif(v, season, consumptionType.Pisgah) +
                         yellowHours(hours, season) * AllFunctions.Tarrif(v, season, consumptionType.Geva) +
                         greenHours(hours, season) * AllFunctions.Tarrif(v, season, consumptionType.Shefel);*/
        float totalMin = redHours(hours, season) * tariff.GetTariff(v, season, consumptionType.Pisgah) +
                         yellowHours(hours, season) * tariff.GetTariff(v, season, consumptionType.Geva) +
                         greenHours(hours, season) * tariff.GetTariff(v, season, consumptionType.Shefel);

        return totalMin;
    }

    private int redHours(bool[,] Hours, Season S)
    {
        int temp = 0;
        for (int i = 0; i < 24; i++)
        {
            for (int j = 0; j < 7; j++)
            {
                if (Hours[i, 6 - j] && AllFunctions.ConsumptionType(j + 1, S, i) == consumptionType.Pisgah)
                    temp++;

            };
        };
        return temp;

    }
    private int yellowHours(bool[,] Hours, Season S)
    {
        int temp = 0;
        for (int i = 0; i < 24; i++)
        {
            for (int j = 0; j < 7; j++)
            {
                if (Hours[i, 6 - j] && AllFunctions.ConsumptionType(j + 1, S, i) == consumptionType.Geva)
                    temp++;

            };
        };
        return temp;

    }
    private int greenHours(bool[,] Hours, Season S)
    {
        int temp = 0;
        for (int i = 0; i < 24; i++)
        {
            for (int j = 0; j < 7; j++)
            {
                if (Hours[i, 6 - j] && AllFunctions.ConsumptionType(j + 1, S, i) == consumptionType.Shefel)
                    temp++;

            };
        };
        return temp;
    }

    private bool[,] Optimize(int nWH, Season S)
    {
        bool[,] Hours = new bool[24, 7];
        int i, j, nTempWH;

        nTempWH = nWH;

        for (i = 0; i < 7 && nTempWH > 0; i++)
        {
            for (j = 0; j < 24 && nTempWH > 0; j++)
            {
                if (!IsSabbat(i, j/*, S*/) || chkSabbat.Checked)
                    if (AllFunctions.ConsumptionType(i + 1, S, j) == consumptionType.Shefel)
                    {
                        Hours[j, 6 - i] = true;
                        nTempWH--;
                    }
            }
        }
        for (i = 0; i < 7 && nTempWH > 0; i++)
        {
            for (j = 0; j < 24 && nTempWH > 0; j++)
            {
                if (Hours[j, 6 - i] == false)
                {
                    if (!IsSabbat(i, j/*, S*/) || chkSabbat.Checked)
                        if (AllFunctions.ConsumptionType(i + 1, S, j) == consumptionType.Geva)
                        {
                            Hours[j, 6 - i] = true;
                            nTempWH--;
                        }
                }
            }
        }
        for (i = 0; i < 7 && nTempWH > 0; i++)
        {
            for (j = 0; j < 24 && nTempWH > 0; j++)
            {
                if (Hours[j, 6 - i] == false)
                {
                    if (!IsSabbat(i, j/*, S*/) || chkSabbat.Checked)
                        if (AllFunctions.ConsumptionType(i + 1, S, j) == consumptionType.Pisgah)
                        {
                            Hours[j, 6 - i] = true;
                            nTempWH--;
                        }
                }
            }
        }

        //if (chkSabbat.Checked) 
        //    SaturdayWorkMode(Hours, S);

        return Hours;
    }

    private bool IsSabbat(int day, int hour, Season season)
    {
        //days: 0-6, hours: 0-23
        int[] sabbatEntryHour = { 16 - 1, 14 - 1, 15 - 1 };//summer, winter, spring
        int[] sabbatExitHour = { 22, 20, 21 };

        //System.Globalization.HebrewCalendar hCal = new System.Globalization.HebrewCalendar();
        //DayOfWeek x = hCal.GetDayOfWeek(DateTime.Parse("2010-02-13 00:00"));

        return (day == 5 && hour > sabbatEntryHour[(int)season - 1] ||
                day == 6 && hour < sabbatExitHour[(int)season - 1]);
    }
    private bool IsSabbat(int day, int hour)
    {
        int sabbatEntryHour = 15 - 1;
        int sabbatExitHour = 21;

        return (day == 5 && hour > sabbatEntryHour ||
                day == 6 && hour < sabbatExitHour);
    }

    private void SaturdayWorkMode(bool[,] Hours, Season S)
    {
        // trying transfer red hours during week to saturday
        for (int i = 0; i < 6; i++)
        {
            int greenIndex = this.freeGreenHour(S, 6, Hours);
            int redIndex = this.redBusyHour(S, i, Hours);
            while ((greenIndex != -1) && (redIndex != -1))
            {
                Hours[greenIndex, 0] = true; Hours[redIndex, 6 - i] = false;
                greenIndex = this.freeGreenHour(S, 6, Hours);
                redIndex = this.redBusyHour(S, i, Hours);
            };
        };
        // trying transfer yellow hour during week to saturday
        // העברת פיסגה לגבע
        for (int i = 0; i < 6; i++)
        {
            int greenIndex = this.freeGreenHour(S, 6, Hours);
            int yellowIndex = this.yellowBusyHour(S, i, Hours);
            while ((greenIndex != -1) && (yellowIndex != -1))
            {
                Hours[greenIndex, 0] = true; Hours[yellowIndex, 6 - i] = false;
                greenIndex = this.freeGreenHour(S, 6, Hours);
                yellowIndex = this.yellowBusyHour(S, i, Hours);
            }
        };
    }

    private int freeGreenHour(Season S, int Day, bool[,] Hours)
    {
        //S -עונה , Day -selected Day number between 0 to 6
        //index = -1;
        for (int i = 0; i < 24; i++)
        {
            if ((!Hours[i, 6 - Day]) && (AllFunctions.ConsumptionType(Day + 1, S, i) == consumptionType.Shefel)) return i;
        };
        return -1;
    }
    private int redBusyHour(Season S, int Day, bool[,] Hours)
    {
        int index = -1;
        for (int i = 0; i < 24; i++)
        {
            if ((Hours[i, 6 - Day]) && (AllFunctions.ConsumptionType(Day + 1, S, i) == consumptionType.Pisgah))
                index = i;
        };
        return index;
    }
    private int yellowBusyHour(Season S, int Day, bool[,] Hours)
    {
        int index = -1;
        for (int i = 0; i < 24; i++)
        {
            if ((Hours[i, 6 - Day]) && (AllFunctions.ConsumptionType(Day + 1, S, i) == consumptionType.Geva))
                index = i;
        };
        return index;
    }

    private List<Point> FindPartsForDay(int Day)
    {
        List<Point> temp = new List<Point>();
        int i = 0;
        Point p = new Point();
        bool flag = false;
        int count = 0;
        while (i < 24)
        {
            if (hours[i, 7 - Day] && !flag)
            {
                flag = true;
                p = new Point(i, i);
                temp.Add(p); count++;
                // flag = true;
            };
            if (hours[i, 7 - Day] && flag)
            {
                Point pp = temp.ElementAt(count - 1);
                temp.RemoveAt(count - 1);
                pp.Y = i;
                temp.Insert(count - 1, pp);
            }
            else
            {
                flag = false;
            };
            i++;
        };
        return temp;
    }
}
