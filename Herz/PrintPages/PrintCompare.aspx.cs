﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class PrintPages_PrintCompare : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (AllFunctions.ForCompareSQL == "")
        {
            return;
        }
        string SQL = AllFunctions.ForCompareSQL + ", 1";
        GridCompare.DataSource = AllFunctions.Populate(SQL);
        GridCompare.DataBind();
    }
}