﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PrintPages/PrintTitle.master" AutoEventWireup="true" CodeFile="PrintMonthes.aspx.cs" Inherits="MemberPages_Default" %>

<%--<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>
--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        #ChTab
        {
            width: 950px;
        }
    </style>
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ForPrint" Runat="Server">

    <div>
    <p dir='rtl' lang="he" style="text-align: center">
        <asp:Label Text="ראטקא" runat="server" ID="lblClient" Font-Size="36px" 
            Font-Bold="True" ForeColor="#0033CC" Font-Names="Calibri" />
        <br />
        <asp:Label Text="ראגדה" runat="server" ID="lblConsumer" Font-Size="30px" 
            Font-Bold="True" ForeColor="#0066FF" Font-Names="Calibri" />
    </p>
    </div>
    <div  class="UnprintedItem" style="text-align: center">
        <table border="0" cellpadding="0" cellspacing="0" id="ChTab" >
            <tr>
                <td>
                    <asp:Label Text="מחודש" runat="server" />
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="fromMM">
                        <asp:ListItem Text="ינואר" Value="1" />
                        <asp:ListItem Text="פברואר" Value="2" />
                        <asp:ListItem Text="מרץ" Value="3" />
                        <asp:ListItem Text="אפריל" Value="4" />
                        <asp:ListItem Text="מאי" Value="5" />
                        <asp:ListItem Text="יוני" Value="6" />
                        <asp:ListItem Text="יולי" Value="7" />
                        <asp:ListItem Text="אוגוסט" Value="8" />
                        <asp:ListItem Text="ספטמבר" Value="9" />
                        <asp:ListItem Text="אוקטובר" Value="10" />
                        <asp:ListItem Text="נובמבר" Value="11" />
                        <asp:ListItem Text="דצמבר" Value="12" />
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="fromYY">
                        <asp:ListItem Text="2012" />
                        <asp:ListItem Text="2013" />
                        <asp:ListItem Text="2014" />
                        <asp:ListItem Text="2015" />
                        <asp:ListItem Text="2016" />
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:Label ID="Label1" Text="לחודש" runat="server" />
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="tillMM">
                        <asp:ListItem Text="ינואר" Value="1" />
                        <asp:ListItem Text="פברואר" Value="2" />
                        <asp:ListItem Text="מרץ" Value="3" />
                        <asp:ListItem Text="אפריל" Value="4" />
                        <asp:ListItem Text="מאי" Value="5" />
                        <asp:ListItem Text="יוני" Value="6" />
                        <asp:ListItem Text="יולי" Value="7" />
                        <asp:ListItem Text="אוגוסט" Value="8" />
                        <asp:ListItem Text="ספטמבר" Value="9" />
                        <asp:ListItem Text="אוקטובר" Value="10" />
                        <asp:ListItem Text="נובמבר" Value="11" />
                        <asp:ListItem Text="דצמבר" Value="12" />
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="tillYY">
                        <asp:ListItem Text="2012" />
                        <asp:ListItem Text="2013" />
                        <asp:ListItem Text="2014" />
                        <asp:ListItem Text="2015" />
                        <asp:ListItem Text="2016" />
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
    </div>
    <asp:GridView ID="PreviewMonthes" runat="server" BackColor="#CCCCCC" 
    BorderColor="#999999" BorderStyle="Solid" BorderWidth="3px" CellPadding="6" 
    CellSpacing="2" ForeColor="Black" Font-Names="Courier New" Font-Size="20px" Font-Bold="true" 
    HorizontalAlign="Center" Width="100%" AllowPaging="True" 
        onpageindexchanging="PreviewMonthes_PageIndexChanging" PageSize="24" >
        <FooterStyle BackColor="#CCCCCC" />
        <HeaderStyle BackColor="Silver" Font-Bold="True" ForeColor="Black" Font-Size="X-Large" />
        <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
        <RowStyle BackColor="White" />
        <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
        <SortedAscendingCellStyle BackColor="#F1F1F1" />
        <SortedAscendingHeaderStyle BackColor="#808080" />
        <SortedDescendingCellStyle BackColor="#CAC9C9" />
        <SortedDescendingHeaderStyle BackColor="#383838" />
    </asp:GridView>
    <p class="UnprintedItem">
        <asp:ImageButton ImageUrl="~/images/ShowMe.png" runat="server" 
            ID="MonPrint" onclick="MonPrint_Click" />
    </p>



</asp:Content>

