﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class MemberPages_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string SS = AllFunctions.ConsName;
        lblConsumer.Text = SS;
        SS = "SELECT TCL.Name FROM dbo.T_Client AS TCL INNER JOIN "
            + "dbo.tbl_Consumers_0 AS TC0 ON TCL.ID = TC0.sql_clientID "
            + "WHERE TC0.sql_Consumer_ID = " + AllFunctions.ConsID;

        DataClassesDataContext dc = new DataClassesDataContext();
        string CS = dc.Connection.ConnectionString;
        using (SqlConnection CN = new SqlConnection(CS))
        {
            SqlCommand command = new SqlCommand(SS, CN);
            CN.Open();
            try
            {
                SS = command.ExecuteScalar().ToString();
            }
            catch (Exception)
            {

                SS = "";
            }
        } 
        lblClient.Text = SS;
    }

    protected void MonPrint_Click(object sender, ImageClickEventArgs e)
    {
        DataTable te = new DataTable();
        DataClassesDataContext dc = new DataClassesDataContext();
        string CS = dc.Connection.ConnectionString;
        string SQL = "";
        SQL = "EXEC [dbo].[S_prepareToPrintMonthes] @StartMonth = "  + fromMM.SelectedValue.ToString() + ", "
		    + "@StartYear = " + fromYY.SelectedValue.ToString() + ", "
		    + "@FinMonth = " + tillMM.SelectedValue.ToString() + ", "
		    + "@FinYear = " + tillYY.SelectedValue.ToString() + ", "
		    + "@Consumers = N'" + AllFunctions.ConsID + "'";
        using (SqlConnection CN = new SqlConnection(CS))
        {
            try
            {
                CN.Open();
                SqlCommand command = new SqlCommand(SQL, CN);
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = command;

                adapter.Fill(te);
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }
        }
        PreviewMonthes.DataSource = te;
        PreviewMonthes.DataBind();
    }
    protected void PrintPrw_Click(object sender, ImageClickEventArgs e)
    {
        //string SCont = "";
        //string SID = "001";
        //string SIT = "";
        //if (SID == "003")
        //{
        //    SIT = "מ``ק/שעה";
        //}
        //else
        //    SIT = "קוט``ש";

        //if (SID == "002")
        //{
        //    SCont = "";
        //}
        //else
        //    SCont = ", '', '', '', '', '', '";



        //PrintPrw.Attributes.Add("OnClick", "return CallPrint('" + SID + "', '" + SIT + "', '"
        //    + "" + "', '" + lblConsumer.Text + "','"
        //    + "',' 323');");
    }
    protected void PreviewMonthes_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        PreviewMonthes.PageIndex = e.NewPageIndex;
        ImageClickEventArgs ie = new ImageClickEventArgs(0, 0);
        MonPrint_Click(this, ie); 
    }
}