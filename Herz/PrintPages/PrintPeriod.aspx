﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PrintPages/PrintTitle.master" AutoEventWireup="true" CodeFile="PrintPeriod.aspx.cs" Inherits="PrintPages_PrintPeriod" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ForPrint" Runat="Server">

    <br />
    <p dir='rtl' lang="he" style="text-align: center">
        <asp:Label Text="ראטקא" runat="server" ID="lb1" Font-Size="36px" 
            Font-Bold="True" ForeColor="#0033CC" Font-Names="Calibri" />
        <br />
        <br />
        <asp:Label Text="ראגדה" runat="server" ID="lb2" Font-Size="30px" 
            Font-Bold="True" ForeColor="#0066FF" Font-Names="Calibri" />
    </p>
    <br />
    <p dir='rtl' lang="he" style="text-align: center">
        <asp:Label ID="lbDates" runat="server" Text="From Date To Date" 
            Font-Bold="True" Font-Names="Times New Roman" Font-Size="24px"></asp:Label>
    </p>
    <br />
    <div dir='rtl' lang="he" style="text-align: center">
    <table id="TaData" align='center' width='450' border='1' 
        runat="server" dir='rtl' class="NewFont1" style="margin: auto">
    <tr>
        <th colspan='2'>סך הכל</th>
    </tr>
    <tr>
        <th>
            <asp:Label  runat="server" ID="TypData"/>
        </th>
        <th>
            <asp:Label runat="server" ID="Alut0"/>
        </th>
    </tr>
    <tr>
        <td align='center'>
            <asp:Label runat="server" ID="V0" />
        </td>
        <td align='center'>
            <asp:Label runat="server" ID="C0" />
        </td>
    </tr>
    <tr>
        <th colspan='2'>
            <asp:Label runat="server" ID="Pisga"/>
        </th>
   </tr><tr>
        <th>
            <asp:Label  runat="server" ID="TypData1"/>
        </th>
        <th>
            <asp:Label runat="server" ID="Alut1"/>
        </th>
    </tr><tr>
        <td align='center'>
            <asp:Label runat="server" ID="V1" />
        </td>
        <td align='center'>
            <asp:Label runat="server" ID="C1" />
        </td>
    </tr>
    <tr>
        <th colspan='2'>
            <asp:Label runat="server" ID="Geva"/>
        </th>
    </tr>
    <tr>
        <th>
            <asp:Label  runat="server" ID="TypData2"/>
        </th>
        <th>
            <asp:Label runat="server" ID="Alut2"/>
        </th>
    </tr><tr>
        <td align='center'>
            <asp:Label runat="server" ID="V2" />
        </td>
        <td align='center'>
            <asp:Label runat="server" ID="C2" />
        </td>
    </tr>

    <tr>
        <th colspan='2'>
            <asp:Label runat="server" ID="Shefel"/>
        </th>
    </tr><tr>
        <th>
            <asp:Label  runat="server" ID="TypData3"/>
        </th>
        <th>
            <asp:Label runat="server" ID="Alut3"/>
        </th>
    </tr><tr>
        <td align='center'>
            <asp:Label runat="server" ID="V3" />
        </td>
        <td align='center'>
            <asp:Label runat="server" ID="C3" />
        </td>
    </tr>
</table>
</div>        
</asp:Content>

