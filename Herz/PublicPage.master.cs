﻿using System;
using System;
using System.IO;

public partial class PublicPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string currentPageFileName = new FileInfo(this.Request.Url.LocalPath).Name;
        string Lang = currentPageFileName.Substring(currentPageFileName.Length - 7, 2);
        //if (Lang == "in") 
            Lang = "HE";
        string SQL = "", SS = "";
        DataClassesDataContext db = new DataClassesDataContext();
        if (AllFunctions.ConnectStr == "")
        {
            AllFunctions.ConnectStr = db.Connection.ConnectionString;
        }

        SQL = "SELECT [" + Lang + "] FROM Util_ResLang WHERE ID = 50001";
        SS = AllFunctions.GetScalar(SQL);
        if (SS == "")
        {
            SS = "50001";
        }
        Home.Text = SS;

        SQL = "SELECT [" + Lang + "] FROM Util_ResLang WHERE ID = 50002";
        SS = AllFunctions.GetScalar(SQL);
        if (SS == "")
        {
            SS = "50002";
        }
        AboutUs.Text = SS;

        SQL = "SELECT [" + Lang + "] FROM Util_ResLang WHERE ID = 50003";
        SS = AllFunctions.GetScalar(SQL);
        if (SS == "")
        {
            SS = "50003";
        }
        Services.Text = SS;

        SQL = "SELECT [" + Lang + "] FROM Util_ResLang WHERE ID = 50004";
        SS = AllFunctions.GetScalar(SQL);
        if (SS == "")
        {
            SS = "50004";
        }
        Products.Text = SS;

        SQL = "SELECT [" + Lang + "] FROM Util_ResLang WHERE ID = 50005";
        SS = AllFunctions.GetScalar(SQL);
        if (SS == "")
        {
            SS = "50005";
        }
        EnterSystem.Text = SS;

        SQL = "SELECT [" + Lang + "] FROM Util_ResLang WHERE ID = 50006";
        SS = AllFunctions.GetScalar(SQL);
        if (SS == "")
        {
            SS = "50006";
        }
        Contact.Text = SS;

        SQL = "SELECT [" + Lang + "] FROM Util_ResLang WHERE ID = 50007";
        SS = AllFunctions.GetScalar(SQL);
        if (SS == "")
        {
            SS = "50007";
        }
        AllRights.Text = SS;

    }
}
