﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TableSeasonProfit.ascx.cs" Inherits="TableSeasonProfit" %>
<table dir="rtl">
    <tr>
        <td style="min-width:150px">
            </td>
        <td style="min-width:100px">
            חודשי
        </td>
        <td style="min-width:100px">
            עונתי
            </td>
    </tr>
    <tr>
        <td>
            עלות החשמל במצב הנוכחי, ש"ח
        </td>
        <td>
            <asp:Label ID="lblCurMonth" runat="server" Text="0"></asp:Label>
            </td>
        <td>
            <asp:Label ID="lblCurSeason" runat="server" Text="0"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            עלות החשמל במצב האופטימלי, ש&quot;ח
        </td>
        <td>
            <asp:Label ID="lblOptMonth" runat="server" Text="0"></asp:Label>
        </td>
        <td>
            <asp:Label ID="lblOptSeason" runat="server" Text="0"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            חיסכון, ש&quot;ח</td>
        <td>
            <asp:Label ID="lblProfitMonth" runat="server" Text="0"></asp:Label>
        </td>
        <td>
            <asp:Label ID="lblProfitSeason" runat="server" Text="0"></asp:Label>
        </td>
    </tr>
</table>
