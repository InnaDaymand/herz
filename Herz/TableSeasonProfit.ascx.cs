﻿using System;

public partial class TableSeasonProfit : System.Web.UI.UserControl
{
    private int monthsInSeason;
    private float payCurMonth, payOptMonth;

    protected void Page_Load(object sender, EventArgs e)
    {

    }
    public Season Season
    {
        set
        {
            if (value == Season.summer)
                monthsInSeason = 2;
            else if (value == Season.winter)
                monthsInSeason = 3;
            else
                monthsInSeason = 7;
        }
    }
    public float PayCurMonth
    {
        set 
        {
            payCurMonth = value;
            lblCurMonth.Text = String.Format("{0:#,##0}", value);
            lblCurSeason.Text = String.Format("{0:#,##0}", value * monthsInSeason);

            lblProfitMonth.Text = String.Format("{0:#,##0}", payCurMonth - payOptMonth);
            lblProfitSeason.Text = String.Format("{0:#,##0}", (payCurMonth - payOptMonth) * monthsInSeason);
        }
    }
    public float PayOptMonth
    {
        set 
        {
            payOptMonth = value;
            lblOptMonth.Text = String.Format("{0:#,##0}", value);
            lblOptSeason.Text = String.Format("{0:#,##0}", value * monthsInSeason);

            lblProfitMonth.Text = String.Format("{0:#,##0}", payCurMonth - payOptMonth);
            lblProfitSeason.Text = String.Format("{0:#,##0}", (payCurMonth - payOptMonth) * monthsInSeason);
        }
    }
}
